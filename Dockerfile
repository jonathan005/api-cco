# Start with a base image containing Java runtime
#FROM openjdk:8-jdk-alpine

# Add Maintainer Info
#LABEL maintainer="steven.alvarado@sasf.net"

# Add a volume pointing to /app
#VOLUME /app

#ARG DEPENDENCY=target/dependency
#COPY ${DEPENDENCY}/BOOT-INF/lib /app/lib
#COPY ${DEPENDENCY}/META-INF/app/META-INF
#COPY ${DEPENDENCY}/BOOT-INF/classes /app
#ENTRYPOINT ["java","-cp","app:app/lib/*","sasf.net.age.AgeApplication"]

FROM openjdk:8-jdk-alpine

# Add Maintainer Info
LABEL maintainer="steven.alvarado@sasf.net"

# Add a volume pointing to /tmp
VOLUME /cco

# Add the application's jar to the container
#ADD /src/main/resources/winter-sequence-282920-f589c5a8f374.p12 gdkey.p12
ADD /target/*.jar app.jar

# Run the jar file  RUN apk add xfce4
RUN apk add xfce4
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-Djava.awt.headless=true","-DSERVER_PORT=${SERVER_PORT}","-DPROFILE:${PROFILE}","-jar","app.jar"]
 
