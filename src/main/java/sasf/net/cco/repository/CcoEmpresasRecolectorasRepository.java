package sasf.net.cco.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import sasf.net.cco.model.CcoEmpresasRecolectoras;
import sasf.net.cco.model.CcoEmpresasRecolectorasPK;

public interface CcoEmpresasRecolectorasRepository
		extends JpaRepository<CcoEmpresasRecolectoras, CcoEmpresasRecolectorasPK> {
	
	@Query("SELECT c From CcoEmpresasRecolectoras c "
			+ "WHERE (c.estado<>:estadoAnulado) "
			+ "AND (c.id.ageLicencCodigo =:ageLicencCodigo) "
			+ "AND (:codigo is  null or c.id.codigo=:codigo) "
			+ "AND (:nombreEmpresa is null or UPPER(c.nombreEmpresa) like %:nombreEmpresa%) "
			)
	public List<CcoEmpresasRecolectoras> buscarPorParametros(@Param("ageLicencCodigo")Integer ageLicencCodigo
			,@Param("codigo")Long codigo
			,@Param("nombreEmpresa") String nombreEmpresa
			,@Param("estadoAnulado") String estadoAnulado);

}
