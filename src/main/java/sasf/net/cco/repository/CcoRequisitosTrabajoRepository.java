package sasf.net.cco.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import sasf.net.cco.model.CcoRequisitosTrabajo;
import sasf.net.cco.model.CcoRequisitosTrabajoPK;

public interface CcoRequisitosTrabajoRepository extends JpaRepository<CcoRequisitosTrabajo, CcoRequisitosTrabajoPK> {
	
	@Query("SELECT c From CcoRequisitosTrabajo c "
			+ "WHERE (c.estado<>:estadoAnulado) "
			+ "AND (c.id.ageLicencCodigo =:ageLicencCodigo) "
			+ "AND (:codigo is  null or c.id.codigo=:codigo) "
			+ "and (:descripcion is  null or c.descripcion like %:descripcion%)"
			+ "and (:tipoArchivo is  null or c.tipoArchivo = :tipoArchivo)"
			)
	public List<CcoRequisitosTrabajo> buscarPorParametros(@Param("ageLicencCodigo")Integer ageLicencCodigo,@Param("codigo")Long codigo,@Param("descripcion")String descripcion,@Param("tipoArchivo")String tipoArchivo,@Param("estadoAnulado") String estadoAnulado);
}
