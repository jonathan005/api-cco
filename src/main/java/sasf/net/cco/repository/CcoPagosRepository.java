package sasf.net.cco.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import sasf.net.cco.model.CcoPagos;
import sasf.net.cco.model.CcoPagosPK;


public interface CcoPagosRepository extends JpaRepository<CcoPagos, CcoPagosPK> {

	@Query("SELECT c From CcoPagos c "
			+ "WHERE (c.estado<>:estadoAnulado) "
			+ "AND (c.id.ageLicencCodigo =:ageLicencCodigo) "
			+ "AND (:codigo is  null or c.id.codigo=:codigo) "
			+ "AND (:ccoConducCodigo is  null or c.ccoConducCodigo=:ccoConducCodigo) "
			+ "AND (:CcoRepartCodigo is  null or c.CcoRepartCodigo=:CcoRepartCodigo) "
			+ "AND (:ccoCenAcCodigo is  null or c.ccoCenAcCodigo=:ccoCenAcCodigo) "
			+ "and (:descripcion is  null or c.descripcion like %:descripcion%) "
			
			)
	public List<CcoPagos> buscarPorParametros(@Param("ageLicencCodigo")Integer ageLicencCodigo,@Param("codigo")Long codigo,
			@Param("ccoConducCodigo")Long ccoConducCodigo,
			@Param("CcoRepartCodigo")Long CcoRepartCodigo,
			@Param("ccoCenAcCodigo")Long ccoCenAcCodigo,
			@Param("descripcion")String descripcion,
			@Param("estadoAnulado") String estadoAnulado);
}
