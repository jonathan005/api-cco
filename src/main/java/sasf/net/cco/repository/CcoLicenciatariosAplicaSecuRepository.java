package sasf.net.cco.repository;

import java.util.List;

import javax.persistence.LockModeType;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import sasf.net.cco.model.CcoLicenciatariosAplicaSecu;
import sasf.net.cco.model.CcoLicenciatariosAplicaSecuPK;


public interface CcoLicenciatariosAplicaSecuRepository  extends JpaRepository<CcoLicenciatariosAplicaSecu, CcoLicenciatariosAplicaSecuPK>  {
	
	@Query("SELECT c FROM CcoLicenciatariosAplicaSecu c "
			+ "where c.id.ccoLicApCcoLicencCodigo=:licenciatario "
			+ "and c.id.ccoLicApCcoAplicaCodigo=:aplicacion "
			+ "and c.id.codigo=:codigo ")
	@Lock(value = LockModeType.PESSIMISTIC_READ)
	public CcoLicenciatariosAplicaSecu consultarPorSecuencia(@Param(value="licenciatario")Integer licenciatario,@Param(value="aplicacion") Integer aplicacion,@Param(value="codigo") Integer codigo);
	
	public List<CcoLicenciatariosAplicaSecu> findByIdCcoLicApCcoLicencCodigoAndEstadoOrEstado(Integer codigoLicenciatario, String estado, String estado2);
	
	@Query("SELECT c FROM CcoLicenciatariosAplicaSecu c "
			+ "where c.id.ccoLicApCcoLicencCodigo=:licenciatario "
			+ "and c.id.ccoLicApCcoAplicaCodigo=:aplicacion "
			)
	public List<CcoLicenciatariosAplicaSecu> buscarPorAplicacion(@Param("licenciatario")Integer licenciatario,@Param("aplicacion")Integer aplicacion);
	
	public List<CcoLicenciatariosAplicaSecu> findByIdCcoLicApCcoLicencCodigoAndIdCcoLicApCcoAplicaCodigo(Integer licenciatario,Integer aplicacion);
}
