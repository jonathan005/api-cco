package sasf.net.cco.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import sasf.net.cco.model.CcoConductorPaquetesCab;
import sasf.net.cco.model.CcoConductorPaquetesCabPK;

public interface CcoConductorPaquetesCabRepository
		extends JpaRepository<CcoConductorPaquetesCab, CcoConductorPaquetesCabPK> {
	@Query("SELECT c From CcoConductorPaquetesCab c "
			+ "WHERE (c.estado<>:estadoAnulado) "
			+ "AND (c.id.ageLicencCodigo =:ageLicencCodigo) "
			+ "AND (:codigo is  null or c.id.codigo=:codigo) "
			)
	public List<CcoConductorPaquetesCab> buscarPorParametros(@Param("ageLicencCodigo")Integer ageLicencCodigo,@Param("codigo")Long codigo,@Param("estadoAnulado") String estadoAnulado);
}
