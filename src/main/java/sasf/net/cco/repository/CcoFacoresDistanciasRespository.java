package sasf.net.cco.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import sasf.net.cco.model.CcoFactoresDistancias;
import sasf.net.cco.model.CcoFactoresDistanciasPK;

public interface CcoFacoresDistanciasRespository extends JpaRepository<CcoFactoresDistancias, CcoFactoresDistanciasPK> {
	
	@Query("SELECT c From CcoFactoresDistancias c "
			+ "WHERE (c.estado<>:estadoAnulado) "
			+ "AND (c.id.ageLicencCodigo =:ageLicencCodigo) "
			+ "AND (:codigo is  null or c.id.codigo=:codigo) "
			)
	public List<CcoFactoresDistancias> buscarPorParametros(@Param("ageLicencCodigo")Integer ageLicencCodigo,@Param("codigo")Long codigo,@Param("estadoAnulado") String estadoAnulado);
}
