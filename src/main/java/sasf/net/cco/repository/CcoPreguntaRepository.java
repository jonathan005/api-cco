package sasf.net.cco.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import sasf.net.cco.model.CcoEmbalajes;
import sasf.net.cco.model.CcoPregunta;
import sasf.net.cco.model.CcoPreguntaPK;

public interface CcoPreguntaRepository extends JpaRepository<CcoPregunta, CcoPreguntaPK> {
	
	@Query("SELECT c From CcoPregunta c "
			+ "WHERE (c.estado<>:estadoAnulado) "
			+ "AND (c.id.ageLicencCodigo =:ageLicencCodigo) "
			+ "AND (:codigo is  null or c.id.codigo=:codigo) "
			+ "and (:pregunta is  null or c.pregunta like %:pregunta%)"
			+ "and (:respuesta is  null or c.respuesta like %:respuesta%)"
			)
	
	public List<CcoPregunta> buscarPorParametros(@Param("ageLicencCodigo")Integer ageLicencCodigo,@Param("codigo")Long codigo,@Param("pregunta")String pregunta,@Param("respuesta")String respuesta,@Param("estadoAnulado") String estadoAnulado);
}
