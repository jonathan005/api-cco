package sasf.net.cco.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import sasf.net.cco.model.CcoConductores;
import sasf.net.cco.model.CcoConductoresPK;

public interface CcoConductoresRepository extends JpaRepository<CcoConductores, CcoConductoresPK> {

	
	@Query("SELECT c From CcoConductores c "
			+ "WHERE (c.estado<>:estadoAnulado) "
			+ "AND (c.id.ageLicencCodigo =:ageLicencCodigo) "
			+ "AND (:codigo is  null or c.id.codigo=:codigo) "
			+ "AND (:matricula is  null or c.numeroMatricula=:matricula) "
			+ "AND (:correo is  null or c.correoElectronico=:correo)"
			+ "AND (:ccoEmpReCodigo is  null or c.ccoEmpReCodigo=:ccoEmpReCodigo)"
			
			)
	 public List<CcoConductores> buscarPorParametros(@Param("ageLicencCodigo")Integer ageLicencCodigo,
			                                           @Param("codigo")Long codigo ,
			                                           @Param("matricula")String matricula ,
			                                           @Param("correo")String correo,
			                                           @Param("ccoEmpReCodigo")Long ccoEmpReCodigo,
	                                                @Param("estadoAnulado") String estadoAnulado 
	                                                  )      ;
	
}
