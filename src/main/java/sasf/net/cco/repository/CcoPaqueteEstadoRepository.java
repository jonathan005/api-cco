package sasf.net.cco.repository;

import java.util.Date;
import java.util.List;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import sasf.net.cco.model.CcoPaqueteEstado;
import sasf.net.cco.model.CcoPaqueteEstadoPK;


public interface CcoPaqueteEstadoRepository extends JpaRepository<CcoPaqueteEstado, CcoPaqueteEstadoPK> {
	
	@Query("SELECT c From CcoPaqueteEstado c "
			+ "WHERE (c.estado<>:estadoAnulado) "
			+ "AND (c.id.cooPedidoAgeLicencCodigo = :cooPedidoAgeLicencCodigo) "
			+ "AND (c.id.codigo= :codigoPaquete) "
			+ "AND (:estadoPaquete is  null or c.paqueteEstado=:estadoPaquete) "
			+ "AND (cast(:fechaDesde as timestamp) is null or c.fechaPaqueteEstado >= :fechaDesde) "
			+ "AND (cast(:fechaHasta as timestamp) is null or c.fechaPaqueteEstado <= :fechaHasta) "
			)
	public List<CcoPaqueteEstado> buscarPorParametros(@Param("cooPedidoAgeLicencCodigo")Integer cooPedidoAgeLicencCodigo,@Param("codigoPaquete")Long codigoPaquete,@Param("estadoPaquete") String estadoPaquete,@Param("fechaDesde") Date fechaDesde,@Param("fechaHasta") Date fechaHasta,@Param("estadoAnulado") String estadoAnulado);
	
}
