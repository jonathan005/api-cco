package sasf.net.cco.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


import sasf.net.cco.model.CcoInventarioDiario;
import sasf.net.cco.model.CcoInventarioDiarioPK;

public interface CcoInventarioDiarioRepository extends JpaRepository<CcoInventarioDiario, CcoInventarioDiarioPK> {
	
	@Query("SELECT c From CcoInventarioDiario c "
			+ "WHERE (c.estado<>:estadoAnulado) "
			+ "AND (c.id.ageLicencCodigo =:ageLicencCodigo) "
			+ "AND (:codigo is  null or c.id.codigo=:codigo) "
			+ "AND (:ccoPaqueteCodigo is  null or c.ccoPaqueteCodigo=:ccoPaqueteCodigo) "
			+ "AND (:ccoPaqueteCodigoH is  null or c.ccoPaqueteCodigoH=:ccoPaqueteCodigoH) "
			+ "AND (cast(:fechaDesde as timestamp) is null or c.fecha >= :fechaDesde) "
			+ "AND (cast(:fechaHasta as timestamp) is null or c.fecha <= :fechaHasta) "
			+ " ORDER BY c.fecha ASC"
			)
	public List<CcoInventarioDiario> buscarPorParametros(@Param("ageLicencCodigo")Integer ageLicencCodigo,@Param("codigo")Long codigo,@Param("ccoPaqueteCodigo")Long ccoPaqueteCodigo,@Param("ccoPaqueteCodigoH")Long ccoPaqueteCodigoH,@Param("fechaDesde")Date fechaDesde,@Param("fechaHasta")Date fechaHasta,@Param("estadoAnulado") String estadoAnulado);
	
	
}
