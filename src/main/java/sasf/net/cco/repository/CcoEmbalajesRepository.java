package sasf.net.cco.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import sasf.net.cco.model.CcoEmbalajes;
import sasf.net.cco.model.CcoEmbalajesPK;


public interface CcoEmbalajesRepository extends JpaRepository<CcoEmbalajes, CcoEmbalajesPK> {
	@Query("SELECT c From CcoEmbalajes c "
			+ "WHERE (c.estado<>:estadoAnulado) "
			+ "AND (c.id.ageLicencCodigo =:ageLicencCodigo) "
			+ "AND (:codigo is  null or c.id.codigo=:codigo) "
			+ "and (:descripcion is  null or c.descripcion like %:descripcion%)")
	public List<CcoEmbalajes> buscarPorParametros(@Param("ageLicencCodigo")Integer ageLicencCodigo,@Param("codigo")Long codigo,@Param("descripcion")String descripcion,@Param("estadoAnulado") String estadoAnulado);
	
}
