package sasf.net.cco.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


import sasf.net.cco.model.CcoPaquete;
import sasf.net.cco.model.CcoPaquetePK;

public interface CcoPaqueteRespository extends JpaRepository<CcoPaquete, CcoPaquetePK> {
	
	@Query("SELECT c From CcoPaquete c "
			+ "WHERE (c.estado<>:estadoAnulado) "
			+ "AND (c.id.ageLicencCodigo =:ageLicencCodigo) "
			+ "AND (:codigo is  null or c.id.codigo=:codigo) "
			+ "AND (:tipoPaquete is  null or c.tipoPaquete=:tipoPaquete) "
			+ "AND (:estadoPaquete is  null or c.estadoPaquete=:estadoPaquete) "
			+ "AND (cast(:fechaDesde as timestamp) is null or c.fechaEnvio >= :fechaDesde) "
			+ "AND (cast(:fechaHasta as timestamp) is null or c.fechaEnvio <= :fechaHasta) "
			+ "AND (:usuarioIngreso is  null or c.usuarioIngreso= :usuarioIngreso) "
			+ "AND (:cedulaPersonaEntrega is  null or c.cedulaPersonaEntrega= :cedulaPersonaEntrega) "
			+ "AND (:ageSucursCodigo is  null or c.ageSucursCodigo= :ageSucursCodigo) "
			+ "AND (:cliClientCodigo is  null or c.cliClientCodigo= :cliClientCodigo) "
			+ "AND (:descripcion is  null or c.descripcion= :descripcion) "
			+ "AND (:codigoPaqueteDesde is  null or c.id.codigo >=:codigoPaqueteDesde) "
			+ "AND (:codigoPaqueteHasta is  null or c.id.codigo <=:codigoPaqueteHasta) "
			)
	public List<CcoPaquete> buscarPorParametros(@Param("ageLicencCodigo")Integer ageLicencCodigo,@Param("codigo")Long codigo,@Param("tipoPaquete")String tipoPaquete,@Param("fechaDesde") Date fechaDesde,@Param("fechaHasta") Date fechaHasta,@Param("estadoPaquete") String estadoPaquete,@Param("usuarioIngreso")Long usuarioIngreso,@Param("cedulaPersonaEntrega") String cedulaPersonaEntrega,@Param("ageSucursCodigo")Long ageSucursCodigo,@Param("cliClientCodigo")Long cliClientCodigo,@Param("descripcion") String descripcion,@Param("codigoPaqueteDesde")Long codigoPaqueteDesde,@Param("codigoPaqueteHasta")Long codigoPaqueteHasta,@Param("estadoAnulado") String estadoAnulado);
	
}
