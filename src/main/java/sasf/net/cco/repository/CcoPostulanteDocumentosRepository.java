package sasf.net.cco.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import sasf.net.cco.model.CcoPostulanteDocumentos;
import sasf.net.cco.model.CcoPostulanteDocumentosPK;


public interface CcoPostulanteDocumentosRepository extends JpaRepository<CcoPostulanteDocumentos, CcoPostulanteDocumentosPK> {
	
	@Query("SELECT c From CcoPostulanteDocumentos c "
			+ "WHERE (c.estado<>:estadoAnulado) "
			+ "AND (:ccoPostulAgeLicencCodigo is  null or c.id.ccoPostulAgeLicencCodigo=:ccoPostulAgeLicencCodigo) "
			+ "AND (:ccoPostulCodigo is  null or c.id.ccoPostulCodigo=:ccoPostulCodigo) "
			+ "AND (:ccoReTraCodigo is  null or c.id.ccoReTraCodigo=:ccoReTraCodigo) "
			
			)
	public List<CcoPostulanteDocumentos> buscarPorParametros(@Param("ccoPostulAgeLicencCodigo")Integer ccoPostulAgeLicencCodigo,@Param("ccoPostulCodigo")Long ccoPostulCodigo,@Param("ccoReTraCodigo")Long ccoReTraCodigo,@Param("estadoAnulado") String estadoAnulado);
	
	@Query("SELECT c From CcoPostulanteDocumentos c "
			+ "WHERE (c.estado<>:estadoAnulado) "
			+ "AND (c.id.ccoPostulAgeLicencCodigo =:ccoPostulAgeLicencCodigo) "
			+ "AND (c.id.ccoPostulCodigo =:ccoPostulCodigo) "
			)
	public List<CcoPostulanteDocumentos> buscarPorPostulante(@Param("ccoPostulAgeLicencCodigo")Integer ccoPostulAgeLicencCodigo,@Param("ccoPostulCodigo")Long ccoPostulCodigo,@Param("estadoAnulado") String estadoAnulado);
	
	@Query("SELECT c From CcoPostulanteDocumentos c "
			+ "WHERE (c.estado<>:estadoAnulado) "
			+ "AND (c.id.ccoReTraAgeLicencCodigo =:ccoReTraAgeLicencCodigo) "
			+ "AND (c.id.ccoReTraCodigo =:ccoReTraCodigo) "
			)
	public List<CcoPostulanteDocumentos> buscarPorRequisitoTrabajo(@Param("ccoReTraAgeLicencCodigo")Integer ccoReTraAgeLicencCodigo,@Param("ccoReTraCodigo")Long ccoReTraCodigo,@Param("estadoAnulado") String estadoAnulado);

}
