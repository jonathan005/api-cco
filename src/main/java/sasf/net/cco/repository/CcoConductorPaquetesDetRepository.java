package sasf.net.cco.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import sasf.net.cco.model.CcoConductorPaquetesDet;
import sasf.net.cco.model.CcoConductorPaquetesDetPK;

public interface CcoConductorPaquetesDetRepository
		extends JpaRepository<CcoConductorPaquetesDet, CcoConductorPaquetesDetPK> {
	
	@Query("SELECT c From CcoConductorPaquetesDet c "
			+ "WHERE (c.estado<>:estadoAnulado) "
			+ "AND (c.id.ccoCpCapAgeLicencCodigo =:ccoCpCapAgeLicencCodigo) "
			+ "AND (:codigo is  null or c.id.codigo=:codigo) "
			+ "AND (:ccoCpCapCodigo is  null or c.id.ccoCpCapCodigo=:ccoCpCapCodigo) "
			+ "AND (:ccoPaquetCodigo is  null or c.ccoPaquetCodigo=:ccoPaquetCodigo) "
			)
	public List<CcoConductorPaquetesDet> buscarPorParametros(@Param("ccoCpCapAgeLicencCodigo")Integer ccoCpCapAgeLicencCodigo,@Param("codigo")Long codigo,@Param("ccoCpCapCodigo")Long ccoCpCapCodigo,@Param("ccoPaquetCodigo")Long ccoPaquetCodigo,@Param("estadoAnulado") String estadoAnulado);
}
