package sasf.net.cco.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import sasf.net.cco.model.CcoPuestoTrabajoRequisito;
import sasf.net.cco.model.CcoPuestoTrabajoRequisitoPK;


public interface CcoPuestoTrabajoRequisitoRepository extends JpaRepository<CcoPuestoTrabajoRequisito, CcoPuestoTrabajoRequisitoPK> {
	
	@Query("SELECT c From CcoPuestoTrabajoRequisito c "
			+ "WHERE (c.estado<>:estadoAnulado) "
			+ "AND (:ccoPuTraAgeLicencCodigo is  null or c.id.ccoPuTraAgeLicencCodigo=:ccoPuTraAgeLicencCodigo) "
			+ "AND (:ccoPuTraCodigo is  null or c.id.ccoPuTraCodigo=:ccoPuTraCodigo) "
			+ "AND (:ccoReTraCodigo is  null or c.id.ccoReTraCodigo=:ccoReTraCodigo) "
			)
	public List<CcoPuestoTrabajoRequisito> buscarPorParametros(@Param("ccoPuTraAgeLicencCodigo")Integer ccoPuTraAgeLicencCodigo,@Param("ccoPuTraCodigo")Long ccoPuTraCodigo,@Param("ccoReTraCodigo")Long ccoReTraCodigo,@Param("estadoAnulado") String estadoAnulado);
	
	@Query("SELECT c From CcoPuestoTrabajoRequisito c "
			+ "WHERE (c.estado<>:estadoAnulado) "
			+ "AND (c.id.ccoPuTraAgeLicencCodigo =:ccoPuTraAgeLicencCodigo) "
			+ "AND (c.id.ccoPuTraCodigo =:ccoPuTraCodigo) "
			)
	public List<CcoPuestoTrabajoRequisito> buscarPorPuestoTrabajo(@Param("ccoPuTraAgeLicencCodigo")Integer ccoPuTraAgeLicencCodigo,@Param("ccoPuTraCodigo")Long ccoPuTraCodigo,@Param("estadoAnulado") String estadoAnulado);
	
	@Query("SELECT c From CcoPuestoTrabajoRequisito c "
			+ "WHERE (c.estado<>:estadoAnulado) "
			+ "AND (c.id.ccoReTraAgeLicencCodigo =:ccoReTraAgeLicencCodigo) "
			+ "AND (c.id.ccoReTraCodigo =:ccoReTraCodigo) "
			)
	public List<CcoPuestoTrabajoRequisito> buscarPorRequisitoTrabajo(@Param("ccoReTraAgeLicencCodigo")Integer ccoReTraAgeLicencCodigo,@Param("ccoReTraCodigo")Long ccoReTraCodigo,@Param("estadoAnulado") String estadoAnulado);
}
