package sasf.net.cco.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


import sasf.net.cco.model.CcoQuejasReclamosTrabajo;
import sasf.net.cco.model.CcoQuejasReclamosTrabajoPK;

public interface CcoQuejasReclamosTrabajoRepository extends JpaRepository<CcoQuejasReclamosTrabajo, CcoQuejasReclamosTrabajoPK> {
	
	@Query("SELECT c From CcoQuejasReclamosTrabajo c "
			+ "WHERE (c.estado<>:estadoAnulado) "
			+ "AND (c.id.ageLicencCodigo =:ageLicencCodigo) "
			+ "AND (:codigo is  null or c.id.codigo=:codigo) "
			+ "AND (:descripcion is  null or c.descripcion like %:descripcion%) "
			+ "AND (:tipo is  null or c.tipo = :tipo) "
			+ "AND (:cliClientCodigo is  null or c.cliClientCodigo= :cliClientCodigo) "
			)
	public List<CcoQuejasReclamosTrabajo> buscarPorParametros(@Param("ageLicencCodigo")Integer ageLicencCodigo,@Param("codigo")Long codigo,@Param("descripcion")String descripcion,@Param("estadoAnulado") String estadoAnulado,@Param("tipo") String tipo,@Param("cliClientCodigo")Long cliClientCodigo);
}
