package sasf.net.cco.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import sasf.net.cco.model.CcoPostulante;
import sasf.net.cco.model.CcoPostulantePK;


public interface CcoPostulanteRepository extends JpaRepository<CcoPostulante,CcoPostulantePK>{
	
	@Query("SELECT c From CcoPostulante c "
			+ "WHERE (c.estado<>:estadoAnulado) "
			+ "AND (c.id.ageLicencCodigo =:ageLicencCodigo) "
			+ "AND (:codigo is  null or c.id.codigo=:codigo) "
			+ "and (:ccoPuTraCodigo is  null or c.ccoPuTraCodigo = :ccoPuTraCodigo) "
			+ "and (:estadoPostulante is  null or c.ccoPuTraCodigo = :estadoPostulante) "
			+ "and (:correoElectronico is  null or c.correoElectronico = :correoElectronico) "
			
			)
	public List<CcoPostulante> buscarPorParametros(@Param("ageLicencCodigo")Integer ageLicencCodigo,@Param("codigo")Long codigo,@Param("ccoPuTraCodigo")Long ccoPuTraCodigo,@Param("estadoPostulante") String estadoPostulante,@Param("correoElectronico") String correoElectronico,@Param("estadoAnulado") String estadoAnulado);
}
