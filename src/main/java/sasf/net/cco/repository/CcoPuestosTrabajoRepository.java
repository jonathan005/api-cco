package sasf.net.cco.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import sasf.net.cco.model.CcoPuestosTrabajo;
import sasf.net.cco.model.CcoPuestosTrabajoPK;



public interface CcoPuestosTrabajoRepository extends JpaRepository<CcoPuestosTrabajo,CcoPuestosTrabajoPK>{
	
	@Query("SELECT c From CcoPuestosTrabajo c "
			+ "WHERE (c.estado<>:estadoAnulado) "
			+ "AND (c.id.ageLicencCodigo =:ageLicencCodigo) "
			+ "AND (:codigo is  null or c.id.codigo=:codigo) "
			+ "and (:descripcion is  null or c.descripcion like %:descripcion%)")
	public List<CcoPuestosTrabajo> buscarPorParametros(@Param("ageLicencCodigo")Integer ageLicencCodigo,@Param("codigo")Long codigo,@Param("descripcion")String descripcion,@Param("estadoAnulado") String estadoAnulado);
}
