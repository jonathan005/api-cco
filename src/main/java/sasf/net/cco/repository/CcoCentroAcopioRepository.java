package sasf.net.cco.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import sasf.net.cco.model.CcoCentroAcopio;
import sasf.net.cco.model.CcoCentroAcopioPK;


public interface CcoCentroAcopioRepository extends JpaRepository<CcoCentroAcopio, CcoCentroAcopioPK> {

	@Query("SELECT c From CcoCentroAcopio c "
			+ "WHERE (c.estado<>:estadoAnulado) "
			+ "AND (c.id.ageLicencCodigo =:ageLicencCodigo) "
			+ "AND (:codigo is  null or c.id.codigo=:codigo) "
			+ "and (:nombres is null or UPPER(c.nombres) like %:nombres%) "
			+ "and (:apellidos is null or UPPER(c.apellidos) like %:apellidos%) "
			)
	public List<CcoCentroAcopio> buscarPorParametros(@Param("ageLicencCodigo")Integer ageLicencCodigo,@Param("codigo")Long codigo,
			@Param("nombres") String nombres,
			@Param("apellidos") String apellidos,@Param("estadoAnulado") String estadoAnulado);
}
