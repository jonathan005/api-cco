package sasf.net.cco.controller;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import javax.persistence.NoResultException;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import javassist.NotFoundException;
import sasf.net.cco.model.CcoFactoresPesos;
import sasf.net.cco.model.CcoFactoresPesosPK;
import sasf.net.cco.repository.CcoFactoresPesosRespository;
import sasf.net.cco.utils.EntidadCamposGenerales;
import sasf.net.cco.utils.FuncionesGenerales;
import sasf.net.cco.utils.Globales;
import sasf.net.cco.utils.ManejadorExcepciones;

@RestController
@RequestMapping("/ccoFactorPeso")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class CcoFactoresPesosController {
	
	@Autowired
	private TransactionTemplate transactionTemplate;
	
	@Autowired
	private FuncionesGenerales fg;
	
	@Autowired
	private CcoFactoresPesosRespository repository;
	
	@ApiOperation(value = "", authorizations = {@Authorization(value="Autorización token módulo cco")})
	@GetMapping(path = "/{codigoLicenciatario}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public List<?> consultar(@PathVariable Integer codigoLicenciatario,
			@RequestParam(name = "codigo", required = false) Long codigo
			) {
		return repository.buscarPorParametros(codigoLicenciatario, codigo, Globales.ESTADO_ANULADO);
	}
	
	@ApiOperation(value = "", authorizations = {@Authorization(value="Autorización token módulo cco")})
	@GetMapping(path = "/{codigoLicenciatario}/{codigo}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<?> consultarPorId(@PathVariable Integer codigoLicenciatario, @PathVariable Long codigo)
			throws NotFoundException {

		Optional<CcoFactoresPesos> factorPeso = repository.findById(new CcoFactoresPesosPK(codigo, codigoLicenciatario));
		if (!factorPeso.isPresent() && factorPeso.get().getEstado().equals(Globales.ESTADO_ANULADO)) {
			throw new NoResultException("Factor peso con código " + codigo + " no existe");
		}
		return new ResponseEntity<CcoFactoresPesos>(factorPeso.get(), HttpStatus.OK);
	}
	
	@ApiOperation(value = "", authorizations = {@Authorization(value="Autorización token módulo cco")})
	@PostMapping
	public ResponseEntity<?> ingresar(@RequestBody CcoFactoresPesos transaccion,
			HttpServletRequest request) {
		if (transaccion.getFactor().floatValue()<=0) {
			return ManejadorExcepciones.error(HttpStatus.BAD_REQUEST, null, 
					"Factor debe ser mayor a cero");
		}
		if(transaccion.getPesoDesde().floatValue()<=0) {
			return ManejadorExcepciones.error(HttpStatus.BAD_REQUEST, null, 
					"Peso desde debe ser mayor a cero");
		}
		if(transaccion.getPesoHasta().floatValue()<=0) {
			return ManejadorExcepciones.error(HttpStatus.BAD_REQUEST, null, 
					"Peso Hasta debe ser mayor a cero");
		}
		if(transaccion.getPesoDesde().floatValue()>transaccion.getPesoHasta().longValue()) {
			return ManejadorExcepciones.error(HttpStatus.BAD_REQUEST, null, 
					"Peso desde debe ser menor que peso hasta");
		}
		

		fg.validar(transaccion, CcoFactoresPesos.FactoresPesosCreation.class);
		fg.validar(transaccion, EntidadCamposGenerales.CamposGeneralesCreation.class);
		transaccion.setUbicacionIngreso(request.getRemoteAddr());

		transactionTemplate.execute(new TransactionCallbackWithoutResult() {
			@Override
			public void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
				Long id = fg.obtenerSecuencia(transaccion.getId().getAgeLicencCodigo(), Globales.CODIGO_APLICACION,
						Globales.CODIGO_SECUENCIA_FACTOR_PESO);
				if (id == 0) {
					throw new DataIntegrityViolationException("Secuencia " + Globales.CODIGO_SECUENCIA_FACTOR_PESO
							+ " para la aplicación " + Globales.CODIGO_APLICACION + " no existe");
				}
				transaccion.getId().setCodigo(id);
				if(repository.existsById(transaccion.getId())){
					throw new DataIntegrityViolationException(
							"Factor peso con código " + id + " ya existe");
				}
				repository.save(transaccion);
			}
		});

		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{codigoLicenciatario}/{id}")
				.buildAndExpand(transaccion.getId().getAgeLicencCodigo(), transaccion.getId().getCodigo()).toUri();
		return ResponseEntity.created(location).build();
	}
	
	@ApiOperation(value = "", authorizations = {@Authorization(value="Autorización token módulo cco")})
	@PostMapping("/varios")
	public ResponseEntity<?> ingresarVarios(@RequestBody List<CcoFactoresPesos> transacciones, 
			HttpServletRequest request) {
		
		transactionTemplate.execute(new TransactionCallbackWithoutResult() {
			@Override
			public void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
				for (CcoFactoresPesos transaccion : transacciones) {
					fg.validar(transaccion, CcoFactoresPesos.FactoresPesosCreation.class);
					fg.validar(transaccion, EntidadCamposGenerales.CamposGeneralesCreation.class);
					
					if (transaccion.getFactor().floatValue()<=0) {
						throw new DataIntegrityViolationException( 
								"Factor debe ser mayor a cero");
					}
					if(transaccion.getPesoDesde().floatValue()<=0) {
						throw new DataIntegrityViolationException( 
								"Peso desde debe ser mayor a cero");
					}
					if(transaccion.getPesoHasta().floatValue()<=0) {
						throw new DataIntegrityViolationException( 
								"Peso Hasta debe ser mayor a cero");
					}
					if(transaccion.getPesoDesde().floatValue()>transaccion.getPesoHasta().floatValue()) {
						throw new DataIntegrityViolationException( 
								"Peso desde debe ser menor que peso hasta");
					}
					transaccion.setUbicacionIngreso(request.getRemoteAddr());
					Long id = fg.obtenerSecuencia(transaccion.getId().getAgeLicencCodigo(), Globales.CODIGO_APLICACION,
							Globales.CODIGO_SECUENCIA_FACTOR_PESO);
					if (id == 0) {
						throw new DataIntegrityViolationException("Secuencia " + Globales.CODIGO_SECUENCIA_FACTOR_PESO
								+ " para la aplicación " + Globales.CODIGO_APLICACION + " no existe");
					}
					transaccion.getId().setCodigo(id);
					if(repository.existsById(transaccion.getId())){
						throw new DataIntegrityViolationException(
								"Embalaje con código " + id + " ya existe");
					}
					repository.save(transaccion);
				}
			}
		});

		return new ResponseEntity<List<CcoFactoresPesos>>(transacciones, HttpStatus.CREATED);
	}
	
	@ApiOperation(value = "", authorizations = {@Authorization(value="Autorización token módulo cco")})
	@PutMapping
	public ResponseEntity<?> actualizar(@RequestBody CcoFactoresPesos transaccion, HttpServletRequest request) {
		fg.validar(transaccion, CcoFactoresPesos.FactoresPesosUpdate.class);
		fg.validar(transaccion, EntidadCamposGenerales.CamposGeneralesUpdate.class);
		transaccion.setUbicacionModificacion(request.getRemoteAddr());
		Optional<CcoFactoresPesos> fPeso=repository.findById(transaccion.getId());
		
		if (!repository.existsById(
				new CcoFactoresPesosPK(transaccion.getId().getCodigo(), transaccion.getId().getAgeLicencCodigo()))) {
			return ManejadorExcepciones.error(HttpStatus.NOT_FOUND, null,
					"Factor peso con código " + transaccion.getId().getCodigo() + " no encontrado");
		}
		if(transaccion.getFactor()==null) {
			transaccion.setFactor(fPeso.get().getFactor());
		}
		if(transaccion.getPesoDesde()==null) {
			transaccion.setPesoDesde(fPeso.get().getFactor());
		}
		if(transaccion.getPesoHasta()==null) {
			transaccion.setPesoHasta(fPeso.get().getFactor());
		}
		
		if(transaccion.getPesoDesde().floatValue()>transaccion.getPesoHasta().longValue()) {
			return ManejadorExcepciones.error(HttpStatus.BAD_REQUEST, null, 
					"Peso desde debe ser menor que peso hasta");
		}
		
		repository.save(transaccion);

		return new ResponseEntity<String>("", HttpStatus.OK);
	}
	
	@ApiOperation(value = "", authorizations = {@Authorization(value="Autorización token módulo cco")})
	@PutMapping("/varios")
	public ResponseEntity<?> actualizarVarios(@RequestBody List<CcoFactoresPesos> transacciones,
			HttpServletRequest request) {
		transactionTemplate.execute(new TransactionCallbackWithoutResult() {
			@Override
			public void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
				for (CcoFactoresPesos transaccion : transacciones) {
					fg.validar(transaccion, CcoFactoresPesos.FactoresPesosUpdate.class);
					fg.validar(transaccion, EntidadCamposGenerales.CamposGeneralesUpdate.class);
					Optional<CcoFactoresPesos> fPeso=repository.findById(transaccion.getId());
					
					if (!repository.existsById(transaccion.getId())) {
						throw new NoResultException("Factor peso con código " + transaccion.getId().getCodigo() + " no encontrado");
					} 
					transaccion.setUbicacionModificacion(request.getRemoteAddr());
					
					if(transaccion.getFactor()==null) {
						transaccion.setFactor(fPeso.get().getFactor());
					}
					if(transaccion.getPesoDesde()==null) {
						transaccion.setPesoDesde(fPeso.get().getFactor());
					}
					if(transaccion.getPesoHasta()==null) {
						transaccion.setPesoHasta(fPeso.get().getFactor());
					}
					if(transaccion.getPesoDesde().floatValue()>transaccion.getPesoHasta().floatValue()) {
						throw new DataIntegrityViolationException( 
								"Peso desde debe ser menor que peso hasta");
					}
					
					repository.save(transaccion);
				}
			}
		});
		return new ResponseEntity<List<CcoFactoresPesos>>(transacciones, HttpStatus.CREATED);
	}
}
