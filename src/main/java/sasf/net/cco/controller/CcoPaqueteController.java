package sasf.net.cco.controller;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.net.URI;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.NoResultException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.web.util.CookieGenerator;
import org.springframework.web.util.UriComponentsBuilder;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import javassist.NotFoundException;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import sasf.net.cco.model.CcoCotizacion;
import sasf.net.cco.model.CcoEmbalajes;
import sasf.net.cco.model.CcoEmbalajesPK;
import sasf.net.cco.model.CcoFactoresDistancias;
import sasf.net.cco.model.CcoFactoresPesos;
import sasf.net.cco.model.CcoPaquete;
import sasf.net.cco.model.CcoPaqueteEstado;
import sasf.net.cco.model.CcoPaqueteEstadoPK;
import sasf.net.cco.model.CcoPaquetePK;
import sasf.net.cco.repository.CcoEmbalajesRepository;
import sasf.net.cco.repository.CcoFacoresDistanciasRespository;
import sasf.net.cco.repository.CcoFactoresPesosRespository;
import sasf.net.cco.repository.CcoPaqueteRespository;
import sasf.net.cco.utils.EntidadCamposGenerales;
import sasf.net.cco.utils.FuncionesGenerales;
import sasf.net.cco.utils.Globales;
import sasf.net.cco.utils.ManejadorExcepciones;

@RestController
@RequestMapping("/ccoPaquete")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class CcoPaqueteController {

	@Autowired
	private TransactionTemplate transactionTemplate;

	@Autowired
	private FuncionesGenerales fg;

	@Autowired
	private CcoPaqueteRespository repository;

	@Autowired
	private CcoEmbalajesRepository embalajeRepository;

	@Autowired
	private CcoFacoresDistanciasRespository distanciasRepository;
	
	@Autowired
	private CcoFactoresPesosRespository pesosRepository;
	
	@Autowired
	private CcoPaqueteEstadoController paqueteEstadoController;

	@Value("${age.urlBase}")
	private String ageUrlBase;
	
	@Value("${cli.urlCliente}")
	private String urlCliente;

	private final Logger log = LoggerFactory.getLogger(this.getClass());

	 @ApiOperation(value = "", authorizations =
	 {@Authorization(value="Autorización token módulo cco")})
	@GetMapping(path = "/{codigoLicenciatario}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public List<CcoPaquete> consultar(@PathVariable Integer codigoLicenciatario,
			@RequestParam(name = "codigo", required = false) Long codigo,
			@RequestParam(name = "tipoPaquete", required = false) String tipoPaquete,
			@RequestParam(value = "fechaDesde", required = false) @DateTimeFormat(iso = ISO.DATE, pattern = "yyyy-MM-dd") Date fechaDesde,
			@RequestParam(value = "fechaHasta", required = false) @DateTimeFormat(iso = ISO.DATE, pattern = "yyyy-MM-dd") Date fechaHasta,
			@RequestParam(name = "estadoPaquete", required = false) String estadoPaquete,
			@RequestParam(name = "usuarioIngreso", required = false) Long usuarioIngreso,
			@RequestParam(name = "cedulaPersonaEntrega", required = false) String cedulaPersonaEntrega,
			@RequestParam(name = "codigoSucursal", required = false) Long codigoSucursal,
			@RequestParam(name = "codigoCliente", required = false) Long codigoCliente,
			@RequestParam(name = "descripcion", required = false) String descripcion,
			@RequestParam(name = "codigoPaqueteDesde", required = false) Long codigoPaqueteDesde,
			@RequestParam(name = "codigoPaqueteHasta", required = false) Long codigoPaqueteHasta) {
		return repository.buscarPorParametros(codigoLicenciatario, codigo, tipoPaquete, fechaDesde, fechaHasta,
				estadoPaquete, usuarioIngreso, cedulaPersonaEntrega, codigoSucursal, codigoCliente, descripcion,
				codigoPaqueteDesde, codigoPaqueteHasta, Globales.ESTADO_ANULADO);
	}

	 @ApiOperation(value = "", authorizations =
	 {@Authorization(value="Autorización token módulo cco")})
	@GetMapping(path = "/{codigoLicenciatario}/{codigo}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<?> consultarPorId(@PathVariable Integer codigoLicenciatario, @PathVariable Long codigo)
			throws NotFoundException {

		Optional<CcoPaquete> paquete = repository.findById(new CcoPaquetePK(codigo, codigoLicenciatario));
		if (!paquete.isPresent() && paquete.get().getEstado().equals(Globales.ESTADO_ANULADO)) {
			throw new NoResultException("Queja, reclamo o trabajo con código " + codigo + " y licenciatario "
					+ codigoLicenciatario + " no existe");
		}
		return new ResponseEntity<CcoPaquete>(paquete.get(), HttpStatus.OK);
	}

	 @ApiOperation(value = "", authorizations =
	 {@Authorization(value="Autorización token módulo cco")})
	@PostMapping
	public ResponseEntity<?> ingresar(@RequestBody CcoPaquete transaccion, HttpServletRequest request) {

		fg.validar(transaccion, CcoPaquete.PaqueteCreation.class);
		fg.validar(transaccion, EntidadCamposGenerales.CamposGeneralesCreation.class);
		transaccion.setUbicacionIngreso(request.getRemoteAddr());

		if (!embalajeRepository.existsById(
				new CcoEmbalajesPK(transaccion.getCooEmbalaCodigo(), transaccion.getCooEmbalaAgeLicencCodigo()))) {
			return ManejadorExcepciones.error(HttpStatus.NOT_FOUND, null,
					"Embalaje con código " + transaccion.getCooEmbalaCodigo() + " y licenciatario "
							+ transaccion.getCooEmbalaAgeLicencCodigo() + " no encontrado");
		}

		transactionTemplate.execute(new TransactionCallbackWithoutResult() {
			@Override
			public void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
				Long id = fg.obtenerSecuencia(transaccion.getId().getAgeLicencCodigo(), Globales.CODIGO_APLICACION,
						Globales.CODIGO_SECUENCIA_PAQUETE);
				if (id == 0) {
					throw new DataIntegrityViolationException("Secuencia " + Globales.CODIGO_SECUENCIA_PAQUETE
							+ " para la aplicación " + Globales.CODIGO_APLICACION + " no existe");
				}
				transaccion.getId().setCodigo(id);
				if (repository.existsById(transaccion.getId())) {
					throw new DataIntegrityViolationException("Paquete con código " + id + " y licenciatario "
							+ transaccion.getId().getAgeLicencCodigo() + " ya existe");
				}

				String strCodigo = String.valueOf(transaccion.getId().getCodigo());
				String strLicenciatario = String.valueOf(transaccion.getId().getAgeLicencCodigo());
				String digito = strLicenciatario + "";
				for (int i = 0; i < (10 - (strCodigo.length() + strLicenciatario.length())); i++) {
					digito = digito + "0";
				}
				digito = digito + "" + strCodigo;
				transaccion.setCodigoPaquete(digito);
				if (transaccion.getCodigoPaquete().length() != 10) {
					throw new DataIntegrityViolationException("Hubo un problema en asignar el código de paquete");
				}

				repository.save(transaccion);

				CcoPaqueteEstado paqueteEstado = new CcoPaqueteEstado(new CcoPaqueteEstadoPK(null,
						transaccion.getId().getCodigo(), transaccion.getId().getAgeLicencCodigo()),
						transaccion.getEstadoPaquete(), new Date());
				paqueteEstado.setEstado(transaccion.getEstado());
				paqueteEstado.setPaqueteEstado(transaccion.getEstadoPaquete());
				paqueteEstado.setUsuarioIngreso(transaccion.getUsuarioIngreso());
				ResponseEntity<?> respEntPaqEst = paqueteEstadoController.ingresar(paqueteEstado, request);
				if (respEntPaqEst.getStatusCode() != HttpStatus.CREATED) {
					throw new DataIntegrityViolationException("Hubo un problema al registrar el Estado de Paquete");
				}
			}
		});

		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{codigoLicenciatario}/{id}")
				.buildAndExpand(transaccion.getId().getAgeLicencCodigo(), transaccion.getId().getCodigo()).toUri();
		return ResponseEntity.created(location).body(transaccion);
	}

	 @ApiOperation(value = "", authorizations =
	 {@Authorization(value="Autorización token módulo cco")})
	@PostMapping("/varios")
	public ResponseEntity<?> ingresarVarios(@RequestBody List<CcoPaquete> transacciones, HttpServletRequest request) {

		transactionTemplate.execute(new TransactionCallbackWithoutResult() {
			@Override
			public void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
				for (CcoPaquete transaccion : transacciones) {
					fg.validar(transaccion, CcoPaquete.PaqueteCreation.class);
					fg.validar(transaccion, EntidadCamposGenerales.CamposGeneralesCreation.class);
					transaccion.setUbicacionIngreso(request.getRemoteAddr());

					if (!embalajeRepository.existsById(new CcoEmbalajesPK(transaccion.getCooEmbalaCodigo(),
							transaccion.getCooEmbalaAgeLicencCodigo()))) {
						throw new NoResultException("Embalaje con código " + transaccion.getCooEmbalaCodigo()
								+ " y licenciatario " + transaccion.getCooEmbalaAgeLicencCodigo() + " no encontrado");
					}

					Long id = fg.obtenerSecuencia(transaccion.getId().getAgeLicencCodigo(), Globales.CODIGO_APLICACION,
							Globales.CODIGO_SECUENCIA_PAQUETE);
					if (id == 0) {
						throw new DataIntegrityViolationException("Secuencia " + Globales.CODIGO_SECUENCIA_PAQUETE
								+ " para la aplicación " + Globales.CODIGO_APLICACION + " y licenciatario "
								+ transaccion.getId().getAgeLicencCodigo() + " no existe");
					}
					transaccion.getId().setCodigo(id);
					if (repository.existsById(transaccion.getId())) {
						throw new DataIntegrityViolationException("Paquete con código " + id + "y licenciatario "
								+ transaccion.getId().getAgeLicencCodigo() + " ya existe");
					}

					String strCodigo = String.valueOf(transaccion.getId().getCodigo());
					String strLicenciatario = String.valueOf(transaccion.getId().getAgeLicencCodigo());
					String digito = strLicenciatario + "";
					for (int i = 0; i < (10 - (strCodigo.length() + strLicenciatario.length())); i++) {
						digito = digito + "0";
					}
					digito = digito + "" + strCodigo;
					transaccion.setCodigoPaquete(digito);

					repository.save(transaccion);

					CcoPaqueteEstado paqueteEstado = new CcoPaqueteEstado(
							new CcoPaqueteEstadoPK(null, transaccion.getId().getCodigo(),
									transaccion.getId().getAgeLicencCodigo()),
							transaccion.getEstadoPaquete(), new Date());
					paqueteEstado.setEstado(transaccion.getEstado());
					paqueteEstado.setPaqueteEstado(transaccion.getEstadoPaquete());
					paqueteEstado.setUsuarioIngreso(transaccion.getUsuarioIngreso());
					ResponseEntity<?> respEntPaqEst = paqueteEstadoController.ingresar(paqueteEstado, request);
					if (respEntPaqEst.getStatusCode() != HttpStatus.CREATED) {
						throw new DataIntegrityViolationException("Hubo un problema al registrar el Estado de Paquete");
					}

				}
			}
		});

		return new ResponseEntity<List<CcoPaquete>>(transacciones, HttpStatus.CREATED);
	}

	 @ApiOperation(value = "", authorizations =
	 {@Authorization(value="Autorización token módulo cco")})
	@PutMapping
	public ResponseEntity<?> actualizar(@RequestBody CcoPaquete transaccion, HttpServletRequest request, @RequestHeader(name = "Authorization") String token) {
		fg.validar(transaccion, CcoPaquete.PaqueteUpdate.class);
		fg.validar(transaccion, EntidadCamposGenerales.CamposGeneralesUpdate.class);
		transaccion.setUbicacionModificacion(request.getRemoteAddr());
		Optional<CcoPaquete> paquete=repository.findById(transaccion.getId());
		
		if (!repository.existsById(transaccion.getId())) {
			return ManejadorExcepciones.error(HttpStatus.NOT_FOUND, null,
					"Paquete con código " + transaccion.getId().getCodigo() + " no encontrado");
		}
		
		if(transaccion.getTipoPaquete()==null || transaccion.getTipoPaquete().equals("") ) {
			transaccion.setTipoPaquete(paquete.get().getTipoPaquete());
		}
		if(transaccion.getFechaEnvio()==null) {
			transaccion.setFechaEnvio(paquete.get().getFechaEnvio());
		}
		
		if (transaccion.getPeso()==null || transaccion.getPeso().doubleValue()<=0) {
			transaccion.setPeso(paquete.get().getPeso());
		}
		if (transaccion.getCosto()==null || transaccion.getCosto().doubleValue()<=0) {
			transaccion.setCosto(paquete.get().getCosto());
		}
		
		if(transaccion.getCostoAnterior()==null) {
			transaccion.setCostoAnterior(paquete.get().getCostoAnterior());
		}
		if(transaccion.getPesoAnterior()==null) {
			transaccion.setPesoAnterior(paquete.get().getPesoAnterior());
		}
		
		if (transaccion.getEstadoPaquete()==null || transaccion.getEstadoPaquete().equals("")) {
			transaccion.setEstadoPaquete(paquete.get().getEstadoPaquete());
		}
		else {
			if(transaccion.getEstadoPaquete()!=null && transaccion.getEstadoPaquete()!=paquete.get().getEstadoPaquete()) {
				CcoPaqueteEstado paqueteEstado =new CcoPaqueteEstado(new CcoPaqueteEstadoPK(null, transaccion.getId().getCodigo(), transaccion.getId().getAgeLicencCodigo()), transaccion.getEstadoPaquete(), new Date()); 
				paqueteEstado.setEstado(transaccion.getEstado());
				paqueteEstado.setPaqueteEstado(transaccion.getEstadoPaquete());
				paqueteEstado.setUsuarioIngreso(transaccion.getUsuarioModificacion());
		        ResponseEntity<?> respEntPaqEst=paqueteEstadoController.ingresar(paqueteEstado, request);
		        if(respEntPaqEst.getStatusCode()!=HttpStatus.CREATED) {
		        	throw new DataIntegrityViolationException("Hubo un problema al registrar el Estado de Paquete");
		        }
		        
		        
		        JSONObject notificacion = new JSONObject();
		        JSONObject id = new JSONObject();
		        JSONObject data = new JSONObject();
		        
		        id.put("ageLicencCodigo", transaccion.getId().getAgeLicencCodigo());
		        notificacion.put("ageUsuariAgeLicencCodigo", transaccion.getId().getAgeLicencCodigo());
		        notificacion.put("ageUsuariCodigo", transaccion.getUsuarioIngreso()); 
		        notificacion.put("data", "{\"accion\": \"E\" , \"licenciatario\" :" + transaccion.getId().getAgeLicencCodigo() + ",\"paqueteId\":" + transaccion.getId().getCodigo() + "}");
		        notificacion.put("id", id);
		        notificacion.put("estado", "A");
		        notificacion.put("titulo", "Información sobre su paquete " + transaccion.getCodigoPaquete());
		        String estadoPaquete = "";
		        if(transaccion.getEstadoPaquete().equals(Globales.PAQUETE_ESTADO_OFICINA)) {
		        	estadoPaquete = "EN LA OFICINA PRINCIPAL";
		        } else if (transaccion.getEstadoPaquete().equals(Globales.PAQUETE_ESTADO_EN_CAMINO)) {
		        	estadoPaquete = "EN CAMINO";
		        } else if (transaccion.getEstadoPaquete().equals(Globales.PAQUETE_ESTADO_ENTREGADO)) {
		        	estadoPaquete = "ENTREGADO";
		        } else if (transaccion.getEstadoPaquete().equals(Globales.PAQUETE_ESTADO_CENTRO_ACOPIO)) {
		        	estadoPaquete = "EN CENTRO DE ACOPIO";
		        } else if (transaccion.getEstadoPaquete().equals(Globales.PAQUETE_ESTADO_INGRESADO)) {
		        	estadoPaquete = "INGRESADO";
		        } else if (transaccion.getEstadoPaquete().equals(Globales.PAQUETE_ESTADO_PENDIENTE_PAGO)) {
		        	estadoPaquete = "PENDIENTE DE PAGO";
		        } else if (transaccion.getEstadoPaquete().equals(Globales.PAQUETE_ESTADO_SOLICITUD_RECOLECCION)) {
		        	estadoPaquete = "SOLICITUD DE RECOLECCION";
		        }
		        notificacion.put("mensaje", "Su paquete " + transaccion.getCodigoPaquete() + " ah cambiado a estado " + estadoPaquete);
		        notificacion.put("usuarioIngreso", transaccion.getUsuarioModificacion());
		        ingresarNotificacion(notificacion, request, token);
			}
		}
		
		if(transaccion.getCooEmbalaCodigo()==null || transaccion.getCooEmbalaCodigo()<=0) {
			transaccion.setCooEmbalaCodigo(paquete.get().getCooEmbalaCodigo());
		}
		if(transaccion.getCooEmbalaAgeLicencCodigo()==null || transaccion.getCooEmbalaAgeLicencCodigo()<=0) {
			transaccion.setCooEmbalaAgeLicencCodigo(paquete.get().getCooEmbalaAgeLicencCodigo());
		}
		
		if(!embalajeRepository.existsById(new CcoEmbalajesPK(transaccion.getCooEmbalaCodigo(), transaccion.getCooEmbalaAgeLicencCodigo()))) {
			return ManejadorExcepciones.error(HttpStatus.NOT_FOUND, null,
					"Embalaje con código " + transaccion.getCooEmbalaCodigo() +" y licenciatario "+transaccion.getCooEmbalaAgeLicencCodigo()+ " no encontrado");
		}
		if(transaccion.getAgeLocaliCodigo()==null ||transaccion.getAgeLocaliCodigo()<=0) {
			transaccion.setAgeLocaliCodigo(paquete.get().getAgeLocaliCodigo());
		}
		if(transaccion.getAgeLocaliTipLoCodigo()==null ||transaccion.getAgeLocaliTipLoCodigo()<=0) {
			transaccion.setAgeLocaliTipLoCodigo(paquete.get().getAgeLocaliTipLoCodigo());
		}
		if(transaccion.getAgecaliTipLoAgePaisCodigo()==null || transaccion.getAgecaliTipLoAgePaisCodigo()<=0) {
			transaccion.setAgecaliTipLoAgePaisCodigo(paquete.get().getAgecaliTipLoAgePaisCodigo());
		}
		if(transaccion.getAgeLocaliCodigoH()==null || transaccion.getAgeLocaliCodigoH() <=0) {
			transaccion.setAgeLocaliCodigoH(paquete.get().getAgeLocaliCodigoH());
		}
		if(transaccion.getAgeLocaliTipLoCodigoH()==null || transaccion.getAgeLocaliTipLoCodigoH()<=0) {
			transaccion.setAgeLocaliTipLoCodigoH(paquete.get().getAgeLocaliTipLoCodigoH());
		}
		if(transaccion.getAgeliTipLoAgePaisCodigoH()==null || transaccion.getAgeliTipLoAgePaisCodigoH() <=0) {
			transaccion.setAgeliTipLoAgePaisCodigoH(paquete.get().getAgeliTipLoAgePaisCodigoH());
		}
		
		if(transaccion.getAgeForPaCodigo()==null|| transaccion.getAgeForPaCodigo()<=0) {
			transaccion.setAgeForPaCodigo(paquete.get().getAgeForPaCodigo());
		}
		if(transaccion.getAgeForPaAgeLicencCodigo()==null || transaccion.getAgeForPaAgeLicencCodigo()<=0) {
			transaccion.setAgeForPaAgeLicencCodigo(paquete.get().getAgeForPaAgeLicencCodigo());
		}
		
		if(transaccion.getLugarEntrega()==null || transaccion.getLugarEntrega().equals("")) {
			transaccion.setLugarEntrega(paquete.get().getLugarEntrega());
		}
		if(transaccion.getLatitudEntrega()==null || transaccion.getLatitudEntrega().equals("")) {
			transaccion.setLatitudEntrega(paquete.get().getLatitudEntrega());
		}
		if(transaccion.getLongitudEntrega()==null || transaccion.getLongitudEntrega().equals("")) {
			transaccion.setLongitudEntrega(paquete.get().getLongitudEntrega());
		}
		if(transaccion.getLugarRecoleccion()==null || transaccion.getLugarRecoleccion().equals("")) {
			transaccion.setLugarRecoleccion(paquete.get().getLugarRecoleccion());
		}
		if(transaccion.getLatitudRecoleccion()==null || transaccion.getLatitudRecoleccion().equals("")) {
			transaccion.setLatitudRecoleccion(paquete.get().getLatitudRecoleccion());
		}
		if(transaccion.getLongitudRecoleccion()==null || transaccion.getLongitudRecoleccion().equals("")) {
			transaccion.setLongitudRecoleccion(paquete.get().getLongitudRecoleccion());
		}
		
		if(transaccion.getAgeSucursCodigo()==null) {
			transaccion.setAgeSucursCodigo(paquete.get().getAgeSucursCodigo());
		}
		
		if(transaccion.getAgeSucursAgeLicencCodigo()==null) {
			transaccion.setAgeSucursAgeLicencCodigo(paquete.get().getAgeSucursAgeLicencCodigo());
		}
		
		repository.save(transaccion);

		return new ResponseEntity<String>("", HttpStatus.OK);
	}

	private void ingresarNotificacion(JSONObject notificacionPush, HttpServletRequest request, String token) {

		
		System.out.println(token);
		try {
			RestTemplate restTemplate = new RestTemplate();
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.set("Authorization", token);
			HttpEntity<String> entity = new HttpEntity<String>(notificacionPush.toString(), headers);

			HttpEntity<String> response = restTemplate.exchange(ageUrlBase + "/ageNotificacionesPush", HttpMethod.POST, entity, String.class);
			System.out.println(response);
		} catch (Exception e) {
			e.printStackTrace();
			throw new DataIntegrityViolationException("Problemas al ingresa la notificación push");
		}

	}

	 @ApiOperation(value = "", authorizations =
	 {@Authorization(value="Autorización token módulo cco")})
	@PutMapping("/varios")
	public ResponseEntity<?> actualizarVarios(@RequestBody List<CcoPaquete> transacciones, HttpServletRequest request, @RequestHeader(name = "Authorization") String token) {
		transactionTemplate.execute(new TransactionCallbackWithoutResult() {
			@Override
			public void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
				for (CcoPaquete transaccion : transacciones) {
					fg.validar(transaccion, CcoPaquete.PaqueteUpdate.class);
					fg.validar(transaccion, EntidadCamposGenerales.CamposGeneralesUpdate.class);

					Optional<CcoPaquete> paquete = repository.findById(transaccion.getId());

					if (!repository.existsById(transaccion.getId())) {
						throw new NoResultException(
								"Paquete con código " + transaccion.getId().getCodigo() + " no encontrado");
					}
					transaccion.setUbicacionModificacion(request.getRemoteAddr());

					if (transaccion.getTipoPaquete() == null || transaccion.getTipoPaquete().equals("")) {
						transaccion.setTipoPaquete(paquete.get().getTipoPaquete());
					}
					if (transaccion.getFechaEnvio() == null) {
						transaccion.setFechaEnvio(paquete.get().getFechaEnvio());
					}

					if (transaccion.getPeso() == null || transaccion.getPeso().doubleValue() <= 0) {
						transaccion.setPeso(paquete.get().getPeso());
					}
					if (transaccion.getCosto() == null || transaccion.getCosto().doubleValue() <= 0) {
						transaccion.setCosto(paquete.get().getCosto());
					}
					
					if(transaccion.getCostoAnterior()==null) {
						transaccion.setCostoAnterior(paquete.get().getCostoAnterior());
					}
					if(transaccion.getPesoAnterior()==null) {
						transaccion.setPesoAnterior(paquete.get().getPesoAnterior());
					}

					if (transaccion.getEstadoPaquete() == null || transaccion.getEstadoPaquete().equals("")) {
						transaccion.setEstadoPaquete(paquete.get().getEstadoPaquete());
					} else {
						if (transaccion.getEstadoPaquete() != null
								&& transaccion.getEstadoPaquete() != paquete.get().getEstadoPaquete()) {

							CcoPaqueteEstado paqueteEstado = new CcoPaqueteEstado(
									new CcoPaqueteEstadoPK(null, transaccion.getId().getCodigo(),
											transaccion.getId().getAgeLicencCodigo()),
									transaccion.getEstadoPaquete(), new Date());
							paqueteEstado.setEstado(transaccion.getEstado());
							paqueteEstado.setPaqueteEstado(transaccion.getEstadoPaquete());
							paqueteEstado.setUsuarioIngreso(transaccion.getUsuarioModificacion());
							ResponseEntity<?> respEntPaqEst = paqueteEstadoController.ingresar(paqueteEstado, request);
							if (respEntPaqEst.getStatusCode() != HttpStatus.CREATED) {
								throw new DataIntegrityViolationException(
										"Hubo un problema al registrar el Estado de Paquete");
							}
							
							JSONObject notificacion = new JSONObject();
					        JSONObject id = new JSONObject();
					        JSONObject data = new JSONObject();
					        
					        id.put("ageLicencCodigo", transaccion.getId().getAgeLicencCodigo());
					        notificacion.put("ageUsuariAgeLicencCodigo", transaccion.getId().getAgeLicencCodigo());
					        notificacion.put("ageUsuariCodigo", transaccion.getUsuarioIngreso()); 
					        notificacion.put("data", "{\"accion\": \"E\" , \"licenciatario\" :" + transaccion.getId().getAgeLicencCodigo() + ",\"paqueteId\":" + transaccion.getId().getCodigo() + "}");
					        notificacion.put("id", id);
					        notificacion.put("estado", "A");
					        notificacion.put("titulo", "Información sobre su paquete " + transaccion.getCodigoPaquete());
					        String estadoPaquete = "";
					        if(transaccion.getEstadoPaquete().equals(Globales.PAQUETE_ESTADO_OFICINA)) {
					        	estadoPaquete = "EN LA OFICINA PRINCIPAL";
					        } else if (transaccion.getEstadoPaquete().equals(Globales.PAQUETE_ESTADO_EN_CAMINO)) {
					        	estadoPaquete = "EN CAMINO";
					        } else if (transaccion.getEstadoPaquete().equals(Globales.PAQUETE_ESTADO_ENTREGADO)) {
					        	estadoPaquete = "ENTREGADO";
					        } else if (transaccion.getEstadoPaquete().equals(Globales.PAQUETE_ESTADO_CENTRO_ACOPIO)) {
					        	estadoPaquete = "EN CENTRO DE ACOPIO";
					        } else if (transaccion.getEstadoPaquete().equals(Globales.PAQUETE_ESTADO_INGRESADO)) {
					        	estadoPaquete = "INGRESADO";
					        } else if (transaccion.getEstadoPaquete().equals(Globales.PAQUETE_ESTADO_PENDIENTE_PAGO)) {
					        	estadoPaquete = "PENDIENTE DE PAGO";
					        } else if (transaccion.getEstadoPaquete().equals(Globales.PAQUETE_ESTADO_SOLICITUD_RECOLECCION)) {
					        	estadoPaquete = "SOLICITUD DE RECOLECCION";
					        }
					        notificacion.put("mensaje", "Su paquete " + transaccion.getCodigoPaquete() + " ah cambiado a estado " + estadoPaquete);
					        notificacion.put("usuarioIngreso", transaccion.getUsuarioModificacion());
					        ingresarNotificacion(notificacion, request, token);
						}
					}

					if (transaccion.getCooEmbalaCodigo() == null || transaccion.getCooEmbalaCodigo() <= 0) {
						transaccion.setCooEmbalaCodigo(paquete.get().getCooEmbalaCodigo());
					}
					if (transaccion.getCooEmbalaAgeLicencCodigo() == null
							|| transaccion.getCooEmbalaAgeLicencCodigo() <= 0) {
						transaccion.setCooEmbalaAgeLicencCodigo(paquete.get().getCooEmbalaAgeLicencCodigo());
					}

					if (!embalajeRepository.existsById(new CcoEmbalajesPK(transaccion.getCooEmbalaCodigo(),
							transaccion.getCooEmbalaAgeLicencCodigo()))) {
						throw new NoResultException("Embalaje con código " + transaccion.getCooEmbalaCodigo()
								+ " y licenciatario " + transaccion.getCooEmbalaAgeLicencCodigo() + " no encontrado");
					}
					if (transaccion.getAgeLocaliCodigo() == null || transaccion.getAgeLocaliCodigo() <= 0) {
						transaccion.setAgeLocaliCodigo(paquete.get().getAgeLocaliCodigo());
					}
					if (transaccion.getAgeLocaliTipLoCodigo() == null || transaccion.getAgeLocaliTipLoCodigo() <= 0) {
						transaccion.setAgeLocaliTipLoCodigo(paquete.get().getAgeLocaliTipLoCodigo());
					}
					if (transaccion.getAgecaliTipLoAgePaisCodigo() == null
							|| transaccion.getAgecaliTipLoAgePaisCodigo() <= 0) {
						transaccion.setAgecaliTipLoAgePaisCodigo(paquete.get().getAgecaliTipLoAgePaisCodigo());
					}
					if (transaccion.getAgeLocaliCodigoH() == null || transaccion.getAgeLocaliCodigoH() <= 0) {
						transaccion.setAgeLocaliCodigoH(paquete.get().getAgeLocaliCodigoH());
					}
					if (transaccion.getAgeLocaliTipLoCodigoH() == null || transaccion.getAgeLocaliTipLoCodigoH() <= 0) {
						transaccion.setAgeLocaliTipLoCodigoH(paquete.get().getAgeLocaliTipLoCodigoH());
					}
					if (transaccion.getAgeliTipLoAgePaisCodigoH() == null
							|| transaccion.getAgeliTipLoAgePaisCodigoH() <= 0) {
						transaccion.setAgeliTipLoAgePaisCodigoH(paquete.get().getAgeliTipLoAgePaisCodigoH());
					}

					if (transaccion.getAgeForPaCodigo() == null || transaccion.getAgeForPaCodigo() <= 0) {
						transaccion.setAgeForPaCodigo(paquete.get().getAgeForPaCodigo());
					}
					if (transaccion.getAgeForPaAgeLicencCodigo() == null
							|| transaccion.getAgeForPaAgeLicencCodigo() <= 0) {
						transaccion.setAgeForPaAgeLicencCodigo(paquete.get().getAgeForPaAgeLicencCodigo());
					}

					if (transaccion.getLugarEntrega() == null || transaccion.getLugarEntrega().equals("")) {
						transaccion.setLugarEntrega(paquete.get().getLugarEntrega());
					}
					if (transaccion.getLatitudEntrega() == null || transaccion.getLatitudEntrega().equals("")) {
						transaccion.setLatitudEntrega(paquete.get().getLatitudEntrega());
					}
					if (transaccion.getLongitudEntrega() == null || transaccion.getLongitudEntrega().equals("")) {
						transaccion.setLongitudEntrega(paquete.get().getLongitudEntrega());
					}
					if (transaccion.getLugarRecoleccion() == null || transaccion.getLugarRecoleccion().equals("")) {
						transaccion.setLugarRecoleccion(paquete.get().getLugarRecoleccion());
					}
					if (transaccion.getLatitudRecoleccion() == null || transaccion.getLatitudRecoleccion().equals("")) {
						transaccion.setLatitudRecoleccion(paquete.get().getLatitudRecoleccion());
					}
					if (transaccion.getLongitudRecoleccion() == null
							|| transaccion.getLongitudRecoleccion().equals("")) {
						transaccion.setLongitudRecoleccion(paquete.get().getLongitudRecoleccion());
					}

					if (transaccion.getAgeSucursCodigo() == null) {
						transaccion.setAgeSucursCodigo(paquete.get().getAgeSucursCodigo());
					}

					if (transaccion.getAgeSucursAgeLicencCodigo() == null) {
						transaccion.setAgeSucursAgeLicencCodigo(paquete.get().getAgeSucursAgeLicencCodigo());
					}

					repository.save(transaccion);
				}
			}
		});
		return new ResponseEntity<List<CcoPaquete>>(transacciones, HttpStatus.CREATED);
	}
	 
	@PostMapping("/calcularCostoEnvio") 
	public ResponseEntity<?> calcularCostoEnvio(@RequestBody CcoCotizacion cotizacion) {
		List<CcoFactoresDistancias> distancias = distanciasRepository.findAll();
		List<CcoFactoresPesos> pesos = pesosRepository.findAll();
		Optional<CcoEmbalajes> embalaje = embalajeRepository.findById(cotizacion.getEmbalajeId());
		BigDecimal valorEnvio = BigDecimal.ZERO;
		BigDecimal factorPeso = BigDecimal.ONE;
		BigDecimal factorDistancia = BigDecimal.ONE;
		for(CcoFactoresDistancias distancia: distancias) {
			if (cotizacion.getDistancia().compareTo(distancia.getDistanciaDesde()) > 0 &&
					cotizacion.getDistancia().compareTo(distancia.getDistanciaHasta()) <= 0) {
				if(cotizacion.getPeso().compareTo(BigDecimal.valueOf(14)) > 0) {
					factorDistancia = BigDecimal.valueOf(0.010).multiply(cotizacion.getDistancia());
				} else {
					factorDistancia = distancia.getFactor();	
				}
			}
		}
		for (CcoFactoresPesos peso: pesos) {
			if (cotizacion.getPeso().compareTo(peso.getPesoDesde()) > 0 &&
					cotizacion.getPeso().compareTo(peso.getPesoHasta()) <= 0) {
				factorPeso = peso.getFactor();
			}
		}
		valorEnvio = factorDistancia.multiply(factorPeso).multiply(cotizacion.getPeso()).add(embalaje.get().getCostoAdicional());
		if (valorEnvio.compareTo(BigDecimal.valueOf(3)) < 0) {
			valorEnvio = BigDecimal.valueOf(3);
		}
		return new ResponseEntity<BigDecimal> (valorEnvio, HttpStatus.OK);
	}
	
	@PostMapping("/calcularCostoAdicional")
	public ResponseEntity<?> calcularCostoAdicional (@RequestBody CcoCotizacion cotizacion){
		BigDecimal valorDesde = BigDecimal.ZERO;
		BigDecimal valorHasta = BigDecimal.valueOf(14);
		BigDecimal factor = BigDecimal.valueOf(0.35);
		BigDecimal costoAdicional = BigDecimal.ZERO;
		if (cotizacion.getDistancia().compareTo(valorDesde) > 0 && 
				cotizacion.getDistancia().compareTo(valorHasta) <= 0){
			costoAdicional = factor.multiply(cotizacion.getDistancia());
			if (costoAdicional.compareTo( BigDecimal.valueOf(1.5))<0) {
				costoAdicional = BigDecimal.valueOf(1.5);
			}
		} else {
			costoAdicional = BigDecimal.valueOf(5);
		}
		return new ResponseEntity<BigDecimal> (costoAdicional, HttpStatus.OK);
	}
	
	@ApiOperation(value = "", authorizations = { @Authorization(value = "Autorización token módulo cco") })
	@GetMapping("/getPDF")
	public @ResponseBody void getReportPdf(HttpServletResponse response) {
		try {

			InputStream jasperStream = this.getClass().getResourceAsStream("/report1.jrxml");
			JasperDesign design = JRXmlLoader.load(jasperStream);
			JasperReport report = JasperCompileManager.compileReport(design);

			Map<String, Object> parametersMap = new HashMap<>();

			List<CcoPaquete> paquetes = repository.findAll();
			Collections.sort(paquetes, (imp1, imp2) -> imp1.getId().getCodigo().compareTo(imp2.getId().getCodigo()));

			JRDataSource jrDataSource = new JRBeanCollectionDataSource(paquetes);

			parametersMap.put("DataSource", jrDataSource);

			JasperPrint jasperPrint = JasperFillManager.fillReport(report, parametersMap, jrDataSource);
			response.setContentType("application/pdf");
			response.setHeader("Content-disposition", "attachment; filename=\"reporte.pdf\"");
			final OutputStream outputStream = response.getOutputStream();

			JasperExportManager.exportReportToPdfStream(jasperPrint, outputStream);

		} catch (JRException e) {
			e.printStackTrace();
			log.error("Hubo un problema con el archivo jrxml");
		} catch (IOException e) {
			log.error("Hubo un problema al transformar los archivos");
		} catch (Exception e) {
			log.warn("Hubo un problema con con el servicio PDF: \n" + e.toString());

		}
	}

	@ApiOperation(value = "", authorizations = { @Authorization(value = "Autorización token módulo cco") })

	@GetMapping(path = "/getFormPDF/{codigoLicenciatario}")
	public @ResponseBody void getFormPdf(@PathVariable(name = "codigoLicenciatario") Integer codigoLicenciatario,
			@RequestParam(name = "codigo", required = false) Long codigo,
			@RequestParam(name = "tipoPaquete", required = false) String tipoPaquete,
			@RequestParam(value = "fechaDesde", required = false) @DateTimeFormat(iso = ISO.DATE, pattern = "yyyy-MM-dd") Date fechaDesde,
			@RequestParam(value = "fechaHasta", required = false) @DateTimeFormat(iso = ISO.DATE, pattern = "yyyy-MM-dd") Date fechaHasta,
			@RequestParam(name = "estadoPaquete", required = false) String estadoPaquete,
			@RequestParam(name = "usuarioIngreso", required = false) Long usuarioIngreso,
			@RequestParam(name = "cedulaPersonaEntrega", required = false) String cedulaPersonaEntrega,
			@RequestParam(name = "codigoSucursal", required = false) Long codigoSucursal,
			@RequestParam(name = "codigoCliente", required = false) Long codigoCliente,
			@RequestParam(name = "descripcion", required = false) String descripcion,
			@RequestParam(name = "codigoPaqueteDesde", required = false) Long codigoPaqueteDesde,
			@RequestParam(name = "codigoPaqueteHasta", required = false) Long codigoPaqueteHasta,
			HttpServletResponse response, HttpServletRequest request) {
		try {

			InputStream jasperStream = this.getClass().getResourceAsStream("/report2.jrxml");
			JasperDesign design = JRXmlLoader.load(jasperStream);
			JasperReport report = JasperCompileManager.compileReport(design);

			Map<String, Object> parametersMap = new HashMap<>();
			List<CcoPaquete> paquetes;
			if (codigoLicenciatario == null) {
				paquetes = repository.findAll().stream().map(t -> {
					if (t.getEstadoPaquete().equals(Globales.PAQUETE_ESTADO_OFICINA) || t.getEstadoPaquete() == Globales.PAQUETE_ESTADO_OFICINA) {
						t.setEstadoPaquete("En oficina");
					} else if (t.getEstadoPaquete().equals(Globales.PAQUETE_ESTADO_EN_CAMINO) || t.getEstadoPaquete() == Globales.PAQUETE_ESTADO_OFICINA) {
						t.setEstadoPaquete("En camino");
					} else if (t.getEstadoPaquete().equals(Globales.PAQUETE_ESTADO_ENTREGADO) || t.getEstadoPaquete() == Globales.PAQUETE_ESTADO_ENTREGADO) {
						t.setEstadoPaquete("Entregado");
					}else if (t.getEstadoPaquete().equals(Globales.PAQUETE_ESTADO_CENTRO_ACOPIO) || t.getEstadoPaquete() == Globales.PAQUETE_ESTADO_CENTRO_ACOPIO) {
						t.setEstadoPaquete("En centro de acopio");
					}else if (t.getEstadoPaquete().equals(Globales.PAQUETE_ESTADO_INGRESADO) || t.getEstadoPaquete() == Globales.PAQUETE_ESTADO_INGRESADO) {
						t.setEstadoPaquete("Ingresado");
					}else if (t.getEstadoPaquete().equals(Globales.PAQUETE_ESTADO_PENDIENTE_PAGO) || t.getEstadoPaquete() == Globales.PAQUETE_ESTADO_PENDIENTE_PAGO) {
						t.setEstadoPaquete("Pendiente de pago");
					}else if (t.getEstadoPaquete().equals(Globales.PAQUETE_ESTADO_SOLICITUD_RECOLECCION) || t.getEstadoPaquete() == Globales.PAQUETE_ESTADO_SOLICITUD_RECOLECCION) {
						t.setEstadoPaquete("Solicitud de recolección");
					}
					if (t.getTipoPaquete().equals("P") || t.getTipoPaquete() == "P") {
						t.setTipoPaquete("Paquete");
					} else if (t.getTipoPaquete().equals("D") || t.getTipoPaquete() == "D") {
						t.setTipoPaquete("Documento");
					}
					t.setDescripcionEmbalaje(embalajeRepository
							.findById(new CcoEmbalajesPK(t.getCooEmbalaCodigo(), t.getCooEmbalaAgeLicencCodigo())).get()
							.getDescripcion());
					return t;
				}).collect(Collectors.toList());
			} else {
				paquetes = this.consultar(codigoLicenciatario, codigo, tipoPaquete, fechaDesde, fechaHasta,
						estadoPaquete, usuarioIngreso, cedulaPersonaEntrega, codigoSucursal, codigoCliente, descripcion,
						codigoPaqueteDesde, codigoPaqueteHasta).stream().map(t -> {
							if (t.getEstadoPaquete().equals(Globales.PAQUETE_ESTADO_OFICINA) || t.getEstadoPaquete() == Globales.PAQUETE_ESTADO_OFICINA) {
								t.setEstadoPaquete("En oficina");
							} else if (t.getEstadoPaquete().equals(Globales.PAQUETE_ESTADO_EN_CAMINO) || t.getEstadoPaquete() == Globales.PAQUETE_ESTADO_OFICINA) {
								t.setEstadoPaquete("En camino");
							} else if (t.getEstadoPaquete().equals(Globales.PAQUETE_ESTADO_ENTREGADO) || t.getEstadoPaquete() == Globales.PAQUETE_ESTADO_ENTREGADO) {
								t.setEstadoPaquete("Entregado");
							}else if (t.getEstadoPaquete().equals(Globales.PAQUETE_ESTADO_CENTRO_ACOPIO) || t.getEstadoPaquete() == Globales.PAQUETE_ESTADO_CENTRO_ACOPIO) {
								t.setEstadoPaquete("En centro de acopio");
							}else if (t.getEstadoPaquete().equals(Globales.PAQUETE_ESTADO_INGRESADO) || t.getEstadoPaquete() == Globales.PAQUETE_ESTADO_INGRESADO) {
								t.setEstadoPaquete("Ingresado");
							}else if (t.getEstadoPaquete().equals(Globales.PAQUETE_ESTADO_PENDIENTE_PAGO) || t.getEstadoPaquete() == Globales.PAQUETE_ESTADO_PENDIENTE_PAGO) {
								t.setEstadoPaquete("Pendiente de pago");
							}else if (t.getEstadoPaquete().equals(Globales.PAQUETE_ESTADO_SOLICITUD_RECOLECCION) || t.getEstadoPaquete() == Globales.PAQUETE_ESTADO_SOLICITUD_RECOLECCION) {
								t.setEstadoPaquete("Solicitud de recolección");
							}

							if (t.getAgeLocaliCodigo() != null && t.getAgeLocaliTipLoCodigo() != null
									&& t.getAgecaliTipLoAgePaisCodigo() != null) {
								JSONObject joLocaliDesde = getAgeLocalidadPorId(t.getAgeLocaliCodigo(),
										t.getAgeLocaliTipLoCodigo(), t.getAgecaliTipLoAgePaisCodigo(), request);
								if (joLocaliDesde != null) {
									t.setLocalidadDesde(joLocaliDesde.getString("descripcion"));
								}
							}

							if (t.getAgeLocaliCodigoH() != null && t.getAgeLocaliTipLoCodigoH() != null
									&& t.getAgeliTipLoAgePaisCodigoH() != null) {
								JSONObject joLocaliHasta = getAgeLocalidadPorId(t.getAgeLocaliCodigoH(),
										t.getAgeLocaliTipLoCodigoH(), t.getAgeliTipLoAgePaisCodigoH(), request);
								if (joLocaliHasta != null) {
									t.setLocalidadHasta(joLocaliHasta.getString("descripcion"));
								}
							}
							
							
							JSONObject joCliente=getCliente(t, request);
							if(joCliente.length()!=0) {
								t.setClienteNombres(joCliente.getString("nombres")+" "+joCliente.getString("apellidos"));
								t.setClienteIdentificacion(joCliente.getString("numeroIdentificacion"));
								if(joCliente.getString("telefonoCelular")!=JSONObject.NULL) {
									t.setClienteTelefono(joCliente.getString("telefonoCelular"));
								}
							}

							t.setDescripcionEmbalaje(embalajeRepository
									.findById(
											new CcoEmbalajesPK(t.getCooEmbalaCodigo(), t.getCooEmbalaAgeLicencCodigo()))
									.get().getDescripcion());
							return t;
						}).collect(Collectors.toList());
			}

			Collections.sort(paquetes, (imp1, imp2) -> imp1.getId().getCodigo().compareTo(imp2.getId().getCodigo()));

			JRDataSource jrDataSource = new JRBeanCollectionDataSource(paquetes);

			parametersMap.put("DataSource", jrDataSource);

			JasperPrint jasperPrint = JasperFillManager.fillReport(report, parametersMap, jrDataSource);
			response.setContentType("application/pdf");
			response.setHeader("Content-disposition", "attachment; filename=\"reporte.pdf\"");
			final OutputStream outputStream = response.getOutputStream();

			JasperExportManager.exportReportToPdfStream(jasperPrint, outputStream);

		} catch (JRException e) {
			e.printStackTrace();
			log.error("Hubo un problema con el archivo jrxml");
		} catch (IOException e) {
			log.error("Hubo un problema al transformar los archivos");
		} catch (Exception e) {
			log.warn("Hubo un problema con con el servicio PDF: \n" + e.toString());

		}
	}

	@ApiOperation(value = "", authorizations = { @Authorization(value = "Autorización token módulo cco") })
	@GetMapping("/getXLS")
	public @ResponseBody void getReportXls(HttpServletResponse response) {
		try {

			InputStream jasperStream = this.getClass().getResourceAsStream("/report1.jrxml");
			JasperDesign design = JRXmlLoader.load(jasperStream);
			JasperReport report = JasperCompileManager.compileReport(design);

			Map<String, Object> parametersMap = new HashMap<>();

			List<CcoPaquete> paquetes = repository.findAll().stream().map(t -> {
				if (t.getEstadoPaquete().equals(Globales.PAQUETE_ESTADO_OFICINA) || t.getEstadoPaquete() == Globales.PAQUETE_ESTADO_OFICINA) {
					t.setEstadoPaquete("En oficina");
				} else if (t.getEstadoPaquete().equals(Globales.PAQUETE_ESTADO_EN_CAMINO) || t.getEstadoPaquete() == Globales.PAQUETE_ESTADO_OFICINA) {
					t.setEstadoPaquete("En camino");
				} else if (t.getEstadoPaquete().equals(Globales.PAQUETE_ESTADO_ENTREGADO) || t.getEstadoPaquete() == Globales.PAQUETE_ESTADO_ENTREGADO) {
					t.setEstadoPaquete("Entregado");
				}else if (t.getEstadoPaquete().equals(Globales.PAQUETE_ESTADO_CENTRO_ACOPIO) || t.getEstadoPaquete() == Globales.PAQUETE_ESTADO_CENTRO_ACOPIO) {
					t.setEstadoPaquete("En centro de acopio");
				}else if (t.getEstadoPaquete().equals(Globales.PAQUETE_ESTADO_INGRESADO) || t.getEstadoPaquete() == Globales.PAQUETE_ESTADO_INGRESADO) {
					t.setEstadoPaquete("Ingresado");
				}else if (t.getEstadoPaquete().equals(Globales.PAQUETE_ESTADO_PENDIENTE_PAGO) || t.getEstadoPaquete() == Globales.PAQUETE_ESTADO_PENDIENTE_PAGO) {
					t.setEstadoPaquete("Pendiente de pago");
				}else if (t.getEstadoPaquete().equals(Globales.PAQUETE_ESTADO_SOLICITUD_RECOLECCION) || t.getEstadoPaquete() == Globales.PAQUETE_ESTADO_SOLICITUD_RECOLECCION) {
					t.setEstadoPaquete("Solicitud de recolección");
				}
				if (t.getTipoPaquete().equals("P") || t.getTipoPaquete() == "P") {
					t.setTipoPaquete("Paquete");
				} else if (t.getTipoPaquete().equals("D") || t.getTipoPaquete() == "D") {
					t.setTipoPaquete("Documento");
				}
				return t;
			}).collect(Collectors.toList());
			Collections.sort(paquetes, (imp1, imp2) -> imp1.getId().getCodigo().compareTo(imp2.getId().getCodigo()));

			JRDataSource jrDataSource = new JRBeanCollectionDataSource(paquetes);

			parametersMap.put("DataSource", jrDataSource);

			JasperPrint jasperPrint = JasperFillManager.fillReport(report, parametersMap, jrDataSource);
			response.setContentType("application/xls");
			response.setHeader("Content-disposition", "attachment; filename=\"reporte.xls\"");
			final OutputStream outputStream = response.getOutputStream();

			JRXlsExporter exporterXLS = new JRXlsExporter();
			exporterXLS.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
			exporterXLS.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, outputStream);
			exporterXLS.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.TRUE);
			exporterXLS.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
			exporterXLS.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
			exporterXLS.exportReport();

		} catch (JRException e) {
			e.printStackTrace();
			log.error("Hubo un problema con el archivo jrxml");
		} catch (IOException e) {
			log.error("Hubo un problema al transformar los archivos");
		} catch (Exception e) {
			log.warn("Hubo un problema con con el servicio XLS: \n" + e.toString());

		}
	}

	/*
	 * @ApiOperation(value = "", authorizations =
	 * {@Authorization(value="Autorización token módulo cco")})
	 * 
	 * @PostMapping(path = "/prueba",produces = {MediaType.APPLICATION_JSON_VALUE})
	 * public ResponseEntity<?> prueba(@RequestBody CcoPaquete transaccion,
	 * HttpServletRequest request) { JSONObject
	 * jo=getAgeLocalidadPorId(transaccion.getAgeLocaliCodigo(),
	 * transaccion.getAgeLocaliTipLoCodigo(),
	 * transaccion.getAgecaliTipLoAgePaisCodigo(), request); return
	 * ResponseEntity.ok(jo.toString()); }
	 */
	
	private JSONObject getCliente(CcoPaquete transaccion,HttpServletRequest request) {
		
		RestTemplate restTemplate = new RestTemplate();
				
		UriComponentsBuilder builder = UriComponentsBuilder
				.fromHttpUrl(urlCliente + "/" + transaccion.getCliClientAgeLicencCodigo())
				.queryParam("codigo", transaccion.getCliClientCodigo());

		HttpEntity<String> result = restTemplate.exchange(builder.toUriString(), HttpMethod.GET,
				asignacionHeaderHttp(request), String.class);
		
		JSONArray jsonArr = new JSONArray(result.getBody());
		if(jsonArr.length()==0) {
			return new JSONObject();
		}
		if(jsonArr.length()>1) {
			throw new DataIntegrityViolationException(
					"Existe mas de un cliente con el código de cliente "+transaccion.getUsuarioIngreso()+" y licenciatario "+ transaccion.getId().getAgeLicencCodigo());
		}
		return jsonArr.getJSONObject(0);
	}

	private JSONObject getAgeLocalidadPorId(Integer codigo, Integer ageTipLoCodigo, Integer ageTipLoAgePaisCodigo,
			HttpServletRequest request) {
		try {

			RestTemplate restTemplate = new RestTemplate();

			UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(
					ageUrlBase + "/ageLocalidades" + "/" + ageTipLoAgePaisCodigo + "/" + ageTipLoCodigo + "/" + codigo);

			HttpEntity<String> result = restTemplate.exchange(builder.toUriString(), HttpMethod.GET,
					asignacionHeaderHttp(request), String.class);

			return new JSONObject(result.getBody());
		} catch (Exception e) {
			return new JSONObject();
		}
	}

	private HttpEntity<String> asignacionHeaderHttp(HttpServletRequest request) {
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", request.getHeader("Authorization"));
		return new HttpEntity<String>("parameters", headers);
	}

}
