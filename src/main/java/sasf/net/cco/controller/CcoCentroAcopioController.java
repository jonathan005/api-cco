package sasf.net.cco.controller;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import javax.persistence.NoResultException;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import javassist.NotFoundException;
import sasf.net.cco.model.CcoCentroAcopio;
import sasf.net.cco.model.CcoCentroAcopioPK;
import sasf.net.cco.repository.CcoCentroAcopioRepository;
import sasf.net.cco.utils.EntidadCamposGenerales;
import sasf.net.cco.utils.FuncionesGenerales;
import sasf.net.cco.utils.Globales;
import sasf.net.cco.utils.ManejadorExcepciones;

@RestController
@RequestMapping("/ccoCentroAcopio")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class CcoCentroAcopioController {
	
	@Autowired
	private TransactionTemplate transactionTemplate;

	@Autowired
	private FuncionesGenerales fg;
	
	@Autowired
	private CcoCentroAcopioRepository repository;
	
	@ApiOperation(value = "", authorizations = {@Authorization(value="Autorización token módulo cco")})
	@GetMapping(path = "/{codigoLicenciatario}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public List<?> consultar(@PathVariable Integer codigoLicenciatario,
			@RequestParam(name = "codigo", required = false) Long codigo,
			@RequestParam(name = "nombres", required = false) String nombres,
			@RequestParam(name = "apellidos", required = false) String apellidos
			) {
		return repository.buscarPorParametros(codigoLicenciatario, codigo, 
				(nombres== null )? null:nombres.toUpperCase(),
				(apellidos ==null)? null:apellidos.toUpperCase(),
				Globales.ESTADO_ANULADO);
	}
	
	@ApiOperation(value = "", authorizations = {@Authorization(value="Autorización token módulo cco")})
	@GetMapping(path = "/{codigoLicenciatario}/{codigo}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<?> consultarPorId(@PathVariable Integer codigoLicenciatario, @PathVariable Long codigo)
			throws NotFoundException {

		Optional<CcoCentroAcopio> centroAcopio = repository.findById(new CcoCentroAcopioPK(codigo, codigoLicenciatario));
		if (!centroAcopio.isPresent() || centroAcopio.get().getEstado().equals(Globales.ESTADO_ANULADO)) {
			throw new NoResultException("Centro de acopio con código " + codigo + " no existe");
		}
		return new ResponseEntity<CcoCentroAcopio>(centroAcopio.get(), HttpStatus.OK);
	}
	
	@ApiOperation(value = "", authorizations = {@Authorization(value="Autorización token módulo cco")})
	@PostMapping
	public ResponseEntity<?> ingresar(@RequestBody CcoCentroAcopio transaccion,
			HttpServletRequest request) {

		fg.validar(transaccion, CcoCentroAcopio.CentroAcopioCreation.class);
		fg.validar(transaccion, EntidadCamposGenerales.CamposGeneralesCreation.class);
		transaccion.setUbicacionIngreso(request.getRemoteAddr());

		transactionTemplate.execute(new TransactionCallbackWithoutResult() {
			@Override
			public void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
				Long id = fg.obtenerSecuencia(transaccion.getId().getAgeLicencCodigo(), Globales.CODIGO_APLICACION,
						Globales.CODIGO_SECUENCIA_CENTRO_ACOPIO);
				if (id == 0) {
					throw new DataIntegrityViolationException("Secuencia " + Globales.CODIGO_SECUENCIA_CENTRO_ACOPIO
							+ " para la aplicación " + Globales.CODIGO_APLICACION + " no existe");
				}
				transaccion.getId().setCodigo(id);
				if(repository.existsById(transaccion.getId())){
					throw new DataIntegrityViolationException(
							"Centro acopio con código " + id + " ya existe");
				}
				repository.save(transaccion);
			}
		});

		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{codigoLicenciatario}/{id}")
				.buildAndExpand(transaccion.getId().getAgeLicencCodigo(), transaccion.getId().getCodigo()).toUri();
		return ResponseEntity.created(location).build();
	}
	
	@ApiOperation(value = "", authorizations = {@Authorization(value="Autorización token módulo cco")})
	@PostMapping("/varios")
	public ResponseEntity<?> ingresarVarios(@RequestBody List<CcoCentroAcopio> transacciones, 
			HttpServletRequest request) {

		transactionTemplate.execute(new TransactionCallbackWithoutResult() {
			@Override
			public void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
				for (CcoCentroAcopio transaccion : transacciones) {
					fg.validar(transaccion, CcoCentroAcopio.CentroAcopioCreation.class);
					fg.validar(transaccion, EntidadCamposGenerales.CamposGeneralesCreation.class);
					transaccion.setUbicacionIngreso(request.getRemoteAddr());
					Long id = fg.obtenerSecuencia(transaccion.getId().getAgeLicencCodigo(), Globales.CODIGO_APLICACION,
							Globales.CODIGO_SECUENCIA_CENTRO_ACOPIO);
					if (id == 0) {
						throw new DataIntegrityViolationException("Secuencia " + Globales.CODIGO_SECUENCIA_CENTRO_ACOPIO
								+ " para la aplicación " + Globales.CODIGO_APLICACION + " no existe");
					}
					transaccion.getId().setCodigo(id);
					if(repository.existsById(transaccion.getId())){
						throw new DataIntegrityViolationException(
								"Centro acopio con código " + id + " ya existe");
					}
					repository.save(transaccion);
				}
			}
		});

		return new ResponseEntity<List<CcoCentroAcopio>>(transacciones, HttpStatus.CREATED);
	}
	
	@ApiOperation(value = "", authorizations = {@Authorization(value="Autorización token módulo cco")})
	@PutMapping
	public ResponseEntity<?> actualizar(@RequestBody CcoCentroAcopio transaccion, HttpServletRequest request) {
		fg.validar(transaccion, CcoCentroAcopio.CentroAcopioUpdate.class);
		fg.validar(transaccion, EntidadCamposGenerales.CamposGeneralesUpdate.class);
		transaccion.setUbicacionModificacion(request.getRemoteAddr());
		Optional<CcoCentroAcopio> centroAcopio=repository.findById(transaccion.getId());
		
		if (!repository.existsById(
				new CcoCentroAcopioPK(transaccion.getId().getCodigo(), transaccion.getId().getAgeLicencCodigo()))) {
			return ManejadorExcepciones.error(HttpStatus.NOT_FOUND, null,
					"Centro acopio con código " + transaccion.getId().getCodigo() + " no encontrado");
		}
		
		if(transaccion.getNombres()==null) {
			transaccion.setNombres(centroAcopio.get().getNombres());
		}
		if(transaccion.getApellidos()==null) {
			transaccion.setApellidos(centroAcopio.get().getApellidos());
		}
		if(transaccion.getNumeroCelular()==null) {
			transaccion.setNumeroCelular(centroAcopio.get().getNumeroCelular());
		}
		if(transaccion.getCorreoElectronico()==null) {
			transaccion.setCorreoElectronico(centroAcopio.get().getCorreoElectronico());
		}
		if(transaccion.getDireccion()==null) {
			transaccion.setDireccion(centroAcopio.get().getDireccion());
		}
		if(transaccion.getLatitud()==null) {
			transaccion.setLatitud(centroAcopio.get().getLatitud());
		}
		if(transaccion.getLongitud()==null) {
			transaccion.setLongitud(centroAcopio.get().getLongitud());
		}
		
		repository.save(transaccion);

		return new ResponseEntity<String>("", HttpStatus.OK);
	}
	
	@ApiOperation(value = "", authorizations = {@Authorization(value="Autorización token módulo cco")})
	@PutMapping("/varios")
	public ResponseEntity<?> actualizarVarios(@RequestBody List<CcoCentroAcopio> transacciones,
			HttpServletRequest request) {
		transactionTemplate.execute(new TransactionCallbackWithoutResult() {
			@Override
			public void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
				for (CcoCentroAcopio transaccion : transacciones) {
					fg.validar(transaccion, CcoCentroAcopio.CentroAcopioUpdate.class);
					fg.validar(transaccion, EntidadCamposGenerales.CamposGeneralesUpdate.class);
					Optional<CcoCentroAcopio> centroAcopio=repository.findById(transaccion.getId());
					
					if (!repository.existsById(transaccion.getId())) {
						throw new NoResultException("Embalaje con código " + transaccion.getId().getCodigo() + " no encontrado");
					} 
					transaccion.setUbicacionModificacion(request.getRemoteAddr());
					
					if(transaccion.getNombres()==null) {
						transaccion.setNombres(centroAcopio.get().getNombres());
					}
					if(transaccion.getApellidos()==null) {
						transaccion.setApellidos(centroAcopio.get().getApellidos());
					}
					if(transaccion.getNumeroCelular()==null) {
						transaccion.setNumeroCelular(centroAcopio.get().getNumeroCelular());
					}
					if(transaccion.getCorreoElectronico()==null) {
						transaccion.setCorreoElectronico(centroAcopio.get().getCorreoElectronico());
					}
					if(transaccion.getDireccion()==null) {
						transaccion.setDireccion(centroAcopio.get().getDireccion());
					}
					if(transaccion.getLatitud()==null) {
						transaccion.setLatitud(centroAcopio.get().getLatitud());
					}
					if(transaccion.getLongitud()==null) {
						transaccion.setLongitud(centroAcopio.get().getLongitud());
					}
					
					repository.save(transaccion);
				}
			}
		});
		return new ResponseEntity<List<CcoCentroAcopio>>(transacciones, HttpStatus.CREATED);
	}
	
}
