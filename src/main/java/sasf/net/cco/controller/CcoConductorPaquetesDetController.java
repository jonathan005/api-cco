package sasf.net.cco.controller;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import javax.persistence.NoResultException;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import javassist.NotFoundException;
import sasf.net.cco.model.CcoConductorPaquetesCab;
import sasf.net.cco.model.CcoConductorPaquetesCabPK;
import sasf.net.cco.model.CcoConductorPaquetesDet;
import sasf.net.cco.model.CcoConductorPaquetesDetPK;
import sasf.net.cco.model.CcoPaquete;
import sasf.net.cco.model.CcoPaquetePK;
import sasf.net.cco.repository.CcoConductorPaquetesCabRepository;
import sasf.net.cco.repository.CcoConductorPaquetesDetRepository;
import sasf.net.cco.repository.CcoPaqueteRespository;
import sasf.net.cco.utils.EntidadCamposGenerales;
import sasf.net.cco.utils.FuncionesGenerales;
import sasf.net.cco.utils.Globales;
import sasf.net.cco.utils.ManejadorExcepciones;

@RestController
@RequestMapping("/ccoConductorPaquetesDet")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class CcoConductorPaquetesDetController {
	@Autowired
	private TransactionTemplate transactionTemplate;
	
	@Autowired
	private FuncionesGenerales fg;

	@Autowired
	private CcoConductorPaquetesDetRepository repository;
	
	@Autowired
	private CcoConductorPaquetesCabRepository ccoConductorPaqueteCabRepository;
	
	@Autowired
	private CcoPaqueteRespository ccoPaqueteRespository;
	
	@ApiOperation(value = "", authorizations = {@Authorization(value="Autorización token módulo cco")})
	@GetMapping(path = "/{codigoLicenciatario}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public List<?> consultar(@PathVariable Integer ccoCpCapAgeLicencCodigo,
			@RequestParam(name = "codigo", required = false) Long codigo,
			@RequestParam(name = "ccoCpCapCodigo", required = false) Long ccoCpCapCodigo,
			@RequestParam(name = "ccoPaquetCodigo", required = false) Long ccoPaquetCodigo,
			@RequestParam(name = "descripcion", required = false) String descripcion
			) {
		return repository.buscarPorParametros(ccoCpCapAgeLicencCodigo, codigo, ccoCpCapCodigo, ccoPaquetCodigo, Globales.ESTADO_ANULADO);
	}
	
	@ApiOperation(value = "", authorizations = {@Authorization(value="Autorización token módulo cco")})
	@GetMapping(path = "/{ccoCpCapAgeLicencCodigo}/{ccoCpCapCodigo}/{codigo}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<?> consultarPorId(@PathVariable Integer ccoCpCapAgeLicencCodigo,@PathVariable Long ccoCpCapCodigo, @PathVariable Long codigo)
			throws NotFoundException {

		Optional<CcoConductorPaquetesDet> condPaqDet = repository.findById(new CcoConductorPaquetesDetPK(codigo, ccoCpCapCodigo, ccoCpCapAgeLicencCodigo));
		if (!condPaqDet.isPresent() || condPaqDet.get().getEstado().equals(Globales.ESTADO_ANULADO)) {
			throw new NoResultException("Conductor paquete detalle con código " + codigo + " no existe");
		}
		return new ResponseEntity<CcoConductorPaquetesDet>(condPaqDet.get(), HttpStatus.OK);
	}
	
	@ApiOperation(value = "", authorizations = {@Authorization(value="Autorización token módulo cco")})
	@PostMapping
	public ResponseEntity<?> ingresar(@RequestBody CcoConductorPaquetesDet transaccion,
			HttpServletRequest request) {

		fg.validar(transaccion, CcoConductorPaquetesDet.ConductorPaquetesDetCreation.class);
		fg.validar(transaccion, EntidadCamposGenerales.CamposGeneralesCreation.class);
		transaccion.setUbicacionIngreso(request.getRemoteAddr());
		
		Optional<CcoConductorPaquetesCab> condPaqCab= ccoConductorPaqueteCabRepository.findById(new CcoConductorPaquetesCabPK(transaccion.getId().getCcoCpCapCodigo(), transaccion.getId().getCcoCpCapAgeLicencCodigo()));
		if(!condPaqCab.isPresent() || condPaqCab.get().getEstado().contentEquals(Globales.ESTADO_ANULADO)) {
			return ManejadorExcepciones.error(HttpStatus.NOT_FOUND, null, 
					"Conductor paquete cabecera con código "+ transaccion.getId().getCcoCpCapCodigo()+" y licenciatario "+transaccion.getId().getCcoCpCapAgeLicencCodigo()+" no existe");
		}
		
		Optional<CcoPaquete> paquete= ccoPaqueteRespository.findById(new CcoPaquetePK(transaccion.getCcoPaquetCodigo(), transaccion.getCcoPaquetAgeLicencCodigo()));
		if(!paquete.isPresent() || paquete.get().getEstado().contentEquals(Globales.ESTADO_ANULADO)) {
			return ManejadorExcepciones.error(HttpStatus.NOT_FOUND, null, 
					"Paquete con código "+ transaccion.getCcoPaquetCodigo()+" y licenciatario "+transaccion.getCcoPaquetAgeLicencCodigo()+" no existe");
		}
		
		
		transactionTemplate.execute(new TransactionCallbackWithoutResult() {
			@Override
			public void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
				Long id = fg.obtenerSecuencia(transaccion.getId().getCcoCpCapAgeLicencCodigo(), Globales.CODIGO_APLICACION,
						Globales.CODIGO_SECUENCIA_CONDUCTOR_PAQUETE_DETALLE);
				if (id == 0) {
					throw new DataIntegrityViolationException("Secuencia " + Globales.CODIGO_SECUENCIA_CONDUCTOR_PAQUETE_DETALLE
							+ " para la aplicación " + Globales.CODIGO_APLICACION + " no existe");
				}
				transaccion.getId().setCodigo(id);
				if(repository.existsById(transaccion.getId())){
					throw new DataIntegrityViolationException(
							"Embalaje con código " + id + " ya existe");
				}
				repository.save(transaccion);
			}
		});

		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{ccoCpCapAgeLicencCodigo}/{ccoCpCapCodigo}/{codigo}")
				.buildAndExpand(transaccion.getId().getCcoCpCapAgeLicencCodigo(),transaccion.getId().getCcoCpCapCodigo(),transaccion.getId().getCodigo()).toUri();
		return ResponseEntity.created(location).build();
	}
	
	@ApiOperation(value = "", authorizations = {@Authorization(value="Autorización token módulo cco")})
	@PostMapping("/varios")
	public ResponseEntity<?> ingresarVarios(@RequestBody List<CcoConductorPaquetesDet> transacciones, 
			HttpServletRequest request) {

		transactionTemplate.execute(new TransactionCallbackWithoutResult() {
			@Override
			public void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
				for (CcoConductorPaquetesDet transaccion : transacciones) {
					fg.validar(transaccion, CcoConductorPaquetesDet.ConductorPaquetesDetCreation.class);
					fg.validar(transaccion, EntidadCamposGenerales.CamposGeneralesCreation.class);
					transaccion.setUbicacionIngreso(request.getRemoteAddr());
					
					Optional<CcoConductorPaquetesCab> condPaqCab= ccoConductorPaqueteCabRepository.findById(new CcoConductorPaquetesCabPK(transaccion.getId().getCcoCpCapCodigo(), transaccion.getId().getCcoCpCapAgeLicencCodigo()));
					if(!condPaqCab.isPresent() || condPaqCab.get().getEstado().contentEquals(Globales.ESTADO_ANULADO)) {
						throw new DataIntegrityViolationException( 
								"Conductor paquete cabecera con código "+ transaccion.getId().getCcoCpCapCodigo()+" y licenciatario "+transaccion.getId().getCcoCpCapAgeLicencCodigo()+" no existe");
					}
					
					Optional<CcoPaquete> paquete= ccoPaqueteRespository.findById(new CcoPaquetePK(transaccion.getCcoPaquetCodigo(), transaccion.getCcoPaquetAgeLicencCodigo()));
					if(!paquete.isPresent() || paquete.get().getEstado().contentEquals(Globales.ESTADO_ANULADO)) {
						throw new DataIntegrityViolationException( 
								"Paquete con código "+ transaccion.getCcoPaquetCodigo()+" y licenciatario "+transaccion.getCcoPaquetAgeLicencCodigo()+" no existe");
					}
					
					Long id = fg.obtenerSecuencia(transaccion.getId().getCcoCpCapAgeLicencCodigo(), Globales.CODIGO_APLICACION,
							Globales.CODIGO_SECUENCIA_CONDUCTOR_PAQUETE_DETALLE);
					if (id == 0) {
						throw new DataIntegrityViolationException("Secuencia " + Globales.CODIGO_SECUENCIA_CONDUCTOR_PAQUETE_DETALLE
								+ " para la aplicación " + Globales.CODIGO_APLICACION + " no existe");
					}
					transaccion.getId().setCodigo(id);
					
					if(repository.existsById(transaccion.getId())){
						throw new DataIntegrityViolationException(
								"Conductor paquete detalle con código " + id + " ya existe");
					}
					repository.save(transaccion);
				}
			}
		});

		return new ResponseEntity<List<CcoConductorPaquetesDet>>(transacciones, HttpStatus.CREATED);
	}
	
	@ApiOperation(value = "", authorizations = {@Authorization(value="Autorización token módulo cco")})
	@PutMapping
	public ResponseEntity<?> actualizar(@RequestBody CcoConductorPaquetesDet transaccion, HttpServletRequest request) {
		fg.validar(transaccion, CcoConductorPaquetesDet.ConductorPaquetesDetUpdate.class);
		fg.validar(transaccion, EntidadCamposGenerales.CamposGeneralesUpdate.class);
		transaccion.setUbicacionModificacion(request.getRemoteAddr());
		Optional<CcoConductorPaquetesDet> condPaqDet=repository.findById(transaccion.getId());
		
		if (!repository.existsById(transaccion.getId())){
			return ManejadorExcepciones.error(HttpStatus.NOT_FOUND, null,
					"Conductor paquete detalle con código " + transaccion.getId().getCodigo() + " no encontrado");
		}
		
		if(transaccion.getCcoPaquetCodigo()!=null || transaccion.getCcoPaquetCodigo()<=0) {
			transaccion.setCcoPaquetCodigo(condPaqDet.get().getCcoPaquetCodigo());
		}
		if(transaccion.getCcoPaquetAgeLicencCodigo()!=null || transaccion.getCcoPaquetAgeLicencCodigo()<=0) {
			transaccion.setCcoPaquetAgeLicencCodigo(condPaqDet.get().getCcoPaquetAgeLicencCodigo());
		}
		
		Optional<CcoPaquete> paquete= ccoPaqueteRespository.findById(new CcoPaquetePK(transaccion.getCcoPaquetCodigo(), transaccion.getCcoPaquetAgeLicencCodigo()));
		if(!paquete.isPresent() || paquete.get().getEstado().contentEquals(Globales.ESTADO_ANULADO)) {
			throw new DataIntegrityViolationException( 
					"Paquete con código "+ transaccion.getCcoPaquetCodigo()+" y licenciatario "+transaccion.getCcoPaquetAgeLicencCodigo()+" no existe");
		}
		
		if(transaccion.getValor()!=null || transaccion.getValor().floatValue()<=0) {
			transaccion.setValor(condPaqDet.get().getValor());
		}
		
		
		repository.save(transaccion);

		return new ResponseEntity<String>("", HttpStatus.OK);
	}
	
	@ApiOperation(value = "", authorizations = {@Authorization(value="Autorización token módulo cco")})
	@PutMapping("/varios")
	public ResponseEntity<?> actualizarVarios(@RequestBody List<CcoConductorPaquetesDet> transacciones,
			HttpServletRequest request) {
		transactionTemplate.execute(new TransactionCallbackWithoutResult() {
			@Override
			public void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
				for (CcoConductorPaquetesDet transaccion : transacciones) {
					fg.validar(transaccion, CcoConductorPaquetesDet.ConductorPaquetesDetUpdate.class);
					fg.validar(transaccion, EntidadCamposGenerales.CamposGeneralesUpdate.class);
					Optional<CcoConductorPaquetesDet> condPaqDet=repository.findById(transaccion.getId());
					
					if (!repository.existsById(transaccion.getId())) {
						throw new NoResultException("Conductor paquete detalle con código " + transaccion.getId().getCodigo() + " no encontrado");
					} 
					transaccion.setUbicacionModificacion(request.getRemoteAddr());
					
					if(transaccion.getCcoPaquetCodigo()!=null || transaccion.getCcoPaquetCodigo()<=0) {
						transaccion.setCcoPaquetCodigo(condPaqDet.get().getCcoPaquetCodigo());
					}
					if(transaccion.getCcoPaquetAgeLicencCodigo()!=null || transaccion.getCcoPaquetAgeLicencCodigo()<=0) {
						transaccion.setCcoPaquetAgeLicencCodigo(condPaqDet.get().getCcoPaquetAgeLicencCodigo());
					}
					
					Optional<CcoPaquete> paquete= ccoPaqueteRespository.findById(new CcoPaquetePK(transaccion.getCcoPaquetCodigo(), transaccion.getCcoPaquetAgeLicencCodigo()));
					if(!paquete.isPresent() || paquete.get().getEstado().contentEquals(Globales.ESTADO_ANULADO)) {
						throw new DataIntegrityViolationException( 
								"Paquete con código "+ transaccion.getCcoPaquetCodigo()+" y licenciatario "+transaccion.getCcoPaquetAgeLicencCodigo()+" no existe");
					}
					
					if(transaccion.getValor()!=null || transaccion.getValor().floatValue()<=0) {
						transaccion.setValor(condPaqDet.get().getValor());
					}
					
					repository.save(transaccion);
				}
			}
		});
		return new ResponseEntity<List<CcoConductorPaquetesDet>>(transacciones, HttpStatus.CREATED);
	}
	
}
