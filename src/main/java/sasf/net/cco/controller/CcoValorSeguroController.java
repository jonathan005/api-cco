package sasf.net.cco.controller;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import javax.persistence.NoResultException;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import javassist.NotFoundException;
import sasf.net.cco.model.CcoValorSeguro;
import sasf.net.cco.model.CcoValorSeguroPK;
import sasf.net.cco.repository.CcoValorSeguroRepository;
import sasf.net.cco.utils.EntidadCamposGenerales;
import sasf.net.cco.utils.FuncionesGenerales;
import sasf.net.cco.utils.Globales;
import sasf.net.cco.utils.ManejadorExcepciones;

@RestController
@RequestMapping("/ccoValorSeguro")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class CcoValorSeguroController {
	
	@Autowired
	private TransactionTemplate transactionTemplate;
	
	@Autowired
	private FuncionesGenerales fg;
	
	@Autowired
	private CcoValorSeguroRepository repository;
	
	@ApiOperation(value = "", authorizations = {@Authorization(value="Autorización token módulo cco")})
	@GetMapping(path = "/{codigoLicenciatario}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public List<?> consultar(@PathVariable Integer codigoLicenciatario,
			@RequestParam(name = "codigo", required = false) Long codigo
			) {
		return repository.buscarPorParametros(codigoLicenciatario, codigo, Globales.ESTADO_ANULADO);
	}
	
	@ApiOperation(value = "", authorizations = {@Authorization(value="Autorización token módulo cco")})
	@GetMapping(path = "/{codigoLicenciatario}/{codigo}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<?> consultarPorId(@PathVariable Integer codigoLicenciatario, @PathVariable Long codigo)
			throws NotFoundException {

		Optional<CcoValorSeguro> valorSeguro = repository.findById(new CcoValorSeguroPK(codigo, codigoLicenciatario));
		if (!valorSeguro.isPresent() && valorSeguro.get().getEstado().equals(Globales.ESTADO_ANULADO)) {
			throw new NoResultException("Valor seguro con código " + codigo + " no existe");
		}
		return new ResponseEntity<CcoValorSeguro>(valorSeguro.get(), HttpStatus.OK);
	}
	
	@ApiOperation(value = "", authorizations = {@Authorization(value="Autorización token módulo cco")})
	@PostMapping
	public ResponseEntity<?> ingresar(@RequestBody CcoValorSeguro transaccion,
			HttpServletRequest request) {

		fg.validar(transaccion, CcoValorSeguro.ValorSeguroCreation.class);
		fg.validar(transaccion, EntidadCamposGenerales.CamposGeneralesCreation.class);
		transaccion.setUbicacionIngreso(request.getRemoteAddr());
		
		if(transaccion.getPesoHasta().floatValue()<transaccion.getPesoDesde().floatValue()) {
			throw new DataIntegrityViolationException(
					"Peso hasta no debe ser menor que peso desde");
		}
		
		transactionTemplate.execute(new TransactionCallbackWithoutResult() {
			@Override
			public void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
				Long id = fg.obtenerSecuencia(transaccion.getId().getAgeLicencCodigo(), Globales.CODIGO_APLICACION,
						Globales.CODIGO_SECUENCIA_VALOR_SEGURO);
				if (id == 0) {
					throw new DataIntegrityViolationException("Secuencia " + Globales.CODIGO_SECUENCIA_VALOR_SEGURO
							+ " para la aplicación " + Globales.CODIGO_APLICACION + " no existe");
				}
				transaccion.getId().setCodigo(id);
				if(repository.existsById(transaccion.getId())){
					throw new DataIntegrityViolationException(
							"Valor seguro con código " + id + " ya existe");
				}
				repository.save(transaccion);
			}
		});

		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{codigoLicenciatario}/{id}")
				.buildAndExpand(transaccion.getId().getAgeLicencCodigo(), transaccion.getId().getCodigo()).toUri();
		return ResponseEntity.created(location).build();
	}
	
	@ApiOperation(value = "", authorizations = {@Authorization(value="Autorización token módulo cco")})
	@PostMapping("/varios")
	public ResponseEntity<?> ingresarVarios(@RequestBody List<CcoValorSeguro> transacciones, 
			HttpServletRequest request) {

		transactionTemplate.execute(new TransactionCallbackWithoutResult() {
			@Override
			public void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
				for (CcoValorSeguro transaccion : transacciones) {
					fg.validar(transaccion, CcoValorSeguro.ValorSeguroCreation.class);
					fg.validar(transaccion, EntidadCamposGenerales.CamposGeneralesCreation.class);
					transaccion.setUbicacionIngreso(request.getRemoteAddr());
					
					if(transaccion.getPesoHasta().floatValue()<transaccion.getPesoDesde().floatValue()) {
						throw new DataIntegrityViolationException(
								"Peso hasta no debe ser menor que peso desde");
					}
					
					Long id = fg.obtenerSecuencia(transaccion.getId().getAgeLicencCodigo(), Globales.CODIGO_APLICACION,
							Globales.CODIGO_SECUENCIA_VALOR_SEGURO);
					if (id == 0) {
						throw new DataIntegrityViolationException("Secuencia " + Globales.CODIGO_SECUENCIA_VALOR_SEGURO
								+ " para la aplicación " + Globales.CODIGO_APLICACION + " no existe");
					}
					transaccion.getId().setCodigo(id);
					if(repository.existsById(transaccion.getId())){
						throw new DataIntegrityViolationException(
								"Valor seguro con código " + id + " ya existe");
					}
					repository.save(transaccion);
				}
			}
		});

		return new ResponseEntity<List<CcoValorSeguro>>(transacciones, HttpStatus.CREATED);
	}
	
	@ApiOperation(value = "", authorizations = {@Authorization(value="Autorización token módulo cco")})
	@PutMapping
	public ResponseEntity<?> actualizar(@RequestBody CcoValorSeguro transaccion, HttpServletRequest request) {
		fg.validar(transaccion, CcoValorSeguro.ValorSeguroUpdate.class);
		fg.validar(transaccion, EntidadCamposGenerales.CamposGeneralesUpdate.class);
		transaccion.setUbicacionModificacion(request.getRemoteAddr());
		Optional<CcoValorSeguro> valorSeguro=repository.findById(transaccion.getId());
		
		if (!repository.existsById(
				new CcoValorSeguroPK(transaccion.getId().getCodigo(), transaccion.getId().getAgeLicencCodigo()))) {
			return ManejadorExcepciones.error(HttpStatus.NOT_FOUND, null,
					"Valor seguro con código " + transaccion.getId().getCodigo() + " no encontrado");
		}
		
		if(transaccion.getPesoDesde()==null || transaccion.getPesoDesde().floatValue()<0) {
			transaccion.setPesoDesde(valorSeguro.get().getPesoDesde());
		}
		if(transaccion.getPesoHasta()==null || transaccion.getPesoHasta().floatValue()<0) {
			transaccion.setPesoHasta(valorSeguro.get().getPesoHasta());
		}
		
		if(transaccion.getPesoHasta().floatValue()<transaccion.getPesoDesde().floatValue()) {
			throw new DataIntegrityViolationException(
					"Peso hasta no debe ser menor que peso desde");
		}
		
		if(transaccion.getValor()==null || transaccion.getValor().floatValue()<0) {
			transaccion.setValor(valorSeguro.get().getValor());
		}
		
		repository.save(transaccion);

		return new ResponseEntity<String>("", HttpStatus.OK);
	}
	
	@ApiOperation(value = "", authorizations = {@Authorization(value="Autorización token módulo cco")})
	@PutMapping("/varios")
	public ResponseEntity<?> actualizarVarios(@RequestBody List<CcoValorSeguro> transacciones,
			HttpServletRequest request) {
		transactionTemplate.execute(new TransactionCallbackWithoutResult() {
			@Override
			public void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
				for (CcoValorSeguro transaccion : transacciones) {
					fg.validar(transaccion, CcoValorSeguro.ValorSeguroUpdate.class);
					fg.validar(transaccion, EntidadCamposGenerales.CamposGeneralesUpdate.class);
					Optional<CcoValorSeguro> valorSeguro=repository.findById(transaccion.getId());
					
					if (!repository.existsById(transaccion.getId())) {
						throw new NoResultException("Valor seguro con código " + transaccion.getId().getCodigo() + " no encontrado");
					} 
					transaccion.setUbicacionModificacion(request.getRemoteAddr());
					
					if(transaccion.getPesoDesde()==null || transaccion.getPesoDesde().floatValue()<0) {
						transaccion.setPesoDesde(valorSeguro.get().getPesoDesde());
					}
					if(transaccion.getPesoHasta()==null || transaccion.getPesoHasta().floatValue()<0) {
						transaccion.setPesoHasta(valorSeguro.get().getPesoHasta());
					}
					if(transaccion.getPesoHasta().floatValue()<transaccion.getPesoDesde().floatValue()) {
						throw new DataIntegrityViolationException(
								"Peso hasta no debe ser menor que peso desde");
					}
					
					if(transaccion.getValor()==null || transaccion.getValor().floatValue()<0) {
						transaccion.setValor(valorSeguro.get().getValor());
					}
					repository.save(transaccion);
				}
			}
		});
		return new ResponseEntity<List<CcoValorSeguro>>(transacciones, HttpStatus.CREATED);
	}
	
}
