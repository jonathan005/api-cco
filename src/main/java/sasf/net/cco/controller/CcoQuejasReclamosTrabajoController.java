package sasf.net.cco.controller;

import java.io.IOException;
import java.net.URI;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.NoResultException;
import javax.servlet.http.HttpServletRequest;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.web.util.UriComponentsBuilder;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import javassist.NotFoundException;
import sasf.net.cco.model.CcoPaquete;
import sasf.net.cco.model.CcoPaquetePK;
import sasf.net.cco.model.CcoQuejasReclamosTrabajo;
import sasf.net.cco.model.CcoQuejasReclamosTrabajoPK;
import sasf.net.cco.repository.CcoPaqueteRespository;
import sasf.net.cco.repository.CcoQuejasReclamosTrabajoRepository;
import sasf.net.cco.service.AmazonClient;
import sasf.net.cco.utils.EntidadCamposGenerales;
import sasf.net.cco.utils.FuncionesGenerales;
import sasf.net.cco.utils.Globales;
import sasf.net.cco.utils.ManejadorExcepciones;

@RestController
@RequestMapping("/ccoQuejasReclamosTrabajo")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class CcoQuejasReclamosTrabajoController {

	@Autowired
	private TransactionTemplate transactionTemplate;

	@Autowired
	private FuncionesGenerales fg;

	@Autowired
	private CcoQuejasReclamosTrabajoRepository repository;

	@Autowired
	private CcoPaqueteRespository paqueteRepository;

	private AmazonClient amazonClient;

	@Autowired
	CcoQuejasReclamosTrabajoController(AmazonClient amazonClient) {
		this.amazonClient = amazonClient;
	}

	@Value("${cli.urlCliente}")
	private String urlCliente;

	// @Autowired
	// private GoogleDriveService driveService;

	private static final Logger logger = LoggerFactory.getLogger(CcoQuejasReclamosTrabajoController.class);

	@ApiOperation(value = "", authorizations = { @Authorization(value = "Autorización token módulo cco") })
	@GetMapping(path = "/{codigoLicenciatario}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public List<?> consultar(@PathVariable Integer codigoLicenciatario,
			@RequestParam(name = "codigo", required = false) Long codigo,
			@RequestParam(name = "descripcion", required = false) String descripcion,
			@RequestParam(name = "tipo", required = false) String tipo,
			@RequestParam(name = "codigo", required = false) Long codigoCliente,
			HttpServletRequest request
			) {
		return repository.buscarPorParametros(codigoLicenciatario, codigo, descripcion, Globales.ESTADO_ANULADO, tipo,codigoCliente).stream().map(t -> {
			if(t.getCliClientCodigo()!=null && t.getCliClientAgeLicencCodigo()!=null){
			CcoQuejasReclamosTrabajo queja=repository.findById(new CcoQuejasReclamosTrabajoPK(t.getId().getCodigo(), t.getId().getAgeLicencCodigo())).get();			
			JSONObject jo=getCliente(queja, request);
			if(jo!=null) {
				t.setNombresCliente(jo.getString("nombres"));
				t.setApellidosCliente(jo.getString("apellidos"));
				t.setTelefonoCelularCliente(jo.getString("telefonoCelular"));
				t.setCorreoElectronicoCliente(jo.getString("correoElectronico"));
			}
			}
			return t;
		}).collect(Collectors.toList());
		
	}

	@ApiOperation(value = "", authorizations = { @Authorization(value = "Autorización token módulo cco") })
	@GetMapping(path = "/{codigoLicenciatario}/{codigo}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<?> consultarPorId(@PathVariable Integer codigoLicenciatario, @PathVariable Long codigo,
			HttpServletRequest request)
			throws NotFoundException {

		Optional<CcoQuejasReclamosTrabajo> quejaReclamoTrabajo = repository
				.findById(new CcoQuejasReclamosTrabajoPK(codigo, codigoLicenciatario));
		if (!quejaReclamoTrabajo.isPresent() && quejaReclamoTrabajo.get().getEstado().equals(Globales.ESTADO_ANULADO)) {
			throw new NoResultException("Queja, reclamo o trabajo con código " + codigo + " y licenciatario "
					+ codigoLicenciatario + " no existe");
		}
		if(quejaReclamoTrabajo.get().getCliClientCodigo()!=null && quejaReclamoTrabajo.get().getCliClientAgeLicencCodigo()!=null) {
		JSONObject jo=getCliente(quejaReclamoTrabajo.get(), request);
		quejaReclamoTrabajo.get().setNombresCliente(jo.getString("nombres"));
		quejaReclamoTrabajo.get().setApellidosCliente(jo.getString("apellidos"));
		quejaReclamoTrabajo.get().setTelefonoCelularCliente(jo.getString("telefonoCelular"));
		quejaReclamoTrabajo.get().setCorreoElectronicoCliente(jo.getString("correoElectronico"));
		}
		
		return new ResponseEntity<CcoQuejasReclamosTrabajo>(quejaReclamoTrabajo.get(), HttpStatus.OK);
	}

	@ApiOperation(value = "", authorizations = { @Authorization(value = "Autorización token módulo cco") })
	@PostMapping
	public ResponseEntity<?> ingresar(@RequestBody CcoQuejasReclamosTrabajo transaccion, HttpServletRequest request) {

		fg.validar(transaccion, CcoQuejasReclamosTrabajo.QuejaReclamoTrabajoCreation.class);
		fg.validar(transaccion, EntidadCamposGenerales.CamposGeneralesCreation.class);
		transaccion.setUbicacionIngreso(request.getRemoteAddr());

		if (transaccion.getCcoPaqueteCodigo() != null && transaccion.getCcoPaqueteAgeLicencCodigo() != null) {
			Optional<CcoPaquete> paquete = paqueteRepository.findById(
					new CcoPaquetePK(transaccion.getCcoPaqueteCodigo(), transaccion.getCcoPaqueteAgeLicencCodigo()));
			if (!paquete.isPresent() || paquete.get().getEstado().equals(Globales.ESTADO_ANULADO)) {
				return ManejadorExcepciones.error(HttpStatus.BAD_REQUEST, null,
						"Paquete con código " + transaccion.getCcoPaqueteCodigo() + " y licenciatario "
								+ transaccion.getCcoPaqueteAgeLicencCodigo() + " no existe");
			}
		} else {
			if ((transaccion.getCcoPaqueteCodigo() != null && transaccion.getCcoPaqueteAgeLicencCodigo() == null)
					|| (transaccion.getCcoPaqueteCodigo() == null
							&& transaccion.getCcoPaqueteAgeLicencCodigo() != null)) {
				return ManejadorExcepciones.error(HttpStatus.BAD_REQUEST, null,
						"Todos los campos de identificación de paquete deben ser llenados");
			}
		}

		transactionTemplate.execute(new TransactionCallbackWithoutResult() {
			@Override
			public void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
				Long id = fg.obtenerSecuencia(transaccion.getId().getAgeLicencCodigo(), Globales.CODIGO_APLICACION,
						Globales.CODIGO_SECUENCIA_QUEJA_RECLAMO_TRABAJO);
				if (id == 0) {
					throw new DataIntegrityViolationException(
							"Secuencia " + Globales.CODIGO_SECUENCIA_QUEJA_RECLAMO_TRABAJO + " para la aplicación "
									+ Globales.CODIGO_APLICACION + " no existe");
				}
				transaccion.getId().setCodigo(id);
				if (repository.existsById(transaccion.getId())) {
					throw new DataIntegrityViolationException("Queja, reclamo o trabajo con código " + id
							+ " y licenciatario " + transaccion.getId().getAgeLicencCodigo() + " ya existe");
				}
				repository.save(transaccion);
			}
		});

		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{codigoLicenciatario}/{id}")
				.buildAndExpand(transaccion.getId().getAgeLicencCodigo(), transaccion.getId().getCodigo()).toUri();
		return ResponseEntity.created(location).build();
	}

	@ApiOperation(value = "", authorizations = { @Authorization(value = "Autorización token módulo cco") })
	@PostMapping("/varios")
	public ResponseEntity<?> ingresarVarios(@RequestBody List<CcoQuejasReclamosTrabajo> transacciones,
			HttpServletRequest request) {

		transactionTemplate.execute(new TransactionCallbackWithoutResult() {
			@Override
			public void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
				for (CcoQuejasReclamosTrabajo transaccion : transacciones) {
					fg.validar(transaccion, CcoQuejasReclamosTrabajo.QuejaReclamoTrabajoCreation.class);
					fg.validar(transaccion, EntidadCamposGenerales.CamposGeneralesCreation.class);
					transaccion.setUbicacionIngreso(request.getRemoteAddr());

					if (transaccion.getCcoPaqueteCodigo() != null
							&& transaccion.getCcoPaqueteAgeLicencCodigo() != null) {
						Optional<CcoPaquete> paquete = paqueteRepository.findById(new CcoPaquetePK(
								transaccion.getCcoPaqueteCodigo(), transaccion.getCcoPaqueteAgeLicencCodigo()));
						if (!paquete.isPresent() || paquete.get().getEstado().equals(Globales.ESTADO_ANULADO)) {
							throw new DataIntegrityViolationException(
									"Paquete con código " + transaccion.getCcoPaqueteCodigo() + " y licenciatario "
											+ transaccion.getCcoPaqueteAgeLicencCodigo() + " no existe");
						}
					} else {
						if ((transaccion.getCcoPaqueteCodigo() != null
								&& transaccion.getCcoPaqueteAgeLicencCodigo() == null)
								|| (transaccion.getCcoPaqueteCodigo() == null
										&& transaccion.getCcoPaqueteAgeLicencCodigo() != null)) {
							throw new DataIntegrityViolationException(
									"Todos los campos de identificación de paquete deben ser llenados");
						}
					}

					Long id = fg.obtenerSecuencia(transaccion.getId().getAgeLicencCodigo(), Globales.CODIGO_APLICACION,
							Globales.CODIGO_SECUENCIA_QUEJA_RECLAMO_TRABAJO);
					if (id == 0) {
						throw new DataIntegrityViolationException(
								"Secuencia " + Globales.CODIGO_SECUENCIA_QUEJA_RECLAMO_TRABAJO + " para la aplicación "
										+ Globales.CODIGO_APLICACION + " no existe");
					}
					transaccion.getId().setCodigo(id);
					if (repository.existsById(transaccion.getId())) {
						throw new DataIntegrityViolationException("Queja, reclamo o trabajo con código " + id
								+ " y licenciatario " + transaccion.getId().getAgeLicencCodigo() + " ya existe");
					}
					repository.save(transaccion);
				}
			}
		});

		return new ResponseEntity<List<CcoQuejasReclamosTrabajo>>(transacciones, HttpStatus.CREATED);
	}

	
	
	private JSONObject getCliente(CcoQuejasReclamosTrabajo transaccion,HttpServletRequest request) {
		
		RestTemplate restTemplate = new RestTemplate();
				
		UriComponentsBuilder builder = UriComponentsBuilder
				.fromHttpUrl(urlCliente + "/" + transaccion.getCliClientAgeLicencCodigo())
				.queryParam("codigo", transaccion.getCliClientCodigo());

		HttpEntity<String> result = restTemplate.exchange(builder.toUriString(), HttpMethod.GET,
				asignacionHeaderHttp(request), String.class);
		
		JSONArray jsonArr = new JSONArray(result.getBody());
		if(jsonArr.length()==0) {
			return null;
		}
		if(jsonArr.length()>1) {
			throw new DataIntegrityViolationException(
					"Existe mas de un cliente con el código de cliente "+transaccion.getUsuarioIngreso()+" y licenciatario "+ transaccion.getId().getAgeLicencCodigo());
		}
		return jsonArr.getJSONObject(0);
	}
	
	private HttpEntity<String> asignacionHeaderHttp(HttpServletRequest request) {
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", request.getHeader("Authorization"));

		return new HttpEntity<String>("parameters", headers);
	}

	@ApiOperation(value = "", authorizations = { @Authorization(value = "Autorización token módulo cco") })
	@PutMapping
	public ResponseEntity<?> actualizar(@RequestBody CcoQuejasReclamosTrabajo transaccion, HttpServletRequest request) {
		fg.validar(transaccion, CcoQuejasReclamosTrabajo.QuejaReclamoTrabajoUpdate.class);
		fg.validar(transaccion, EntidadCamposGenerales.CamposGeneralesUpdate.class);
		transaccion.setUbicacionModificacion(request.getRemoteAddr());

		Optional<CcoQuejasReclamosTrabajo> quejaReclamoTrabajo = repository.findById(transaccion.getId());

		if (!repository.existsById(transaccion.getId())) {
			return ManejadorExcepciones.error(HttpStatus.NOT_FOUND, null,
					"Queja, reclamo o trabajo con código " + transaccion.getId().getCodigo() + " no encontrado");
		}

		if (transaccion.getDescripcion() == null || transaccion.getDescripcion().equals("")) {
			transaccion.setDescripcion(quejaReclamoTrabajo.get().getDescripcion());
		}

		if (transaccion.getRutaImagen() == null || transaccion.getRutaImagen().equals("")) {
			transaccion.setRutaImagen(quejaReclamoTrabajo.get().getRutaImagen());
		}

		if (transaccion.getTipo() == null || transaccion.getTipo().equals("")) {
			transaccion.setTipo(quejaReclamoTrabajo.get().getTipo());
		}

		if (transaccion.getCcoPaqueteCodigo() == null && transaccion.getCcoPaqueteAgeLicencCodigo() == null) {
			transaccion.setCcoPaqueteCodigo(quejaReclamoTrabajo.get().getCcoPaqueteCodigo());
			transaccion.setCcoPaqueteAgeLicencCodigo(quejaReclamoTrabajo.get().getCcoPaqueteAgeLicencCodigo());
		} else if ((transaccion.getCcoPaqueteCodigo() != null && transaccion.getCcoPaqueteAgeLicencCodigo() == null)
				|| (transaccion.getCcoPaqueteCodigo() == null && transaccion.getCcoPaqueteAgeLicencCodigo() != null)) {
			return ManejadorExcepciones.error(HttpStatus.BAD_REQUEST, null,
					"Todos los campos de identificación de paquete deben ser llenados");
		} else {

			Optional<CcoPaquete> paquete = paqueteRepository.findById(
					new CcoPaquetePK(transaccion.getCcoPaqueteCodigo(), transaccion.getCcoPaqueteAgeLicencCodigo()));
			if (!paquete.isPresent() || paquete.get().getEstado().equals(Globales.ESTADO_ANULADO)) {
				throw new DataIntegrityViolationException("Paquete con código " + transaccion.getCcoPaqueteCodigo()
						+ " y licenciatario " + transaccion.getCcoPaqueteAgeLicencCodigo() + " no existe");
			}
		}

		repository.save(transaccion);

		return new ResponseEntity<String>("", HttpStatus.OK);
	}

	@ApiOperation(value = "", authorizations = { @Authorization(value = "Autorización token módulo cco") })
	@PutMapping("/varios")
	public ResponseEntity<?> actualizarVarios(@RequestBody List<CcoQuejasReclamosTrabajo> transacciones,
			HttpServletRequest request) {
		transactionTemplate.execute(new TransactionCallbackWithoutResult() {
			@Override
			public void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
				for (CcoQuejasReclamosTrabajo transaccion : transacciones) {
					fg.validar(transaccion, CcoQuejasReclamosTrabajo.QuejaReclamoTrabajoUpdate.class);
					fg.validar(transaccion, EntidadCamposGenerales.CamposGeneralesUpdate.class);
					Optional<CcoQuejasReclamosTrabajo> quejaReclamoTrabajo = repository.findById(transaccion.getId());

					if (!repository.existsById(transaccion.getId())) {
						throw new NoResultException("Queja, reclamo o trabajo con código "
								+ transaccion.getId().getCodigo() + " no encontrado");
					}
					transaccion.setUbicacionModificacion(request.getRemoteAddr());

					if (transaccion.getDescripcion() == null || transaccion.getDescripcion().equals("")) {
						transaccion.setDescripcion(quejaReclamoTrabajo.get().getDescripcion());
					}

					if (transaccion.getRutaImagen() == null || transaccion.getRutaImagen().equals("")) {
						transaccion.setRutaImagen(quejaReclamoTrabajo.get().getRutaImagen());
					}

					if (transaccion.getTipo() == null || transaccion.getTipo().equals("")) {
						transaccion.setTipo(quejaReclamoTrabajo.get().getTipo());
					}

					if (transaccion.getCcoPaqueteCodigo() == null
							&& transaccion.getCcoPaqueteAgeLicencCodigo() == null) {
						transaccion.setCcoPaqueteCodigo(quejaReclamoTrabajo.get().getCcoPaqueteCodigo());
						transaccion
								.setCcoPaqueteAgeLicencCodigo(quejaReclamoTrabajo.get().getCcoPaqueteAgeLicencCodigo());
					} else if ((transaccion.getCcoPaqueteCodigo() != null
							&& transaccion.getCcoPaqueteAgeLicencCodigo() == null)
							|| (transaccion.getCcoPaqueteCodigo() == null
									&& transaccion.getCcoPaqueteAgeLicencCodigo() != null)) {
						throw new DataIntegrityViolationException(
								"Todos los campos de identificación de paquete deben ser llenados");
					} else {

						Optional<CcoPaquete> paquete = paqueteRepository.findById(new CcoPaquetePK(
								transaccion.getCcoPaqueteCodigo(), transaccion.getCcoPaqueteAgeLicencCodigo()));
						if (!paquete.isPresent() || paquete.get().getEstado().equals(Globales.ESTADO_ANULADO)) {
							throw new NoResultException("Paquete con código " + transaccion.getCcoPaqueteCodigo()
									+ " y licenciatario " + transaccion.getCcoPaqueteAgeLicencCodigo() + " no existe");
						}
					}
					repository.save(transaccion);
				}
			}
		});
		return new ResponseEntity<List<CcoQuejasReclamosTrabajo>>(transacciones, HttpStatus.CREATED);
	}

	@ApiOperation(value = "", authorizations = { @Authorization(value = "Autorización token módulo cco") })
	@PostMapping(path = "/uploadFile", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<?> uploadFile(@RequestPart(value = "file") MultipartFile file) {
		JSONObject jo = new JSONObject();
		if (file != null) {
			transactionTemplate.execute(new TransactionCallbackWithoutResult() {
				@Override
				public void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
					try {
						String id = amazonClient.uploadFile(file);
						jo.put("idRutaImagen", id);
						jo.put("rutaExterna", amazonClient.getEndpointUrl() + id);
					} catch (IOException e) {
						logger.error(e.getClass() + " : " + e.getMessage());
						throw new DataIntegrityViolationException("Hubo un problema al subir el archivo");

					} catch (Exception e) {
						logger.error(e.getClass() + " : " + e.getMessage());
						throw new DataIntegrityViolationException("Hubo un problema con el servicio de Bucket AWS");
					}
				}
			});

			return new ResponseEntity<String>(jo.toString(), HttpStatus.OK);

		}

		return new ResponseEntity<String>("No ha subido ninguna Imagen", HttpStatus.BAD_REQUEST);
	}

	/*
	 * @ApiOperation(value = "", authorizations =
	 * {@Authorization(value="Autorización token módulo cco")})
	 * 
	 * @PostMapping(path = "/archivo", consumes = {
	 * MediaType.MULTIPART_FORM_DATA_VALUE },produces =
	 * {MediaType.APPLICATION_JSON_VALUE}) public ResponseEntity<?>
	 * subirArchivo(@RequestPart("file") MultipartFile file) { JSONObject jo = new
	 * JSONObject(); if (file != null) {
	 * 
	 * transactionTemplate.execute(new TransactionCallbackWithoutResult() {
	 * 
	 * @Override public void doInTransactionWithoutResult(TransactionStatus
	 * transactionStatus) { try{ File convertFile=
	 * fg.convertMultipartFiletoFile(file); com.google.api.services.drive.model.File
	 * driveFile = driveService.upLoadFile(convertFile.getName(),
	 * convertFile.getAbsolutePath(),file.getContentType());
	 * System.out.println(driveFile.toPrettyString());
	 * logger.warn(driveFile.toPrettyString()); if(driveFile==null ||
	 * driveFile.getId()==null || driveFile.getId().equals("")) { throw new
	 * DataIntegrityViolationException("Problema al obtener la URL de la imagen"); }
	 * 
	 * 
	 * jo.put("idRutaImagen",driveFile.getId()); convertFile.delete();
	 * 
	 * 
	 * } catch(IOException e){ throw new
	 * DataIntegrityViolationException("Problema al subir la imagen"); }
	 * 
	 * }});
	 * 
	 * 
	 * 
	 * return new ResponseEntity<String>(jo.toString(), HttpStatus.OK);
	 * 
	 * } return new ResponseEntity<String>("No ha subido ninguna Imagen",
	 * HttpStatus.BAD_REQUEST); }
	 * 
	 * @ApiOperation(value = "", authorizations =
	 * {@Authorization(value="Autorización token módulo cco")})
	 * 
	 * @PutMapping(path = "/actualizarArchivo", consumes = {
	 * MediaType.MULTIPART_FORM_DATA_VALUE },produces =
	 * {MediaType.APPLICATION_JSON_VALUE}) public ResponseEntity<?>
	 * actualizarArchivo(@RequestPart("file") MultipartFile
	 * file, @RequestParam("codigo") Long codigo,
	 * 
	 * @RequestParam("ageLicencCodigo") Integer ageLicencCodigo) {
	 * 
	 * if (file != null) {
	 * 
	 * Optional<CcoQuejasReclamosTrabajo> quejaReclamoTrabajo =
	 * repository.findById(new CcoQuejasReclamosTrabajoPK(codigo, ageLicencCodigo));
	 * if (!quejaReclamoTrabajo.isPresent()) { return
	 * ManejadorExcepciones.error(HttpStatus.NOT_FOUND, null,
	 * "Queja, reclamo o trabajo con código " + codigo +" y licenciatario "+
	 * ageLicencCodigo+ " no encontrado"); } transactionTemplate.execute(new
	 * TransactionCallbackWithoutResult() {
	 * 
	 * @Override public void doInTransactionWithoutResult(TransactionStatus
	 * transactionStatus) { try{ File convertFile=
	 * fg.convertMultipartFiletoFile(file); com.google.api.services.drive.model.File
	 * driveFile = driveService.upLoadFile(convertFile.getName(),
	 * convertFile.getAbsolutePath(),file.getContentType());
	 * System.out.println(driveFile.toPrettyString());
	 * logger.warn(driveFile.toPrettyString()); if(driveFile==null ||
	 * driveFile.getId()==null || driveFile.getId().equals("")) { throw new
	 * DataIntegrityViolationException("Problema al obtener la URL de la imagen"); }
	 * //driveFile.getWebViewLink();
	 * 
	 * quejaReclamoTrabajo.get().setRutaImagen(driveFile.getId());
	 * convertFile.delete(); repository.save(quejaReclamoTrabajo.get());
	 * 
	 * 
	 * } catch(IOException e){ throw new
	 * DataIntegrityViolationException("Problema al subir la imagen"); }
	 * 
	 * }}); Map<String, Long> map = new HashMap<>(); map.put("codigo",
	 * quejaReclamoTrabajo.get().getId().getCodigo()); map.put("ageLicencCodigo",new
	 * Long(quejaReclamoTrabajo.get().getId().getAgeLicencCodigo())); JSONObject jo
	 * = new JSONObject(); jo.put("id", map); jo.put("idRutaImagen",
	 * quejaReclamoTrabajo.get().getRutaImagen());
	 * 
	 * return new ResponseEntity<String>(jo.toString(), HttpStatus.OK);
	 * 
	 * } return new ResponseEntity<String>("No ha subido ninguna Imagen",
	 * HttpStatus.BAD_REQUEST); }
	 */
	
	/*@ApiOperation(value = "", authorizations = { @Authorization(value = "Autorización token módulo cco") })
	@PostMapping(path = "/prueba", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<?> prueba(@RequestBody CcoQuejasReclamosTrabajo transaccion, HttpServletRequest request) {
		
		JSONObject jo=getCliente(transaccion, request);
		/*RestTemplate restTemplate = new RestTemplate();

		UriComponentsBuilder builder = UriComponentsBuilder
				.fromHttpUrl(urlCliente + "/" + transaccion.getId().getAgeLicencCodigo())
				.queryParam("codigoUsuario", transaccion.getUsuarioIngreso());

		HttpEntity<String> result = restTemplate.exchange(builder.toUriString(), HttpMethod.GET,
				asignacionHeaderHttp(request), String.class);
		
		JSONArray jsonArr = new JSONArray(result.getBody());
		
		if(jsonArr.length()==0) {
			throw new DataIntegrityViolationException(
					"No existe cliente con el código de usuario "+transaccion.getUsuarioIngreso()+" y licenciatario "+ transaccion.getId().getAgeLicencCodigo());
		}
		if(jsonArr.length()>1) {
			throw new DataIntegrityViolationException(
					"Existe mas de una cliente con el código de usuario "+transaccion.getUsuarioIngreso()+" y licenciatario "+ transaccion.getId().getAgeLicencCodigo());
		}
		System.out.println(jsonArr.getJSONObject(0));
		JSONObject jo=jsonArr.getJSONObject(0);*/
		/*for(int i=0;i<jsonArr.length();i++) {
			JSONObject obj = jsonArr.getJSONObject(i);
			System.out.println(obj.getString("nombres"));
			
		}*/
		
		// Map<String, Long> params = new HashMap<String, Long>();
				// params.put("codigoLicenciatario", new
				// Long(transaccion.getId().getAgeLicencCodigo()));
				// params.put("codigoUsuario", 2L);
				// ResponseEntity<String> result = restTemplate.exchange(builder,
				// HttpMethod.GET, asignacionHeaderHttp(request), String.class, params);
		/*
		 * if(result.getBody().equals("null")||result.getStatusCode()!=HttpStatus.OK) {
		 * System.out.println("No se encuentra al cliente especifico"); } try {
		 * JSONObject obj = new JSONObject(result.getBody());
		 * System.out.println(obj.getString("nombres"));
		 * System.out.println(obj.getString("apellidos"));
		 * System.out.println(obj.getString("telefonoCelular"));
		 * System.out.println(obj.getString("correoElectronico")); } catch(Exception e)
		 * { System.out.println("hubo un error"); }
		 
		return ResponseEntity.ok(jo.toString());

	}*/
}
