package sasf.net.cco.controller;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.NoResultException;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import javassist.NotFoundException;
import sasf.net.cco.model.CcoPuestoTrabajoRequisito;
import sasf.net.cco.model.CcoPuestosTrabajo;
import sasf.net.cco.model.CcoPuestosTrabajoPK;
import sasf.net.cco.model.CcoRequisitosTrabajo;
import sasf.net.cco.model.CcoRequisitosTrabajoPK;
import sasf.net.cco.repository.CcoPuestoTrabajoRequisitoRepository;
import sasf.net.cco.repository.CcoPuestosTrabajoRepository;
import sasf.net.cco.repository.CcoRequisitosTrabajoRepository;
import sasf.net.cco.utils.EntidadCamposGenerales;
import sasf.net.cco.utils.FuncionesGenerales;
import sasf.net.cco.utils.Globales;
import sasf.net.cco.utils.ManejadorExcepciones;

@RestController
@RequestMapping("/ccoPuestosTrabajo")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class CcoPuestosTrabajoController {

	@Autowired
	private TransactionTemplate transactionTemplate;

	@Autowired
	private FuncionesGenerales fg;

	@Autowired
	private CcoPuestosTrabajoRepository repository;

	@Autowired
	private CcoPuestoTrabajoRequisitoRepository puestoTrabajoRequisitoRepository;

	@Autowired
	private CcoRequisitosTrabajoRepository requisitosTrabajoRepository;

	@ApiOperation(value = "", authorizations = { @Authorization(value = "Autorización token módulo cco") })
	@GetMapping(path = "/{codigoLicenciatario}",produces = { MediaType.APPLICATION_JSON_VALUE })
	public List<?> consultar(@PathVariable Integer codigoLicenciatario,
			@RequestParam(name = "codigo", required = false) Long codigo,
			@RequestParam(name = "descripcion", required = false) String descripcion) {
		return repository.buscarPorParametros(codigoLicenciatario, codigo, descripcion, Globales.ESTADO_ANULADO)
				.stream().map(t -> {
					List<CcoPuestoTrabajoRequisito> puestoTrabRequisitos = puestoTrabajoRequisitoRepository
							.buscarPorPuestoTrabajo(t.getId().getAgeLicencCodigo(), t.getId().getCodigo(),
									Globales.ESTADO_ANULADO);

					List<CcoRequisitosTrabajo> requisitosTrabajo = new ArrayList<CcoRequisitosTrabajo>();

					for (CcoPuestoTrabajoRequisito pTrabReq : puestoTrabRequisitos) {
						Optional<CcoRequisitosTrabajo> reqTrab = requisitosTrabajoRepository
								.findById(new CcoRequisitosTrabajoPK(pTrabReq.getId().getCcoReTraCodigo(),
										pTrabReq.getId().getCcoReTraAgeLicencCodigo()));
						if (reqTrab.isPresent()) {
							requisitosTrabajo.add(reqTrab.get());
						}
					}

					t.setRequisitosTrabajo(requisitosTrabajo);

					return t;
				}).collect(Collectors.toList());
	}

	@ApiOperation(value = "", authorizations = { @Authorization(value = "Autorización token módulo cco") })
	@GetMapping(path = "/{codigoLicenciatario}/{codigo}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<?> consultarPorId(@PathVariable Integer codigoLicenciatario, @PathVariable Long codigo)
			throws NotFoundException {

		Optional<CcoPuestosTrabajo> puestoTrabajo = repository
				.findById(new CcoPuestosTrabajoPK(codigo, codigoLicenciatario));
		if (!puestoTrabajo.isPresent() || puestoTrabajo.get().getEstado().equals(Globales.ESTADO_ANULADO)) {
			throw new NoResultException("Puesto de trabajo con código " + codigo + " no existe");
		}
		
		List<CcoPuestoTrabajoRequisito> puestoTrabRequisitos = puestoTrabajoRequisitoRepository
				.buscarPorPuestoTrabajo(puestoTrabajo.get().getId().getAgeLicencCodigo(), puestoTrabajo.get().getId().getCodigo(),
						Globales.ESTADO_ANULADO);

		List<CcoRequisitosTrabajo> requisitosTrabajo = new ArrayList<CcoRequisitosTrabajo>();

		for (CcoPuestoTrabajoRequisito pTrabReq : puestoTrabRequisitos) {
			Optional<CcoRequisitosTrabajo> reqTrab = requisitosTrabajoRepository
					.findById(new CcoRequisitosTrabajoPK(pTrabReq.getId().getCcoReTraCodigo(),
							pTrabReq.getId().getCcoReTraAgeLicencCodigo()));
			if (reqTrab.isPresent()) {
				requisitosTrabajo.add(reqTrab.get());
			}
		}

		puestoTrabajo.get().setRequisitosTrabajo(requisitosTrabajo);
		
		return new ResponseEntity<CcoPuestosTrabajo>(puestoTrabajo.get(), HttpStatus.OK);
	}

	@ApiOperation(value = "", authorizations = { @Authorization(value = "Autorización token módulo cco") })
	@PostMapping
	public ResponseEntity<?> ingresar(@RequestBody CcoPuestosTrabajo transaccion, HttpServletRequest request) {

		fg.validar(transaccion, CcoPuestosTrabajo.PuestosTrabajoCreation.class);
		fg.validar(transaccion, EntidadCamposGenerales.CamposGeneralesCreation.class);
		transaccion.setUbicacionIngreso(request.getRemoteAddr());

		transactionTemplate.execute(new TransactionCallbackWithoutResult() {
			@Override
			public void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
				Long id = fg.obtenerSecuencia(transaccion.getId().getAgeLicencCodigo(), Globales.CODIGO_APLICACION,
						Globales.CODIGO_SECUENCIA_PUESTO_TRABAJO);
				if (id == 0) {
					throw new DataIntegrityViolationException("Secuencia " + Globales.CODIGO_SECUENCIA_PUESTO_TRABAJO
							+ " para la aplicación " + Globales.CODIGO_APLICACION + " no existe");
				}
				transaccion.getId().setCodigo(id);
				if (repository.existsById(transaccion.getId())) {
					throw new DataIntegrityViolationException("Puesto de trabajo con código " + id + " ya existe");
				}
				repository.save(transaccion);
			}
		});

		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{codigoLicenciatario}/{id}")
				.buildAndExpand(transaccion.getId().getAgeLicencCodigo(), transaccion.getId().getCodigo()).toUri();
		return ResponseEntity.created(location).build();
	}

	@ApiOperation(value = "", authorizations = { @Authorization(value = "Autorización token módulo cco") })
	@PostMapping("/varios")
	public ResponseEntity<?> ingresarVarios(@RequestBody List<CcoPuestosTrabajo> transacciones,
			HttpServletRequest request) {

		transactionTemplate.execute(new TransactionCallbackWithoutResult() {
			@Override
			public void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
				for (CcoPuestosTrabajo transaccion : transacciones) {
					fg.validar(transaccion, CcoPuestosTrabajo.PuestosTrabajoCreation.class);
					fg.validar(transaccion, EntidadCamposGenerales.CamposGeneralesCreation.class);
					transaccion.setUbicacionIngreso(request.getRemoteAddr());
					Long id = fg.obtenerSecuencia(transaccion.getId().getAgeLicencCodigo(), Globales.CODIGO_APLICACION,
							Globales.CODIGO_SECUENCIA_PUESTO_TRABAJO);
					if (id == 0) {
						throw new DataIntegrityViolationException(
								"Secuencia " + Globales.CODIGO_SECUENCIA_PUESTO_TRABAJO + " para la aplicación "
										+ Globales.CODIGO_APLICACION + " no existe");
					}
					transaccion.getId().setCodigo(id);
					if (repository.existsById(transaccion.getId())) {
						throw new DataIntegrityViolationException("Puesto de trabajo con código " + id + " ya existe");
					}
					repository.save(transaccion);
				}
			}
		});

		return new ResponseEntity<List<CcoPuestosTrabajo>>(transacciones, HttpStatus.CREATED);
	}

	@ApiOperation(value = "", authorizations = { @Authorization(value = "Autorización token módulo cco") })
	@PutMapping
	public ResponseEntity<?> actualizar(@RequestBody CcoPuestosTrabajo transaccion, HttpServletRequest request) {
		fg.validar(transaccion, CcoPuestosTrabajo.PuestosTrabajoUpdate.class);
		fg.validar(transaccion, EntidadCamposGenerales.CamposGeneralesUpdate.class);
		transaccion.setUbicacionModificacion(request.getRemoteAddr());
		Optional<CcoPuestosTrabajo> puestoTrabajo = repository.findById(transaccion.getId());

		if (!repository.existsById(
				new CcoPuestosTrabajoPK(transaccion.getId().getCodigo(), transaccion.getId().getAgeLicencCodigo()))) {
			return ManejadorExcepciones.error(HttpStatus.NOT_FOUND, null,
					"Puesto de trabajo con código " + transaccion.getId().getCodigo() + " no encontrado");
		}

		if (transaccion.getDescripcion() == null) {
			transaccion.setDescripcion(puestoTrabajo.get().getDescripcion());
		}

		if (transaccion.getTitulo() == null) {
			transaccion.setTitulo(puestoTrabajo.get().getTitulo());
		}

		if (transaccion.getRutaImagen() == null) {
			transaccion.setRutaImagen(puestoTrabajo.get().getRutaImagen());
		}

		repository.save(transaccion);

		return new ResponseEntity<String>("", HttpStatus.OK);
	}

	@ApiOperation(value = "", authorizations = { @Authorization(value = "Autorización token módulo cco") })
	@PutMapping("/varios")
	public ResponseEntity<?> actualizarVarios(@RequestBody List<CcoPuestosTrabajo> transacciones,
			HttpServletRequest request) {
		transactionTemplate.execute(new TransactionCallbackWithoutResult() {
			@Override
			public void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
				for (CcoPuestosTrabajo transaccion : transacciones) {
					fg.validar(transaccion, CcoPuestosTrabajo.PuestosTrabajoUpdate.class);
					fg.validar(transaccion, EntidadCamposGenerales.CamposGeneralesUpdate.class);
					Optional<CcoPuestosTrabajo> puestoTrabajo = repository.findById(transaccion.getId());

					if (!repository.existsById(transaccion.getId())) {
						throw new NoResultException(
								"Puesto de trabajo con código " + transaccion.getId().getCodigo() + " no encontrado");
					}
					transaccion.setUbicacionModificacion(request.getRemoteAddr());

					if (transaccion.getDescripcion() == null) {
						transaccion.setDescripcion(puestoTrabajo.get().getDescripcion());
					}

					if (transaccion.getTitulo() == null) {
						transaccion.setTitulo(puestoTrabajo.get().getTitulo());
					}

					if (transaccion.getRutaImagen() == null) {
						transaccion.setRutaImagen(puestoTrabajo.get().getRutaImagen());
					}
					repository.save(transaccion);
				}
			}
		});
		return new ResponseEntity<List<CcoPuestosTrabajo>>(transacciones, HttpStatus.CREATED);
	}
}
