package sasf.net.cco.controller;

import java.net.URI;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.persistence.NoResultException;
import javax.servlet.http.HttpServletRequest;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import javassist.NotFoundException;
import sasf.net.cco.model.CcoInventarioDiario;
import sasf.net.cco.model.CcoInventarioDiarioPK;
import sasf.net.cco.model.CcoPaquete;
import sasf.net.cco.model.CcoPaquetePK;
import sasf.net.cco.repository.CcoInventarioDiarioRepository;
import sasf.net.cco.repository.CcoPaqueteRespository;
import sasf.net.cco.utils.EntidadCamposGenerales;
import sasf.net.cco.utils.FuncionesGenerales;
import sasf.net.cco.utils.Globales;
import sasf.net.cco.utils.ManejadorExcepciones;

@RestController
@RequestMapping("/ccoInventarioDiario")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class CcoInventarioDiarioController {
	
	@Autowired
	private FuncionesGenerales fg;
	@Autowired
	private TransactionTemplate transactionTemplate;
	@Autowired
	private CcoInventarioDiarioRepository repository;
	@Autowired
	private CcoPaqueteRespository paqueteRepository;
	
	

	@ApiOperation(value = "", authorizations = {@Authorization(value="Autorización token módulo cco")})
	@GetMapping(path = "/{codigoLicenciatario}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public List<?> consultar(@PathVariable Integer codigoLicenciatario,
			@RequestParam(name = "codigo", required = false) Long codigo,
			@RequestParam(name = "ccoPaqueteCodigo", required = false) Long ccoPaqueteCodigo,
			@RequestParam(name = "ccoPaqueteCodigoH", required = false) Long ccoPaqueteCodigoH,
			@RequestParam(value = "fechaDesde", required = false) @DateTimeFormat(iso = ISO.DATE, pattern = "yyyy-MM-dd") Date fechaDesde,
			@RequestParam(value = "fechaHasta", required = false) @DateTimeFormat(iso = ISO.DATE, pattern = "yyyy-MM-dd") Date fechaHasta
			) {
		if(fechaHasta!=null) {
			Calendar c = Calendar.getInstance(); 
			c.setTime(fechaHasta); 
			c.add(Calendar.DATE, 1);
			fechaHasta = c.getTime();
		}
		
		return repository.buscarPorParametros(codigoLicenciatario,codigo,ccoPaqueteCodigo,ccoPaqueteCodigoH,fechaDesde,fechaHasta,Globales.ESTADO_ANULADO);

	}

	
	@ApiOperation(value = "", authorizations = {@Authorization(value="Autorización token módulo cco")})
	@GetMapping(path = "/{codigoLicenciatario}/{codigo}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<?> consultarPorId(@PathVariable Integer codigoLicenciatario, @PathVariable Long codigo)
			throws NotFoundException {

		Optional<CcoInventarioDiario> invDir = repository.findById(new CcoInventarioDiarioPK(codigo, codigoLicenciatario));
		if (!invDir.isPresent() || invDir.get().getEstado().equals(Globales.ESTADO_ANULADO)) {
			throw new NoResultException("Inventario diario con código " + codigo + " no existe");
		}
		return new ResponseEntity<CcoInventarioDiario>(invDir.get(), HttpStatus.OK);
	} 
	
	
	@ApiOperation(value = "", authorizations = {@Authorization(value="Autorización token módulo cco")})
	@PostMapping
	public ResponseEntity<?> ingresar(@RequestBody CcoInventarioDiario transaccion,
			HttpServletRequest request) {

		fg.validar(transaccion, CcoInventarioDiario.InventarioDiarioCreation.class);
		fg.validar(transaccion, EntidadCamposGenerales.CamposGeneralesCreation.class);
		transaccion.setUbicacionIngreso(request.getRemoteAddr());
		
		Optional<CcoPaquete> paqueteDesde=paqueteRepository.findById(new CcoPaquetePK(transaccion.getCcoPaqueteCodigo(), transaccion.getCcoPaqueteAgeLicencCodigo()));
		if(!paqueteDesde.isPresent() || paqueteDesde.get().getEstado().equals(Globales.ESTADO_ANULADO)) {
			throw new NoResultException(
					"Paquete desde con código "+ transaccion.getCcoPaqueteCodigo() + " con licenciatario "+transaccion.getCcoPaqueteAgeLicencCodigo()+" no existe");
		}
		
		Optional<CcoPaquete> paqueteHasta=paqueteRepository.findById(new CcoPaquetePK(transaccion.getCcoPaqueteCodigoH(), transaccion.getCcoPaqueteAgeLicencCodigoH()));
		if(!paqueteHasta.isPresent() || paqueteHasta.get().getEstado().equals(Globales.ESTADO_ANULADO)) {
			throw new NoResultException(
					"Paquete hasta con código "+ transaccion.getCcoPaqueteCodigoH() + " con licenciatario "+transaccion.getCcoPaqueteAgeLicencCodigoH()+" no existe");
		}
		
		transactionTemplate.execute(new TransactionCallbackWithoutResult() {
			@Override
			public void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
				Long id = fg.obtenerSecuencia(transaccion.getId().getAgeLicencCodigo(), Globales.CODIGO_APLICACION,
						Globales.CODIGO_SECUENCIA_INVENTARIO_DIARIO);
				if (id == 0) {
					throw new DataIntegrityViolationException("Secuencia " + Globales.CODIGO_SECUENCIA_INVENTARIO_DIARIO
							+ " para la aplicación " + Globales.CODIGO_APLICACION + " no existe");
				}
				transaccion.getId().setCodigo(id);
				if(repository.existsById(transaccion.getId())){
					throw new DataIntegrityViolationException(
							"Inventario diario con código " + id + " ya existe");
				}
				repository.save(transaccion);
			}
		});

		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{codigoLicenciatario}/{id}")
				.buildAndExpand(transaccion.getId().getAgeLicencCodigo(), transaccion.getId().getCodigo()).toUri();
		return ResponseEntity.created(location).build();
	}
	
	@ApiOperation(value = "", authorizations = {@Authorization(value="Autorización token módulo cco")})
	@PostMapping("/varios")
	public ResponseEntity<?> ingresarVarios(@RequestBody List<CcoInventarioDiario> transacciones, 
			HttpServletRequest request) {
		
		transactionTemplate.execute(new TransactionCallbackWithoutResult() {
			@Override
			public void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
				for (CcoInventarioDiario transaccion : transacciones) {
					fg.validar(transaccion, CcoInventarioDiario.InventarioDiarioCreation.class);
					fg.validar(transaccion, EntidadCamposGenerales.CamposGeneralesCreation.class);
					transaccion.setUbicacionIngreso(request.getRemoteAddr());
					Optional<CcoPaquete> paqueteDesde=paqueteRepository.findById(new CcoPaquetePK(transaccion.getCcoPaqueteCodigo(), transaccion.getCcoPaqueteAgeLicencCodigo()));
					
					if(!paqueteDesde.isPresent() || paqueteDesde.get().getEstado().equals(Globales.ESTADO_ANULADO)) {
						throw new NoResultException(
								"Paquete desde con código "+ transaccion.getCcoPaqueteCodigo() + " con licenciatario "+transaccion.getCcoPaqueteAgeLicencCodigo()+" no existe");
					}
					
					Optional<CcoPaquete> paqueteHasta=paqueteRepository.findById(new CcoPaquetePK(transaccion.getCcoPaqueteCodigoH(), transaccion.getCcoPaqueteAgeLicencCodigoH()));
					if(!paqueteHasta.isPresent() || paqueteHasta.get().getEstado().equals(Globales.ESTADO_ANULADO)) {
						throw new NoResultException(
								"Paquete hasta con código "+ transaccion.getCcoPaqueteCodigoH() + " con licenciatario "+transaccion.getCcoPaqueteAgeLicencCodigoH()+" no existe");
					}
					Long id = fg.obtenerSecuencia(transaccion.getId().getAgeLicencCodigo(), Globales.CODIGO_APLICACION,
							Globales.CODIGO_SECUENCIA_INVENTARIO_DIARIO);
					if (id == 0) {
						throw new DataIntegrityViolationException("Secuencia " + Globales.CODIGO_SECUENCIA_INVENTARIO_DIARIO
								+ " para la aplicación " + Globales.CODIGO_APLICACION + " no existe");
					}
					transaccion.getId().setCodigo(id);
					if(repository.existsById(transaccion.getId())){
						throw new DataIntegrityViolationException(
								"Inventario diario con código " + id + " ya existe");
					}
					repository.save(transaccion);
				}
			}
		});

		return new ResponseEntity<List<CcoInventarioDiario>>(transacciones, HttpStatus.CREATED);
	}
	
	@ApiOperation(value = "", authorizations = {@Authorization(value="Autorización token módulo cco")})
	@PutMapping
	public ResponseEntity<?> actualizar(@RequestBody CcoInventarioDiario transaccion, HttpServletRequest request) {
		fg.validar(transaccion, CcoInventarioDiario.InventarioDiarioUpdate.class);
		fg.validar(transaccion, EntidadCamposGenerales.CamposGeneralesUpdate.class);
		transaccion.setUbicacionModificacion(request.getRemoteAddr());
		Optional<CcoInventarioDiario> invDir=repository.findById(transaccion.getId());
		
		if (!repository.existsById(
				new CcoInventarioDiarioPK(transaccion.getId().getCodigo(), transaccion.getId().getAgeLicencCodigo()))) {
			return ManejadorExcepciones.error(HttpStatus.NOT_FOUND, null,
					"Inventario diario con código " + transaccion.getId().getCodigo() + " no encontrado");
		}
		
		if(transaccion.getNumeroPaquetes()==null) {
			transaccion.setNumeroPaquetes(invDir.get().getNumeroPaquetes());
		}
		if(transaccion.getFecha()==null) {
			transaccion.setFecha(invDir.get().getFecha());
		}
		
		if(transaccion.getGastos()==null) {
			transaccion.setGastos(invDir.get().getGastos());
		}
		if(transaccion.getCcoPaqueteCodigo()==null) {
			transaccion.setCcoPaqueteCodigo(invDir.get().getCcoPaqueteCodigo());
		}
		if(transaccion.getCcoPaqueteAgeLicencCodigo()==null) {
			transaccion.setCcoPaqueteAgeLicencCodigo(invDir.get().getCcoPaqueteAgeLicencCodigo());
		}
		
		Optional<CcoPaquete> paqueteDesde=paqueteRepository.findById(new CcoPaquetePK(transaccion.getCcoPaqueteCodigo(), transaccion.getCcoPaqueteAgeLicencCodigo()));
		if(!paqueteDesde.isPresent() || paqueteDesde.get().getEstado().equals(Globales.ESTADO_ANULADO)) {
			throw new NoResultException(
					"Paquete desde con código "+ transaccion.getCcoPaqueteCodigo() + " con licenciatario "+transaccion.getCcoPaqueteAgeLicencCodigo()+" no existe");
		}
		
		if(transaccion.getCcoPaqueteCodigoH()==null) {
			transaccion.setCcoPaqueteCodigoH(invDir.get().getCcoPaqueteCodigoH());
		}
		if(transaccion.getCcoPaqueteAgeLicencCodigoH()==null) {
			transaccion.setCcoPaqueteAgeLicencCodigoH(invDir.get().getCcoPaqueteAgeLicencCodigoH());
		}
		
		
		
		Optional<CcoPaquete> paqueteHasta=paqueteRepository.findById(new CcoPaquetePK(transaccion.getCcoPaqueteCodigoH(), transaccion.getCcoPaqueteAgeLicencCodigoH()));
		if(!paqueteHasta.isPresent() || paqueteHasta.get().getEstado().equals(Globales.ESTADO_ANULADO)) {
			throw new NoResultException(
					"Paquete hasta con código "+ transaccion.getCcoPaqueteCodigoH() + " con licenciatario "+transaccion.getCcoPaqueteAgeLicencCodigoH()+" no existe");
		}
		
		if(transaccion.getObservaciones()==null) {
			transaccion.setObservaciones(invDir.get().getObservaciones());
		}
		
		
		
		repository.save(transaccion);

		return new ResponseEntity<String>("", HttpStatus.OK);
	}
	
	@ApiOperation(value = "", authorizations = {@Authorization(value="Autorización token módulo cco")})
	@PutMapping("/varios")
	public ResponseEntity<?> actualizarVarios(@RequestBody List<CcoInventarioDiario> transacciones,
			HttpServletRequest request) {
		transactionTemplate.execute(new TransactionCallbackWithoutResult() {
			@Override
			public void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
				for (CcoInventarioDiario transaccion : transacciones) {
					fg.validar(transaccion, CcoInventarioDiario.InventarioDiarioUpdate.class);
					fg.validar(transaccion, EntidadCamposGenerales.CamposGeneralesUpdate.class);
					Optional<CcoInventarioDiario> invDir=repository.findById(transaccion.getId());
					
					if (!repository.existsById(transaccion.getId())) {
						throw new NoResultException("Inventario diario con código " + transaccion.getId().getCodigo() + " no encontrado");
					} 
					transaccion.setUbicacionModificacion(request.getRemoteAddr());
					
					if(transaccion.getNumeroPaquetes()==null) {
						transaccion.setNumeroPaquetes(invDir.get().getNumeroPaquetes());
					}
					if(transaccion.getFecha()==null) {
						transaccion.setFecha(invDir.get().getFecha());
					}
					
					if(transaccion.getGastos()==null) {
						transaccion.setGastos(invDir.get().getGastos());
					}
					if(transaccion.getCcoPaqueteCodigo()==null) {
						transaccion.setCcoPaqueteCodigo(invDir.get().getCcoPaqueteCodigo());
					}
					if(transaccion.getCcoPaqueteAgeLicencCodigo()==null) {
						transaccion.setCcoPaqueteAgeLicencCodigo(invDir.get().getCcoPaqueteAgeLicencCodigo());
					}
					
					Optional<CcoPaquete> paqueteDesde=paqueteRepository.findById(new CcoPaquetePK(transaccion.getCcoPaqueteCodigo(), transaccion.getCcoPaqueteAgeLicencCodigo()));
					if(!paqueteDesde.isPresent() || paqueteDesde.get().getEstado().equals(Globales.ESTADO_ANULADO)) {
						throw new NoResultException(
								"Paquete desde con código "+ transaccion.getCcoPaqueteCodigo() + " con licenciatario "+transaccion.getCcoPaqueteAgeLicencCodigo()+" no existe");
					}
					
					if(transaccion.getCcoPaqueteCodigoH()==null) {
						transaccion.setCcoPaqueteCodigoH(invDir.get().getCcoPaqueteCodigoH());
					}
					if(transaccion.getCcoPaqueteAgeLicencCodigoH()==null) {
						transaccion.setCcoPaqueteAgeLicencCodigoH(invDir.get().getCcoPaqueteAgeLicencCodigoH());
					}
					
					
					
					Optional<CcoPaquete> paqueteHasta=paqueteRepository.findById(new CcoPaquetePK(transaccion.getCcoPaqueteCodigoH(), transaccion.getCcoPaqueteAgeLicencCodigoH()));
					if(!paqueteHasta.isPresent() || paqueteHasta.get().getEstado().equals(Globales.ESTADO_ANULADO)) {
						throw new NoResultException(
								"Paquete hasta con código "+ transaccion.getCcoPaqueteCodigoH() + " con licenciatario "+transaccion.getCcoPaqueteAgeLicencCodigoH()+" no existe");
					}

					if(transaccion.getObservaciones()==null) {
						transaccion.setObservaciones(invDir.get().getObservaciones());
					}
					
					repository.save(transaccion);
				}
			}
		});
		return new ResponseEntity<List<CcoInventarioDiario>>(transacciones, HttpStatus.CREATED);
	}


	
}
