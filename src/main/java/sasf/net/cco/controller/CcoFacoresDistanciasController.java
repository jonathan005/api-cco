package sasf.net.cco.controller;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import javax.persistence.NoResultException;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import javassist.NotFoundException;
import sasf.net.cco.model.CcoFactoresDistancias;
import sasf.net.cco.model.CcoFactoresDistanciasPK;
import sasf.net.cco.repository.CcoFacoresDistanciasRespository;
import sasf.net.cco.utils.EntidadCamposGenerales;
import sasf.net.cco.utils.FuncionesGenerales;
import sasf.net.cco.utils.Globales;
import sasf.net.cco.utils.ManejadorExcepciones;

@RestController
@RequestMapping("/ccoFactorDistancia")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class CcoFacoresDistanciasController {
	@Autowired
	private TransactionTemplate transactionTemplate;
	
	@Autowired
	private FuncionesGenerales fg;
	
	@Autowired
	private CcoFacoresDistanciasRespository repository;
	
	@ApiOperation(value = "", authorizations = {@Authorization(value="Autorización token módulo cco")})
	@GetMapping(path = "/{codigoLicenciatario}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public List<?> consultar(@PathVariable Integer codigoLicenciatario,
			@RequestParam(name = "codigo", required = false) Long codigo
			) {
		return repository.buscarPorParametros(codigoLicenciatario, codigo, Globales.ESTADO_ANULADO);
	}
	
	@ApiOperation(value = "", authorizations = {@Authorization(value="Autorización token módulo cco")})
	@GetMapping(path = "/{codigoLicenciatario}/{codigo}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<?> consultarPorId(@PathVariable Integer codigoLicenciatario, @PathVariable Long codigo)
			throws NotFoundException {

		Optional<CcoFactoresDistancias> fDist = repository.findById(new CcoFactoresDistanciasPK(codigo, codigoLicenciatario));
		if (!fDist.isPresent() || fDist.get().getEstado().equals(Globales.ESTADO_ANULADO)) {
			throw new NoResultException("Factor Distancia  con código " + codigo + " no existe");
		}
		return new ResponseEntity<CcoFactoresDistancias>(fDist.get(), HttpStatus.OK);
	}
	
	@ApiOperation(value = "", authorizations = {@Authorization(value="Autorización token módulo cco")})
	@PostMapping
	public ResponseEntity<?> ingresar(@RequestBody CcoFactoresDistancias transaccion,
			HttpServletRequest request) {
		
		if (transaccion.getFactor().floatValue()<=0) {
			return ManejadorExcepciones.error(HttpStatus.BAD_REQUEST, null, 
					"Factor debe ser mayor a cero");
		}
		if(transaccion.getDistanciaDesde().floatValue()<=0) {
			return ManejadorExcepciones.error(HttpStatus.BAD_REQUEST, null, 
					"Distancia desde debe ser mayor a cero");
		}
		if(transaccion.getDistanciaHasta().floatValue()<=0) {
			return ManejadorExcepciones.error(HttpStatus.BAD_REQUEST, null, 
					"Distancia Hasta debe ser mayor a cero");
		}
		if(transaccion.getDistanciaDesde().floatValue()>transaccion.getDistanciaHasta().floatValue()) {
			return ManejadorExcepciones.error(HttpStatus.BAD_REQUEST, null, 
					"Distancia desde debe ser menor que distancia hasta");
		}
		

		fg.validar(transaccion, CcoFactoresDistancias.FactoresDistanciasCreation.class);
		fg.validar(transaccion, EntidadCamposGenerales.CamposGeneralesCreation.class);
		transaccion.setUbicacionIngreso(request.getRemoteAddr());

		transactionTemplate.execute(new TransactionCallbackWithoutResult() {
			@Override
			public void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
				Long id = fg.obtenerSecuencia(transaccion.getId().getAgeLicencCodigo(), Globales.CODIGO_APLICACION,
						Globales.CODIGO_SECUENCIA_FACTOR_DISTANCIA);
				if (id == 0) {
					throw new DataIntegrityViolationException("Secuencia " + Globales.CODIGO_SECUENCIA_FACTOR_DISTANCIA
							+ " para la aplicación " + Globales.CODIGO_APLICACION + " no existe");
				}
				transaccion.getId().setCodigo(id);
				if(repository.existsById(transaccion.getId())){
					throw new DataIntegrityViolationException(
							"Factor peso con código " + id + " ya existe");
				}
				repository.save(transaccion);
			}
		});

		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{codigoLicenciatario}/{id}")
				.buildAndExpand(transaccion.getId().getAgeLicencCodigo(), transaccion.getId().getCodigo()).toUri();
		return ResponseEntity.created(location).build();
	}
	
	@ApiOperation(value = "", authorizations = {@Authorization(value="Autorización token módulo cco")})
	@PostMapping("/varios")
	public ResponseEntity<?> ingresarVarios(@RequestBody List<CcoFactoresDistancias> transacciones, 
			HttpServletRequest request) {
		
		transactionTemplate.execute(new TransactionCallbackWithoutResult() {
			@Override
			public void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
				for (CcoFactoresDistancias transaccion : transacciones) {
					fg.validar(transaccion, CcoFactoresDistancias.FactoresDistanciasCreation.class);
					fg.validar(transaccion, EntidadCamposGenerales.CamposGeneralesCreation.class);
					
					if (transaccion.getFactor().longValue()<=0) {
						throw new DataIntegrityViolationException( 
								"Factor debe ser mayor a cero");
					}
					if(transaccion.getDistanciaDesde().longValue()<=0) {
						throw new DataIntegrityViolationException( 
								"Distancia desde debe ser mayor a cero");
					}
					if(transaccion.getDistanciaHasta().longValue()<=0) {
						throw new DataIntegrityViolationException( 
								"Distancia Hasta debe ser mayor a cero");
					}
					if(transaccion.getDistanciaDesde().longValue()>transaccion.getDistanciaHasta().longValue()) {
						throw new DataIntegrityViolationException( 
								"Distancia desde debe ser menor que distancia hasta");
					}
					transaccion.setUbicacionIngreso(request.getRemoteAddr());
					Long id = fg.obtenerSecuencia(transaccion.getId().getAgeLicencCodigo(), Globales.CODIGO_APLICACION,
							Globales.CODIGO_SECUENCIA_FACTOR_DISTANCIA);
					if (id == 0) {
						throw new DataIntegrityViolationException("Secuencia " + Globales.CODIGO_SECUENCIA_FACTOR_DISTANCIA
								+ " para la aplicación " + Globales.CODIGO_APLICACION + " no existe");
					}
					transaccion.getId().setCodigo(id);
					if(repository.existsById(transaccion.getId())){
						throw new DataIntegrityViolationException(
								"Embalaje con código " + id + " ya existe");
					}
					repository.save(transaccion);
				}
			}
		});

		return new ResponseEntity<List<CcoFactoresDistancias>>(transacciones, HttpStatus.CREATED);
	}
	
	@ApiOperation(value = "", authorizations = {@Authorization(value="Autorización token módulo cco")})
	@PutMapping
	public ResponseEntity<?> actualizar(@RequestBody CcoFactoresDistancias transaccion, HttpServletRequest request) {
		fg.validar(transaccion, CcoFactoresDistancias.FactoresDistanciasUpdate.class);
		fg.validar(transaccion, EntidadCamposGenerales.CamposGeneralesUpdate.class);
		transaccion.setUbicacionModificacion(request.getRemoteAddr());
		Optional<CcoFactoresDistancias> fDist=repository.findById(transaccion.getId());
		
		if (!repository.existsById(
				new CcoFactoresDistanciasPK(transaccion.getId().getCodigo(), transaccion.getId().getAgeLicencCodigo()))) {
			return ManejadorExcepciones.error(HttpStatus.NOT_FOUND, null,
					"Factor distancia con código " + transaccion.getId().getCodigo() + " no encontrado");
		}
		if(transaccion.getFactor()==null) {
			transaccion.setFactor(fDist.get().getFactor());
		}
		if(transaccion.getDistanciaDesde()==null) {
			transaccion.setDistanciaDesde(fDist.get().getDistanciaDesde());
		}
		if(transaccion.getDistanciaHasta()==null) {
			transaccion.setDistanciaHasta(fDist.get().getDistanciaHasta());
		}
		if(transaccion.getDistanciaDesde().floatValue()>transaccion.getDistanciaHasta().floatValue()) {
			return ManejadorExcepciones.error(HttpStatus.BAD_REQUEST, null, 
					"Distancia desde debe ser menor que distancia hasta");
		}
		
		repository.save(transaccion);

		return new ResponseEntity<String>("", HttpStatus.OK);
	}
	
	@ApiOperation(value = "", authorizations = {@Authorization(value="Autorización token módulo cco")})
	@PutMapping("/varios")
	public ResponseEntity<?> actualizarVarios(@RequestBody List<CcoFactoresDistancias> transacciones,
			HttpServletRequest request) {
		transactionTemplate.execute(new TransactionCallbackWithoutResult() {
			@Override
			public void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
				for (CcoFactoresDistancias transaccion : transacciones) {
					fg.validar(transaccion, CcoFactoresDistancias.FactoresDistanciasUpdate.class);
					fg.validar(transaccion, EntidadCamposGenerales.CamposGeneralesUpdate.class);
					Optional<CcoFactoresDistancias> fDist=repository.findById(transaccion.getId());
					
					if (!repository.existsById(transaccion.getId())) {
						throw new NoResultException("Factor distancia con código " + transaccion.getId().getCodigo() + " no encontrado");
					} 
					transaccion.setUbicacionModificacion(request.getRemoteAddr());
					
					if(transaccion.getFactor()==null) {
						transaccion.setFactor(fDist.get().getFactor());
					}
					if(transaccion.getDistanciaDesde()==null) {
						transaccion.setDistanciaDesde(fDist.get().getDistanciaDesde());
					}
					if(transaccion.getDistanciaHasta()==null) {
						transaccion.setDistanciaHasta(fDist.get().getDistanciaHasta());
					}
					
					if(transaccion.getDistanciaDesde().floatValue()>transaccion.getDistanciaHasta().floatValue()) {
						throw new DataIntegrityViolationException(
								"Distancia desde debe ser menor que distancia hasta");
					}
				
					repository.save(transaccion);
				}
			}
		});
		return new ResponseEntity<List<CcoFactoresDistancias>>(transacciones, HttpStatus.CREATED);
	}

}
