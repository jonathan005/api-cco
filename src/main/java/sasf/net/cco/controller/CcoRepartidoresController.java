package sasf.net.cco.controller;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import javax.persistence.NoResultException;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import javassist.NotFoundException;
import sasf.net.cco.model.CcoRepartidores;
import sasf.net.cco.model.CcoRepartidoresPK;
import sasf.net.cco.repository.CcoRepartidoresRepository;
import sasf.net.cco.utils.EntidadCamposGenerales;
import sasf.net.cco.utils.FuncionesGenerales;
import sasf.net.cco.utils.Globales;
import sasf.net.cco.utils.ManejadorExcepciones;

@RestController
@RequestMapping("/ccoRepartidores")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class CcoRepartidoresController {
	
	@Autowired
	private TransactionTemplate transactionTemplate;
	
	@Autowired
	private FuncionesGenerales fg;
	
	@Autowired
	private CcoRepartidoresRepository repository;
	
	@ApiOperation(value = "", authorizations = {@Authorization(value="Autorización token módulo cco")})
	@GetMapping(path = "/{codigoLicenciatario}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public List<?> consultar(@PathVariable Integer codigoLicenciatario,
			@RequestParam(name = "codigo", required = false) Long codigo,
			@RequestParam(name = "nombres", required = false) String nombres,
			@RequestParam(name = "apellidos", required = false) String apellidos,
			@RequestParam(name = "correoElectronico", required = false) String correoElectronico
			) {
		return repository.buscarPorParametros(codigoLicenciatario, codigo, 
				(nombres== null )? null:nombres.toUpperCase(),
				(apellidos ==null)? null:apellidos.toUpperCase(),
						correoElectronico,
								Globales.ESTADO_ANULADO);
	}
	
	@ApiOperation(value = "", authorizations = {@Authorization(value="Autorización token módulo cco")})
	@GetMapping(path = "/{codigoLicenciatario}/{codigo}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<?> consultarPorId(@PathVariable Integer codigoLicenciatario, @PathVariable Long codigo)
			throws NotFoundException {

		Optional<CcoRepartidores> repartidor = repository.findById(new CcoRepartidoresPK(codigo, codigoLicenciatario));
		if (!repartidor.isPresent() || repartidor.get().getEstado().equals(Globales.ESTADO_ANULADO)) {
			throw new NoResultException("Repartidor con código " + codigo + " no existe");
		}
		return new ResponseEntity<CcoRepartidores>(repartidor.get(), HttpStatus.OK);
	}
	
	@ApiOperation(value = "", authorizations = {@Authorization(value="Autorización token módulo cco")})
	@PostMapping
	public ResponseEntity<?> ingresar(@RequestBody CcoRepartidores transaccion,
			HttpServletRequest request) {

		fg.validar(transaccion, CcoRepartidores.RepartidoresCreation.class);
		fg.validar(transaccion, EntidadCamposGenerales.CamposGeneralesCreation.class);
		transaccion.setUbicacionIngreso(request.getRemoteAddr());

		transactionTemplate.execute(new TransactionCallbackWithoutResult() {
			@Override
			public void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
				Long id = fg.obtenerSecuencia(transaccion.getId().getAgeLicencCodigo(), Globales.CODIGO_APLICACION,
						Globales.CODIGO_SECUENCIA_REPARTIDOR);
				if (id == 0) {
					throw new DataIntegrityViolationException("Secuencia " + Globales.CODIGO_SECUENCIA_REPARTIDOR
							+ " para la aplicación " + Globales.CODIGO_APLICACION + " no existe");
				}
				transaccion.getId().setCodigo(id);
				if(repository.existsById(transaccion.getId())){
					throw new DataIntegrityViolationException(
							"Repartidor con código " + id + " ya existe");
				}
				repository.save(transaccion);
			}
		});

		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{codigoLicenciatario}/{id}")
				.buildAndExpand(transaccion.getId().getAgeLicencCodigo(), transaccion.getId().getCodigo()).toUri();
		return ResponseEntity.created(location).build();
	}
	
	@ApiOperation(value = "", authorizations = {@Authorization(value="Autorización token módulo cco")})
	@PostMapping("/varios")
	public ResponseEntity<?> ingresarVarios(@RequestBody List<CcoRepartidores> transacciones, 
			HttpServletRequest request) {

		transactionTemplate.execute(new TransactionCallbackWithoutResult() {
			@Override
			public void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
				for (CcoRepartidores transaccion : transacciones) {
					fg.validar(transaccion, CcoRepartidores.RepartidoresCreation.class);
					fg.validar(transaccion, EntidadCamposGenerales.CamposGeneralesCreation.class);
					transaccion.setUbicacionIngreso(request.getRemoteAddr());
					Long id = fg.obtenerSecuencia(transaccion.getId().getAgeLicencCodigo(), Globales.CODIGO_APLICACION,
							Globales.CODIGO_SECUENCIA_REPARTIDOR);
					if (id == 0) {
						throw new DataIntegrityViolationException("Secuencia " + Globales.CODIGO_SECUENCIA_REPARTIDOR
								+ " para la aplicación " + Globales.CODIGO_APLICACION + " no existe");
					}
					transaccion.getId().setCodigo(id);
					if(repository.existsById(transaccion.getId())){
						throw new DataIntegrityViolationException(
								"Repartidor con código " + id + " ya existe");
					}
					repository.save(transaccion);
				}
			}
		});

		return new ResponseEntity<List<CcoRepartidores>>(transacciones, HttpStatus.CREATED);
	}
	
	@ApiOperation(value = "", authorizations = {@Authorization(value="Autorización token módulo cco")})
	@PutMapping
	public ResponseEntity<?> actualizar(@RequestBody CcoRepartidores transaccion, HttpServletRequest request) {
		fg.validar(transaccion, CcoRepartidores.RepartidoresUpdate.class);
		fg.validar(transaccion, EntidadCamposGenerales.CamposGeneralesUpdate.class);
		transaccion.setUbicacionModificacion(request.getRemoteAddr());
		Optional<CcoRepartidores> repartidor=repository.findById(transaccion.getId());
		
		if (!repository.existsById(
				new CcoRepartidoresPK(transaccion.getId().getCodigo(), transaccion.getId().getAgeLicencCodigo()))) {
			return ManejadorExcepciones.error(HttpStatus.NOT_FOUND, null,
					"Repartidor con código " + transaccion.getId().getCodigo() + " no encontrado");
		}
		
		if(transaccion.getNombres()==null) {
			transaccion.setNombres(repartidor.get().getNombres());
		}
		if(transaccion.getApellidos()==null) {
			transaccion.setApellidos(repartidor.get().getApellidos());
		}
		if(transaccion.getNumeroCelular()==null) {
			transaccion.setNumeroCelular(repartidor.get().getNumeroCelular());
		}
		if(transaccion.getCorreoElectronico()==null) {
			transaccion.setCorreoElectronico(repartidor.get().getCorreoElectronico());
		}
		if(transaccion.getDireccion()==null) {
			transaccion.setDireccion(repartidor.get().getDireccion());
		}
		if(transaccion.getEstadoRepartidor()==null) {
			transaccion.setEstadoRepartidor(repartidor.get().getEstadoRepartidor());
		}
		
		repository.save(transaccion);

		return new ResponseEntity<String>("", HttpStatus.OK);
	}
	
	@ApiOperation(value = "", authorizations = {@Authorization(value="Autorización token módulo cco")})
	@PutMapping("/varios")
	public ResponseEntity<?> actualizarVarios(@RequestBody List<CcoRepartidores> transacciones,
			HttpServletRequest request) {
		transactionTemplate.execute(new TransactionCallbackWithoutResult() {
			@Override
			public void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
				for (CcoRepartidores transaccion : transacciones) {
					fg.validar(transaccion, CcoRepartidores.RepartidoresUpdate.class);
					fg.validar(transaccion, EntidadCamposGenerales.CamposGeneralesUpdate.class);
					Optional<CcoRepartidores> repartidor=repository.findById(transaccion.getId());
					
					if (!repository.existsById(transaccion.getId())) {
						throw new NoResultException("Embalaje con código " + transaccion.getId().getCodigo() + " no encontrado");
					} 
					transaccion.setUbicacionModificacion(request.getRemoteAddr());
					
					if(transaccion.getNombres()==null) {
						transaccion.setNombres(repartidor.get().getNombres());
					}
					if(transaccion.getApellidos()==null) {
						transaccion.setApellidos(repartidor.get().getApellidos());
					}
					if(transaccion.getNumeroCelular()==null) {
						transaccion.setNumeroCelular(repartidor.get().getNumeroCelular());
					}
					if(transaccion.getCorreoElectronico()==null) {
						transaccion.setCorreoElectronico(repartidor.get().getCorreoElectronico());
					}
					if(transaccion.getDireccion()==null) {
						transaccion.setDireccion(repartidor.get().getDireccion());
					}
					if(transaccion.getEstadoRepartidor()==null) {
						transaccion.setEstadoRepartidor(repartidor.get().getEstadoRepartidor());
					}
					repository.save(transaccion);
				}
			}
		});
		return new ResponseEntity<List<CcoRepartidores>>(transacciones, HttpStatus.CREATED);
	}
	
	
}
