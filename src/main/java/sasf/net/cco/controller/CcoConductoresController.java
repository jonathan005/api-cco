package sasf.net.cco.controller;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.URI;
import java.util.List;
import java.util.Optional;

import javax.persistence.NoResultException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import javassist.NotFoundException;
import sasf.net.cco.model.CcoConductores;
import sasf.net.cco.model.CcoConductoresPK;
import sasf.net.cco.model.CcoEmpresasRecolectoras;
import sasf.net.cco.model.CcoEmpresasRecolectorasPK;
import sasf.net.cco.repository.CcoConductoresRepository;
import sasf.net.cco.repository.CcoEmpresasRecolectorasRepository;
import sasf.net.cco.service.CcoConductoresService;
import sasf.net.cco.utils.EntidadCamposGenerales;
import sasf.net.cco.utils.FuncionesGenerales;
import sasf.net.cco.utils.Globales;
import sasf.net.cco.utils.ManejadorExcepciones;


@RestController
@RequestMapping("/ccoConductores")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class CcoConductoresController {
	
	@Autowired
    private CcoConductoresRepository repository;
	@Autowired
	private FuncionesGenerales fg;
	@Autowired
	private TransactionTemplate transactionTemplate;
	@Autowired
	private CcoConductoresService ccoConductoresService;
	
	@Autowired
	private CcoEmpresasRecolectorasRepository ccoEmpresasRecolectorasRepository;

	
	@ApiOperation(value = "", authorizations = {@Authorization(value="Autorización token módulo cco")})
	@GetMapping(path = "/{codigoLicenciatario}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public List<?> consultar(@PathVariable Integer codigoLicenciatario,
			@RequestParam(name = "codigo", required = false) Long codigo,
			@RequestParam(name = "matricula", required = false) String matricula ,
			@RequestParam(name = "correo", required = false) String correo,
			@RequestParam(name = "ccoEmpReCodigo", required = false) Long ccoEmpReCodigo
			) {
		//return repository.buscarPorParametros(codigoLicenciatario, codigo, matricula, correo , Globales.ESTADO_ANULADO);
		return repository.buscarPorParametros(codigoLicenciatario,codigo ,matricula, correo,ccoEmpReCodigo,  Globales.ESTADO_ANULADO);

	}

	
	@ApiOperation(value = "", authorizations = {@Authorization(value="Autorización token módulo cco")})
	@GetMapping(path = "/{codigoLicenciatario}/{codigo}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<?> consultarPorId(@PathVariable Integer codigoLicenciatario, @PathVariable Long codigo)
			throws NotFoundException {

		Optional<CcoConductores> conductor = repository.findById(new CcoConductoresPK(codigo, codigoLicenciatario));
		if (!conductor.isPresent() || conductor.get().getEstado().equals(Globales.ESTADO_ANULADO)) {
			throw new NoResultException("EL Conductor con código " + codigo + " no existe");
		}
		return new ResponseEntity<CcoConductores>(conductor.get(), HttpStatus.OK);
	} 
	
	
	@ApiOperation(value = "", authorizations = {@Authorization(value="Autorización token módulo cco")})
	@GetMapping(path = "/reporte_pdf")
	public void reportePdf(HttpServletResponse response) {
		 byte[] pdfReport = ccoConductoresService.crearPdf(repository.findAll()).toByteArray();
		 
	        String mimeType =  "application/pdf";
	        response.setContentType(mimeType);
	        response.setHeader("Content-Disposition", String.format("attachment; filename=\"%s\"", "reporte.pdf"));

	        response.setContentLength(pdfReport.length);
		
	        ByteArrayInputStream inStream = new ByteArrayInputStream( pdfReport);

	        try {
				FileCopyUtils.copy(inStream, response.getOutputStream());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

	}
	
	
	@ApiOperation(value = "", authorizations = {@Authorization(value="Autorización token módulo cco")})
	@GetMapping(path = "/reporte_excel")
	public void reporteExcel(HttpServletResponse response) {
		 byte[] excelReport = ccoConductoresService.crearExcelConductores(repository.findAll()).toByteArray();
		 
	        String mimeType =  "application/octet-stream";
	        response.setContentType(mimeType);
	        response.setHeader("Content-Disposition", String.format("attachment; filename=\"%s\"", "reporte.xls"));

	        response.setContentLength(excelReport.length);
		
	        ByteArrayInputStream inStream = new ByteArrayInputStream( excelReport);

	        try {
				FileCopyUtils.copy(inStream, response.getOutputStream());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

	}
	
	@ApiOperation(value = "", authorizations = {@Authorization(value="Autorización token módulo cco")})
	@GetMapping(path = "/reporte_excel/{codigoLicenciatario}/{codigo}")
	public void reporteExcelConductor( @PathVariable Integer codigoLicenciatario,
			                           @PathVariable Long codigo ,
			                              HttpServletResponse response) {

			Optional<CcoConductores> conductor = repository.findById(new CcoConductoresPK(codigo, codigoLicenciatario));
			if (!conductor.isPresent() && conductor.get().getEstado().equals(Globales.ESTADO_ANULADO)) {
				throw new NoResultException("EL Conductor con código " + codigo + " no existe");
			}
		
		byte[] excelReport = ccoConductoresService.crearExcelConductor(conductor.get()).toByteArray();
		 
	        String mimeType =  "application/octet-stream";
	        response.setContentType(mimeType);
	        response.setHeader("Content-Disposition", String.format("attachment; filename=\"%s\"", "reporte.xls"));

	        response.setContentLength(excelReport.length);
		
	        ByteArrayInputStream inStream = new ByteArrayInputStream( excelReport);

	        try {
				FileCopyUtils.copy(inStream, response.getOutputStream());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

	}
	
	
@ApiOperation(value = "", authorizations = {@Authorization(value="Autorización token módulo cco")})
	@PostMapping
	public ResponseEntity<?> ingresar(@RequestBody CcoConductores transaccion,
			HttpServletRequest request) {

		fg.validar(transaccion, CcoConductores.ConductorCreation.class);
		fg.validar(transaccion, EntidadCamposGenerales.CamposGeneralesCreation.class);
		transaccion.setUbicacionIngreso(request.getRemoteAddr());
		
		Optional<CcoEmpresasRecolectoras>empRec=ccoEmpresasRecolectorasRepository.findById(new CcoEmpresasRecolectorasPK(transaccion.getCcoEmpReCodigo(),transaccion.getCcoEmpReAgeLicencCodigo()));
		if(!empRec.isPresent() || empRec.get().getEstado().equals(Globales.ESTADO_ANULADO)) {
			throw new NoResultException(
					"Empresa recolectora con código "+ transaccion.getCcoEmpReCodigo()+" y licenciatario "+transaccion.getCcoEmpReAgeLicencCodigo()+" no existe");
		}
		
		transactionTemplate.execute(new TransactionCallbackWithoutResult() {
			@Override
			public void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
				Long id = fg.obtenerSecuencia(transaccion.getId().getAgeLicencCodigo(), Globales.CODIGO_APLICACION,
						Globales.CODIGO_SECUENCIA_CONDUCTORES);
				if (id == 0) {
					throw new DataIntegrityViolationException("Secuencia " + Globales.CODIGO_SECUENCIA_CONDUCTORES
							+ " para la aplicación " + Globales.CODIGO_APLICACION + " no existe");
				}
				transaccion.getId().setCodigo(id);
				if(repository.existsById(transaccion.getId())){
					throw new DataIntegrityViolationException(
							"Conductor con código " + id + " ya existe");
				}
				repository.save(transaccion);
			}
		});

		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{codigoLicenciatario}/{id}")
				.buildAndExpand(transaccion.getId().getAgeLicencCodigo(), transaccion.getId().getCodigo()).toUri();
		return ResponseEntity.created(location).build();
	}
	 
	

@ApiOperation(value = "", authorizations = {@Authorization(value="Autorización token módulo cco")})
@PostMapping("/varios")
public ResponseEntity<?> ingresarVarios(@RequestBody List<CcoConductores> transacciones, 
		HttpServletRequest request) {

	transactionTemplate.execute(new TransactionCallbackWithoutResult() {
		@Override
		public void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
			for (CcoConductores transaccion : transacciones) {
			
				fg.validar(transaccion, CcoConductores.ConductorCreation.class);
				fg.validar(transaccion, EntidadCamposGenerales.CamposGeneralesCreation.class);		
				transaccion.setUbicacionIngreso(request.getRemoteAddr());
				
				Optional<CcoEmpresasRecolectoras>empRec=ccoEmpresasRecolectorasRepository.findById(new CcoEmpresasRecolectorasPK(transaccion.getCcoEmpReCodigo(),transaccion.getCcoEmpReAgeLicencCodigo()));
				if(!empRec.isPresent() || empRec.get().getEstado().equals(Globales.ESTADO_ANULADO)) {
					throw new NoResultException(
							"Empresa recolectora con código "+ transaccion.getCcoEmpReCodigo()+" y licenciatario "+transaccion.getCcoEmpReAgeLicencCodigo()+" no existe");
				}
				
				Long id = fg.obtenerSecuencia(transaccion.getId().getAgeLicencCodigo(), Globales.CODIGO_APLICACION,
						Globales.CODIGO_SECUENCIA_CONDUCTORES);
				if (id == 0) {
					throw new DataIntegrityViolationException("Secuencia " + Globales.CODIGO_SECUENCIA_CONDUCTORES
							+ " para la aplicación " + Globales.CODIGO_APLICACION + " no existe");
				}
				transaccion.getId().setCodigo(id);
				if(repository.existsById(transaccion.getId())){
					throw new DataIntegrityViolationException(
							"Conductor con código " + id + " ya existe");
				}
				repository.save(transaccion);
			}
		}
	});

	return new ResponseEntity<List<CcoConductores>>(transacciones, HttpStatus.CREATED);
       
  }

@ApiOperation(value = "", authorizations = {@Authorization(value="Autorización token módulo cco")})
@PutMapping
public ResponseEntity<?> actualizar(@RequestBody CcoConductores transaccion, HttpServletRequest request) {
	fg.validar(transaccion, CcoConductores.ConductorUpdate.class);
	fg.validar(transaccion, EntidadCamposGenerales.CamposGeneralesUpdate.class);
	transaccion.setUbicacionModificacion(request.getRemoteAddr());
	Optional<CcoConductores> conductorActual=repository.findById(transaccion.getId());
	
	if (!repository.existsById(
			new CcoConductoresPK(transaccion.getId().getCodigo(), transaccion.getId().getAgeLicencCodigo()))) {
		return ManejadorExcepciones.error(HttpStatus.NOT_FOUND, null,
				"Conductor con código " + transaccion.getId().getCodigo() + " no encontrado");
	}
		
	if(transaccion.getNombre()==null || transaccion.getNombre().equals("")) {
		transaccion.setNombre(conductorActual.get().getNombre());
	}
	
	if(transaccion.getApellido()==null || transaccion.getApellido().equals("")) {
		transaccion.setApellido(conductorActual.get().getApellido());
	}
	
	if(transaccion.getCorreoElectronico()==null || transaccion.getCorreoElectronico().equals("")) {
		transaccion.setCorreoElectronico(conductorActual.get().getCorreoElectronico());
	}
	
	if(transaccion.getNumeroCelular()==null || transaccion.getNumeroCelular().equals("")) {
		transaccion.setNumeroCelular(conductorActual.get().getNumeroCelular());
	}
	
	if(transaccion.getNumeroMatricula()==null || transaccion.getNumeroMatricula().equals("")) {
		transaccion.setNumeroMatricula(conductorActual.get().getNumeroMatricula());
	}
	if(transaccion.getEstadoConductor()==null) {
		transaccion.setEstadoConductor(conductorActual.get().getEstadoConductor());
	}
	
	
	if(transaccion.getCcoEmpReCodigo()==null) {
		transaccion.setCcoEmpReCodigo(conductorActual.get().getCcoEmpReCodigo());
	}
	if(transaccion.getCcoEmpReAgeLicencCodigo()==null) {
		transaccion.setCcoEmpReAgeLicencCodigo(conductorActual.get().getCcoEmpReAgeLicencCodigo());
	}
	
	
	Optional<CcoEmpresasRecolectoras>empRec=ccoEmpresasRecolectorasRepository.findById(new CcoEmpresasRecolectorasPK(transaccion.getCcoEmpReCodigo(),transaccion.getCcoEmpReAgeLicencCodigo()));
	if(!empRec.isPresent() || empRec.get().getEstado().equals(Globales.ESTADO_ANULADO)) {
		throw new NoResultException(
				"Empresa recolectora con código "+ transaccion.getCcoEmpReCodigo()+" y licenciatario "+transaccion.getCcoEmpReAgeLicencCodigo()+" no existe");
	}
	
	if(transaccion.getAgeLocaliCodigo()==null ||transaccion.getAgeLocaliCodigo()<=0) {
		transaccion.setAgeLocaliCodigo(conductorActual.get().getAgeLocaliCodigo());
	}
	if(transaccion.getAgeLocaliTipLoCodigo()==null ||transaccion.getAgeLocaliTipLoCodigo()<=0) {
		transaccion.setAgeLocaliTipLoCodigo(conductorActual.get().getAgeLocaliTipLoCodigo());
	}
	if(transaccion.getAgecaliTipLoAgePaisCodigo()==null || transaccion.getAgecaliTipLoAgePaisCodigo()<=0) {
		transaccion.setAgecaliTipLoAgePaisCodigo(conductorActual.get().getAgecaliTipLoAgePaisCodigo());
	}
	
	repository.save(transaccion);

	return new ResponseEntity<String>("", HttpStatus.OK);
}

	
@ApiOperation(value = "", authorizations = {@Authorization(value="Autorización token módulo cco")})
@PutMapping("/varios")
public ResponseEntity<?> actualizarVarios(@RequestBody List<CcoConductores> transacciones,
		HttpServletRequest request) {
	transactionTemplate.execute(new TransactionCallbackWithoutResult() {
		@Override
		public void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
			for (CcoConductores transaccion : transacciones) {
				fg.validar(transaccion, CcoConductores.ConductorUpdate.class);
				fg.validar(transaccion, EntidadCamposGenerales.CamposGeneralesUpdate.class);
				Optional<CcoConductores> conductorActual=repository.findById(transaccion.getId());
				
				if (!repository.existsById(transaccion.getId())) {
					throw new NoResultException("Conductor con código " + transaccion.getId().getCodigo() + " no encontrado");
				} 
				transaccion.setUbicacionModificacion(request.getRemoteAddr());
				
				if(transaccion.getNombre()==null || transaccion.getNombre().equals("")) {
					transaccion.setNombre(conductorActual.get().getNombre());
				}
				
				if(transaccion.getApellido()==null || transaccion.getApellido().equals("")) {
					transaccion.setApellido(conductorActual.get().getApellido());
				}
				
				if(transaccion.getCorreoElectronico()==null || transaccion.getCorreoElectronico().equals("")) {
					transaccion.setCorreoElectronico(conductorActual.get().getCorreoElectronico());
				}
				
				if(transaccion.getNumeroCelular()==null || transaccion.getNumeroCelular().equals("")) {
					transaccion.setNumeroCelular(conductorActual.get().getNumeroCelular());
				}
				
				if(transaccion.getNumeroMatricula()==null || transaccion.getNumeroMatricula().equals("")) {
					transaccion.setNumeroMatricula(conductorActual.get().getNumeroMatricula());
				}
				if(transaccion.getEstadoConductor()==null) {
					transaccion.setEstadoConductor(conductorActual.get().getEstadoConductor());
				}
				
				if(transaccion.getCcoEmpReCodigo()==null) {
					transaccion.setCcoEmpReCodigo(conductorActual.get().getCcoEmpReCodigo());
				}
				if(transaccion.getCcoEmpReAgeLicencCodigo()==null) {
					transaccion.setCcoEmpReAgeLicencCodigo(conductorActual.get().getCcoEmpReAgeLicencCodigo());
				}
				
				
				Optional<CcoEmpresasRecolectoras>empRec=ccoEmpresasRecolectorasRepository.findById(new CcoEmpresasRecolectorasPK(transaccion.getCcoEmpReCodigo(),transaccion.getCcoEmpReAgeLicencCodigo()));
				if(!empRec.isPresent() || empRec.get().getEstado().equals(Globales.ESTADO_ANULADO)) {
					throw new NoResultException(
							"Empresa recolectora con código "+ transaccion.getCcoEmpReCodigo()+" y licenciatario "+transaccion.getCcoEmpReAgeLicencCodigo()+" no existe");
				}
				
				if(transaccion.getAgeLocaliCodigo()==null ||transaccion.getAgeLocaliCodigo()<=0) {
					transaccion.setAgeLocaliCodigo(conductorActual.get().getAgeLocaliCodigo());
				}
				if(transaccion.getAgeLocaliTipLoCodigo()==null ||transaccion.getAgeLocaliTipLoCodigo()<=0) {
					transaccion.setAgeLocaliTipLoCodigo(conductorActual.get().getAgeLocaliTipLoCodigo());
				}
				if(transaccion.getAgecaliTipLoAgePaisCodigo()==null || transaccion.getAgecaliTipLoAgePaisCodigo()<=0) {
					transaccion.setAgecaliTipLoAgePaisCodigo(conductorActual.get().getAgecaliTipLoAgePaisCodigo());
				}
				
				repository.save(transaccion);
			}
		}
	});
	return new ResponseEntity<List<CcoConductores>>(transacciones, HttpStatus.CREATED);
}

	
	
	
	
	

}
