package sasf.net.cco.controller;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import javax.persistence.NoResultException;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import javassist.NotFoundException;
import sasf.net.cco.model.CcoEmpresasRecolectoras;
import sasf.net.cco.model.CcoEmpresasRecolectorasPK;
import sasf.net.cco.repository.CcoEmpresasRecolectorasRepository;
import sasf.net.cco.utils.EntidadCamposGenerales;
import sasf.net.cco.utils.FuncionesGenerales;
import sasf.net.cco.utils.Globales;
import sasf.net.cco.utils.ManejadorExcepciones;

@RestController
@RequestMapping("/ccoEmpresasRecolectoras")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class CcoEmpresasRecolectorasController {
	
	@Autowired
	private FuncionesGenerales fg;
	@Autowired
	private TransactionTemplate transactionTemplate;
	@Autowired
	private CcoEmpresasRecolectorasRepository repository;
	
	
	@ApiOperation(value = "", authorizations = {@Authorization(value="Autorización token módulo cco")})
	@GetMapping(path = "/{codigoLicenciatario}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public List<?> consultar(@PathVariable Integer codigoLicenciatario,
			@RequestParam(name = "codigo", required = false) Long codigo,
			@RequestParam(name = "nombreEmpresa", required = false) String nombreEmpresa
			) {
		return repository.buscarPorParametros(codigoLicenciatario, codigo, 
				(nombreEmpresa== null )? null:nombreEmpresa.toUpperCase(),
				Globales.ESTADO_ANULADO);
	}
	
	@ApiOperation(value = "", authorizations = {@Authorization(value="Autorización token módulo cco")})
	@GetMapping(path = "/{codigoLicenciatario}/{codigo}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<?> consultarPorId(@PathVariable Integer codigoLicenciatario, @PathVariable Long codigo)
			throws NotFoundException {

		Optional<CcoEmpresasRecolectoras> empRec = repository.findById(new CcoEmpresasRecolectorasPK(codigo, codigoLicenciatario));
		if (!empRec.isPresent() || empRec.get().getEstado().equals(Globales.ESTADO_ANULADO)) {
			throw new NoResultException("Empresa recolectora con código " + codigo + " no existe");
		}
		return new ResponseEntity<CcoEmpresasRecolectoras>(empRec.get(), HttpStatus.OK);
	}
	
	@ApiOperation(value = "", authorizations = {@Authorization(value="Autorización token módulo cco")})
	@PostMapping
	public ResponseEntity<?> ingresar(@RequestBody CcoEmpresasRecolectoras transaccion,
			HttpServletRequest request) {

		fg.validar(transaccion, CcoEmpresasRecolectoras.EmpresasRecolectorasCreation.class);
		fg.validar(transaccion, EntidadCamposGenerales.CamposGeneralesCreation.class);
		transaccion.setUbicacionIngreso(request.getRemoteAddr());

		transactionTemplate.execute(new TransactionCallbackWithoutResult() {
			@Override
			public void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
				Long id = fg.obtenerSecuencia(transaccion.getId().getAgeLicencCodigo(), Globales.CODIGO_APLICACION,
						Globales.CODIGO_SECUENCIA_EMPRESA_RECOLECTORA);
				if (id == 0) {
					throw new DataIntegrityViolationException("Secuencia " + Globales.CODIGO_SECUENCIA_EMPRESA_RECOLECTORA
							+ " para la aplicación " + Globales.CODIGO_APLICACION + " no existe");
				}
				transaccion.getId().setCodigo(id);
				if(repository.existsById(transaccion.getId())){
					throw new DataIntegrityViolationException(
							"Empresa recolectora con código " + id + " ya existe");
				}
				repository.save(transaccion);
			}
		});

		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{codigoLicenciatario}/{id}")
				.buildAndExpand(transaccion.getId().getAgeLicencCodigo(), transaccion.getId().getCodigo()).toUri();
		return ResponseEntity.created(location).build();
	}
	
	@ApiOperation(value = "", authorizations = {@Authorization(value="Autorización token módulo cco")})
	@PostMapping("/varios")
	public ResponseEntity<?> ingresarVarios(@RequestBody List<CcoEmpresasRecolectoras> transacciones, 
			HttpServletRequest request) {

		transactionTemplate.execute(new TransactionCallbackWithoutResult() {
			@Override
			public void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
				for (CcoEmpresasRecolectoras transaccion : transacciones) {
					fg.validar(transaccion, CcoEmpresasRecolectoras.EmpresasRecolectorasCreation.class);
					fg.validar(transaccion, EntidadCamposGenerales.CamposGeneralesCreation.class);
					transaccion.setUbicacionIngreso(request.getRemoteAddr());
					Long id = fg.obtenerSecuencia(transaccion.getId().getAgeLicencCodigo(), Globales.CODIGO_APLICACION,
							Globales.CODIGO_SECUENCIA_EMPRESA_RECOLECTORA);
					if (id == 0) {
						throw new DataIntegrityViolationException("Secuencia " + Globales.CODIGO_SECUENCIA_EMPRESA_RECOLECTORA
								+ " para la aplicación " + Globales.CODIGO_APLICACION + " no existe");
					}
					transaccion.getId().setCodigo(id);
					if(repository.existsById(transaccion.getId())){
						throw new DataIntegrityViolationException(
								"Empresa recolectora con código " + id + " ya existe");
					}
					repository.save(transaccion);
				}
			}
		});

		return new ResponseEntity<List<CcoEmpresasRecolectoras>>(transacciones, HttpStatus.CREATED);
	}
	
	@ApiOperation(value = "", authorizations = {@Authorization(value="Autorización token módulo cco")})
	@PutMapping
	public ResponseEntity<?> actualizar(@RequestBody CcoEmpresasRecolectoras transaccion, HttpServletRequest request) {
		fg.validar(transaccion, CcoEmpresasRecolectoras.EmpresasRecolectorasUpdate.class);
		fg.validar(transaccion, EntidadCamposGenerales.CamposGeneralesUpdate.class);
		transaccion.setUbicacionModificacion(request.getRemoteAddr());
		Optional<CcoEmpresasRecolectoras> empRec=repository.findById(transaccion.getId());
		
		if (!repository.existsById(
				new CcoEmpresasRecolectorasPK(transaccion.getId().getCodigo(), transaccion.getId().getAgeLicencCodigo()))) {
			return ManejadorExcepciones.error(HttpStatus.NOT_FOUND, null,
					"Empresa recolectora con código " + transaccion.getId().getCodigo() + " no encontrado");
		}
		
		if(transaccion.getNumeroCelular()==null) {
			transaccion.setNumeroCelular(empRec.get().getNumeroCelular());
		}
		if(transaccion.getCorreoElectronico()==null) {
			transaccion.setCorreoElectronico(empRec.get().getCorreoElectronico());
		}
		if(transaccion.getNombreEmpresa()==null) {
			transaccion.setNombreEmpresa(empRec.get().getNombreEmpresa());
		}
		
		repository.save(transaccion);

		return new ResponseEntity<String>("", HttpStatus.OK);
	}
	
	@ApiOperation(value = "", authorizations = {@Authorization(value="Autorización token módulo cco")})
	@PutMapping("/varios")
	public ResponseEntity<?> actualizarVarios(@RequestBody List<CcoEmpresasRecolectoras> transacciones,
			HttpServletRequest request) {
		transactionTemplate.execute(new TransactionCallbackWithoutResult() {
			@Override
			public void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
				for (CcoEmpresasRecolectoras transaccion : transacciones) {
					fg.validar(transaccion, CcoEmpresasRecolectoras.EmpresasRecolectorasUpdate.class);
					fg.validar(transaccion, EntidadCamposGenerales.CamposGeneralesUpdate.class);
					Optional<CcoEmpresasRecolectoras> empRec=repository.findById(transaccion.getId());
					
					if (!repository.existsById(transaccion.getId())) {
						throw new NoResultException("Empresa recolectora con código " + transaccion.getId().getCodigo() + " no encontrado");
					} 
					transaccion.setUbicacionModificacion(request.getRemoteAddr());
					
					if(transaccion.getNumeroCelular()==null) {
						transaccion.setNumeroCelular(empRec.get().getNumeroCelular());
					}
					if(transaccion.getCorreoElectronico()==null) {
						transaccion.setCorreoElectronico(empRec.get().getCorreoElectronico());
					}
					if(transaccion.getNombreEmpresa()==null) {
						transaccion.setNombreEmpresa(empRec.get().getNombreEmpresa());
					}
					
					
					repository.save(transaccion);
				}
			}
		});
		return new ResponseEntity<List<CcoEmpresasRecolectoras>>(transacciones, HttpStatus.CREATED);
	}
	
	
	
}
