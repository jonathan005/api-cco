package sasf.net.cco.controller;

import java.net.URI;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.NoResultException;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import javassist.NotFoundException;
import sasf.net.cco.model.CcoPostulanteDocumentos;
import sasf.net.cco.model.CcoPostulanteDocumentosPK;
import sasf.net.cco.model.CcoPostulantePK;
import sasf.net.cco.model.CcoRequisitosTrabajo;
import sasf.net.cco.model.CcoRequisitosTrabajoPK;
import sasf.net.cco.repository.CcoPostulanteDocumentosRepository;
import sasf.net.cco.repository.CcoPostulanteRepository;
import sasf.net.cco.repository.CcoRequisitosTrabajoRepository;
import sasf.net.cco.utils.EntidadCamposGenerales;
import sasf.net.cco.utils.FuncionesGenerales;
import sasf.net.cco.utils.Globales;
import sasf.net.cco.utils.ManejadorExcepciones;

@RestController
@RequestMapping("/ccoPostulanteDocumentos")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class CcoPostulanteDocumentosController {

	@Autowired
	private TransactionTemplate transactionTemplate;

	@Autowired
	private FuncionesGenerales fg;

	@Autowired
	private CcoPostulanteDocumentosRepository repository;

	@Autowired
	private CcoRequisitosTrabajoRepository requisitosTrabajoRepository;

	@Autowired
	private CcoPostulanteRepository postulanteRepository;

	@ApiOperation(value = "", authorizations = { @Authorization(value = "Autorización token módulo cco") })
	@GetMapping(path = "/{ccoPostulAgeLicencCodigo}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public List<CcoPostulanteDocumentos> consultar(@PathVariable Integer ccoPostulAgeLicencCodigo,
			@RequestParam(name = "ccoPostulCodigo", required = false) Long ccoPostulCodigo,
			@RequestParam(name = "ccoReTraCodigo", required = false) Long ccoReTraCodigo

	) {
		return repository
				.buscarPorParametros(ccoPostulAgeLicencCodigo, ccoPostulCodigo, ccoReTraCodigo, Globales.ESTADO_ANULADO)
				.stream().map(t -> {
					Optional<CcoRequisitosTrabajo> requisitoTrabajo = requisitosTrabajoRepository
							.findById(new CcoRequisitosTrabajoPK(t.getId().getCcoReTraCodigo(),
									t.getId().getCcoReTraAgeLicencCodigo()));
					if (requisitoTrabajo.isPresent()) {
						t.setTipoArchivo(requisitoTrabajo.get().getTipoArchivo());
					}
					return t;
				}).collect(Collectors.toList());
	}

	@ApiOperation(value = "", authorizations = { @Authorization(value = "Autorización token módulo cco") })
	@GetMapping(path = "/{ccoPostulAgeLicencCodigo}/{ccoPostulCodigo}/{ccoReTraAgeLicencCodigo}/{ccoReTraCodigo}", produces = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<?> consultarPorId(@PathVariable Integer ccoPostulAgeLicencCodigo,
			@PathVariable Long ccoPostulCodigo, @PathVariable Integer ccoReTraAgeLicencCodigo,
			@PathVariable Long ccoReTraCodigo) throws NotFoundException {

		Optional<CcoPostulanteDocumentos> postDoc = repository.findById(new CcoPostulanteDocumentosPK(ccoPostulCodigo,
				ccoPostulAgeLicencCodigo, ccoReTraCodigo, ccoReTraAgeLicencCodigo));
		if (!postDoc.isPresent() || postDoc.get().getEstado().equals(Globales.ESTADO_ANULADO)) {
			throw new NoResultException("Postulante documento con código de postulante " + ccoPostulCodigo
					+ " y código de requisito trabajo " + ccoReTraCodigo + " no existe");
		}

		Optional<CcoRequisitosTrabajo> requisitoTrabajo = requisitosTrabajoRepository
				.findById(new CcoRequisitosTrabajoPK(postDoc.get().getId().getCcoReTraCodigo(),
						postDoc.get().getId().getCcoReTraAgeLicencCodigo()));
		if (requisitoTrabajo.isPresent()) {
			postDoc.get().setTipoArchivo(requisitoTrabajo.get().getTipoArchivo());
		}

		return new ResponseEntity<CcoPostulanteDocumentos>(postDoc.get(), HttpStatus.OK);
	}

	@ApiOperation(value = "", authorizations = { @Authorization(value = "Autorización token módulo cco") })
	@PostMapping
	public ResponseEntity<?> ingresar(@RequestBody CcoPostulanteDocumentos transaccion, HttpServletRequest request) {

		fg.validar(transaccion, CcoPostulanteDocumentos.PostulanteDocumentoCreation.class);
		fg.validar(transaccion, EntidadCamposGenerales.CamposGeneralesCreation.class);
		transaccion.setUbicacionIngreso(request.getRemoteAddr());

		if (!postulanteRepository.existsById(new CcoPostulantePK(transaccion.getId().getCcoPostulCodigo(),
				transaccion.getId().getCcoPostulAgeLicencCodigo()))) {
			return ManejadorExcepciones.error(HttpStatus.NOT_FOUND, null,
					"Postulante con codigo " + transaccion.getId().getCcoPostulCodigo() + " y licenciatario "
							+ transaccion.getId().getCcoPostulAgeLicencCodigo() + " no existe");
		}

		if (!requisitosTrabajoRepository.existsById(new CcoRequisitosTrabajoPK(transaccion.getId().getCcoReTraCodigo(),
				transaccion.getId().getCcoReTraAgeLicencCodigo()))) {
			return ManejadorExcepciones.error(HttpStatus.NOT_FOUND, null,
					"Requisito trabajo con codigo " + transaccion.getId().getCcoReTraCodigo() + " y licenciatario "
							+ transaccion.getId().getCcoReTraAgeLicencCodigo() + " no existe");
		}

		transactionTemplate.execute(new TransactionCallbackWithoutResult() {
			@Override
			public void doInTransactionWithoutResult(TransactionStatus transactionStatus) {

				if (repository.existsById(transaccion.getId())) {
					throw new DataIntegrityViolationException("Postulante documento con código de postulante "
							+ transaccion.getId().getCcoPostulCodigo() + " con licenciatario "
							+ transaccion.getId().getCcoPostulAgeLicencCodigo() + " y código de requisito trabajo "
							+ transaccion.getId().getCcoReTraCodigo() + " con licenciatario "
							+ transaccion.getId().getCcoReTraAgeLicencCodigo() + " ya existe");
				}
				repository.save(transaccion);
			}
		});

		URI location = ServletUriComponentsBuilder.fromCurrentRequest()
				.path("/{ccoPostulAgeLicencCodigo}/{ccoPostulCodigo}/{ccoReTraAgeLicencCodigo}/{ccoReTraCodigo}")
				.buildAndExpand(transaccion.getId().getCcoPostulAgeLicencCodigo(),
						transaccion.getId().getCcoPostulCodigo(), transaccion.getId().getCcoReTraAgeLicencCodigo(),
						transaccion.getId().getCcoReTraCodigo())
				.toUri();
		return ResponseEntity.created(location).build();
	}

	@ApiOperation(value = "", authorizations = { @Authorization(value = "Autorización token módulo cco") })
	@PostMapping("/varios")
	public ResponseEntity<?> ingresarVarios(@RequestBody List<CcoPostulanteDocumentos> transacciones,
			HttpServletRequest request) {

		transactionTemplate.execute(new TransactionCallbackWithoutResult() {
			@Override
			public void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
				for (CcoPostulanteDocumentos transaccion : transacciones) {
					fg.validar(transaccion, CcoPostulanteDocumentos.PostulanteDocumentoCreation.class);
					fg.validar(transaccion, EntidadCamposGenerales.CamposGeneralesCreation.class);
					transaccion.setUbicacionIngreso(request.getRemoteAddr());

					if (!postulanteRepository.existsById(new CcoPostulantePK(transaccion.getId().getCcoPostulCodigo(),
							transaccion.getId().getCcoPostulAgeLicencCodigo()))) {
						throw new NoResultException("Postulante con codigo " + transaccion.getId().getCcoPostulCodigo()
								+ " y licenciatario " + transaccion.getId().getCcoPostulAgeLicencCodigo()
								+ " no existe");
					}

					if (!requisitosTrabajoRepository
							.existsById(new CcoRequisitosTrabajoPK(transaccion.getId().getCcoReTraCodigo(),
									transaccion.getId().getCcoReTraAgeLicencCodigo()))) {
						throw new NoResultException("Requisito trabajo con codigo "
								+ transaccion.getId().getCcoReTraCodigo() + " y licenciatario "
								+ transaccion.getId().getCcoReTraAgeLicencCodigo() + " no existe");
					}

					if (repository.existsById(transaccion.getId())) {
						throw new DataIntegrityViolationException("Postulante documento con código de postulante "
								+ transaccion.getId().getCcoPostulCodigo() + " con licenciatario "
								+ transaccion.getId().getCcoPostulAgeLicencCodigo() + " y código de requisito trabajo "
								+ transaccion.getId().getCcoReTraCodigo() + " con licenciatario "
								+ transaccion.getId().getCcoReTraAgeLicencCodigo() + " ya existe");
					}
					repository.save(transaccion);
				}
			}
		});

		return new ResponseEntity<List<CcoPostulanteDocumentos>>(transacciones, HttpStatus.CREATED);
	}

	@ApiOperation(value = "", authorizations = { @Authorization(value = "Autorización token módulo cco") })
	@PutMapping
	public ResponseEntity<?> actualizar(@RequestBody CcoPostulanteDocumentos transaccion, HttpServletRequest request) {
		fg.validar(transaccion, CcoPostulanteDocumentos.PostulanteDocumentoUpdate.class);
		fg.validar(transaccion, EntidadCamposGenerales.CamposGeneralesUpdate.class);
		transaccion.setUbicacionModificacion(request.getRemoteAddr());

		if (!repository.existsById(transaccion.getId())) {
			return ManejadorExcepciones.error(HttpStatus.NOT_FOUND, null,
					"Postulante documento con código de postulante " + transaccion.getId().getCcoPostulCodigo()
							+ " con licenciatario " + transaccion.getId().getCcoPostulAgeLicencCodigo()
							+ " y código de requisito trabajo " + transaccion.getId().getCcoReTraCodigo()
							+ " con licenciatario " + transaccion.getId().getCcoReTraAgeLicencCodigo()
							+ " no encontrado");
		}

		Optional<CcoPostulanteDocumentos> postDoc = repository.findById(transaccion.getId());

		if (transaccion.getRutaRequisito() == null) {
			transaccion.setRutaRequisito(postDoc.get().getRutaRequisito());
		}
		if (transaccion.getRutaRequisito2() == null) {
			transaccion.setRutaRequisito2(postDoc.get().getRutaRequisito2());
		}

		if (transaccion.getValorRequisito() == null) {
			transaccion.setValorRequisito(postDoc.get().getValorRequisito());
		}

		repository.save(transaccion);

		return new ResponseEntity<String>("", HttpStatus.OK);
	}

	@ApiOperation(value = "", authorizations = { @Authorization(value = "Autorización token módulo cco") })
	@PutMapping("/varios")
	public ResponseEntity<?> actualizarVarios(@RequestBody List<CcoPostulanteDocumentos> transacciones,
			HttpServletRequest request) {
		transactionTemplate.execute(new TransactionCallbackWithoutResult() {
			@Override
			public void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
				for (CcoPostulanteDocumentos transaccion : transacciones) {
					fg.validar(transaccion, CcoPostulanteDocumentos.PostulanteDocumentoUpdate.class);
					fg.validar(transaccion, EntidadCamposGenerales.CamposGeneralesUpdate.class);
					transaccion.setUbicacionModificacion(request.getRemoteAddr());

					if (!repository.existsById(transaccion.getId())) {
						throw new NoResultException("Postulante documento con código de postulante "
								+ transaccion.getId().getCcoPostulCodigo() + " con licenciatario "
								+ transaccion.getId().getCcoPostulAgeLicencCodigo() + " y código de requisito trabajo "
								+ transaccion.getId().getCcoReTraCodigo() + " con licenciatario "
								+ transaccion.getId().getCcoReTraAgeLicencCodigo() + " no encontrado");
					}

					Optional<CcoPostulanteDocumentos> postDoc = repository.findById(transaccion.getId());

					if (transaccion.getRutaRequisito() == null) {
						transaccion.setRutaRequisito(postDoc.get().getRutaRequisito());
					}
					if (transaccion.getRutaRequisito2() == null) {
						transaccion.setRutaRequisito2(postDoc.get().getRutaRequisito2());
					}

					if (transaccion.getValorRequisito() == null) {
						transaccion.setValorRequisito(postDoc.get().getValorRequisito());
					}

					repository.save(transaccion);
				}
			}
		});
		return new ResponseEntity<List<CcoPostulanteDocumentos>>(transacciones, HttpStatus.CREATED);
	}
}
