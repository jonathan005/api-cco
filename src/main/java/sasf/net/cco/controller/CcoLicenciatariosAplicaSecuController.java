package sasf.net.cco.controller;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import javassist.NotFoundException;
import sasf.net.cco.model.CcoLicenciatariosAplicaSecu;
import sasf.net.cco.model.CcoLicenciatariosAplicaSecuPK;
import sasf.net.cco.repository.CcoLicenciatariosAplicaSecuRepository;
import sasf.net.cco.utils.EntidadCamposGenerales;
import sasf.net.cco.utils.FuncionesGenerales;
import sasf.net.cco.utils.Globales;
import sasf.net.cco.utils.ManejadorExcepciones;




@RestController
@RequestMapping("/ccoLicenciatariosAplicaSecu")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class CcoLicenciatariosAplicaSecuController {
	
	@Autowired
	private TransactionTemplate transactionTemplate;
	
	@Autowired
	private FuncionesGenerales fg;
	
	@Autowired
	private CcoLicenciatariosAplicaSecuRepository repository;
	
	/*@GetMapping(produces = { MediaType.APPLICATION_JSON_VALUE })
	public List<?> consultaGeneral(){
		return repository.findAll();
	}*/
	
	@ApiOperation(value = "", authorizations = {@Authorization(value="Autorización token módulo cco")})
	@GetMapping(path = "/{codigoLicenciatario}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public List<?> consultar(@PathVariable Integer codigoLicenciatario) {
		return repository.findByIdCcoLicApCcoLicencCodigoAndEstadoOrEstado(codigoLicenciatario, 
				Globales.ESTADO_ACTIVO, 
				Globales.ESTADO_INACTIVO);
	}
	
	@ApiOperation(value = "", authorizations = {@Authorization(value="Autorización token módulo cco")})
	@GetMapping(path = "/{codigoLicenciatario}/{codigoAplicacion}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public List<?> consultarPorAplicacion(@PathVariable Integer codigoLicenciatario, @PathVariable Integer codigoAplicacion)
			throws NotFoundException {
		return repository.buscarPorAplicacion(codigoLicenciatario, codigoAplicacion);
		
	}
	
	@ApiOperation(value = "", authorizations = {@Authorization(value="Autorización token módulo cco")})
	@GetMapping(path = "/{codigoLicenciatario}/{codigoAplicacion}/{codigo}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<?> consultarPorCodigo(@PathVariable Integer codigoLicenciatario, @PathVariable Integer codigoAplicacion, @PathVariable Integer codigo) {
		Optional<CcoLicenciatariosAplicaSecu> ccoLicenciatarioAplicacion = repository.findById(new CcoLicenciatariosAplicaSecuPK(codigo,codigoLicenciatario, codigoAplicacion));
		if (!ccoLicenciatarioAplicacion.isPresent() || ccoLicenciatarioAplicacion.get().getEstado().equals(Globales.ESTADO_ANULADO)) {
			return ManejadorExcepciones.error(HttpStatus.NOT_FOUND, null, 
					"Secuencia con código "+codigo+" y licenciatario " + codigoLicenciatario+" para la aplicación " + codigoAplicacion+" no existe");
		}
		return new ResponseEntity<CcoLicenciatariosAplicaSecu>(ccoLicenciatarioAplicacion.get(),HttpStatus.OK) ;
		
	}
	
	@ApiOperation(value = "", authorizations = {@Authorization(value="Autorización token módulo cco")})
	@PostMapping
	public ResponseEntity<?> ingresar(@RequestBody CcoLicenciatariosAplicaSecu transaccion,
			HttpServletRequest request) {

		fg.validar(transaccion, CcoLicenciatariosAplicaSecu.LicencAplicaSecuCreation.class);
		fg.validar(transaccion, EntidadCamposGenerales.CamposGeneralesCreation.class);
		transaccion.setUbicacionIngreso(request.getRemoteAddr());
		
		if(repository.existsById(new CcoLicenciatariosAplicaSecuPK(transaccion.getId().getCodigo(), transaccion.getId().getCcoLicApCcoLicencCodigo(), transaccion.getId().getCcoLicApCcoAplicaCodigo()))) {
			return ManejadorExcepciones.error(HttpStatus.NOT_FOUND, null,
					"Secuencia con código "+ transaccion.getId().getCodigo()+" con licenciatario "+ transaccion.getId().getCcoLicApCcoLicencCodigo()+ " para la aplicación "+ transaccion.getId().getCcoLicApCcoAplicaCodigo()+" existe");
		}
		
		repository.save(transaccion);

		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{codigoLicenciatario}/{codigoAplicacion}/{codigo}")
				.buildAndExpand(transaccion.getId().getCcoLicApCcoLicencCodigo(), transaccion.getId().getCcoLicApCcoAplicaCodigo(),transaccion.getId().getCodigo()).toUri();
		return ResponseEntity.created(location).build();

	}
	
	@ApiOperation(value = "", authorizations = {@Authorization(value="Autorización token módulo cco")})
	@PostMapping("/varios")
	public ResponseEntity<?> ingresarVarios(@RequestBody List<CcoLicenciatariosAplicaSecu> transacciones, 
			HttpServletRequest request) {

		transactionTemplate.execute(new TransactionCallbackWithoutResult() {
			@Override
			public void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
				for (CcoLicenciatariosAplicaSecu transaccion : transacciones) {
					fg.validar(transaccion, CcoLicenciatariosAplicaSecu.LicencAplicaSecuCreation.class);
					fg.validar(transaccion, EntidadCamposGenerales.CamposGeneralesCreation.class);
					transaccion.setUbicacionIngreso(request.getRemoteAddr());
					if(repository.existsById(new CcoLicenciatariosAplicaSecuPK(transaccion.getId().getCodigo(), transaccion.getId().getCcoLicApCcoLicencCodigo(), transaccion.getId().getCcoLicApCcoAplicaCodigo()))) {
						throw new DataIntegrityViolationException(
								"Secuencia con código "+ transaccion.getId().getCodigo()+" con licenciatario "+ transaccion.getId().getCcoLicApCcoLicencCodigo()+ " para la aplicación "+ transaccion.getId().getCcoLicApCcoAplicaCodigo()+" existe.");
					}
					
					repository.save(transaccion);
				}
			}
		});
		return new ResponseEntity<List<CcoLicenciatariosAplicaSecu>>(transacciones, HttpStatus.CREATED);
	}
	
	@ApiOperation(value = "", authorizations = {@Authorization(value="Autorización token módulo cco")})
	@PutMapping
	public ResponseEntity<?> actualizar(/* @Validated(AgePerfiles.PerfilUpdate.class) */
			@RequestBody CcoLicenciatariosAplicaSecu transaccion, HttpServletRequest request) {
		fg.validar(transaccion, CcoLicenciatariosAplicaSecu.LicencAplicaSecuUpdate.class);
		fg.validar(transaccion, EntidadCamposGenerales.CamposGeneralesUpdate.class);
		transaccion.setUbicacionModificacion(request.getRemoteAddr());
		Optional<CcoLicenciatariosAplicaSecu> cooLicenAplicaSecu= repository.findById(new CcoLicenciatariosAplicaSecuPK(transaccion.getId().getCodigo(), transaccion.getId().getCcoLicApCcoLicencCodigo(), transaccion.getId().getCcoLicApCcoAplicaCodigo()));
		
		if (!cooLicenAplicaSecu.isPresent() || cooLicenAplicaSecu.get().getEstado().equals(Globales.ESTADO_ANULADO)) {
			return ManejadorExcepciones.error(HttpStatus.NOT_FOUND, null,
					"Secuencia con código "+ transaccion.getId().getCodigo()+" con licenciatario "+ transaccion.getId().getCcoLicApCcoLicencCodigo()+ " para la aplicación "+ transaccion.getId().getCcoLicApCcoAplicaCodigo()+" no encontrado");
		}
		if (transaccion.getCiclica()==null || transaccion.getCiclica().equals("")) {
			transaccion.setCiclica(cooLicenAplicaSecu.get().getCiclica());
		}
		if (transaccion.getDescripcion()==null || transaccion.getDescripcion().equals("")) {
			transaccion.setDescripcion(cooLicenAplicaSecu.get().getDescripcion());
		}
		if (transaccion.getIncrementaEn()==null || transaccion.getIncrementaEn()<=0) {
			transaccion.setIncrementaEn(cooLicenAplicaSecu.get().getIncrementaEn());
		}
		if (transaccion.getValorActual()==null || transaccion.getValorActual()<0) {
			transaccion.setValorActual(cooLicenAplicaSecu.get().getValorActual());
		}
		if (transaccion.getValorInicial()==null || transaccion.getValorInicial()<0) {
			transaccion.setValorInicial(cooLicenAplicaSecu.get().getValorInicial());
		}
		
		repository.save(transaccion);

		return new ResponseEntity<String>("", HttpStatus.OK);
	}
	
	@ApiOperation(value = "", authorizations = {@Authorization(value="Autorización token módulo cco")})
	@PutMapping("/varios")
	public ResponseEntity<?> actualizarVarios(@RequestBody List<CcoLicenciatariosAplicaSecu> transacciones,
			HttpServletRequest request) {
		transactionTemplate.execute(new TransactionCallbackWithoutResult() {
			@Override
			public void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
				for (CcoLicenciatariosAplicaSecu transaccion : transacciones) {
					fg.validar(transaccion, CcoLicenciatariosAplicaSecu.LicencAplicaSecuUpdate.class);
					fg.validar(transaccion, EntidadCamposGenerales.CamposGeneralesUpdate.class); 
					transaccion.setUbicacionModificacion(request.getRemoteAddr());
					
					Optional<CcoLicenciatariosAplicaSecu> cooLicenAplicaSecu= repository.findById(new CcoLicenciatariosAplicaSecuPK(transaccion.getId().getCodigo(), transaccion.getId().getCcoLicApCcoLicencCodigo(), transaccion.getId().getCcoLicApCcoAplicaCodigo()));
					
					if (!cooLicenAplicaSecu.isPresent() || cooLicenAplicaSecu.get().getEstado().equals(Globales.ESTADO_ANULADO)) {
						throw new DataIntegrityViolationException(
								"Secuencia con código "+ transaccion.getId().getCodigo()+" con licenciatario "+ transaccion.getId().getCcoLicApCcoLicencCodigo()+ " para la aplicación "+ transaccion.getId().getCcoLicApCcoAplicaCodigo()+" no encontrado");
					}
					if (transaccion.getCiclica()==null || transaccion.getCiclica().equals("")) {
						transaccion.setCiclica(cooLicenAplicaSecu.get().getCiclica());
					}
					if (transaccion.getDescripcion()==null || transaccion.getDescripcion().equals("")) {
						transaccion.setDescripcion(cooLicenAplicaSecu.get().getDescripcion());
					}
					if (transaccion.getIncrementaEn()==null || transaccion.getIncrementaEn()<=0) {
						transaccion.setIncrementaEn(cooLicenAplicaSecu.get().getIncrementaEn());
					}
					if (transaccion.getValorActual()==null || transaccion.getValorActual()<0) {
						transaccion.setValorActual(cooLicenAplicaSecu.get().getValorActual());
					}
					if (transaccion.getValorInicial()==null || transaccion.getValorInicial()<0) {
						transaccion.setValorInicial(cooLicenAplicaSecu.get().getValorInicial());
					}
					
					repository.save(transaccion);

				}
			}
		});
		return new ResponseEntity<List<CcoLicenciatariosAplicaSecu>>(transacciones, HttpStatus.CREATED);
	}
	
}
