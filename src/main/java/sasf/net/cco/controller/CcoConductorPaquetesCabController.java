package sasf.net.cco.controller;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import javax.persistence.NoResultException;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import javassist.NotFoundException;
import sasf.net.cco.model.CcoConductorPaquetesCab;
import sasf.net.cco.model.CcoConductorPaquetesCabPK;
import sasf.net.cco.model.CcoConductores;
import sasf.net.cco.model.CcoConductoresPK;
import sasf.net.cco.repository.CcoConductorPaquetesCabRepository;
import sasf.net.cco.repository.CcoConductoresRepository;
import sasf.net.cco.utils.EntidadCamposGenerales;
import sasf.net.cco.utils.FuncionesGenerales;
import sasf.net.cco.utils.Globales;
import sasf.net.cco.utils.ManejadorExcepciones;

@RestController
@RequestMapping("/ccoConductorPaquetesCab")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class CcoConductorPaquetesCabController {
	
	@Autowired
	private TransactionTemplate transactionTemplate;
	
	@Autowired
	private FuncionesGenerales fg;
	
	@Autowired
	private CcoConductorPaquetesCabRepository repository;
	
	@Autowired
	private CcoConductoresRepository conductorRepository;
	
	@ApiOperation(value = "", authorizations = {@Authorization(value="Autorización token módulo cco")})
	@GetMapping(path = "/{codigoLicenciatario}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public List<?> consultar(@PathVariable Integer codigoLicenciatario,
			@RequestParam(name = "codigo", required = false) Long codigo
			) {
		return repository.buscarPorParametros(codigoLicenciatario, codigo, Globales.ESTADO_ANULADO);
	}
	
	@ApiOperation(value = "", authorizations = {@Authorization(value="Autorización token módulo cco")})
	@GetMapping(path = "/{codigoLicenciatario}/{codigo}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<?> consultarPorId(@PathVariable Integer codigoLicenciatario, @PathVariable Long codigo)
			throws NotFoundException {

		Optional<CcoConductorPaquetesCab> condPaqCab = repository.findById(new CcoConductorPaquetesCabPK(codigo, codigoLicenciatario));
		if (!condPaqCab.isPresent() || condPaqCab.get().getEstado().equals(Globales.ESTADO_ANULADO)) {
			throw new NoResultException("Conductor paquete cabecera con código " + codigo + " no existe");
		}
		return new ResponseEntity<CcoConductorPaquetesCab>(condPaqCab.get(), HttpStatus.OK);
	}
	
	@ApiOperation(value = "", authorizations = {@Authorization(value="Autorización token módulo cco")})
	@PostMapping
	public ResponseEntity<?> ingresar(@RequestBody CcoConductorPaquetesCab transaccion,
			HttpServletRequest request) {

		fg.validar(transaccion, CcoConductorPaquetesCab.ConductorPaquetesCabCreation.class);
		fg.validar(transaccion, EntidadCamposGenerales.CamposGeneralesCreation.class);
		transaccion.setUbicacionIngreso(request.getRemoteAddr());
		Optional<CcoConductores> conductor=conductorRepository.findById(new CcoConductoresPK(transaccion.getCcoConducCodigo(), transaccion.getCcoConducAgeLicencCodigo()));
		if(!conductor.isPresent() || conductor.get().getEstado().equals(Globales.ESTADO_ANULADO)){
			return ManejadorExcepciones.error(HttpStatus.NOT_FOUND, null, 
					"Conductor con código "+ transaccion.getCcoConducCodigo()+ " y licenciatario "+transaccion.getCcoConducAgeLicencCodigo());
		}
		
		if(transaccion.getPaqueteHasta().floatValue()<transaccion.getPaqueteDesde().floatValue()) {
			return ManejadorExcepciones.error(HttpStatus.BAD_REQUEST, null, 
					"Paquete hasta no debe ser menor que paquete desde");
		}
		
		transactionTemplate.execute(new TransactionCallbackWithoutResult() {
			@Override
			public void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
				Long id = fg.obtenerSecuencia(transaccion.getId().getAgeLicencCodigo(), Globales.CODIGO_APLICACION,
						Globales.CODIGO_SECUENCIA_CONDUCTOR_PAQUETE_CABECERA);
				if (id == 0) {
					throw new DataIntegrityViolationException("Secuencia " + Globales.CODIGO_SECUENCIA_CONDUCTOR_PAQUETE_CABECERA
							+ " para la aplicación " + Globales.CODIGO_APLICACION + " no existe");
				}
				
				transaccion.getId().setCodigo(id);
				if(repository.existsById(transaccion.getId())){
					throw new DataIntegrityViolationException(
							"Conductor paquete cabecera con código " + id + " ya existe");
				}
				
				repository.save(transaccion);
			}
		});

		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{codigoLicenciatario}/{id}")
				.buildAndExpand(transaccion.getId().getAgeLicencCodigo(), transaccion.getId().getCodigo()).toUri();
		return ResponseEntity.created(location).build();
	}
	
	@ApiOperation(value = "", authorizations = {@Authorization(value="Autorización token módulo cco")})
	@PostMapping("/varios")
	public ResponseEntity<?> ingresarVarios(@RequestBody List<CcoConductorPaquetesCab> transacciones, 
			HttpServletRequest request) {

		transactionTemplate.execute(new TransactionCallbackWithoutResult() {
			@Override
			public void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
				for (CcoConductorPaquetesCab transaccion : transacciones) {
					fg.validar(transaccion, CcoConductorPaquetesCab.ConductorPaquetesCabCreation.class);
					fg.validar(transaccion, EntidadCamposGenerales.CamposGeneralesCreation.class);
					
					transaccion.setUbicacionIngreso(request.getRemoteAddr());
					
					Optional<CcoConductores> conductor=conductorRepository.findById(new CcoConductoresPK(transaccion.getCcoConducCodigo(), transaccion.getCcoConducAgeLicencCodigo()));
					if(!conductor.isPresent() || conductor.get().getEstado().equals(Globales.ESTADO_ANULADO)){
						throw new DataIntegrityViolationException( 
								"Conductor con código "+ transaccion.getCcoConducCodigo()+ " y licenciatario "+transaccion.getCcoConducAgeLicencCodigo());
					}
					
					if(transaccion.getPaqueteHasta().floatValue()<transaccion.getPaqueteDesde().floatValue()) {
						throw new DataIntegrityViolationException( 
								"Paquete hasta no debe ser menor que paquete desde");
					}
					
					Long id = fg.obtenerSecuencia(transaccion.getId().getAgeLicencCodigo(), Globales.CODIGO_APLICACION,
							Globales.CODIGO_SECUENCIA_CONDUCTOR_PAQUETE_CABECERA);
					if (id == 0) {
						throw new DataIntegrityViolationException("Secuencia " + Globales.CODIGO_SECUENCIA_CONDUCTOR_PAQUETE_CABECERA
								+ " para la aplicación " + Globales.CODIGO_APLICACION + " no existe");
					}
					transaccion.getId().setCodigo(id);
					if(repository.existsById(transaccion.getId())){
						throw new DataIntegrityViolationException(
								"Conductor paquete cabecera con código " + id + " ya existe");
					}
					repository.save(transaccion);
				}
			}
		});

		return new ResponseEntity<List<CcoConductorPaquetesCab>>(transacciones, HttpStatus.CREATED);
	}
	
	@ApiOperation(value = "", authorizations = {@Authorization(value="Autorización token módulo cco")})
	@PutMapping
	public ResponseEntity<?> actualizar(@RequestBody CcoConductorPaquetesCab transaccion, HttpServletRequest request) {
		fg.validar(transaccion, CcoConductorPaquetesCab.ConductorPaquetesCabUpdate.class);
		fg.validar(transaccion, EntidadCamposGenerales.CamposGeneralesUpdate.class);
		transaccion.setUbicacionModificacion(request.getRemoteAddr());
		Optional<CcoConductorPaquetesCab> condPaqCab=repository.findById(transaccion.getId());
		
		if (!repository.existsById(
				new CcoConductorPaquetesCabPK(transaccion.getId().getCodigo(), transaccion.getId().getAgeLicencCodigo()))) {
			return ManejadorExcepciones.error(HttpStatus.NOT_FOUND, null,
					"Conductor paquete cabecera con código " + transaccion.getId().getCodigo() + " no encontrado");
		}
		
		if(transaccion.getCcoConducCodigo()==null || transaccion.getCcoConducCodigo()<=0) {
			transaccion.setCcoConducCodigo(condPaqCab.get().getCcoConducCodigo());
		}
		
		if(transaccion.getCcoConducAgeLicencCodigo()==null || transaccion.getCcoConducAgeLicencCodigo()<=0) {
			transaccion.setCcoConducAgeLicencCodigo(condPaqCab.get().getCcoConducAgeLicencCodigo());
		}
		
		Optional<CcoConductores> conductor=conductorRepository.findById(new CcoConductoresPK(transaccion.getCcoConducCodigo(), transaccion.getCcoConducAgeLicencCodigo()));
		if(!conductor.isPresent() || conductor.get().getEstado().equals(Globales.ESTADO_ANULADO)){
			return ManejadorExcepciones.error(HttpStatus.NOT_FOUND, null, 
					"Conductor con código "+ transaccion.getCcoConducCodigo()+ " y licenciatario "+transaccion.getCcoConducAgeLicencCodigo());
		}
		
		if(transaccion.getValorTotalSeguro()==null || transaccion.getValorTotalSeguro().floatValue()<=0){
			transaccion.setValorTotalSeguro(condPaqCab.get().getValorTotalSeguro());
		}
		
		if(transaccion.getPaqueteDesde()==null || transaccion.getPaqueteDesde().floatValue()<=0){
			transaccion.setPaqueteDesde(condPaqCab.get().getPaqueteDesde());
		}
		
		if(transaccion.getPaqueteHasta()==null || transaccion.getPaqueteHasta().floatValue()<=0){
			transaccion.setPaqueteHasta(condPaqCab.get().getPaqueteHasta());
		}
		
		if(transaccion.getPaqueteHasta().floatValue()<transaccion.getPaqueteDesde().floatValue()) {
			return ManejadorExcepciones.error(HttpStatus.BAD_REQUEST, null, 
					"Paquete hasta no debe ser menor que paquete desde");
		}
		if(transaccion.getNumeroPaquetes()==null) {
			transaccion.setNumeroPaquetes(condPaqCab.get().getNumeroPaquetes());
		}
		if(transaccion.getValorSeguro()==null) {
			transaccion.setValorSeguro(condPaqCab.get().getValorSeguro());
		}
		if(transaccion.getNumeroVehiculos()==null) {
			transaccion.setNumeroVehiculos(condPaqCab.get().getNumeroVehiculos());
		}
		if(transaccion.getHoraSalida()==null) {
			transaccion.setHoraSalida(condPaqCab.get().getHoraSalida());
		}
		if(transaccion.getHoraEntrega()==null) {
			transaccion.setHoraEntrega(condPaqCab.get().getHoraEntrega());
		}
		if(transaccion.getRuta()==null) {
			transaccion.setRuta(condPaqCab.get().getRuta());
		}
		
		repository.save(transaccion);

		return new ResponseEntity<String>("", HttpStatus.OK);
	}
	
	@ApiOperation(value = "", authorizations = {@Authorization(value="Autorización token módulo cco")})
	@PutMapping("/varios")
	public ResponseEntity<?> actualizarVarios(@RequestBody List<CcoConductorPaquetesCab> transacciones,
			HttpServletRequest request) {
		transactionTemplate.execute(new TransactionCallbackWithoutResult() {
			@Override
			public void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
				for (CcoConductorPaquetesCab transaccion : transacciones) {
					fg.validar(transaccion, CcoConductorPaquetesCab.ConductorPaquetesCabUpdate.class);
					fg.validar(transaccion, EntidadCamposGenerales.CamposGeneralesUpdate.class);
					Optional<CcoConductorPaquetesCab> condPaqCab=repository.findById(transaccion.getId());
					
					if (!repository.existsById(transaccion.getId())) {
						throw new NoResultException("Valor seguro con código " + transaccion.getId().getCodigo() + " no encontrado");
					} 
					transaccion.setUbicacionModificacion(request.getRemoteAddr());
					
					if(transaccion.getCcoConducCodigo()==null || transaccion.getCcoConducCodigo()<=0) {
						transaccion.setCcoConducCodigo(condPaqCab.get().getCcoConducCodigo());
					}
					
					if(transaccion.getCcoConducAgeLicencCodigo()==null || transaccion.getCcoConducAgeLicencCodigo()<=0) {
						transaccion.setCcoConducAgeLicencCodigo(condPaqCab.get().getCcoConducAgeLicencCodigo());
					}
					
					Optional<CcoConductores> conductor=conductorRepository.findById(new CcoConductoresPK(transaccion.getCcoConducCodigo(), transaccion.getCcoConducAgeLicencCodigo()));
					if(!conductor.isPresent() || conductor.get().getEstado().equals(Globales.ESTADO_ANULADO)){
						throw new DataIntegrityViolationException(
								"Conductor con código "+ transaccion.getCcoConducCodigo()+ " y licenciatario "+transaccion.getCcoConducAgeLicencCodigo());
					}
					
					if(transaccion.getValorTotalSeguro()==null || transaccion.getValorTotalSeguro().floatValue()<=0){
						transaccion.setValorTotalSeguro(condPaqCab.get().getValorTotalSeguro());
					}
					
					if(transaccion.getPaqueteDesde()==null || transaccion.getPaqueteDesde().floatValue()<=0){
						transaccion.setPaqueteDesde(condPaqCab.get().getPaqueteDesde());
					}
					
					if(transaccion.getPaqueteHasta()==null || transaccion.getPaqueteHasta().floatValue()<=0){
						transaccion.setPaqueteHasta(condPaqCab.get().getPaqueteHasta());
					}
					
					if(transaccion.getPaqueteHasta().floatValue()<transaccion.getPaqueteDesde().floatValue()) {
						throw new DataIntegrityViolationException(
								"Paquete hasta es menor que paquete desde");
					}
					if(transaccion.getNumeroPaquetes()==null) {
						transaccion.setNumeroPaquetes(condPaqCab.get().getNumeroPaquetes());
					}
					if(transaccion.getValorSeguro()==null) {
						transaccion.setValorSeguro(condPaqCab.get().getValorSeguro());
					}
					if(transaccion.getNumeroVehiculos()==null) {
						transaccion.setNumeroVehiculos(condPaqCab.get().getNumeroVehiculos());
					}
					if(transaccion.getHoraSalida()==null) {
						transaccion.setHoraSalida(condPaqCab.get().getHoraSalida());
					}
					if(transaccion.getHoraEntrega()==null) {
						transaccion.setHoraEntrega(condPaqCab.get().getHoraEntrega());
					}
					if(transaccion.getRuta()==null) {
						transaccion.setRuta(condPaqCab.get().getRuta());
					}
					
					repository.save(transaccion);
				}
			}
		});
		return new ResponseEntity<List<CcoConductorPaquetesCab>>(transacciones, HttpStatus.CREATED);
	}

}
