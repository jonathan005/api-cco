package sasf.net.cco.controller;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import javax.persistence.NoResultException;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import javassist.NotFoundException;
import sasf.net.cco.model.CcoCentroAcopio;
import sasf.net.cco.model.CcoCentroAcopioPK;
import sasf.net.cco.model.CcoConductores;
import sasf.net.cco.model.CcoConductoresPK;
import sasf.net.cco.model.CcoPagos;
import sasf.net.cco.model.CcoPagosPK;
import sasf.net.cco.model.CcoRepartidores;
import sasf.net.cco.model.CcoRepartidoresPK;
import sasf.net.cco.repository.CcoCentroAcopioRepository;
import sasf.net.cco.repository.CcoConductoresRepository;
import sasf.net.cco.repository.CcoPagosRepository;
import sasf.net.cco.repository.CcoRepartidoresRepository;
import sasf.net.cco.utils.EntidadCamposGenerales;
import sasf.net.cco.utils.FuncionesGenerales;
import sasf.net.cco.utils.Globales;
import sasf.net.cco.utils.ManejadorExcepciones;

@RestController
@RequestMapping("/ccoPagos")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class CcoPagosController {
	

	@Autowired
	private TransactionTemplate transactionTemplate;
	
	@Autowired
	private FuncionesGenerales fg;
	
	@Autowired
	private CcoPagosRepository repository;
	
	@Autowired
	private CcoCentroAcopioRepository centroAcopioRepository;
	
	@Autowired
	private CcoRepartidoresRepository repartidoresRepository;
	
	@Autowired
	private CcoConductoresRepository conductoresRepository;
	
	@ApiOperation(value = "", authorizations = {@Authorization(value="Autorización token módulo cco")})
	@GetMapping(path = "/{codigoLicenciatario}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public List<?> consultar(@PathVariable Integer codigoLicenciatario,
			@RequestParam(name = "codigo", required = false) Long codigo,
			@RequestParam(name = "ccoConducCodigo", required = false) Long ccoConducCodigo,
			@RequestParam(name = "CcoRepartCodigo", required = false) Long CcoRepartCodigo,
			@RequestParam(name = "ccoCenAcCodigo", required = false) Long ccoCenAcCodigo,
			@RequestParam(name = "descripcion", required = false) String descripcion
			) {
		return repository.buscarPorParametros(codigoLicenciatario, codigo,ccoConducCodigo,CcoRepartCodigo,ccoCenAcCodigo,descripcion, Globales.ESTADO_ANULADO);
	}
	
	@ApiOperation(value = "", authorizations = {@Authorization(value="Autorización token módulo cco")})
	@GetMapping(path = "/{codigoLicenciatario}/{codigo}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<?> consultarPorId(@PathVariable Integer codigoLicenciatario, @PathVariable Long codigo)
			throws NotFoundException {

		Optional<CcoPagos> pago = repository.findById(new CcoPagosPK(codigo, codigoLicenciatario));
		if (!pago.isPresent() || pago.get().getEstado().equals(Globales.ESTADO_ANULADO)) {
			throw new NoResultException("Pago con código " + codigo + " no existe");
		}
		return new ResponseEntity<CcoPagos>(pago.get(), HttpStatus.OK);
	}
	
	@ApiOperation(value = "", authorizations = {@Authorization(value="Autorización token módulo cco")})
	@PostMapping
	public ResponseEntity<?> ingresar(@RequestBody CcoPagos transaccion,
			HttpServletRequest request) {

		fg.validar(transaccion, CcoPagos.PagosCreation.class);
		fg.validar(transaccion, EntidadCamposGenerales.CamposGeneralesCreation.class);
		transaccion.setUbicacionIngreso(request.getRemoteAddr());
		
		validacionConceptoPago(transaccion);
		 
		transactionTemplate.execute(new TransactionCallbackWithoutResult() {
			@Override
			public void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
				Long id = fg.obtenerSecuencia(transaccion.getId().getAgeLicencCodigo(), Globales.CODIGO_APLICACION,
						Globales.CODIGO_SECUENCIA_PAGO);
				if (id == 0) {
					throw new DataIntegrityViolationException("Secuencia " + Globales.CODIGO_SECUENCIA_PAGO
							+ " para la aplicación " + Globales.CODIGO_APLICACION + " no existe");
				}
				transaccion.getId().setCodigo(id);
				if(repository.existsById(transaccion.getId())){
					throw new DataIntegrityViolationException(
							"Pago con código " + id + " ya existe");
				}
				repository.save(transaccion);
			}
		});

		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{codigoLicenciatario}/{id}")
				.buildAndExpand(transaccion.getId().getAgeLicencCodigo(), transaccion.getId().getCodigo()).toUri();
		return ResponseEntity.created(location).build();
	}
	
	@ApiOperation(value = "", authorizations = {@Authorization(value="Autorización token módulo cco")})
	@PostMapping("/varios")
	public ResponseEntity<?> ingresarVarios(@RequestBody List<CcoPagos> transacciones, 
			HttpServletRequest request) {

		transactionTemplate.execute(new TransactionCallbackWithoutResult() {
			@Override
			public void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
				for (CcoPagos transaccion : transacciones) {
					fg.validar(transaccion, CcoPagos.PagosCreation.class);
					fg.validar(transaccion, EntidadCamposGenerales.CamposGeneralesCreation.class);
					transaccion.setUbicacionIngreso(request.getRemoteAddr());
					
					validacionConceptoPago(transaccion);
					
					Long id = fg.obtenerSecuencia(transaccion.getId().getAgeLicencCodigo(), Globales.CODIGO_APLICACION,
							Globales.CODIGO_SECUENCIA_PAGO);
					if (id == 0) {
						throw new DataIntegrityViolationException("Secuencia " + Globales.CODIGO_SECUENCIA_PAGO
								+ " para la aplicación " + Globales.CODIGO_APLICACION + " no existe");
					}
					transaccion.getId().setCodigo(id);
					if(repository.existsById(transaccion.getId())){
						throw new DataIntegrityViolationException(
								"Pago con código " + id + " ya existe");
					}
					repository.save(transaccion);
				}
			}
		});

		return new ResponseEntity<List<CcoPagos>>(transacciones, HttpStatus.CREATED);
	}
	
	@ApiOperation(value = "", authorizations = {@Authorization(value="Autorización token módulo cco")})
	@PutMapping
	public ResponseEntity<?> actualizar(@RequestBody CcoPagos transaccion, HttpServletRequest request) {
		fg.validar(transaccion, CcoPagos.PagosUpdate.class);
		fg.validar(transaccion, EntidadCamposGenerales.CamposGeneralesUpdate.class);
		transaccion.setUbicacionModificacion(request.getRemoteAddr());
		Optional<CcoPagos> pago=repository.findById(transaccion.getId());
		
		if (!repository.existsById(
				new CcoPagosPK(transaccion.getId().getCodigo(), transaccion.getId().getAgeLicencCodigo()))) {
			return ManejadorExcepciones.error(HttpStatus.NOT_FOUND, null,
					"Pago con código " + transaccion.getId().getCodigo() + " no encontrado");
		}
		
		if(transaccion.getDescripcion()==null) {
			transaccion.setDescripcion(pago.get().getDescripcion());
		}
		if(transaccion.getValor()==null) {
			transaccion.setValor(pago.get().getValor());
		}
		
		if(transaccion.getCcoCenAcCodigo()==null 
				&& transaccion.getCcoCenAcAgeLicencCodigo()==null
				&& transaccion.getCcoConducCodigo()==null
				&& transaccion.getCcoConducAgeLicencCodigo()==null
				&& transaccion.getCcoRepartCodigo()==null
				&& transaccion.getCcoRepartAgeLicencCodigo()==null
				) {
			transaccion.setCcoCenAcCodigo(pago.get().getCcoCenAcCodigo());
			transaccion.setCcoCenAcAgeLicencCodigo(pago.get().getCcoCenAcAgeLicencCodigo());
			transaccion.setCcoConducCodigo(pago.get().getCcoConducCodigo());
			transaccion.setCcoConducAgeLicencCodigo(pago.get().getCcoConducAgeLicencCodigo());
			transaccion.setCcoRepartCodigo(pago.get().getCcoRepartCodigo());
			transaccion.setCcoRepartAgeLicencCodigo(pago.get().getCcoRepartAgeLicencCodigo());
		}
		
			
		validacionConceptoPago(transaccion);
		
		
		
		
		repository.save(transaccion);

		return new ResponseEntity<String>("", HttpStatus.OK);
	}
	
	@ApiOperation(value = "", authorizations = {@Authorization(value="Autorización token módulo cco")})
	@PutMapping("/varios")
	public ResponseEntity<?> actualizarVarios(@RequestBody List<CcoPagos> transacciones,
			HttpServletRequest request) {
		transactionTemplate.execute(new TransactionCallbackWithoutResult() {
			@Override
			public void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
				for (CcoPagos transaccion : transacciones) {
					fg.validar(transaccion, CcoPagos.PagosUpdate.class);
					fg.validar(transaccion, EntidadCamposGenerales.CamposGeneralesUpdate.class);
					Optional<CcoPagos> pago=repository.findById(transaccion.getId());
					
					if (!repository.existsById(transaccion.getId())) {
						throw new NoResultException("Pago con código " + transaccion.getId().getCodigo() + " no encontrado");
					} 
					transaccion.setUbicacionModificacion(request.getRemoteAddr());
					
					if(transaccion.getDescripcion()==null) {
						transaccion.setDescripcion(pago.get().getDescripcion());
					}
					if(transaccion.getValor()==null) {
						transaccion.setValor(pago.get().getValor());
					}
					
					if(transaccion.getCcoCenAcCodigo()==null 
							&& transaccion.getCcoCenAcAgeLicencCodigo()==null
							&& transaccion.getCcoConducCodigo()==null
							&& transaccion.getCcoConducAgeLicencCodigo()==null
							&& transaccion.getCcoRepartCodigo()==null
							&& transaccion.getCcoRepartAgeLicencCodigo()==null
							) {
						transaccion.setCcoCenAcCodigo(pago.get().getCcoCenAcCodigo());
						transaccion.setCcoCenAcAgeLicencCodigo(pago.get().getCcoCenAcAgeLicencCodigo());
						transaccion.setCcoConducCodigo(pago.get().getCcoConducCodigo());
						transaccion.setCcoConducAgeLicencCodigo(pago.get().getCcoConducAgeLicencCodigo());
						transaccion.setCcoRepartCodigo(pago.get().getCcoRepartCodigo());
						transaccion.setCcoRepartAgeLicencCodigo(pago.get().getCcoRepartAgeLicencCodigo());
					}
					
						
					validacionConceptoPago(transaccion);
					
					repository.save(transaccion);
				}
			}
		});
		return new ResponseEntity<List<CcoPagos>>(transacciones, HttpStatus.CREATED);
	}
	
	void validacionConceptoPago(CcoPagos transaccion){
		if((transaccion.getCcoCenAcCodigo()!=null &&transaccion.getCcoCenAcAgeLicencCodigo()!=null)
				&& (transaccion.getCcoConducCodigo()==null && transaccion.getCcoConducAgeLicencCodigo()==null
				 &&  transaccion.getCcoRepartCodigo()==null && transaccion.getCcoRepartAgeLicencCodigo()==null)) {
			Optional<CcoCentroAcopio> centroAcopio =centroAcopioRepository.findById(new CcoCentroAcopioPK(transaccion.getCcoCenAcCodigo(), transaccion.getCcoCenAcAgeLicencCodigo()));
			if(!centroAcopio.isPresent() || centroAcopio.get().getEstado().equals(Globales.ESTADO_ANULADO)) {
				throw new NoResultException(
						"Centro acopio con código "+transaccion.getCcoCenAcCodigo()+" y licenciatario "+transaccion.getCcoCenAcAgeLicencCodigo()+" no existe");
			}
		}
		else if((transaccion.getCcoConducCodigo()!=null &&transaccion.getCcoConducAgeLicencCodigo()!=null)
				&& (transaccion.getCcoCenAcCodigo()==null && transaccion.getCcoCenAcAgeLicencCodigo()==null
				 &&  transaccion.getCcoRepartCodigo()==null && transaccion.getCcoRepartAgeLicencCodigo()==null)) {
			Optional<CcoConductores> conductor =conductoresRepository.findById(new CcoConductoresPK(transaccion.getCcoConducCodigo(), transaccion.getCcoConducAgeLicencCodigo()));
			if(!conductor.isPresent() || conductor.get().getEstado().equals(Globales.ESTADO_ANULADO)) {
				throw new NoResultException(
						"Conductor con código "+transaccion.getCcoConducCodigo()+" y licenciatario "+transaccion.getCcoConducAgeLicencCodigo()+" no existe");
			}
		}
		else if((transaccion.getCcoRepartCodigo()!=null &&transaccion.getCcoRepartAgeLicencCodigo()!=null)
				&& (transaccion.getCcoCenAcCodigo()==null && transaccion.getCcoCenAcAgeLicencCodigo()==null
				 &&  transaccion.getCcoConducCodigo()==null && transaccion.getCcoConducAgeLicencCodigo()==null)) {
			Optional<CcoRepartidores> repartidor =repartidoresRepository.findById(new CcoRepartidoresPK(transaccion.getCcoRepartCodigo(), transaccion.getCcoRepartAgeLicencCodigo()));
			if(!repartidor.isPresent() || repartidor.get().getEstado().equals(Globales.ESTADO_ANULADO)) {
				throw new NoResultException(
						"Repartidor con código "+transaccion.getCcoRepartCodigo()+" y licenciatario "+transaccion.getCcoRepartAgeLicencCodigo()+" no existe");
			}
		}
		else {
			if(transaccion.getCcoCenAcCodigo()==null 
					&& transaccion.getCcoCenAcAgeLicencCodigo()==null
					&& transaccion.getCcoConducCodigo()==null
					&& transaccion.getCcoConducAgeLicencCodigo()==null
					&& transaccion.getCcoRepartCodigo()==null
					&& transaccion.getCcoRepartAgeLicencCodigo()==null
					) {}
			else {
				throw new DataIntegrityViolationException(
						"Debe seleccionar solo un concepto de pago");
			}
		}
		
		/*if(transaccion.getCcoCenAcCodigo()!=null && transaccion.getCcoCenAcAgeLicencCodigo()!=null) {
		Optional<CcoCentroAcopio> centroAcopio =centroAcopioRepository.findById(new CcoCentroAcopioPK(transaccion.getCcoCenAcCodigo(), transaccion.getCcoCenAcAgeLicencCodigo()));
		if(!centroAcopio.isPresent() || centroAcopio.get().getEstado().equals(Globales.ESTADO_ANULADO)) {
			throw new NoResultException(
					"Centro acopio con código "+transaccion.getCcoCenAcCodigo()+" y licenciatario "+transaccion.getCcoCenAcAgeLicencCodigo()+" no existe");
		}		
	}else if(transaccion.getCcoCenAcAgeLicencCodigo()!=null || transaccion.getCcoCenAcCodigo()!=null) {
		throw new DataIntegrityViolationException(
				"Debe ingresar los códigos correspondientes al servicio de centro de acopio");
	}
	
	if(transaccion.getCcoConducCodigo()!=null && transaccion.getCcoConducAgeLicencCodigo()!=null) {
		Optional<CcoConductores> conductor =conductoresRepository.findById(new CcoConductoresPK(transaccion.getCcoConducCodigo(), transaccion.getCcoConducAgeLicencCodigo()));
		if(!conductor.isPresent() || conductor.get().getEstado().equals(Globales.ESTADO_ANULADO)) {
			throw new NoResultException(
					"Conductor con código "+transaccion.getCcoConducCodigo()+" y licenciatario "+transaccion.getCcoConducAgeLicencCodigo()+" no existe");
		}		
	}else if(transaccion.getCcoConducAgeLicencCodigo()!=null || transaccion.getCcoConducCodigo()!=null) {
		throw new DataIntegrityViolationException(
				"Debe ingresar los códigos correspondientes al servicio de conductores");
	}
	
	if(transaccion.getCcoRepartCodigo()!=null && transaccion.getCcoRepartAgeLicencCodigo()!=null) {
		Optional<CcoRepartidores> repartidor =repartidoresRepository.findById(new CcoRepartidoresPK(transaccion.getCcoRepartCodigo(), transaccion.getCcoRepartAgeLicencCodigo()));
		if(!repartidor.isPresent() || repartidor.get().getEstado().equals(Globales.ESTADO_ANULADO)) {
			throw new NoResultException(
					"Repartidor con código "+transaccion.getCcoRepartCodigo()+" y licenciatario "+transaccion.getCcoRepartAgeLicencCodigo()+" no existe");
		}		
	}else if(transaccion.getCcoRepartAgeLicencCodigo()!=null || transaccion.getCcoRepartCodigo()!=null) {
		throw new DataIntegrityViolationException(
				"Debe ingresar los códigos correspondientes al servicio de repartidores");
	}*/
	}

}
