package sasf.net.cco.controller;

import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.persistence.NoResultException;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import javassist.NotFoundException;
import sasf.net.cco.model.CcoPaqueteEstado;
import sasf.net.cco.model.CcoPaqueteEstadoPK;
import sasf.net.cco.model.CcoPaquetePK;
import sasf.net.cco.repository.CcoPaqueteEstadoRepository;
import sasf.net.cco.repository.CcoPaqueteRespository;
import sasf.net.cco.utils.EntidadCamposGenerales;
import sasf.net.cco.utils.FuncionesGenerales;
import sasf.net.cco.utils.Globales;
import sasf.net.cco.utils.ManejadorExcepciones;

@RestController
@RequestMapping("/ccoPaqueteEstado")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class CcoPaqueteEstadoController {
	
	@Autowired
	private TransactionTemplate transactionTemplate;
	
	@Autowired
	private FuncionesGenerales fg;
	
	@Autowired
	private CcoPaqueteEstadoRepository repository;
	
	@Autowired
	private CcoPaqueteRespository paqueteRepository;
	
	@ApiOperation(value = "", authorizations = {@Authorization(value="Autorización token módulo cco")})
	@GetMapping(path = "/{codigoLicenciatario}/{codigoPaquete}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public List<?> consultar(@PathVariable Integer codigoLicenciatario,
			@PathVariable Long codigoPaquete,
			@RequestParam(value = "fechaDesde", required = false) @DateTimeFormat(iso = ISO.DATE, pattern = "yyyy-MM-dd") Date fechaDesde,
			@RequestParam(value = "fechaHasta", required = false) @DateTimeFormat(iso = ISO.DATE, pattern = "yyyy-MM-dd") Date fechaHasta,
			@RequestParam(name = "estadoPaquete", required = false) String estadoPaquete
			) {
		return repository.buscarPorParametros(codigoLicenciatario,codigoPaquete,estadoPaquete,fechaDesde,fechaHasta,Globales.ESTADO_ANULADO);
	}
	
	@ApiOperation(value = "", authorizations = {@Authorization(value="Autorización token módulo cco")})
	@GetMapping(path = "/{cooPedidoAgeLicencCodigo}/{cooPedidoCodigo}/{codigo}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<?> consultarPorId(@PathVariable Integer cooPedidoAgeLicencCodigo,@PathVariable Long cooPedidoCodigo, @PathVariable Long codigo)
			throws NotFoundException {
		
		Optional<CcoPaqueteEstado> paqueteEstado = repository.findById(new CcoPaqueteEstadoPK(codigo, cooPedidoCodigo, cooPedidoAgeLicencCodigo) );
		if (!paqueteEstado.isPresent() && paqueteEstado.get().getEstado().equals(Globales.ESTADO_ANULADO)) {
			throw new NoResultException("Paquete estado con código " + codigo + " no existe");
		}
		return new ResponseEntity<CcoPaqueteEstado>(paqueteEstado.get(), HttpStatus.OK);
	}
	
	@ApiOperation(value = "", authorizations = {@Authorization(value="Autorización token módulo cco")})
	@PostMapping
	public ResponseEntity<?> ingresar(@RequestBody CcoPaqueteEstado transaccion,
			HttpServletRequest request) {

		fg.validar(transaccion, CcoPaqueteEstado.PaqueteEstadoCreation.class);
		fg.validar(transaccion, EntidadCamposGenerales.CamposGeneralesCreation.class);
		transaccion.setUbicacionIngreso(request.getRemoteAddr());
		
		if(!paqueteRepository.existsById(new CcoPaquetePK(transaccion.getId().getCooPedidoCodigo(), transaccion.getId().getCooPedidoAgeLicencCodigo()))) {
			return ManejadorExcepciones.error(HttpStatus.NOT_FOUND, null,
					"Paquete con código " + transaccion.getId().getCooPedidoCodigo() +" Y licenciatario "+transaccion.getId().getCooPedidoAgeLicencCodigo() + " no encontrado");
		}
		
		transactionTemplate.execute(new TransactionCallbackWithoutResult() {
			@Override
			public void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
				
				Long id = fg.obtenerSecuencia(transaccion.getId().getCooPedidoAgeLicencCodigo(), Globales.CODIGO_APLICACION,
						Globales.CODIGO_SECUENCIA_PAQUETE_ESTADO);
				
				if (id == 0) {
					throw new DataIntegrityViolationException("Secuencia primaria " + Globales.CODIGO_SECUENCIA_PAQUETE_ESTADO
							+ " para la aplicación " + Globales.CODIGO_APLICACION + " no existe");
				}
				transaccion.getId().setCodigo(id);
				if(repository.existsById(transaccion.getId())){
					throw new DataIntegrityViolationException(
							"Paquete estado con código " + id + " ya existe");
				}
				repository.save(transaccion);
			}
		});

		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{cooPedidoAgeLicencCodigo}/{cooPedidoCodigo}/{codigo}")
				.buildAndExpand(transaccion.getId().getCooPedidoAgeLicencCodigo(), transaccion.getId().getCooPedidoCodigo(),transaccion.getId().getCodigo()).toUri();
		return ResponseEntity.created(location).build();
	}
	
	@ApiOperation(value = "", authorizations = {@Authorization(value="Autorización token módulo cco")})
	@PostMapping("/varios")
	public ResponseEntity<?> ingresarVarios(@RequestBody List<CcoPaqueteEstado> transacciones, 
			HttpServletRequest request) {
		
		
		transactionTemplate.execute(new TransactionCallbackWithoutResult() {
			@Override
			public void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
				for (CcoPaqueteEstado transaccion : transacciones) {
					fg.validar(transaccion, CcoPaqueteEstado.PaqueteEstadoCreation.class);
					fg.validar(transaccion, EntidadCamposGenerales.CamposGeneralesCreation.class);
					transaccion.setUbicacionIngreso(request.getRemoteAddr());
					
					if(!paqueteRepository.existsById(new CcoPaquetePK(transaccion.getId().getCooPedidoCodigo(), transaccion.getId().getCooPedidoAgeLicencCodigo()))) {
						throw new NoResultException(
								"Paquete con código " + transaccion.getId().getCooPedidoCodigo() +" Y licenciatario "+transaccion.getId().getCooPedidoAgeLicencCodigo() + " no encontrado");
					}
					
					
					Long id = fg.obtenerSecuencia(transaccion.getId().getCooPedidoAgeLicencCodigo(), Globales.CODIGO_APLICACION,
							Globales.CODIGO_SECUENCIA_PAQUETE_ESTADO);
					
					if (id == 0) {
						throw new DataIntegrityViolationException("Secuencia primaria" + Globales.CODIGO_SECUENCIA_PAQUETE_ESTADO
								+ " para la aplicación " + Globales.CODIGO_APLICACION + " no existe");
					}
					transaccion.getId().setCodigo(id);
					if(repository.existsById(transaccion.getId())){
						throw new DataIntegrityViolationException(
								"Paquete estado con código " + id + " ya existe");
					}
					repository.save(transaccion);
				}
			}
		});

		return new ResponseEntity<List<CcoPaqueteEstado>>(transacciones, HttpStatus.CREATED);
	}
	
	@ApiOperation(value = "", authorizations = {@Authorization(value="Autorización token módulo cco")})
	@PutMapping
	public ResponseEntity<?> actualizar(@RequestBody CcoPaqueteEstado transaccion, HttpServletRequest request) {
		fg.validar(transaccion, CcoPaqueteEstado.PaqueteEstadoUpdate.class);
		fg.validar(transaccion, EntidadCamposGenerales.CamposGeneralesUpdate.class);
		transaccion.setUbicacionModificacion(request.getRemoteAddr());
		Optional<CcoPaqueteEstado> paqueteEstado=repository.findById(transaccion.getId());
		
		if (!repository.existsById(transaccion.getId())) {
			return ManejadorExcepciones.error(HttpStatus.NOT_FOUND, null,
					"Paquete estado con código " + transaccion.getId().getCodigo() + " no encontrado");
		}
		
		if(transaccion.getPaqueteEstado()==null || transaccion.getPaqueteEstado().equals("")) {
			transaccion.setPaqueteEstado(paqueteEstado.get().getPaqueteEstado());
		}
		if(transaccion.getFechaPaqueteEstado()==null ) {
			transaccion.setFechaPaqueteEstado(paqueteEstado.get().getFechaPaqueteEstado());
		}
		
		repository.save(transaccion);

		return new ResponseEntity<String>("", HttpStatus.OK);
	}
	
	@ApiOperation(value = "", authorizations = {@Authorization(value="Autorización token módulo cco")})
	@PutMapping("/varios")
	public ResponseEntity<?> actualizarVarios(@RequestBody List<CcoPaqueteEstado> transacciones,
			HttpServletRequest request) {
		transactionTemplate.execute(new TransactionCallbackWithoutResult() {
			@Override
			public void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
				for (CcoPaqueteEstado transaccion : transacciones) {
					fg.validar(transaccion, CcoPaqueteEstado.PaqueteEstadoUpdate.class);
					fg.validar(transaccion, EntidadCamposGenerales.CamposGeneralesUpdate.class);
					transaccion.setUbicacionModificacion(request.getRemoteAddr());
					Optional<CcoPaqueteEstado> paqueteEstado=repository.findById(transaccion.getId());
					
					if (!repository.existsById(transaccion.getId())) {
						throw new NoResultException("Paquete estado con código " + transaccion.getId().getCodigo() + " no encontrado");
					}
					
					if(transaccion.getPaqueteEstado()==null || transaccion.getPaqueteEstado().equals("")) {
						transaccion.setPaqueteEstado(paqueteEstado.get().getPaqueteEstado());
					}
					if(transaccion.getFechaPaqueteEstado()==null ) {
						transaccion.setFechaPaqueteEstado(paqueteEstado.get().getFechaPaqueteEstado());
					}
					
				
					repository.save(transaccion);
				}
			}
		});
		return new ResponseEntity<List<CcoPaqueteEstado>>(transacciones, HttpStatus.CREATED);
	}
	
}


