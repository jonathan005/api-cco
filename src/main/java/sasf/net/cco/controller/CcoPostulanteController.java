package sasf.net.cco.controller;

import java.net.URI;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.NoResultException;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import javassist.NotFoundException;
import sasf.net.cco.model.CcoPostulante;
import sasf.net.cco.model.CcoPostulanteDocumentos;
import sasf.net.cco.model.CcoPostulantePK;
import sasf.net.cco.model.CcoPuestosTrabajoPK;
import sasf.net.cco.repository.CcoPostulanteDocumentosRepository;
import sasf.net.cco.repository.CcoPostulanteRepository;
import sasf.net.cco.repository.CcoPuestosTrabajoRepository;
import sasf.net.cco.utils.EntidadCamposGenerales;
import sasf.net.cco.utils.FuncionesGenerales;
import sasf.net.cco.utils.Globales;
import sasf.net.cco.utils.ManejadorExcepciones;

@RestController
@RequestMapping("/ccoPostulante")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class CcoPostulanteController {

	@Autowired
	private TransactionTemplate transactionTemplate;

	@Autowired
	private FuncionesGenerales fg;

	@Autowired
	private CcoPostulanteRepository repository;

	@Autowired
	private CcoPuestosTrabajoRepository puestoTrabajoRepository;

	@Autowired
	// private CcoPostulanteDocumentosRepository postulanteDocumentoRepository;
	private CcoPostulanteDocumentosController postulanteDocumentoController;

	@ApiOperation(value = "", authorizations = { @Authorization(value = "Autorización token módulo cco") })
	@GetMapping(path = "/{codigoLicenciatario}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public List<?> consultar(@PathVariable Integer codigoLicenciatario,
			@RequestParam(name = "codigo", required = false) Long codigo,
			@RequestParam(name = "ccoPuTraCodigo", required = false) Long ccoPuTraCodigo,
			@RequestParam(name = "estadoPostulante", required = false) String estadoPostulante,
			@RequestParam(name = "correoElectronico", required = false) String correoElectronico) {
		return repository.buscarPorParametros(codigoLicenciatario, codigo, ccoPuTraCodigo, estadoPostulante,
				correoElectronico, Globales.ESTADO_ANULADO).stream().map(t -> {
					List<CcoPostulanteDocumentos> postulanteDocumentos = postulanteDocumentoController
							.consultar(t.getId().getAgeLicencCodigo(), t.getId().getCodigo(), null); // postulanteDocumentoRepository.buscarPorPostulante(t.getId().getAgeLicencCodigo(),
																										// t.getId().getCodigo(),
																										// Globales.ESTADO_ANULADO);
					t.setPostulanteDocumentos(postulanteDocumentos);
					return t;
				}).collect(Collectors.toList());
	}

	@ApiOperation(value = "", authorizations = { @Authorization(value = "Autorización token módulo cco") })
	@GetMapping(path = "/{codigoLicenciatario}/{codigo}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<?> consultarPorId(@PathVariable Integer codigoLicenciatario, @PathVariable Long codigo)
			throws NotFoundException {

		Optional<CcoPostulante> postulante = repository.findById(new CcoPostulantePK(codigo, codigoLicenciatario));
		if (!postulante.isPresent() || postulante.get().getEstado().equals(Globales.ESTADO_ANULADO)) {
			throw new NoResultException("Requisito de trabajo con código " + codigo + " no existe");
		}

		List<CcoPostulanteDocumentos> postulanteDocumentos = postulanteDocumentoController
				.consultar(postulante.get().getId().getAgeLicencCodigo(), postulante.get().getId().getCodigo(), null); // postulanteDocumentoRepository.buscarPorPostulante(postulante.get().getId().getAgeLicencCodigo(),
																														// postulante.get().getId().getCodigo(),
																														// Globales.ESTADO_ANULADO);
		postulante.get().setPostulanteDocumentos(postulanteDocumentos);

		return new ResponseEntity<CcoPostulante>(postulante.get(), HttpStatus.OK);
	}

	@ApiOperation(value = "", authorizations = { @Authorization(value = "Autorización token módulo cco") })
	@PostMapping
	public ResponseEntity<?> ingresar(@RequestBody CcoPostulante transaccion, HttpServletRequest request) {

		fg.validar(transaccion, CcoPostulante.PostulanteCreation.class);
		fg.validar(transaccion, EntidadCamposGenerales.CamposGeneralesCreation.class);
		transaccion.setUbicacionIngreso(request.getRemoteAddr());

		if (!puestoTrabajoRepository.existsById(
				new CcoPuestosTrabajoPK(transaccion.getCcoPuTraCodigo(), transaccion.getCcoPuTraAgeLicencCodigo()))) {
			return ManejadorExcepciones.error(HttpStatus.NOT_FOUND, null, "Puesto de trabajo con código "
					+ transaccion.getCcoPuTraCodigo() + " y licenciatario " + transaccion.getCcoPuTraAgeLicencCodigo());
		}

		transactionTemplate.execute(new TransactionCallbackWithoutResult() {
			@Override
			public void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
				Long id = fg.obtenerSecuencia(transaccion.getId().getAgeLicencCodigo(), Globales.CODIGO_APLICACION,
						Globales.CODIGO_SECUENCIA_POSTULANTE);
				if (id == 0) {
					throw new DataIntegrityViolationException("Secuencia " + Globales.CODIGO_SECUENCIA_POSTULANTE
							+ " para la aplicación " + Globales.CODIGO_APLICACION + " no existe");
				}
				transaccion.getId().setCodigo(id);
				if (repository.existsById(transaccion.getId())) {
					throw new DataIntegrityViolationException("Postulante con código " + id + " ya existe");
				}
				repository.save(transaccion);
			}
		});

		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{codigoLicenciatario}/{id}")
				.buildAndExpand(transaccion.getId().getAgeLicencCodigo(), transaccion.getId().getCodigo()).toUri();
		return ResponseEntity.created(location).body(transaccion);
	}

	@ApiOperation(value = "", authorizations = { @Authorization(value = "Autorización token módulo cco") })
	@PostMapping("/varios")
	public ResponseEntity<?> ingresarVarios(@RequestBody List<CcoPostulante> transacciones,
			HttpServletRequest request) {

		transactionTemplate.execute(new TransactionCallbackWithoutResult() {
			@Override
			public void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
				for (CcoPostulante transaccion : transacciones) {
					fg.validar(transaccion, CcoPostulante.PostulanteCreation.class);
					fg.validar(transaccion, EntidadCamposGenerales.CamposGeneralesCreation.class);
					transaccion.setUbicacionIngreso(request.getRemoteAddr());

					if (!puestoTrabajoRepository.existsById(new CcoPuestosTrabajoPK(transaccion.getCcoPuTraCodigo(),
							transaccion.getCcoPuTraAgeLicencCodigo()))) {
						throw new NoResultException("Puesto de trabajo con código " + transaccion.getCcoPuTraCodigo()
								+ " y licenciatario " + transaccion.getCcoPuTraAgeLicencCodigo());
					}

					Long id = fg.obtenerSecuencia(transaccion.getId().getAgeLicencCodigo(), Globales.CODIGO_APLICACION,
							Globales.CODIGO_SECUENCIA_POSTULANTE);
					if (id == 0) {
						throw new DataIntegrityViolationException("Secuencia " + Globales.CODIGO_SECUENCIA_POSTULANTE
								+ " para la aplicación " + Globales.CODIGO_APLICACION + " no existe");
					}
					transaccion.getId().setCodigo(id);
					if (repository.existsById(transaccion.getId())) {
						throw new DataIntegrityViolationException("Postulante con código " + id + " ya existe");
					}
					repository.save(transaccion);
				}
			}
		});

		return new ResponseEntity<List<CcoPostulante>>(transacciones, HttpStatus.CREATED);
	}

	@ApiOperation(value = "", authorizations = { @Authorization(value = "Autorización token módulo cco") })
	@PutMapping
	public ResponseEntity<?> actualizar(@RequestBody CcoPostulante transaccion, HttpServletRequest request) {
		fg.validar(transaccion, CcoPostulante.PostulanteUpdate.class);
		fg.validar(transaccion, EntidadCamposGenerales.CamposGeneralesUpdate.class);
		transaccion.setUbicacionModificacion(request.getRemoteAddr());
		Optional<CcoPostulante> postulante = repository.findById(transaccion.getId());

		if (!repository.existsById(
				new CcoPostulantePK(transaccion.getId().getCodigo(), transaccion.getId().getAgeLicencCodigo()))) {
			return ManejadorExcepciones.error(HttpStatus.NOT_FOUND, null,
					"Postulante con código " + transaccion.getId().getCodigo() + " no encontrado");
		}

		if (transaccion.getCcoPuTraCodigo() == null) {
			transaccion.setCcoPuTraCodigo(postulante.get().getCcoPuTraCodigo());
		}
		if (transaccion.getCcoPuTraAgeLicencCodigo() == null) {
			transaccion.setCcoPuTraAgeLicencCodigo(postulante.get().getCcoPuTraAgeLicencCodigo());
		}

		if (!puestoTrabajoRepository.existsById(
				new CcoPuestosTrabajoPK(transaccion.getCcoPuTraCodigo(), transaccion.getCcoPuTraAgeLicencCodigo()))) {
			return ManejadorExcepciones.error(HttpStatus.NOT_FOUND, null, "Puesto de trabajo con código "
					+ transaccion.getCcoPuTraCodigo() + " y licenciatario " + transaccion.getCcoPuTraAgeLicencCodigo());
		}

		if (transaccion.getEstadoPostulante() == null) {
			transaccion.setEstadoPostulante(postulante.get().getEstadoPostulante());
		}

		if (transaccion.getNombre() == null) {
			transaccion.setNombre(postulante.get().getNombre());
		}
		if (transaccion.getApellido() == null) {
			transaccion.setApellido(postulante.get().getApellido());
		}
		if (transaccion.getTelefono() == null) {
			transaccion.setTelefono(postulante.get().getTelefono());
		}
		if (transaccion.getCorreoElectronico() == null) {
			transaccion.setCorreoElectronico(postulante.get().getCorreoElectronico());
		}
		if (transaccion.getDireccion() == null) {
			transaccion.setDireccion(postulante.get().getDireccion());
		}

		repository.save(transaccion);

		return new ResponseEntity<String>("", HttpStatus.OK);
	}

	@ApiOperation(value = "", authorizations = { @Authorization(value = "Autorización token módulo cco") })
	@PutMapping("/varios")
	public ResponseEntity<?> actualizarVarios(@RequestBody List<CcoPostulante> transacciones,
			HttpServletRequest request) {
		transactionTemplate.execute(new TransactionCallbackWithoutResult() {
			@Override
			public void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
				for (CcoPostulante transaccion : transacciones) {
					fg.validar(transaccion, CcoPostulante.PostulanteUpdate.class);
					fg.validar(transaccion, EntidadCamposGenerales.CamposGeneralesUpdate.class);
					Optional<CcoPostulante> postulante = repository.findById(transaccion.getId());

					if (!repository.existsById(transaccion.getId())) {
						throw new NoResultException(
								"Postulante con código " + transaccion.getId().getCodigo() + " no encontrado");
					}
					transaccion.setUbicacionModificacion(request.getRemoteAddr());

					if (transaccion.getCcoPuTraCodigo() == null) {
						transaccion.setCcoPuTraCodigo(postulante.get().getCcoPuTraCodigo());
					}
					if (transaccion.getCcoPuTraAgeLicencCodigo() == null) {
						transaccion.setCcoPuTraAgeLicencCodigo(postulante.get().getCcoPuTraAgeLicencCodigo());
					}

					if (!puestoTrabajoRepository.existsById(new CcoPuestosTrabajoPK(transaccion.getCcoPuTraCodigo(),
							transaccion.getCcoPuTraAgeLicencCodigo()))) {
						throw new NoResultException("Puesto de trabajo con código " + transaccion.getCcoPuTraCodigo()
								+ " y licenciatario " + transaccion.getCcoPuTraAgeLicencCodigo());
					}

					if (transaccion.getEstadoPostulante() == null) {
						transaccion.setEstadoPostulante(postulante.get().getEstadoPostulante());
					}

					if (transaccion.getNombre() == null) {
						transaccion.setNombre(postulante.get().getNombre());
					}
					if (transaccion.getApellido() == null) {
						transaccion.setApellido(postulante.get().getApellido());
					}
					if (transaccion.getTelefono() == null) {
						transaccion.setTelefono(postulante.get().getTelefono());
					}
					if (transaccion.getCorreoElectronico() == null) {
						transaccion.setCorreoElectronico(postulante.get().getCorreoElectronico());
					}
					if (transaccion.getDireccion() == null) {
						transaccion.setDireccion(postulante.get().getDireccion());
					}

					repository.save(transaccion);
				}
			}
		});
		return new ResponseEntity<List<CcoPostulante>>(transacciones, HttpStatus.CREATED);
	}

}
