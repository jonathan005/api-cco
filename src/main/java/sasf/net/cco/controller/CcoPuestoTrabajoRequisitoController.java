package sasf.net.cco.controller;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import javax.persistence.NoResultException;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import javassist.NotFoundException;
import sasf.net.cco.model.CcoPuestoTrabajoRequisito;
import sasf.net.cco.model.CcoPuestoTrabajoRequisitoPK;
import sasf.net.cco.model.CcoPuestosTrabajoPK;
import sasf.net.cco.model.CcoRequisitosTrabajoPK;
import sasf.net.cco.repository.CcoPuestoTrabajoRequisitoRepository;
import sasf.net.cco.repository.CcoPuestosTrabajoRepository;
import sasf.net.cco.repository.CcoRequisitosTrabajoRepository;
import sasf.net.cco.utils.EntidadCamposGenerales;
import sasf.net.cco.utils.FuncionesGenerales;
import sasf.net.cco.utils.Globales;
import sasf.net.cco.utils.ManejadorExcepciones;

@RestController
@RequestMapping("/ccoPuestoTrabajoRequisito")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class CcoPuestoTrabajoRequisitoController {
	
	@Autowired
	private TransactionTemplate transactionTemplate;
	
	@Autowired
	private FuncionesGenerales fg;
	
	@Autowired
	private CcoPuestoTrabajoRequisitoRepository repository;
	
	@Autowired
	private CcoPuestosTrabajoRepository puestoTrabajoRepository;
	
	@Autowired
	private CcoRequisitosTrabajoRepository requisitoTrabajoRepository;
	
	@ApiOperation(value = "", authorizations = {@Authorization(value="Autorización token módulo cco")})
	@GetMapping(path = "/{ccoPuTraAgeLicencCodigo}",produces = { MediaType.APPLICATION_JSON_VALUE })
	public List<?> consultar(@PathVariable Integer ccoPuTraAgeLicencCodigo,
			@RequestParam(name = "ccoPuTraCodigo", required = false) Long ccoPuTraCodigo,
			@RequestParam(name = "ccoReTraCodigo", required = false) Long ccoReTraCodigo
			) {
		return repository.buscarPorParametros(ccoPuTraAgeLicencCodigo,ccoPuTraCodigo,ccoReTraCodigo,Globales.ESTADO_ANULADO);
	}
	
	@ApiOperation(value = "", authorizations = {@Authorization(value="Autorización token módulo cco")})
	@GetMapping(path = "/{ccoPuTraAgeLicencCodigo}/{ccoPuTraCodigo}/{ccoReTraAgeLicencCodigo}/{ccoReTraCodigo}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<?> consultarPorId(@PathVariable Integer ccoPuTraAgeLicencCodigo
			, @PathVariable Long ccoPuTraCodigo,
			@PathVariable Integer ccoReTraAgeLicencCodigo
			, @PathVariable Long ccoReTraCodigo)
			throws NotFoundException {

		Optional<CcoPuestoTrabajoRequisito> postRequisito = repository.findById(new CcoPuestoTrabajoRequisitoPK(ccoPuTraCodigo, ccoPuTraAgeLicencCodigo, ccoReTraCodigo, ccoReTraAgeLicencCodigo));
		if (!postRequisito.isPresent() || postRequisito.get().getEstado().equals(Globales.ESTADO_ANULADO)) {
			throw new NoResultException(
					"Puesto trabajo requisito con código de puesto de trabajo " + ccoPuTraCodigo + " y código de requisito de trabajo "+ ccoReTraCodigo+" no existe");
		}
		return new ResponseEntity<CcoPuestoTrabajoRequisito>(postRequisito.get(), HttpStatus.OK);
	}
	
	@ApiOperation(value = "", authorizations = {@Authorization(value="Autorización token módulo cco")})
	@PostMapping
	public ResponseEntity<?> ingresar(@RequestBody CcoPuestoTrabajoRequisito transaccion,
			HttpServletRequest request) {

		fg.validar(transaccion, CcoPuestoTrabajoRequisito.PuestoTrabajoRequisitoCreation.class);
		fg.validar(transaccion, EntidadCamposGenerales.CamposGeneralesCreation.class);
		transaccion.setUbicacionIngreso(request.getRemoteAddr());
		
		if(!puestoTrabajoRepository.existsById(new CcoPuestosTrabajoPK(transaccion.getId().getCcoPuTraCodigo(), transaccion.getId().getCcoPuTraAgeLicencCodigo()))) {
			return ManejadorExcepciones.error(HttpStatus.NOT_FOUND, null, 
					"Puesto de trabajo con código "+ transaccion.getId().getCcoPuTraCodigo()+" y licenciatario "+ transaccion.getId().getCcoPuTraAgeLicencCodigo()+" no existe");
		}
		System.out.println("***"+transaccion.getId().getCcoReTraCodigo()+" "+transaccion.getId().getCcoReTraAgeLicencCodigo());
		if(!requisitoTrabajoRepository.existsById(new CcoRequisitosTrabajoPK(transaccion.getId().getCcoReTraCodigo(), transaccion.getId().getCcoReTraAgeLicencCodigo()))) {
			return ManejadorExcepciones.error(HttpStatus.NOT_FOUND, null, 
					"Requisito de trabajo con código "+ transaccion.getId().getCcoReTraCodigo()+" y licenciatario "+ transaccion.getId().getCcoReTraAgeLicencCodigo()+" no existe");
		}
		
		
		transactionTemplate.execute(new TransactionCallbackWithoutResult() {
			@Override
			public void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
				
				if(repository.existsById(transaccion.getId())){
					throw new DataIntegrityViolationException(
							"Puesto trabajo requisito con código de puesto de trabajo " + transaccion.getId().getCcoPuTraCodigo() + " y código de requisito de trabajo "+ transaccion.getId().getCcoReTraCodigo()+"ya existe");
				}
				repository.save(transaccion);
			}
		});

		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{ccoPuTraAgeLicencCodigo}/{ccoPuTraCodigo}/{ccoReTraAgeLicencCodigo}/{ccoReTraCodigo}")
				.buildAndExpand(transaccion.getId().getCcoPuTraAgeLicencCodigo(),
						transaccion.getId().getCcoPuTraCodigo(),
						transaccion.getId().getCcoReTraAgeLicencCodigo(),
						transaccion.getId().getCcoReTraCodigo()
						).toUri();
		return ResponseEntity.created(location).build();
	}
	
	@ApiOperation(value = "", authorizations = {@Authorization(value="Autorización token módulo cco")})
	@PostMapping("/varios")
	public ResponseEntity<?> ingresarVarios(@RequestBody List<CcoPuestoTrabajoRequisito> transacciones, 
			HttpServletRequest request) {

		transactionTemplate.execute(new TransactionCallbackWithoutResult() {
			@Override
			public void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
				for (CcoPuestoTrabajoRequisito transaccion : transacciones) {
					fg.validar(transaccion, CcoPuestoTrabajoRequisito.PuestoTrabajoRequisitoCreation.class);
					fg.validar(transaccion, EntidadCamposGenerales.CamposGeneralesCreation.class);
					transaccion.setUbicacionIngreso(request.getRemoteAddr());
					
					if(!puestoTrabajoRepository.existsById(new CcoPuestosTrabajoPK(transaccion.getId().getCcoPuTraCodigo(), transaccion.getId().getCcoPuTraAgeLicencCodigo()))) {
						throw new NoResultException( 
								"Puesto de trabajo con código "+ transaccion.getId().getCcoPuTraCodigo()+" y licenciatario "+ transaccion.getId().getCcoPuTraAgeLicencCodigo()+" no existe");
					}
					if(!requisitoTrabajoRepository.existsById(new CcoRequisitosTrabajoPK(transaccion.getId().getCcoReTraCodigo(), transaccion.getId().getCcoReTraAgeLicencCodigo()))) {
						throw new NoResultException( 
								"Requisito de trabajo con código "+ transaccion.getId().getCcoReTraCodigo()+" y licenciatario "+ transaccion.getId().getCcoReTraAgeLicencCodigo()+" no existe");
					}
					
					if(repository.existsById(transaccion.getId())){
						throw new DataIntegrityViolationException(
								"Puesto trabajo requisito con código de puesto de trabajo " + transaccion.getId().getCcoPuTraCodigo() + " y código de requisito de trabajo "+ transaccion.getId().getCcoReTraCodigo()+" ya existe");
					}
					repository.save(transaccion);
				}
			}
		});

		return new ResponseEntity<List<CcoPuestoTrabajoRequisito>>(transacciones, HttpStatus.CREATED);
	}
	
	@ApiOperation(value = "", authorizations = {@Authorization(value="Autorización token módulo cco")})
	@PutMapping
	public ResponseEntity<?> actualizar(@RequestBody CcoPuestoTrabajoRequisito transaccion, HttpServletRequest request) {
		fg.validar(transaccion, CcoPuestoTrabajoRequisito.PuestoTrabajoRequisitoUpdate.class);
		fg.validar(transaccion, EntidadCamposGenerales.CamposGeneralesUpdate.class);
		transaccion.setUbicacionModificacion(request.getRemoteAddr());
		
		if (!repository.existsById(transaccion.getId())) {
			return ManejadorExcepciones.error(HttpStatus.NOT_FOUND, null,
					"Puesto trabajo requisito con código de puesto de trabajo " + transaccion.getId().getCcoPuTraCodigo()+" con licenciatario "+transaccion.getId().getCcoPuTraAgeLicencCodigo() + " y código de requisito de trabajo "+ transaccion.getId().getCcoReTraCodigo()+" con licenciatario "+transaccion.getId().getCcoReTraAgeLicencCodigo()+" no existe");
		}
		
		repository.save(transaccion);

		return new ResponseEntity<String>("", HttpStatus.OK);
	}
	
	@ApiOperation(value = "", authorizations = {@Authorization(value="Autorización token módulo cco")})
	@PutMapping("/varios")
	public ResponseEntity<?> actualizarVarios(@RequestBody List<CcoPuestoTrabajoRequisito> transacciones,
			HttpServletRequest request) {
		transactionTemplate.execute(new TransactionCallbackWithoutResult() {
			@Override
			public void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
				for (CcoPuestoTrabajoRequisito transaccion : transacciones) {
					fg.validar(transaccion, CcoPuestoTrabajoRequisito.PuestoTrabajoRequisitoUpdate.class);
					fg.validar(transaccion, EntidadCamposGenerales.CamposGeneralesUpdate.class);
					transaccion.setUbicacionModificacion(request.getRemoteAddr());
					
					if (!repository.existsById(transaccion.getId())) {
						throw new NoResultException(
								"Puesto trabajo requisito con código de puesto de trabajo " + transaccion.getId().getCcoPuTraCodigo()+" con licenciatario "+transaccion.getId().getCcoPuTraAgeLicencCodigo() + " y código de requisito de trabajo "+ transaccion.getId().getCcoReTraCodigo()+" con licenciatario "+transaccion.getId().getCcoReTraAgeLicencCodigo()+" no existe");
					} 
					
					repository.save(transaccion);
				}
			}
		});
		return new ResponseEntity<List<CcoPuestoTrabajoRequisito>>(transacciones, HttpStatus.CREATED);
	}
	
}
