package sasf.net.cco.service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

import org.apache.poi.hssf.record.cf.BorderFormatting;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.hssf.util.HSSFColor.HSSFColorPredefined;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.RegionUtil;
import org.springframework.stereotype.Service;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import sasf.net.cco.model.CcoConductores;

@Service
public class CcoConductoresService {

	public ByteArrayOutputStream crearPdf(List<CcoConductores> conductores) {
		
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		Document documento = new Document(PageSize.A4 , 15,15,45,30);
		
		try {
			 // Asignamos la variable ByteArrayOutputStream bos donde se escribirá el documento
			PdfWriter writer = PdfWriter.getInstance(documento, bos) ;
			documento.open();
		    Font fuentetitulo = FontFactory.getFont("Arial", 10, BaseColor.BLACK);
		    Paragraph parrafo = new Paragraph("Conductores", fuentetitulo);
		    parrafo.setAlignment(Element.ALIGN_CENTER);
		    parrafo.setIndentationLeft(10);
		    parrafo.setIndentationRight(10);
		    parrafo.setSpacingAfter(10);
		    
		    documento.add(parrafo);
		     // creo la tabla con sus propiedades
		    PdfPTable tabla = new PdfPTable(5);
		     tabla.setWidthPercentage(100);
		     tabla.setSpacingAfter(10);
		     tabla.setSpacingBefore(10);
			
		     Font fuenteheader = FontFactory.getFont("Arial", 10, BaseColor.BLACK);
		     Font fuentecampos = FontFactory.getFont("Arial", 9, BaseColor.BLACK);
		     
		     float[] columWidth = {2f , 2f,2f,2f , 2f}; 
		     
		     tabla.setWidths(columWidth);
		     
		     PdfPCell celda_nombre = new PdfPCell(new Paragraph("Nombre" ,fuenteheader) ) ;
		     celda_nombre.setBorderColor(BaseColor.BLACK);
		     celda_nombre.setPadding(10);
		     celda_nombre.setHorizontalAlignment(Element.ALIGN_CENTER);
		     celda_nombre.setVerticalAlignment(Element.ALIGN_CENTER);
		     celda_nombre.setBackgroundColor(BaseColor.GRAY);
		     celda_nombre.setExtraParagraphSpace(5f);
		     tabla.addCell(celda_nombre);
		     
		     
		     PdfPCell celda_apellido = new PdfPCell(new Paragraph("Apellido" ,fuenteheader) ) ;
		     celda_apellido.setBorderColor(BaseColor.BLACK);
		     celda_apellido.setPadding(10);
		     celda_apellido.setHorizontalAlignment(Element.ALIGN_CENTER);
		     celda_apellido.setVerticalAlignment(Element.ALIGN_CENTER);
		     celda_apellido.setBackgroundColor(BaseColor.GRAY);
		     celda_apellido.setExtraParagraphSpace(5f);
		     tabla.addCell(celda_apellido);
		     
		     PdfPCell celda_celular = new PdfPCell(new Paragraph("Celular" ,fuenteheader) ) ;
		     celda_celular.setBorderColor(BaseColor.BLACK);
		     celda_celular.setPadding(10);
		     celda_celular.setHorizontalAlignment(Element.ALIGN_CENTER);
		     celda_celular.setVerticalAlignment(Element.ALIGN_CENTER);
		     celda_celular.setBackgroundColor(BaseColor.GRAY);
		     celda_celular.setExtraParagraphSpace(5f);
		     tabla.addCell(celda_celular);
		     
		     PdfPCell celda_matricula = new PdfPCell(new Paragraph("Matricula" ,fuenteheader) ) ;
		     celda_matricula.setBorderColor(BaseColor.BLACK);
		     celda_matricula.setPadding(10);
		     celda_matricula.setHorizontalAlignment(Element.ALIGN_CENTER);
		     celda_matricula.setVerticalAlignment(Element.ALIGN_CENTER);
		     celda_matricula.setBackgroundColor(BaseColor.GRAY);
		     celda_matricula.setExtraParagraphSpace(5f);
		     tabla.addCell(celda_matricula);
		     
		     PdfPCell celda_correo = new PdfPCell(new Paragraph("Correo Electronico" ,fuenteheader) ) ;
		     celda_correo.setBorderColor(BaseColor.BLACK);
		     celda_correo.setPadding(10);
		     celda_correo.setHorizontalAlignment(Element.ALIGN_CENTER);
		     celda_correo.setVerticalAlignment(Element.ALIGN_CENTER);
		     celda_correo.setBackgroundColor(BaseColor.GRAY);
		     celda_correo.setExtraParagraphSpace(5f);
		     tabla.addCell(celda_correo);
		     
		
			
	     for (CcoConductores conductor : conductores) {
			  
	    	    celda_nombre = new PdfPCell(new Paragraph(conductor.getNombre() ,fuentecampos) ) ;
			     celda_nombre.setBorderColor(BaseColor.BLACK);
			     celda_nombre.setPadding(10);
			     celda_nombre.setHorizontalAlignment(Element.ALIGN_CENTER);
			     celda_nombre.setVerticalAlignment(Element.ALIGN_CENTER);
			    // celda_nombre.setBackgroundColor(BaseColor.GRAY);
			     celda_nombre.setExtraParagraphSpace(5f);
			     tabla.addCell(celda_nombre);
			     
			     
			    celda_apellido = new PdfPCell(new Paragraph(conductor.getApellido() ,fuentecampos) ) ;
			     celda_apellido.setBorderColor(BaseColor.BLACK);
			     celda_apellido.setPadding(10);
			     celda_apellido.setHorizontalAlignment(Element.ALIGN_CENTER);
			     celda_apellido.setVerticalAlignment(Element.ALIGN_CENTER);
			   //  celda_apellido.setBackgroundColor(BaseColor.GRAY);
			     celda_apellido.setExtraParagraphSpace(5f);
			     tabla.addCell(celda_apellido);
			     
			      celda_celular = new PdfPCell(new Paragraph(conductor.getNumeroCelular() ,fuentecampos) ) ;
			     celda_celular.setBorderColor(BaseColor.BLACK);
			     celda_celular.setPadding(10);
			     celda_celular.setHorizontalAlignment(Element.ALIGN_CENTER);
			     celda_celular.setVerticalAlignment(Element.ALIGN_CENTER);
			  //   celda_celular.setBackgroundColor(BaseColor.GRAY);
			     celda_celular.setExtraParagraphSpace(5f);
			     tabla.addCell(celda_celular);
			     
			      celda_matricula = new PdfPCell(new Paragraph(conductor.getNumeroMatricula() ,fuentecampos) ) ;
			     celda_matricula.setBorderColor(BaseColor.BLACK);
			     celda_matricula.setPadding(10);
			     celda_matricula.setHorizontalAlignment(Element.ALIGN_CENTER);
			     celda_matricula.setVerticalAlignment(Element.ALIGN_CENTER);
			   //  celda_matricula.setBackgroundColor(BaseColor.GRAY);
			     celda_matricula.setExtraParagraphSpace(5f);
			     tabla.addCell(celda_matricula);
			     
			     celda_correo = new PdfPCell(new Paragraph(conductor.getCorreoElectronico() ,fuentecampos) ) ;
			     celda_correo.setBorderColor(BaseColor.BLACK);
			     celda_correo.setPadding(10);
			     celda_correo.setHorizontalAlignment(Element.ALIGN_CENTER);
			     celda_correo.setVerticalAlignment(Element.ALIGN_CENTER);
			   //  celda_correo.setBackgroundColor(BaseColor.GRAY);
			     celda_correo.setExtraParagraphSpace(5f);
			     tabla.addCell(celda_correo);
			     
	    	 
		}
	     
	     documento.add(tabla); 
	 	documento.close();
	 	writer.close();

			
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return bos;
	}
	
	
	
	

	
	public ByteArrayOutputStream crearExcelConductor(CcoConductores conductor) {
		
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		
		HSSFWorkbook workbook = new HSSFWorkbook();
		HSSFSheet worksheet = workbook.createSheet("Conductores");
		worksheet.setDefaultColumnWidth(30);
		
		CellStyle style = workbook.createCellStyle();
		style.setBorderBottom(BorderStyle.MEDIUM);
		style.setBorderTop(BorderStyle.MEDIUM);
		style.setBorderRight(BorderStyle.MEDIUM);
		style.setBorderLeft(BorderStyle.MEDIUM);
		
	//	HSSFCellStyle styleHeader = workbook.createCellStyle();
	//	styleHeader.setFillForegroundColor(HSSFColor.toHSSFColor());
	//	styleHeader.setFillPattern(HSSFColorPredefined.); 

		HSSFRow rw1 =  worksheet.createRow(1);
		HSSFCell celda = rw1.createCell(1);
		celda.setCellValue("Nombres");
		celda.setCellStyle(style);

		rw1.createCell(2).setCellValue(conductor.getNombre() + " " + conductor.getApellido());
	    CellRangeAddress cellMerge = new CellRangeAddress(1, 1, 2, 4); 
	    worksheet.addMergedRegion(cellMerge); 
	    setBordersToMergedCells(worksheet , cellMerge );
	  

        
        HSSFRow rw2 =  worksheet.createRow(2);
	    celda = rw2.createCell(1);
		celda.setCellValue("Celular");
		celda.setCellStyle(style);
        
        celda = rw2.createCell(2);
        celda.setCellValue(conductor.getNumeroCelular());
		celda.setCellStyle(style);
		
        celda = rw2.createCell(3);
        celda.setCellValue("Matricula");
		celda.setCellStyle(style);
		       
        celda = rw2.createCell(4);
        celda.setCellValue(conductor.getNumeroMatricula());
		celda.setCellStyle(style);
        
        HSSFRow rw3 =  worksheet.createRow(3);
	       
         celda = rw3.createCell(1);
         celda.setCellValue("Correo Electronico");
	    celda.setCellStyle(style);
	    
        rw3.createCell(2).setCellValue(conductor.getCorreoElectronico());
        cellMerge = new CellRangeAddress(3, 3, 2, 4); 
        worksheet.addMergedRegion(cellMerge);
        setBordersToMergedCells(worksheet , cellMerge );	
	     
	     try {
			workbook.write(bos);
			workbook.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	     
		return bos;
	}
	
	private void setBordersToMergedCells(HSSFSheet sheet, CellRangeAddress rangeAddress) { 
		
	    RegionUtil.setBorderTop(BorderStyle.MEDIUM, rangeAddress, sheet); 
	    RegionUtil.setBorderLeft(BorderStyle.MEDIUM, rangeAddress, sheet); 
	    RegionUtil.setBorderRight(BorderStyle.MEDIUM, rangeAddress, sheet); 
	    RegionUtil.setBorderBottom(BorderStyle.MEDIUM, rangeAddress, sheet); 
	} 
	
		
	public ByteArrayOutputStream crearExcelConductores(List<CcoConductores> conductores) {
		
		String cabecera[]  = {"Nombre" , "Apellido" , "Celular" , "Matricula" , "Correo Electronico"};
		
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		HSSFWorkbook workbook = new HSSFWorkbook();
		HSSFSheet worksheet = workbook.createSheet("Conductores");
		worksheet.setDefaultColumnWidth(30);
		
	//	HSSFCellStyle styleHeader = workbook.createCellStyle();
	//	styleHeader.setFillForegroundColor(HSSFColor.toHSSFColor());
	//	styleHeader.setFillPattern(HSSFColorPredefined.); 

		HSSFRow headerRow =  worksheet.createRow(0);

		for (int i = 0; i < cabecera.length; i++) {
			HSSFCell campo = headerRow.createCell(i);
		     campo.setCellValue(cabecera[i]);
		}
		
    
	 int registro = 1;
			
	     for (CcoConductores conductor : conductores) {
			  
	    	 HSSFRow  datosRow =  worksheet.createRow(registro);
	    	 
	    	 datosRow.createCell(0).setCellValue(conductor.getNombre());
			 datosRow.createCell(1).setCellValue(conductor.getApellido());
             datosRow.createCell(2).setCellValue(conductor.getNumeroCelular());
			 datosRow.createCell(3).setCellValue(conductor.getNumeroMatricula());			
			 datosRow.createCell(4).setCellValue(conductor.getCorreoElectronico());
	    	 
	    	 registro ++;
		}
	     
	     try {
	    	 workbook.write(bos);
		     workbook.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return bos;
	}
	
}
