package sasf.net.cco.security;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter{
	
	private static final String[] AUTH_WHITELIST = {
            "/v2/api-docs",
            "/swagger-resources",
            "/swagger-resources/**",
            "/configuration/ui",
            "/configuration/security",
            "/swagger-ui.html",
            "/webjars/**"
            
    };
	@Override
	public void configure(WebSecurity web) throws Exception {
	    web.ignoring().antMatchers(AUTH_WHITELIST)
	    //.and().ignoring().antMatchers("/ccoQuejasReclamosTrabajo/prueba","/ccoQuejasReclamosTrabajo/prueba**","/ccoQuejasReclamosTrabajo/prueba");
	    //.and().ignoring().antMatchers("/ccoPaquete/getPDF**")
	    ;
	    
	}

	
}
