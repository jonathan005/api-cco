package sasf.net.cco.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import sasf.net.cco.utils.EntidadCamposGenerales;

@Cacheable(false)
@Entity
@Table(name="cco_conductor_paquetes_det")
@NamedQuery(name="CcoConductorPaquetesDet.findAll", query="SELECT c FROM CcoConductorPaquetesDet c")
public class CcoConductorPaquetesDet extends EntidadCamposGenerales implements Serializable{
	
	private static final long serialVersionUID = 1L;

	public interface ConductorPaquetesDetCreation {}

	public interface ConductorPaquetesDetUpdate {}
	
	@Valid
	@NotNull(groups = {ConductorPaquetesDetCreation.class, ConductorPaquetesDetUpdate.class}, message = "El id de conductor paquetes detalles no puede ser nulo")
	@EmbeddedId
	private CcoConductorPaquetesDetPK id;
	
	@NotNull(groups = {ConductorPaquetesDetCreation.class}, message = "Valor no puede ser nulo")
	@Column(name="valor",updatable=true,nullable = false,precision =18,scale = 4)
	private BigDecimal valor;
	
	@NotNull(groups = ConductorPaquetesDetCreation.class, message = "El código de paquete no puede ser nulo")
	@Column(name="cco_paquet_codigo",length = 10,nullable = false,updatable = true)
	private Long ccoPaquetCodigo;
	
	@NotNull(groups = ConductorPaquetesDetCreation.class, message = "El código de licenciatario de paquete no puede ser nulo")
	@Column(name="cco_paquet_age_licenc_codigo",length = 5,nullable = false,updatable = true)
	private Integer ccoPaquetAgeLicencCodigo;

	@PrePersist
	void preInsert() {
		this.fechaIngreso = new Date();
		this.fechaEstado = new Date();
	}
	
	@PreUpdate
	void preUpdate() {
		this.fechaModificacion = new Date();
		if(this.estado != null){
			this.fechaEstado = new Date();
		}
	}
	
	public CcoConductorPaquetesDet() {
		super();
	}

	public CcoConductorPaquetesDet(@Valid @NotNull(groups = { ConductorPaquetesDetCreation.class,
			ConductorPaquetesDetUpdate.class }, message = "El id de conductor paquetes detalles no puede ser nulo") CcoConductorPaquetesDetPK id,
			@NotNull(groups = ConductorPaquetesDetCreation.class, message = "Valor no puede ser nulo") BigDecimal valor,
			@NotNull(groups = ConductorPaquetesDetCreation.class, message = "El código de paquete no puede ser nulo") Long ccoPaquetCodigo,
			@NotNull(groups = ConductorPaquetesDetCreation.class, message = "El código de licenciatario de paquete no puede ser nulo") Integer ccoPaquetAgeLicencCodigo) {
		super();
		this.id = id;
		this.valor = valor;
		this.ccoPaquetCodigo = ccoPaquetCodigo;
		this.ccoPaquetAgeLicencCodigo = ccoPaquetAgeLicencCodigo;
	}

	public CcoConductorPaquetesDetPK getId() {
		return id;
	}

	public void setId(CcoConductorPaquetesDetPK id) {
		this.id = id;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public Long getCcoPaquetCodigo() {
		return ccoPaquetCodigo;
	}

	public void setCcoPaquetCodigo(Long ccoPaquetCodigo) {
		this.ccoPaquetCodigo = ccoPaquetCodigo;
	}

	public Integer getCcoPaquetAgeLicencCodigo() {
		return ccoPaquetAgeLicencCodigo;
	}

	public void setCcoPaquetAgeLicencCodigo(Integer ccoPaquetAgeLicencCodigo) {
		this.ccoPaquetAgeLicencCodigo = ccoPaquetAgeLicencCodigo;
	}
	
	
	
	
}
