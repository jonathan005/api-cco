package sasf.net.cco.model;

import java.io.Serializable;
import java.math.BigDecimal;

public class CcoCotizacion implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private BigDecimal peso;
	
	private BigDecimal distancia;
	
	private CcoEmbalajesPK embalajeId;

	public CcoCotizacion(BigDecimal peso, BigDecimal distancia, CcoEmbalajesPK embalajeId) {
		super();
		this.peso = peso;
		this.distancia = distancia;
		this.embalajeId = embalajeId;
	}

	public BigDecimal getPeso() {
		return peso;
	}

	public void setPeso(BigDecimal peso) {
		this.peso = peso;
	}

	public BigDecimal getDistancia() {
		return distancia;
	}

	public void setDistancia(BigDecimal distancia) {
		this.distancia = distancia;
	}

	public CcoEmbalajesPK getEmbalajeId() {
		return embalajeId;
	}

	public void setEmbalajeId(CcoEmbalajesPK embalajeId) {
		this.embalajeId = embalajeId;
	}
	
	
}
