package sasf.net.cco.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import sasf.net.cco.utils.EntidadCamposGenerales;

@Cacheable(false)
@Entity
@Table(name="cco_inventario_diario")
@NamedQuery(name="CcoInventarioDiario.findAll", query="SELECT c FROM CcoInventarioDiario c")
public class CcoInventarioDiario extends EntidadCamposGenerales implements Serializable  {
	
	private static final long serialVersionUID = 1L;

	public interface InventarioDiarioCreation {}

	public interface InventarioDiarioUpdate {}
	
	@Valid
	@NotNull(groups = {InventarioDiarioCreation.class, InventarioDiarioUpdate.class}, message = "El id de inventario diario no puede ser nulo")
	@EmbeddedId
	private CcoInventarioDiarioPK id;
	
	@NotNull(groups = {InventarioDiarioCreation.class}, message = "Fecha no puede ser nulo")
	private Date fecha;
	
	@NotNull(groups = {InventarioDiarioCreation.class}, message = "Código de paquete desde no puede ser nulo")
	@Column(name="cco_paquete_codigo",updatable=true,nullable = false)
	private Long ccoPaqueteCodigo;
	
	@NotNull(groups = {InventarioDiarioCreation.class}, message = "Código de licenciatario de paquete desde no puede ser nulo")
	@Column(name="cco_paquete_age_licenc_codigo",updatable=true,nullable = false)
	private Integer ccoPaqueteAgeLicencCodigo;
	
	@NotNull(groups = {InventarioDiarioCreation.class}, message = "Código de paquete hasta no puede ser nulo")
	@Column(name="cco_paquete_codigo_h",updatable=true,nullable = false)
	private Long ccoPaqueteCodigoH;
	
	@NotNull(groups = {InventarioDiarioCreation.class}, message = "Código de licenciatario paquete hasta no puede ser nulo")
	@Column(name="cco_paquete_age_licenc_codigo_h",updatable=true,nullable = false)
	private Integer ccoPaqueteAgeLicencCodigoH;
	
	@NotNull(groups = {InventarioDiarioCreation.class}, message = "El número de paquete no puede ser nulo")
	@Column(name="numero_paquetes",updatable=true,nullable = true)
	private Integer numeroPaquetes;
	
	@Column(name="gastos",updatable=true,nullable = true,precision =18 ,scale = 4)
	private BigDecimal gastos;
	
	@Column(name="observaciones",updatable=true,nullable = true,length = 1000)
	private String observaciones;
	
	
	@PrePersist
	void preInsert() {
		this.fechaIngreso = new Date();
		this.fechaEstado = new Date();
	}
	
	@PreUpdate
	void preUpdate() {
		this.fechaModificacion = new Date();
		if(this.estado != null){
			this.fechaEstado = new Date();
		}
	}



	public CcoInventarioDiario(@Valid @NotNull(groups = { InventarioDiarioCreation.class,
			InventarioDiarioUpdate.class }, message = "El id de inventario diario no puede ser nulo") CcoInventarioDiarioPK id,
			@NotNull(groups = InventarioDiarioCreation.class, message = "Fecha no puede ser nulo") Date fecha,
			@NotNull(groups = InventarioDiarioCreation.class, message = "Código de paquete desde no puede ser nulo") Long ccoPaqueteCodigo,
			@NotNull(groups = InventarioDiarioCreation.class, message = "Código de licenciatario de paquete desde no puede ser nulo") Integer ccoPaqueteAgeLicencCodigo,
			@NotNull(groups = InventarioDiarioCreation.class, message = "Código de paquete hasta no puede ser nulo") Long ccoPaqueteCodigoH,
			@NotNull(groups = InventarioDiarioCreation.class, message = "Código de licenciatario paquete hasta no puede ser nulo") Integer ccoPaqueteAgeLicencCodigoH,
			@NotNull(groups = InventarioDiarioCreation.class, message = "El número de paquete no puede ser nulo") Integer numeroPaquetes,
			BigDecimal gastos, String observaciones) {
		super();
		this.id = id;
		this.fecha = fecha;
		this.ccoPaqueteCodigo = ccoPaqueteCodigo;
		this.ccoPaqueteAgeLicencCodigo = ccoPaqueteAgeLicencCodigo;
		this.ccoPaqueteCodigoH = ccoPaqueteCodigoH;
		this.ccoPaqueteAgeLicencCodigoH = ccoPaqueteAgeLicencCodigoH;
		this.numeroPaquetes = numeroPaquetes;
		this.gastos = gastos;
		this.observaciones = observaciones;
	}

	public CcoInventarioDiario() {
		super();
	}

	public CcoInventarioDiarioPK getId() {
		return id;
	}

	public void setId(CcoInventarioDiarioPK id) {
		this.id = id;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public Long getCcoPaqueteCodigo() {
		return ccoPaqueteCodigo;
	}

	public void setCcoPaqueteCodigo(Long ccoPaqueteCodigo) {
		this.ccoPaqueteCodigo = ccoPaqueteCodigo;
	}

	public Integer getCcoPaqueteAgeLicencCodigo() {
		return ccoPaqueteAgeLicencCodigo;
	}

	public void setCcoPaqueteAgeLicencCodigo(Integer ccoPaqueteAgeLicencCodigo) {
		this.ccoPaqueteAgeLicencCodigo = ccoPaqueteAgeLicencCodigo;
	}

	public Long getCcoPaqueteCodigoH() {
		return ccoPaqueteCodigoH;
	}

	public void setCcoPaqueteCodigoH(Long ccoPaqueteCodigoH) {
		this.ccoPaqueteCodigoH = ccoPaqueteCodigoH;
	}

	public Integer getCcoPaqueteAgeLicencCodigoH() {
		return ccoPaqueteAgeLicencCodigoH;
	}

	public void setCcoPaqueteAgeLicencCodigoH(Integer ccoPaqueteAgeLicencCodigoH) {
		this.ccoPaqueteAgeLicencCodigoH = ccoPaqueteAgeLicencCodigoH;
	}

	public Integer getNumeroPaquetes() {
		return numeroPaquetes;
	}

	public void setNumeroPaquetes(Integer numeroPaquetes) {
		this.numeroPaquetes = numeroPaquetes;
	}

	public BigDecimal getGastos() {
		return gastos;
	}

	public void setGastos(BigDecimal gastos) {
		this.gastos = gastos;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}


	
	
	
	
	
}
