package sasf.net.cco.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

import sasf.net.cco.model.CcoQuejasReclamosTrabajo.QuejaReclamoTrabajoCreation;
import sasf.net.cco.model.CcoQuejasReclamosTrabajo.QuejaReclamoTrabajoUpdate;



@Embeddable
public class CcoQuejasReclamosTrabajoPK implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@NotNull(groups = QuejaReclamoTrabajoUpdate.class, message = "El código de quejas, reclamos o trabajos no puede ser nulo")
	@Column(name="codigo",length = 5,nullable = false,updatable = false)
	private Long codigo;

	@NotNull(groups = {QuejaReclamoTrabajoCreation.class, QuejaReclamoTrabajoUpdate.class}, message = "El código del licenciatario no puede ser nulo")	
	@Column(name="age_licenc_codigo",length = 5,nullable = false,updatable = false)
	private Integer ageLicencCodigo;

	public CcoQuejasReclamosTrabajoPK() {
		super();
	}

	public CcoQuejasReclamosTrabajoPK(
			@NotNull(groups = QuejaReclamoTrabajoUpdate.class, message = "El código de quejas, reclamos o trabajos no puede ser nulo") Long codigo,
			@NotNull(groups = { QuejaReclamoTrabajoCreation.class,
					QuejaReclamoTrabajoUpdate.class }, message = "El código del licenciatario no puede ser nulo") Integer ageLicencCodigo) {
		super();
		this.codigo = codigo;
		this.ageLicencCodigo = ageLicencCodigo;
	}

	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public Integer getAgeLicencCodigo() {
		return ageLicencCodigo;
	}

	public void setAgeLicencCodigo(Integer ageLicencCodigo) {
		this.ageLicencCodigo = ageLicencCodigo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ageLicencCodigo == null) ? 0 : ageLicencCodigo.hashCode());
		result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CcoQuejasReclamosTrabajoPK other = (CcoQuejasReclamosTrabajoPK) obj;
		if (ageLicencCodigo == null) {
			if (other.ageLicencCodigo != null)
				return false;
		} else if (!ageLicencCodigo.equals(other.ageLicencCodigo))
			return false;
		if (codigo == null) {
			if (other.codigo != null)
				return false;
		} else if (!codigo.equals(other.codigo))
			return false;
		return true;
	}
	
	
	
	
}
