package sasf.net.cco.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;


import sasf.net.cco.utils.EntidadCamposGenerales;

@Cacheable(false)
@Entity
@Table(name="cco_valor_seguro")
@NamedQuery(name="CcoValorSeguro.findAll", query="SELECT c FROM CcoValorSeguro c")
public class CcoValorSeguro extends EntidadCamposGenerales implements Serializable{
	
	private static final long serialVersionUID = 1L;

	public interface ValorSeguroCreation {}

	public interface ValorSeguroUpdate {}
	
	@Valid
	@NotNull(groups = {ValorSeguroCreation.class, ValorSeguroUpdate.class}, message = "El id de valor seguro no puede ser nulo")
	@EmbeddedId
	private CcoValorSeguroPK id;
	
	@NotNull(groups = {ValorSeguroCreation.class}, message = "Peso desde no puede ser nulo")
	@Column(name="peso_desde",updatable=true,nullable = false,precision =18 ,scale = 4)
	private BigDecimal pesoDesde;
	
	@NotNull(groups = {ValorSeguroCreation.class}, message = "Peso hasta no puede ser nulo")
	@Column(name="peso_hasta",updatable=true,nullable = false,precision =18 ,scale = 4)
	private BigDecimal pesoHasta;
	
	@NotNull(groups = {ValorSeguroCreation.class}, message = "Valor no puede ser nulo")
	@Column(name="valor",updatable=true,nullable = false,precision =18 ,scale = 4)
	private BigDecimal valor;
	
	@PrePersist
	void preInsert() {
		this.fechaIngreso = new Date();
		this.fechaEstado = new Date();
	}
	
	@PreUpdate
	void preUpdate() {
		this.fechaModificacion = new Date();
		if(this.estado != null){
			this.fechaEstado = new Date();
		}
	}
	
	public CcoValorSeguro() {
		super();
	}

	

	public CcoValorSeguro(
			@Valid @NotNull(groups = { ValorSeguroCreation.class,
					ValorSeguroUpdate.class }, message = "El id de valor seguro no puede ser nulo") CcoValorSeguroPK id,
			@NotNull(groups = ValorSeguroCreation.class, message = "Peso desde no puede ser nulo") BigDecimal pesoDesde,
			@NotNull(groups = ValorSeguroCreation.class, message = "Peso hasta no puede ser nulo") BigDecimal pesoHasta,
			@NotNull(groups = ValorSeguroCreation.class, message = "Valor no puede ser nulo") BigDecimal valor) {
		super();
		this.id = id;
		this.pesoDesde = pesoDesde;
		this.pesoHasta = pesoHasta;
		this.valor = valor;
	}

	public CcoValorSeguroPK getId() {
		return id;
	}

	public void setId(CcoValorSeguroPK id) {
		this.id = id;
	}

	public BigDecimal getPesoDesde() {
		return pesoDesde;
	}

	public void setPesoDesde(BigDecimal pesoDesde) {
		this.pesoDesde = pesoDesde;
	}

	public BigDecimal getPesoHasta() {
		return pesoHasta;
	}

	public void setPesoHasta(BigDecimal pesoHasta) {
		this.pesoHasta = pesoHasta;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}
	
	
	
}
