package sasf.net.cco.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;

import sasf.net.cco.model.CcoLicenciatariosAplicaSecu.LicencAplicaSecuCreation;
import sasf.net.cco.model.CcoLicenciatariosAplicaSecu.LicencAplicaSecuUpdate;

;

public class CcoLicenciatariosAplicaSecuPK implements Serializable {
	//default serial version id, required for serializable classes.
		private static final long serialVersionUID = 1L;

		@NotNull(groups = {LicencAplicaSecuCreation.class,LicencAplicaSecuUpdate.class}, message = "El código de la secuencia no puede ser nulo")
		private Integer codigo;

		@NotNull(groups = {LicencAplicaSecuCreation.class, LicencAplicaSecuUpdate.class}, message = "El código del licenciatario no puede ser nulo")
		@Column(name="cco_lic_ap_cib_licenc_codigo")
		private Integer ccoLicApCcoLicencCodigo;

		@NotNull(groups = {LicencAplicaSecuCreation.class, LicencAplicaSecuUpdate.class}, message = "El código de la aplicación no puede ser nulo")
		@Column(name="cco_lic_ap_cib_aplica_codigo")
		private Integer ccoLicApCcoAplicaCodigo;

		public CcoLicenciatariosAplicaSecuPK() {
			super();
		}

		public CcoLicenciatariosAplicaSecuPK(@NotNull(groups = { LicencAplicaSecuCreation.class,
				LicencAplicaSecuUpdate.class }, message = "El código de la secuencia no puede ser nulo") Integer codigo,
				@NotNull(groups = { LicencAplicaSecuCreation.class,
						LicencAplicaSecuUpdate.class }, message = "El código del licenciatario no puede ser nulo") Integer ccoLicApCcoLicencCodigo,
				@NotNull(groups = { LicencAplicaSecuCreation.class,
						LicencAplicaSecuUpdate.class }, message = "El código de la aplicación no puede ser nulo") Integer ccoLicApCcoAplicaCodigo) {
			super();
			this.codigo = codigo;
			this.ccoLicApCcoLicencCodigo = ccoLicApCcoLicencCodigo;
			this.ccoLicApCcoAplicaCodigo = ccoLicApCcoAplicaCodigo;
		}

		public Integer getCodigo() {
			return codigo;
		}

		public void setCodigo(Integer codigo) {
			this.codigo = codigo;
		}

		public Integer getCcoLicApCcoLicencCodigo() {
			return ccoLicApCcoLicencCodigo;
		}

		public void setCcoLicApCcoLicencCodigo(Integer ccoLicApCcoLicencCodigo) {
			this.ccoLicApCcoLicencCodigo = ccoLicApCcoLicencCodigo;
		}

		public Integer getCcoLicApCcoAplicaCodigo() {
			return ccoLicApCcoAplicaCodigo;
		}

		public void setCcoLicApCcoAplicaCodigo(Integer ccoLicApCcoAplicaCodigo) {
			this.ccoLicApCcoAplicaCodigo = ccoLicApCcoAplicaCodigo;
		}
		
		

		

}
