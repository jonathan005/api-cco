package sasf.net.cco.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import sasf.net.cco.utils.EntidadCamposGenerales;



@Cacheable(false)
@Entity
@Table(name="cco_puestos_trabajo")
@NamedQuery(name="CcoPuestosTrabajo.findAll", query="SELECT c FROM CcoPuestosTrabajo c")
public class CcoPuestosTrabajo extends EntidadCamposGenerales implements Serializable {
	
	private static final long serialVersionUID = 1L;

	public interface PuestosTrabajoCreation {}

	public interface PuestosTrabajoUpdate {}
	
	@Valid
	@NotNull(groups = {PuestosTrabajoCreation.class, PuestosTrabajoUpdate.class}, message = "El id de puestos trabajo no puede ser nulo")
	@EmbeddedId
	private CcoPuestosTrabajoPK id;
	
	@NotNull(groups = {PuestosTrabajoCreation.class}, message = "La descripción no puede ser nula")
	@NotBlank(groups = {PuestosTrabajoCreation.class}, message = "La descripción no puede estar vacío")
	@Column(name="descripcion",updatable=true,nullable = false,length=200)
	private String descripcion;
	
	@NotNull(groups = {PuestosTrabajoCreation.class}, message = "El título no puede ser nula")
	@NotBlank(groups = {PuestosTrabajoCreation.class}, message = "El título no puede estar vacío")
	@Column(name="titulo",updatable=true,nullable = false,length=200)
	private String titulo;
	
	@NotNull(groups = {PuestosTrabajoCreation.class}, message = "La ruta imagen no puede ser nula")
	@NotBlank(groups = {PuestosTrabajoCreation.class}, message = "La ruta imagen no puede estar vacío")
	@Size(groups = {PuestosTrabajoCreation.class},max = 500,message = "La ruta imagen no debe "
			+ "contener má.s de 500 caracteres")
	@Column(name="rutaImagen",updatable=true,nullable = false,length=500)
	private String rutaImagen;
	
	@Transient
	private List<CcoRequisitosTrabajo> requisitosTrabajo;
	
	@PrePersist
	void preInsert() {
		this.fechaIngreso = new Date();
		this.fechaEstado = new Date();
	}
	
	@PreUpdate
	void preUpdate() {
		this.fechaModificacion = new Date();
		if(this.estado != null){
			this.fechaEstado = new Date();
		}
	}
	
	
	public CcoPuestosTrabajo() {
		super();
	}



	public CcoPuestosTrabajo(@Valid @NotNull(groups = { PuestosTrabajoCreation.class,
			PuestosTrabajoUpdate.class }, message = "El id de puestos trabajo no puede ser nulo") CcoPuestosTrabajoPK id,
			@NotNull(groups = PuestosTrabajoCreation.class, message = "La descripción no puede ser nula") @NotBlank(groups = PuestosTrabajoCreation.class, message = "La descripción no puede estar vacío") String descripcion,
			@NotNull(groups = PuestosTrabajoCreation.class, message = "El título no puede ser nula") @NotBlank(groups = PuestosTrabajoCreation.class, message = "El título no puede estar vacío") String titulo,
			@NotNull(groups = PuestosTrabajoCreation.class, message = "La ruta imagen no puede ser nula") @NotBlank(groups = PuestosTrabajoCreation.class, message = "La ruta imagen no puede estar vacío") String rutaImagen,
			List<CcoRequisitosTrabajo> requisitosTrabajo) {
		super();
		this.id = id;
		this.descripcion = descripcion;
		this.titulo = titulo;
		this.rutaImagen = rutaImagen;
		this.requisitosTrabajo = requisitosTrabajo;
	}

	public CcoPuestosTrabajoPK getId() {
		return id;
	}

	public void setId(CcoPuestosTrabajoPK id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getRutaImagen() {
		return rutaImagen;
	}

	public void setRutaImagen(String rutaImagen) {
		this.rutaImagen = rutaImagen;
	}

	public List<CcoRequisitosTrabajo> getRequisitosTrabajo() {
		return requisitosTrabajo;
	}

	public void setRequisitosTrabajo(List<CcoRequisitosTrabajo> requisitosTrabajo) {
		this.requisitosTrabajo = requisitosTrabajo;
	}


	
	
	
	
	
	
	
}
