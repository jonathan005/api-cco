package sasf.net.cco.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import sasf.net.cco.utils.EntidadCamposGenerales;




@Cacheable(false)
@Entity
@Table(name="cco_licenciatarios_aplica_secu")
@NamedQuery(name="CcoLicenciatariosAplicaSecu.findAll", query="SELECT c FROM CcoLicenciatariosAplicaSecu c")
public class CcoLicenciatariosAplicaSecu extends EntidadCamposGenerales implements Serializable{
	
	private static final long serialVersionUID = 1L;

	public interface LicencAplicaSecuCreation {
	}

	public interface LicencAplicaSecuUpdate {
	}
	
	@Valid
	@NotNull(groups = LicencAplicaSecuUpdate.class, message = "El código de la secuencia por licenciatario no puede ser nulo")
	@EmbeddedId
	private CcoLicenciatariosAplicaSecuPK id;

	@NotNull(groups = LicencAplicaSecuCreation.class, message = "El campo ciclica no puede ser nulo")
	@Pattern(groups = {LicencAplicaSecuCreation.class, LicencAplicaSecuUpdate.class}, regexp = "[SN]", message = "El campo ciclica debe ser S o N")
	private String ciclica;

	@NotNull(groups = LicencAplicaSecuCreation.class, message = "La descripción no puede ser nula")
	private String descripcion;

	@NotNull(groups = LicencAplicaSecuCreation.class, message = "El incremento de la secuencia no puede ser nulo")
	@Min(groups = LicencAplicaSecuCreation.class, value = 1, message = "El incremento de la secuencia debe ser mayor a 0")
	@Column(name="incrementa_en")
	private Integer incrementaEn;

	@Column(name="valor_actual")
	private Long valorActual;

	@NotNull(groups = LicencAplicaSecuCreation.class, message = "El inicio de la secuencia no puede ser nulo")
	@Column(name="valor_inicial")
	private Long valorInicial;

	@PrePersist
	void preInsert() {
		this.fechaIngreso = new Date();
		this.fechaEstado = new Date();
		if(this.valorActual == null) {
			this.valorActual = 0l;
		}
	}
	
	@PreUpdate
	void preUpdate() {
		this.fechaModificacion = new Date();
	}

	public CcoLicenciatariosAplicaSecu() {
		super();
	}

	public CcoLicenciatariosAplicaSecu(
			@Valid @NotNull(groups = LicencAplicaSecuUpdate.class, message = "El código de la secuencia por licenciatario no puede ser nulo") CcoLicenciatariosAplicaSecuPK id,
			@NotNull(groups = LicencAplicaSecuCreation.class, message = "El campo ciclica no puede ser nulo") @Pattern(groups = {
					LicencAplicaSecuCreation.class,
					LicencAplicaSecuUpdate.class }, regexp = "[SN]", message = "El campo ciclica debe ser S o N") String ciclica,
			@NotNull(groups = LicencAplicaSecuCreation.class, message = "La descripción no puede ser nula") String descripcion,
			@NotNull(groups = LicencAplicaSecuCreation.class, message = "El incremento de la secuencia no puede ser nulo") @Min(groups = LicencAplicaSecuCreation.class, value = 1, message = "El incremento de la secuencia debe ser mayor a 0") Integer incrementaEn,
			Long valorActual,
			@NotNull(groups = LicencAplicaSecuCreation.class, message = "El inicio de la secuencia no puede ser nulo") Long valorInicial) {
		super();
		this.id = id;
		this.ciclica = ciclica;
		this.descripcion = descripcion;
		this.incrementaEn = incrementaEn;
		this.valorActual = valorActual;
		this.valorInicial = valorInicial;
	}

	public CcoLicenciatariosAplicaSecuPK getId() {
		return id;
	}

	public void setId(CcoLicenciatariosAplicaSecuPK id) {
		this.id = id;
	}

	public String getCiclica() {
		return ciclica;
	}

	public void setCiclica(String ciclica) {
		this.ciclica = ciclica;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Integer getIncrementaEn() {
		return incrementaEn;
	}

	public void setIncrementaEn(Integer incrementaEn) {
		this.incrementaEn = incrementaEn;
	}

	public Long getValorActual() {
		return valorActual;
	}

	public void setValorActual(Long valorActual) {
		this.valorActual = valorActual;
	}

	public Long getValorInicial() {
		return valorInicial;
	}

	public void setValorInicial(Long valorInicial) {
		this.valorInicial = valorInicial;
	}
	
	

}
