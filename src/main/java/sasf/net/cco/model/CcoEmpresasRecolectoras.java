package sasf.net.cco.model;

import java.io.Serializable;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;


import sasf.net.cco.utils.EntidadCamposGenerales;

@Cacheable(false)
@Entity
@Table(name="cco_empresas_recolectoras")
@NamedQuery(name="CcoEmpresasRecolectoras.findAll", query="SELECT c FROM CcoEmpresasRecolectoras c")
public class CcoEmpresasRecolectoras extends EntidadCamposGenerales implements Serializable{
	
	private static final long serialVersionUID = 1L;

	public interface EmpresasRecolectorasCreation {}

	public interface EmpresasRecolectorasUpdate {}
	
	@Valid
	@NotNull(groups = {EmpresasRecolectorasCreation.class, EmpresasRecolectorasUpdate.class}, message = "El id de empresa recolectora no puede ser nulo")
	@EmbeddedId
	private CcoEmpresasRecolectorasPK id;
	
	@NotNull(groups = {EmpresasRecolectorasCreation.class} ,message = "El número celular no puede ser nulo")
	@NotBlank (groups = {EmpresasRecolectorasCreation.class} ,message = "El número celular no puede estar vacío")
	@Column(name="numero_celular",updatable=true,nullable = false,length=20)
	private String numeroCelular ;
	
	@Column(name="correo_electronico",updatable=true,nullable = true,length=100)
	private String correoElectronico ;
	
	@NotNull(groups = {EmpresasRecolectorasCreation.class} ,message = "El nombre de la empresa no puede ser nulo")
	@Column(name="nombre_empresa",updatable=true,nullable = false,length=2000)
	private String nombreEmpresa ;

	
	
	public CcoEmpresasRecolectoras() {
		super();
	}



	public CcoEmpresasRecolectoras(@Valid @NotNull(groups = { EmpresasRecolectorasCreation.class,
			EmpresasRecolectorasUpdate.class }, message = "El id de empresa recolectora no puede ser nulo") CcoEmpresasRecolectorasPK id,
			@NotNull(groups = EmpresasRecolectorasCreation.class, message = "El número celular no puede ser nulo") @NotBlank(groups = EmpresasRecolectorasCreation.class, message = "El número celular no puede estar vacío") String numeroCelular,
			String correoElectronico,
			@NotNull(groups = EmpresasRecolectorasCreation.class, message = "El nombre de la empresa no puede ser nulo") String nombreEmpresa) {
		super();
		this.id = id;
		this.numeroCelular = numeroCelular;
		this.correoElectronico = correoElectronico;
		this.nombreEmpresa = nombreEmpresa;
	}



	public CcoEmpresasRecolectorasPK getId() {
		return id;
	}



	public void setId(CcoEmpresasRecolectorasPK id) {
		this.id = id;
	}



	public String getNumeroCelular() {
		return numeroCelular;
	}



	public void setNumeroCelular(String numeroCelular) {
		this.numeroCelular = numeroCelular;
	}



	public String getCorreoElectronico() {
		return correoElectronico;
	}



	public void setCorreoElectronico(String correoElectronico) {
		this.correoElectronico = correoElectronico;
	}



	public String getNombreEmpresa() {
		return nombreEmpresa;
	}



	public void setNombreEmpresa(String nombreEmpresa) {
		this.nombreEmpresa = nombreEmpresa;
	}
	
	
	
}
