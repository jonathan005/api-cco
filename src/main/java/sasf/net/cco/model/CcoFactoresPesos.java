package sasf.net.cco.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;


import sasf.net.cco.utils.EntidadCamposGenerales;

@Cacheable(false)
@Entity
@Table(name="cco_factores_pesos")
@NamedQuery(name="CcoFactoresPesos.findAll", query="SELECT c FROM CcoFactoresPesos c")
public class CcoFactoresPesos extends EntidadCamposGenerales implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	public interface FactoresPesosCreation {}

	public interface FactoresPesosUpdate {}
	
	@Valid
	@NotNull(groups = {FactoresPesosCreation.class, FactoresPesosUpdate.class}, message = "El id de factor peso no puede ser nulo")
	@EmbeddedId
	private CcoFactoresPesosPK id;
	
	@NotNull(groups = {FactoresPesosCreation.class}, message = "El factor no puede ser nulo")
	@Column(name="factor",updatable=true,nullable = false,precision =18 ,scale = 4)
	private BigDecimal factor;
	
	@NotNull(groups = {FactoresPesosCreation.class}, message = "El peso desde no puede ser nulo")
	@Column(name="peso_desde",updatable=true,nullable = false,precision =18 ,scale = 4)
	private BigDecimal pesoDesde;
	
	@NotNull(groups = {FactoresPesosCreation.class}, message = "El peso hasta no puede ser nulo")
	@Column(name="peso_hasta",updatable=true,nullable = false,precision =18 ,scale = 4)
	private BigDecimal pesoHasta;
	
	@PrePersist
	void preInsert() {
		this.fechaIngreso = new Date();
		this.fechaEstado = new Date();
	}
	
	@PreUpdate
	void preUpdate() {
		this.fechaModificacion = new Date();
		if(this.estado != null){
			this.fechaEstado = new Date();
		}
	}

	
	public CcoFactoresPesos() {
		super();
	}

	public CcoFactoresPesos(
			@NotNull(groups = FactoresPesosCreation.class, message = "El factor no puede ser nulo") BigDecimal factor,
			@NotNull(groups = FactoresPesosCreation.class, message = "El peso desde no puede ser nulo") BigDecimal pesoDesde,
			@NotNull(groups = FactoresPesosCreation.class, message = "El peso hasta no puede ser nulo") BigDecimal pesoHasta) {
		super();
		this.factor = factor;
		this.pesoDesde = pesoDesde;
		this.pesoHasta = pesoHasta;
	}

	
	public CcoFactoresPesosPK getId() {
		return id;
	}

	public void setId(CcoFactoresPesosPK id) {
		this.id = id;
	}

	public BigDecimal getFactor() {
		return factor;
	}

	public void setFactor(BigDecimal factor) {
		this.factor = factor;
	}

	public BigDecimal getPesoDesde() {
		return pesoDesde;
	}

	public void setPesoDesde(BigDecimal pesoDesde) {
		this.pesoDesde = pesoDesde;
	}

	public BigDecimal getPesoHasta() {
		return pesoHasta;
	}

	public void setPesoHasta(BigDecimal pesoHasta) {
		this.pesoHasta = pesoHasta;
	}
	
	
	
	

}
