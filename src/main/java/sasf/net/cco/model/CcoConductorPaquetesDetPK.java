package sasf.net.cco.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

import sasf.net.cco.model.CcoConductorPaquetesDet.ConductorPaquetesDetCreation;
import sasf.net.cco.model.CcoConductorPaquetesDet.ConductorPaquetesDetUpdate;


@Embeddable
public class CcoConductorPaquetesDetPK implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@NotNull(groups = ConductorPaquetesDetUpdate.class, message = "El código de conductores paquetes detalles no puede ser nulo")
	@Column(name="codigo",length = 10,nullable = false,updatable = false)
	private Long codigo;
	
	@NotNull(groups = {ConductorPaquetesDetCreation.class,ConductorPaquetesDetUpdate.class}, message = "El código de cabecera de conductor paquetes no puede ser nulo")
	@Column(name="cco_cp_cap_codigo",length = 10,nullable = false,updatable = false)
	private Long ccoCpCapCodigo;
	
	@NotNull(groups = {ConductorPaquetesDetCreation.class,ConductorPaquetesDetUpdate.class}, message = "El código de licenciatario no puede ser nulo")
	@Column(name="cco_cp_cap_age_licenc_codigo",length = 5,nullable = false,updatable = false)
	private Integer ccoCpCapAgeLicencCodigo;

	public CcoConductorPaquetesDetPK() {
		super();
	}

	public CcoConductorPaquetesDetPK(
			@NotNull(groups = ConductorPaquetesDetUpdate.class, message = "El código de conductores paquetes detalles no puede ser nulo") Long codigo,
			@NotNull(groups = { ConductorPaquetesDetCreation.class,
					ConductorPaquetesDetUpdate.class }, message = "El código de cabecera de conductor paquetes no puede ser nulo") Long ccoCpCapCodigo,
			@NotNull(groups = { ConductorPaquetesDetCreation.class,
					ConductorPaquetesDetUpdate.class }, message = "El código de licenciatario no puede ser nulo") Integer ccoCpCapAgeLicencCodigo) {
		super();
		this.codigo = codigo;
		this.ccoCpCapCodigo = ccoCpCapCodigo;
		this.ccoCpCapAgeLicencCodigo = ccoCpCapAgeLicencCodigo;
	}

	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public Long getCcoCpCapCodigo() {
		return ccoCpCapCodigo;
	}

	public void setCcoCpCapCodigo(Long ccoCpCapCodigo) {
		this.ccoCpCapCodigo = ccoCpCapCodigo;
	}

	public Integer getCcoCpCapAgeLicencCodigo() {
		return ccoCpCapAgeLicencCodigo;
	}

	public void setCcoCpCapAgeLicencCodigo(Integer ccoCpCapAgeLicencCodigo) {
		this.ccoCpCapAgeLicencCodigo = ccoCpCapAgeLicencCodigo;
	}
	
	
	
}
