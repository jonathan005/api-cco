package sasf.net.cco.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import sasf.net.cco.utils.EntidadCamposGenerales;
import sasf.net.cco.utils.Globales;

@Cacheable(false)
@Entity
@Table(name="cco_paquete")
@NamedQuery(name="CcoPaquete.findAll", query="SELECT c FROM CcoPaquete c")
public class CcoPaquete extends EntidadCamposGenerales implements Serializable{
	
	private static final long serialVersionUID = 1L;

	public interface PaqueteCreation {}

	public interface PaqueteUpdate {}
	
	@Valid
	@NotNull(groups = {PaqueteCreation.class, PaqueteUpdate.class}, message = "El id de paquete no puede ser nulo")
	@EmbeddedId
	private CcoPaquetePK id;

	@Column(name="codigo_paquete",updatable=false,nullable = false,length=200)
	private String codigoPaquete;
	
	@NotNull(groups = {PaqueteCreation.class}, message = "El tipo de paquete no puede ser nula")
	@Pattern(groups = { PaqueteCreation.class,PaqueteUpdate.class}, regexp = "["+Globales.PAQUETE+Globales.DOCUMENTO+"]", message = "El campo tipo de paquete debe ser "+Globales.PAQUETE+" o "+Globales.DOCUMENTO+"")
	@Column(name="tipo_paquete",updatable=true,nullable = false,length=1)
	private String tipoPaquete;
	
	
	@NotNull(groups = {PaqueteCreation.class}, message = "La fecha de envio no puede ser nula")
	@Column(name = "fecha_envio",nullable=false,updatable = true)
	private Date fechaEnvio;
	
	@NotNull(groups = {PaqueteCreation.class}, message = "El peso no puede ser nulo")
	@Column(name="peso",updatable=true,nullable = false,precision =18 ,scale = 4)
	private BigDecimal peso;
	
	@NotNull(groups = {PaqueteCreation.class}, message = "El costo no puede ser nulo")
	@Column(name="costo",updatable=true,nullable = false,precision =18 ,scale = 4)
	private BigDecimal costo;
	
	@NotNull(groups = {PaqueteCreation.class}, message = "El estado de paquete no puede ser nula")
	@Pattern(groups = { PaqueteCreation.class,
			PaqueteUpdate.class }, regexp = "("+Globales.PAQUETE_ESTADO_CENTRO_ACOPIO+")|("+Globales.PAQUETE_ESTADO_EN_CAMINO+")|("+Globales.PAQUETE_ESTADO_ENTREGADO+")|("+Globales.PAQUETE_ESTADO_INGRESADO+")|("+Globales.PAQUETE_ESTADO_OFICINA+")|("+Globales.PAQUETE_ESTADO_PENDIENTE_PAGO+")|("+Globales.PAQUETE_ESTADO_SOLICITUD_RECOLECCION+")", message = "El estado de paquete debe ser "+Globales.PAQUETE_ESTADO_CENTRO_ACOPIO+", "+Globales.PAQUETE_ESTADO_EN_CAMINO+", "+Globales.PAQUETE_ESTADO_ENTREGADO+", "+Globales.PAQUETE_ESTADO_INGRESADO+", "+Globales.PAQUETE_ESTADO_OFICINA+", "+Globales.PAQUETE_ESTADO_PENDIENTE_PAGO+" o "+Globales.PAQUETE_ESTADO_SOLICITUD_RECOLECCION)
	@Column(name="estado_paquete",updatable=true,nullable = false,length=2)
	private String estadoPaquete;
	
	@NotNull(groups = {PaqueteCreation.class}, message = "El código de licenciatario de embalaje no puede ser nulo")	
	@Min(value=1,groups = {PaqueteCreation.class,PaqueteUpdate.class}, message = "El código de licenciatario de embalaje debe ser mayor a cero")
	@Column(name="coo_embala_age_licenc_codigo",length = 5,nullable = false,updatable = true)
	private Integer CooEmbalaAgeLicencCodigo;
	
	@NotNull(groups = {PaqueteCreation.class}, message = "El código de embalaje no puede ser nulo")	
	@Min(value=1,groups = {PaqueteCreation.class,PaqueteUpdate.class}, message = "El código de embalaje debe ser mayor a cero")
	@Column(name="coo_embala_codigo",length = 5,nullable = false,updatable = true)
	private Long CooEmbalaCodigo;
	
	@NotNull(groups = {PaqueteCreation.class}, message = "El código de localidad desde no puede ser nulo")	
	@Min(value=1,groups = {PaqueteCreation.class,PaqueteUpdate.class}, message = "El código de localidad desde debe ser mayor a cero")
	@Column(name="age_locali_codigo",length = 5,nullable = false,updatable = true)
	private Integer ageLocaliCodigo;
	
	@NotNull(groups = {PaqueteCreation.class}, message = "El código de tipo de localidad desde no puede ser nulo")	
	@Min(value=1,groups = {PaqueteCreation.class,PaqueteUpdate.class}, message = "El código tipo de localidad desde debe ser mayor a cero")
	@Column(name="age_locali_tip_lo_codigo",length = 5,nullable = false,updatable = true)
	private Integer ageLocaliTipLoCodigo;
	
	@NotNull(groups = {PaqueteCreation.class}, message = "El código de país desde no puede ser nulo")	
	@Min(value=1,groups = {PaqueteCreation.class,PaqueteUpdate.class}, message = "El código país desde debe ser mayor a cero")
	@Column(name="agecali_tip_lo_age_pais_codigo",length = 5,nullable = false,updatable = true)
	private Integer agecaliTipLoAgePaisCodigo;
	
	@NotNull(groups = {PaqueteCreation.class}, message = "El código de localidad hasta no puede ser nulo")
	@Min(value=1,groups = {PaqueteCreation.class,PaqueteUpdate.class}, message = "El código de localidad hasta debe ser mayor a cero")
	@Column(name="age_locali_codigo_h",length = 5,nullable = false,updatable = true)
	private Integer ageLocaliCodigoH;
	
	@NotNull(groups = {PaqueteCreation.class}, message = "El código de tipo de localidad hasta no puede ser nulo")	
	@Min(value=1,groups = {PaqueteCreation.class,PaqueteUpdate.class}, message = "El código tipo de localidad hasta debe ser mayor a cero")
	@Column(name="age_locali_tip_lo_codigo_h",length = 5,nullable = false,updatable = true)
	private Integer ageLocaliTipLoCodigoH;
	
	@NotNull(groups = {PaqueteCreation.class}, message = "El código de país hasta no puede ser nulo")	
	@Min(value=1,groups = {PaqueteCreation.class,PaqueteUpdate.class}, message = "El código país hasta debe ser mayor a cero")
	@Column(name="ageli_tip_lo_age_pais_codigo_h",length = 5,nullable = false,updatable = true)
	private Integer ageliTipLoAgePaisCodigoH;
	
	@NotNull(groups = {PaqueteCreation.class}, message = "El código de licenciatario de forma de pago no puede ser nulo")	
	@Min(value=1,groups = {PaqueteCreation.class,PaqueteUpdate.class}, message = "El código de licenciatario de formas de pago debe ser mayor a cero")
	@Column(name="age_for_pa_age_licenc_codigo",length = 5,nullable = false,updatable = true)
	private Integer ageForPaAgeLicencCodigo;
	
	@NotNull(groups = {PaqueteCreation.class}, message = "El código de forma de pago no puede ser nulo")	
	@Min(value=1,groups = {PaqueteCreation.class,PaqueteUpdate.class}, message = "El código formas de pago debe ser mayor a cero")
	@Column(name="age_for_pa_codigo",length = 5,nullable = false,updatable = true)
	private Integer ageForPaCodigo;
	
	
	@Column(name="lugar_recoleccion",length = 200,nullable = true,updatable = true)
	private String lugarRecoleccion;
	
	@Column(name="latitud_recoleccion",length = 20,nullable = true,updatable = true)
	private String latitudRecoleccion;
	
	@Column(name="longitud_recoleccion",length = 20,nullable = true,updatable = true)
	private String longitudRecoleccion;
	
	@Column(name="lugar_entrega",length = 2000,nullable = true,updatable = true)
	private String lugarEntrega;
	
	@Column(name="latitud_entrega",length = 20,nullable = true,updatable = true)
	private String latitudEntrega;
	
	@Column(name="longitud_entrega",length = 20,nullable = true,updatable = true)
	private String longitudEntrega;
	
	@Column(name="nombre_persona_entrega",length = 200,nullable = true,updatable = true)
	private String nombrePersonaEntrega;
	
	@Column(name="cedula_persona_entrega",length = 30,nullable = true,updatable = true)
	private String cedulaPersonaEntrega;
	
	@Column(name="telefono_persona_entrega",length = 30,nullable = true,updatable = true)
	private String telefonoPersonaEntrega;
	
	@NotNull(groups = {PaqueteCreation.class}, message = "El código de sucursal no puede ser nulo")
	@Min(value=1,groups = {PaqueteCreation.class,PaqueteUpdate.class}, message = "El código sucursal no puede ser menor o igual a cero")
	@Column(name="age_sucurs_codigo",length = 5,nullable = false,updatable = true)
	private Long ageSucursCodigo;
	
	@NotNull(groups = {PaqueteCreation.class}, message = "El código de licenciatario de sucursal no puede ser nulo")
	@Min(value=1,groups = {PaqueteCreation.class,PaqueteUpdate.class}, message = "El código licenciatario de sucursal no puede ser menor o igual a cero")
	@Column(name="age_sucurs_age_licenc_codigo",length = 5,nullable = false,updatable = true)
	private Integer ageSucursAgeLicencCodigo;
	
	@NotNull(groups = {PaqueteCreation.class}, message = "El código de cliente no puede ser nulo")
	@Min(value=1,groups = {PaqueteCreation.class,PaqueteUpdate.class}, message = "El código de cliente debe ser mayor a cero")
	@Column(name="cli_client_codigo",length = 10,nullable = false,updatable = true)
	private Long cliClientCodigo;
	
	@NotNull(groups = {PaqueteCreation.class}, message = "El código de licenciatario de cliente no puede ser nulo")
	@Min(value=1,groups = {PaqueteCreation.class,PaqueteUpdate.class}, message = "El código licenciatario de cliente debe ser mayor a cero")
	@Column(name="cli_client_age_licenc_codigo",length = 5,nullable = false,updatable = true)
	private Integer cliClientAgeLicencCodigo;
	
	@NotNull(groups = {PaqueteCreation.class}, message = "La descripción no puede ser nula")
	@NotBlank(groups = {PaqueteCreation.class,PaqueteUpdate.class}, message = "La descripción no puede estar vacío")
	@Column(name="descripcion",length=200,nullable = false,updatable = true)
	private String descripcion;
	
	
	@Column(name="peso_anterior",updatable=true,nullable = true,precision =18 ,scale = 4)
	private BigDecimal pesoAnterior;
	
	
	@Column(name="costo_anterior",updatable=true,nullable = true,precision =18 ,scale = 4)
	private BigDecimal costoAnterior;
	
	@Transient
	private String descripcionEmbalaje;
	@Transient
	private String localidadDesde;
	@Transient
	private String localidadHasta;
	
	@Transient
	private String clienteNombres;
	@Transient
	private String clienteIdentificacion;
	@Transient
	private String clienteTelefono;
	
	@PrePersist
	void preInsert() {
		this.fechaIngreso = new Date();
		this.fechaEstado = new Date();
	}
	
	@PreUpdate
	void preUpdate() {
		this.fechaModificacion = new Date();
		if(this.estado != null){
			this.fechaEstado = new Date();
		}
	}

	public CcoPaquete() {
		super();
	}

	


	public CcoPaquete(
			@Valid @NotNull(groups = { PaqueteCreation.class,
					PaqueteUpdate.class }, message = "El id de paquete no puede ser nulo") CcoPaquetePK id,
			String codigoPaquete,
			@NotNull(groups = PaqueteCreation.class, message = "El tipo de paquete no puede ser nula") @Pattern(groups = {
					PaqueteCreation.class,
					PaqueteUpdate.class }, regexp = "[PD]", message = "El campo tipo de paquete debe ser P o D") String tipoPaquete,
			@NotNull(groups = PaqueteCreation.class, message = "La fecha de envio no puede ser nula") Date fechaEnvio,
			@NotNull(groups = PaqueteCreation.class, message = "El peso no puede ser nulo") BigDecimal peso,
			@NotNull(groups = PaqueteCreation.class, message = "El costo no puede ser nulo") BigDecimal costo,
			@NotNull(groups = PaqueteCreation.class, message = "El estado de paquete no puede ser nula") @Pattern(groups = {
					PaqueteCreation.class,
					PaqueteUpdate.class }, regexp = "(CA)|(EC)|(EN)|(IN)|(O)|(PP)|(SR)", message = "El estado de paquete debe ser CA, EC, EN, IN, O, PP o SR") String estadoPaquete,
			@NotNull(groups = PaqueteCreation.class, message = "El código de licenciatario de embalaje no puede ser nulo") @Min(value = 1, groups = {
					PaqueteCreation.class,
					PaqueteUpdate.class }, message = "El código de licenciatario de embalaje debe ser mayor a cero") Integer cooEmbalaAgeLicencCodigo,
			@NotNull(groups = PaqueteCreation.class, message = "El código de embalaje no puede ser nulo") @Min(value = 1, groups = {
					PaqueteCreation.class,
					PaqueteUpdate.class }, message = "El código de embalaje debe ser mayor a cero") Long cooEmbalaCodigo,
			@NotNull(groups = PaqueteCreation.class, message = "El código de localidad desde no puede ser nulo") @Min(value = 1, groups = {
					PaqueteCreation.class,
					PaqueteUpdate.class }, message = "El código de localidad desde debe ser mayor a cero") Integer ageLocaliCodigo,
			@NotNull(groups = PaqueteCreation.class, message = "El código de tipo de localidad desde no puede ser nulo") @Min(value = 1, groups = {
					PaqueteCreation.class,
					PaqueteUpdate.class }, message = "El código tipo de localidad desde debe ser mayor a cero") Integer ageLocaliTipLoCodigo,
			@NotNull(groups = PaqueteCreation.class, message = "El código de país desde no puede ser nulo") @Min(value = 1, groups = {
					PaqueteCreation.class,
					PaqueteUpdate.class }, message = "El código país desde debe ser mayor a cero") Integer agecaliTipLoAgePaisCodigo,
			@NotNull(groups = PaqueteCreation.class, message = "El código de localidad hasta no puede ser nulo") @Min(value = 1, groups = {
					PaqueteCreation.class,
					PaqueteUpdate.class }, message = "El código de localidad hasta debe ser mayor a cero") Integer ageLocaliCodigoH,
			@NotNull(groups = PaqueteCreation.class, message = "El código de tipo de localidad hasta no puede ser nulo") @Min(value = 1, groups = {
					PaqueteCreation.class,
					PaqueteUpdate.class }, message = "El código tipo de localidad hasta debe ser mayor a cero") Integer ageLocaliTipLoCodigoH,
			@NotNull(groups = PaqueteCreation.class, message = "El código de país hasta no puede ser nulo") @Min(value = 1, groups = {
					PaqueteCreation.class,
					PaqueteUpdate.class }, message = "El código país hasta debe ser mayor a cero") Integer ageliTipLoAgePaisCodigoH,
			@NotNull(groups = PaqueteCreation.class, message = "El código de licenciatario de forma de pago no puede ser nulo") @Min(value = 1, groups = {
					PaqueteCreation.class,
					PaqueteUpdate.class }, message = "El código de licenciatario de formas de pago debe ser mayor a cero") Integer ageForPaAgeLicencCodigo,
			@NotNull(groups = PaqueteCreation.class, message = "El código de forma de pago no puede ser nulo") @Min(value = 1, groups = {
					PaqueteCreation.class,
					PaqueteUpdate.class }, message = "El código formas de pago debe ser mayor a cero") Integer ageForPaCodigo,
			String lugarRecoleccion, String latitudRecoleccion, String longitudRecoleccion, String lugarEntrega,
			String latitudEntrega, String longitudEntrega, String nombrePersonaEntrega, String cedulaPersonaEntrega,
			String telefonoPersonaEntrega,
			@NotNull(groups = PaqueteCreation.class, message = "El código de sucursal no puede ser nulo") @Min(value = 1, groups = {
					PaqueteCreation.class,
					PaqueteUpdate.class }, message = "El código sucursal no puede ser menor o igual a cero") Long ageSucursCodigo,
			@NotNull(groups = PaqueteCreation.class, message = "El código de licenciatario de sucursal no puede ser nulo") @Min(value = 1, groups = {
					PaqueteCreation.class,
					PaqueteUpdate.class }, message = "El código licenciatario de sucursal no puede ser menor o igual a cero") Integer ageSucursAgeLicencCodigo,
			@NotNull(groups = PaqueteCreation.class, message = "El código de cliente no puede ser nulo") @Min(value = 1, groups = {
					PaqueteCreation.class,
					PaqueteUpdate.class }, message = "El código de cliente debe ser mayor a cero") Long cliClientCodigo,
			@NotNull(groups = PaqueteCreation.class, message = "El código de licenciatario de cliente no puede ser nulo") @Min(value = 1, groups = {
					PaqueteCreation.class,
					PaqueteUpdate.class }, message = "El código licenciatario de cliente debe ser mayor a cero") Integer cliClientAgeLicencCodigo,
			@NotNull(groups = PaqueteCreation.class, message = "La descripción no puede ser nula") @NotBlank(groups = {
					PaqueteCreation.class,
					PaqueteUpdate.class }, message = "La descripción no puede estar vacío") String descripcion,
			String descripcionEmbalaje, String localidadDesde, String localidadHasta, String clienteNombres,
			String clienteIdentificacion, String clienteTelefono) {
		super();
		this.id = id;
		this.codigoPaquete = codigoPaquete;
		this.tipoPaquete = tipoPaquete;
		this.fechaEnvio = fechaEnvio;
		this.peso = peso;
		this.costo = costo;
		this.estadoPaquete = estadoPaquete;
		CooEmbalaAgeLicencCodigo = cooEmbalaAgeLicencCodigo;
		CooEmbalaCodigo = cooEmbalaCodigo;
		this.ageLocaliCodigo = ageLocaliCodigo;
		this.ageLocaliTipLoCodigo = ageLocaliTipLoCodigo;
		this.agecaliTipLoAgePaisCodigo = agecaliTipLoAgePaisCodigo;
		this.ageLocaliCodigoH = ageLocaliCodigoH;
		this.ageLocaliTipLoCodigoH = ageLocaliTipLoCodigoH;
		this.ageliTipLoAgePaisCodigoH = ageliTipLoAgePaisCodigoH;
		this.ageForPaAgeLicencCodigo = ageForPaAgeLicencCodigo;
		this.ageForPaCodigo = ageForPaCodigo;
		this.lugarRecoleccion = lugarRecoleccion;
		this.latitudRecoleccion = latitudRecoleccion;
		this.longitudRecoleccion = longitudRecoleccion;
		this.lugarEntrega = lugarEntrega;
		this.latitudEntrega = latitudEntrega;
		this.longitudEntrega = longitudEntrega;
		this.nombrePersonaEntrega = nombrePersonaEntrega;
		this.cedulaPersonaEntrega = cedulaPersonaEntrega;
		this.telefonoPersonaEntrega = telefonoPersonaEntrega;
		this.ageSucursCodigo = ageSucursCodigo;
		this.ageSucursAgeLicencCodigo = ageSucursAgeLicencCodigo;
		this.cliClientCodigo = cliClientCodigo;
		this.cliClientAgeLicencCodigo = cliClientAgeLicencCodigo;
		this.descripcion = descripcion;
		this.descripcionEmbalaje = descripcionEmbalaje;
		this.localidadDesde = localidadDesde;
		this.localidadHasta = localidadHasta;
		this.clienteNombres = clienteNombres;
		this.clienteIdentificacion = clienteIdentificacion;
		this.clienteTelefono = clienteTelefono;
	}

	public CcoPaquetePK getId() {
		return id;
	}

	public void setId(CcoPaquetePK id) {
		this.id = id;
	}

	public String getCodigoPaquete() {
		return codigoPaquete;
	}

	public void setCodigoPaquete(String codigoPaquete) {
		this.codigoPaquete = codigoPaquete;
	}

	public String getTipoPaquete() {
		return tipoPaquete;
	}

	public void setTipoPaquete(String tipoPaquete) {
		this.tipoPaquete = tipoPaquete;
	}

	public Date getFechaEnvio() {
		return fechaEnvio;
	}

	public void setFechaEnvio(Date fechaEnvio) {
		this.fechaEnvio = fechaEnvio;
	}

	public BigDecimal getPeso() {
		return peso;
	}

	public void setPeso(BigDecimal peso) {
		this.peso = peso;
	}

	public BigDecimal getCosto() {
		return costo;
	}

	public void setCosto(BigDecimal costo) {
		this.costo = costo;
	}

	public String getEstadoPaquete() {
		return estadoPaquete;
	}

	public void setEstadoPaquete(String estadoPaquete) {
		this.estadoPaquete = estadoPaquete;
	}

	public Integer getCooEmbalaAgeLicencCodigo() {
		return CooEmbalaAgeLicencCodigo;
	}

	public void setCooEmbalaAgeLicencCodigo(Integer cooEmbalaAgeLicencCodigo) {
		CooEmbalaAgeLicencCodigo = cooEmbalaAgeLicencCodigo;
	}

	public Long getCooEmbalaCodigo() {
		return CooEmbalaCodigo;
	}

	public void setCooEmbalaCodigo(Long cooEmbalaCodigo) {
		CooEmbalaCodigo = cooEmbalaCodigo;
	}

	public Integer getAgeLocaliCodigo() {
		return ageLocaliCodigo;
	}

	public void setAgeLocaliCodigo(Integer ageLocaliCodigo) {
		this.ageLocaliCodigo = ageLocaliCodigo;
	}

	public Integer getAgeLocaliTipLoCodigo() {
		return ageLocaliTipLoCodigo;
	}

	public void setAgeLocaliTipLoCodigo(Integer ageLocaliTipLoCodigo) {
		this.ageLocaliTipLoCodigo = ageLocaliTipLoCodigo;
	}

	public Integer getAgecaliTipLoAgePaisCodigo() {
		return agecaliTipLoAgePaisCodigo;
	}

	public void setAgecaliTipLoAgePaisCodigo(Integer agecaliTipLoAgePaisCodigo) {
		this.agecaliTipLoAgePaisCodigo = agecaliTipLoAgePaisCodigo;
	}

	public Integer getAgeLocaliCodigoH() {
		return ageLocaliCodigoH;
	}

	public void setAgeLocaliCodigoH(Integer ageLocaliCodigoH) {
		this.ageLocaliCodigoH = ageLocaliCodigoH;
	}

	public Integer getAgeLocaliTipLoCodigoH() {
		return ageLocaliTipLoCodigoH;
	}

	public void setAgeLocaliTipLoCodigoH(Integer ageLocaliTipLoCodigoH) {
		this.ageLocaliTipLoCodigoH = ageLocaliTipLoCodigoH;
	}

	public Integer getAgeliTipLoAgePaisCodigoH() {
		return ageliTipLoAgePaisCodigoH;
	}

	public void setAgeliTipLoAgePaisCodigoH(Integer ageliTipLoAgePaisCodigoH) {
		this.ageliTipLoAgePaisCodigoH = ageliTipLoAgePaisCodigoH;
	}

	public Integer getAgeForPaAgeLicencCodigo() {
		return ageForPaAgeLicencCodigo;
	}

	public void setAgeForPaAgeLicencCodigo(Integer ageForPaAgeLicencCodigo) {
		this.ageForPaAgeLicencCodigo = ageForPaAgeLicencCodigo;
	}

	public Integer getAgeForPaCodigo() {
		return ageForPaCodigo;
	}

	public void setAgeForPaCodigo(Integer ageForPaCodigo) {
		this.ageForPaCodigo = ageForPaCodigo;
	}

	public String getLugarRecoleccion() {
		return lugarRecoleccion;
	}

	public void setLugarRecoleccion(String lugarRecoleccion) {
		this.lugarRecoleccion = lugarRecoleccion;
	}

	public String getLatitudRecoleccion() {
		return latitudRecoleccion;
	}

	public void setLatitudRecoleccion(String latitudRecoleccion) {
		this.latitudRecoleccion = latitudRecoleccion;
	}

	public String getLongitudRecoleccion() {
		return longitudRecoleccion;
	}

	public void setLongitudRecoleccion(String longitudRecoleccion) {
		this.longitudRecoleccion = longitudRecoleccion;
	}

	public String getLugarEntrega() {
		return lugarEntrega;
	}

	public void setLugarEntrega(String lugarEntrega) {
		this.lugarEntrega = lugarEntrega;
	}

	public String getLatitudEntrega() {
		return latitudEntrega;
	}

	public void setLatitudEntrega(String latitudEntrega) {
		this.latitudEntrega = latitudEntrega;
	}

	public String getLongitudEntrega() {
		return longitudEntrega;
	}

	public void setLongitudEntrega(String longitudEntrega) {
		this.longitudEntrega = longitudEntrega;
	}

	public String getNombrePersonaEntrega() {
		return nombrePersonaEntrega;
	}

	public void setNombrePersonaEntrega(String nombrePersonaEntrega) {
		this.nombrePersonaEntrega = nombrePersonaEntrega;
	}

	public String getCedulaPersonaEntrega() {
		return cedulaPersonaEntrega;
	}

	public void setCedulaPersonaEntrega(String cedulaPersonaEntrega) {
		this.cedulaPersonaEntrega = cedulaPersonaEntrega;
	}

	public String getTelefonoPersonaEntrega() {
		return telefonoPersonaEntrega;
	}

	public void setTelefonoPersonaEntrega(String telefonoPersonaEntrega) {
		this.telefonoPersonaEntrega = telefonoPersonaEntrega;
	}

	public Long getAgeSucursCodigo() {
		return ageSucursCodigo;
	}

	public void setAgeSucursCodigo(Long ageSucursCodigo) {
		this.ageSucursCodigo = ageSucursCodigo;
	}

	public Integer getAgeSucursAgeLicencCodigo() {
		return ageSucursAgeLicencCodigo;
	}

	public void setAgeSucursAgeLicencCodigo(Integer ageSucursAgeLicencCodigo) {
		this.ageSucursAgeLicencCodigo = ageSucursAgeLicencCodigo;
	}

	public Long getCliClientCodigo() {
		return cliClientCodigo;
	}

	public void setCliClientCodigo(Long cliClientCodigo) {
		this.cliClientCodigo = cliClientCodigo;
	}

	public Integer getCliClientAgeLicencCodigo() {
		return cliClientAgeLicencCodigo;
	}

	public void setCliClientAgeLicencCodigo(Integer cliClientAgeLicencCodigo) {
		this.cliClientAgeLicencCodigo = cliClientAgeLicencCodigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getDescripcionEmbalaje() {
		return descripcionEmbalaje;
	}

	public void setDescripcionEmbalaje(String descripcionEmbalaje) {
		this.descripcionEmbalaje = descripcionEmbalaje;
	}

	public String getLocalidadDesde() {
		return localidadDesde;
	}

	public void setLocalidadDesde(String localidadDesde) {
		this.localidadDesde = localidadDesde;
	}

	public String getLocalidadHasta() {
		return localidadHasta;
	}

	public void setLocalidadHasta(String localidadHasta) {
		this.localidadHasta = localidadHasta;
	}

	public String getClienteNombres() {
		return clienteNombres;
	}

	public void setClienteNombres(String clienteNombres) {
		this.clienteNombres = clienteNombres;
	}

	public String getClienteIdentificacion() {
		return clienteIdentificacion;
	}

	public void setClienteIdentificacion(String clienteIdentificacion) {
		this.clienteIdentificacion = clienteIdentificacion;
	}

	public String getClienteTelefono() {
		return clienteTelefono;
	}

	public void setClienteTelefono(String clienteTelefono) {
		this.clienteTelefono = clienteTelefono;
	}

	public BigDecimal getPesoAnterior() {
		return pesoAnterior;
	}

	public void setPesoAnterior(BigDecimal pesoAnterior) {
		this.pesoAnterior = pesoAnterior;
	}

	public BigDecimal getCostoAnterior() {
		return costoAnterior;
	}

	public void setCostoAnterior(BigDecimal costoAnterior) {
		this.costoAnterior = costoAnterior;
	}


	

	
	
	
	
	
	
}
