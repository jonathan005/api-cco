package sasf.net.cco.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;


import sasf.net.cco.utils.EntidadCamposGenerales;

@Cacheable(false)
@Entity
@Table(name="cco_postulante_documentos")
@NamedQuery(name="CcoPostulanteDocumentos.findAll", query="SELECT c FROM CcoPostulanteDocumentos c")
public class CcoPostulanteDocumentos extends EntidadCamposGenerales implements Serializable{
	
	private static final long serialVersionUID = 1L;

	public interface PostulanteDocumentoCreation {}

	public interface PostulanteDocumentoUpdate {}
	
	@Valid
	@NotNull(groups = {PostulanteDocumentoCreation.class, PostulanteDocumentoUpdate.class}, message = "El id de requisito trabajo no puede ser nulo")
	@EmbeddedId
	private CcoPostulanteDocumentosPK id;
		
	@Column(name="ruta_requisito",updatable=true,nullable=true,length = 2000)
	private String rutaRequisito;
	
	@Column(name="ruta_requisito2",updatable=true,nullable=true,length = 2000)
	private String rutaRequisito2;
	
	@Column(name="valor_requisito",updatable=true,nullable=true,length = 2000)
	private String valorRequisito;
	
	@Transient
	private String tipoArchivo;
	
	@PrePersist
	void preInsert() {
		this.fechaIngreso = new Date();
		this.fechaEstado = new Date();
	}
	
	@PreUpdate
	void preUpdate() {
		this.fechaModificacion = new Date();
		if(this.estado != null){
			this.fechaEstado = new Date();
		}
	}


	public CcoPostulanteDocumentos() {
		super();
	}

	public CcoPostulanteDocumentos(@Valid @NotNull(groups = { PostulanteDocumentoCreation.class,
			PostulanteDocumentoUpdate.class }, message = "El id de requisito trabajo no puede ser nulo") CcoPostulanteDocumentosPK id,
			String rutaRequisito, String rutaRequisito2, String valorRequisito, String tipoArchivo) {
		super();
		this.id = id;
		this.rutaRequisito = rutaRequisito;
		this.rutaRequisito2 = rutaRequisito2;
		this.valorRequisito = valorRequisito;
		this.tipoArchivo = tipoArchivo;
	}

	public String getRutaRequisito2() {
		return rutaRequisito2;
	}

	public void setRutaRequisito2(String rutaRequisito2) {
		this.rutaRequisito2 = rutaRequisito2;
	}

	public String getTipoArchivo() {
		return tipoArchivo;
	}

	public void setTipoArchivo(String tipoArchivo) {
		this.tipoArchivo = tipoArchivo;
	}

	public CcoPostulanteDocumentosPK getId() {
		return id;
	}

	public void setId(CcoPostulanteDocumentosPK id) {
		this.id = id;
	}

	public String getRutaRequisito() {
		return rutaRequisito;
	}

	public void setRutaRequisito(String rutaRequisito) {
		this.rutaRequisito = rutaRequisito;
	}

	public String getValorRequisito() {
		return valorRequisito;
	}

	public void setValorRequisito(String valorRequisito) {
		this.valorRequisito = valorRequisito;
	}
	
}
