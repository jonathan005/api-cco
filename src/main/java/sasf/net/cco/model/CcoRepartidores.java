package sasf.net.cco.model;

import java.io.Serializable;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import sasf.net.cco.utils.EntidadCamposGenerales;
import sasf.net.cco.utils.Globales;

@Cacheable(false)
@Entity
@Table(name="cco_repartidores")
@NamedQuery(name="CcoRepartidores.findAll", query="SELECT c FROM CcoRepartidores c")
public class CcoRepartidores extends EntidadCamposGenerales implements Serializable{
	
	private static final long serialVersionUID = 1L;

	public interface RepartidoresCreation {}

	public interface RepartidoresUpdate {}
	
	@Valid
	@NotNull(groups = {RepartidoresCreation.class, RepartidoresUpdate.class}, message = "El id de repartidor no puede ser nulo")
	@EmbeddedId
	private CcoRepartidoresPK id;
	
	@NotNull(groups = {RepartidoresCreation.class}, message = "El nombre no puede ser nula")
	@NotBlank(groups = {RepartidoresCreation.class}, message = "El nombre no puede estar vacío")
	@Column(name="nombres",updatable=true,nullable = false,length=2000)
	private String nombres;
	
	@NotNull(groups = {RepartidoresCreation.class}, message = "El apellido no puede ser nula")
	@NotBlank(groups = {RepartidoresCreation.class}, message = "El apellido no puede estar vacío")
	@Column(name="apellidos",updatable=true,nullable = false,length=2000)
	private String apellidos;
	
	@NotNull(groups = {RepartidoresCreation.class}, message = "El número celular no puede ser nula")
	@NotBlank(groups = {RepartidoresCreation.class}, message = "El número celular no puede estar vacío")
	@Column(name="numero_celular",updatable=true,nullable = false,length=20)
	private String numeroCelular;
	
	@Column(name="correo_electronico",updatable=true,nullable = true,length=100)
	private String correoElectronico;
	
	@Column(name="direccion",updatable=true,nullable = true,length=1000)
	private String direccion;

	@Column(name="latitud",updatable=true,nullable = true,length=50)
	private String latitud;
	
	@Column(name="longitud",updatable=true,nullable = true,length=50)
	private String longitud;
	
	@NotNull(groups = RepartidoresCreation.class, message = "El estado de repartidor no puede ser nulo")
	@Pattern(groups = { RepartidoresCreation.class,
			RepartidoresUpdate.class }, regexp = "["+Globales.ESTADO_CONDUC_REPART_DISPONIBLE+Globales.ESTADO_CONDUC_REPART_ASIGNADO+Globales.ESTADO_CONDUC_REPART_NO_DISPONIBLE+"]", message = "El estado de repartidor debe ser "+Globales.ESTADO_CONDUC_REPART_DISPONIBLE+", "+Globales.ESTADO_CONDUC_REPART_ASIGNADO+" o "+Globales.ESTADO_CONDUC_REPART_NO_DISPONIBLE)
	@Column(name="estado_repartidor",updatable=true,nullable = false,length=1)
	private String estadoRepartidor;
	
	public CcoRepartidores() {
		super();
	}

	public CcoRepartidores(
			@Valid @NotNull(groups = { RepartidoresCreation.class,
					RepartidoresUpdate.class }, message = "El id de repartidor no puede ser nulo") CcoRepartidoresPK id,
			@NotNull(groups = RepartidoresCreation.class, message = "El nombre no puede ser nula") @NotBlank(groups = RepartidoresCreation.class, message = "El nombre no puede estar vacío") String nombres,
			@NotNull(groups = RepartidoresCreation.class, message = "El apellido no puede ser nula") @NotBlank(groups = RepartidoresCreation.class, message = "El apellido no puede estar vacío") String apellidos,
			@NotNull(groups = RepartidoresCreation.class, message = "El número celular no puede ser nula") @NotBlank(groups = RepartidoresCreation.class, message = "El número celular no puede estar vacío") String numeroCelular,
			String correoElectronico, String direccion, String latitud, String longitud,
			@NotNull(groups = RepartidoresCreation.class, message = "El estado de repartidor no puede ser nulo") @Pattern(groups = {
					RepartidoresCreation.class,
					RepartidoresUpdate.class }, regexp = "[DAN]", message = "El estado de repartidor debe ser D, A o N") String estadoRepartidor) {
		super();
		this.id = id;
		this.nombres = nombres;
		this.apellidos = apellidos;
		this.numeroCelular = numeroCelular;
		this.correoElectronico = correoElectronico;
		this.direccion = direccion;
		this.latitud = latitud;
		this.longitud = longitud;
		this.estadoRepartidor = estadoRepartidor;
	}

	public String getLatitud() {
		return latitud;
	}

	public void setLatitud(String latitud) {
		this.latitud = latitud;
	}

	public String getLongitud() {
		return longitud;
	}

	public void setLongitud(String longitud) {
		this.longitud = longitud;
	}

	public CcoRepartidoresPK getId() {
		return id;
	}

	public void setId(CcoRepartidoresPK id) {
		this.id = id;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getNumeroCelular() {
		return numeroCelular;
	}

	public void setNumeroCelular(String numeroCelular) {
		this.numeroCelular = numeroCelular;
	}

	public String getCorreoElectronico() {
		return correoElectronico;
	}

	public void setCorreoElectronico(String correoElectronico) {
		this.correoElectronico = correoElectronico;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getEstadoRepartidor() {
		return estadoRepartidor;
	}

	public void setEstadoRepartidor(String estadoRepartidor) {
		this.estadoRepartidor = estadoRepartidor;
	}
	
	
	
}
