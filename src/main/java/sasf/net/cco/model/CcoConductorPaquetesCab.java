package sasf.net.cco.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import sasf.net.cco.utils.EntidadCamposGenerales;

@Cacheable(false)
@Entity
@Table(name="cco_conductor_paquetes_cab")
@NamedQuery(name="CcoConductorPaquetesCab.findAll", query="SELECT c FROM CcoConductorPaquetesCab c")
public class CcoConductorPaquetesCab extends EntidadCamposGenerales implements Serializable {
	
	private static final long serialVersionUID = 1L;

	public interface ConductorPaquetesCabCreation {}

	public interface ConductorPaquetesCabUpdate {}
	
	@Valid
	@NotNull(groups = {ConductorPaquetesCabCreation.class, ConductorPaquetesCabUpdate.class}, message = "El id de conductor paquetes cab no puede ser nulo")
	@EmbeddedId
	private CcoConductorPaquetesCabPK id;
	
	@NotNull(groups = {ConductorPaquetesCabCreation.class}, message = "Peso de paquete desde no puede ser nulo")
	@Column(name="paquete_desde",updatable=true,nullable = false,precision =18)
	private BigDecimal paqueteDesde;
	
	@NotNull(groups = {ConductorPaquetesCabCreation.class}, message = "Peso de paquete hasta no puede ser nulo")
	@Column(name="paquete_hasta",updatable=true,nullable = false,precision =18)
	private BigDecimal paqueteHasta;
	
	@NotNull(groups = {ConductorPaquetesCabCreation.class}, message = "valor total de seguro no puede ser nulo")
	@Column(name="valor_total_seguro",updatable=true,nullable = false,precision =18,scale = 4)
	private BigDecimal valorTotalSeguro;
	
	@NotNull(groups = {ConductorPaquetesCabCreation.class}, message = "Fecha no puede ser nulo")
	@Column(name="fecha",updatable=true,nullable = false)
	private Date fecha;
	
	@NotNull(groups = {ConductorPaquetesCabCreation.class}, message = "Código de conductor no puede ser nulo")
	@Column(name="cco_conduc_codigo",updatable=true,nullable = false)
	private Long ccoConducCodigo;
	
	@NotNull(groups = {ConductorPaquetesCabCreation.class}, message = "Código de licenciatario de conductor no puede ser nulo")
	@Column(name="cco_conduc_age_licenc_codigo",updatable=true,nullable = false)
	private Integer ccoConducAgeLicencCodigo;
	
	@NotNull(groups = {ConductorPaquetesCabCreation.class}, message = "Número de paquetes no puede ser nulo")
	@Min(value = 1,groups = {ConductorPaquetesCabCreation.class},message="Número de paquetes debe ser mayor a cero")
	@Column(name="numero_paquetes",updatable=true)
	private Integer numeroPaquetes;
	
	@NotNull(groups = {ConductorPaquetesCabCreation.class}, message = "valor seguro no puede ser nulo")
	@Column(name="valor_seguro",updatable=true,precision =18,scale = 4)
	private  BigDecimal valorSeguro;
	
	@NotNull(groups = {ConductorPaquetesCabCreation.class}, message = "Número de vehículos no puede ser nulo")
	@Min(value = 1,groups = {ConductorPaquetesCabCreation.class},message="Número de vehículos debe ser mayor a cero")
	@Column(name="numero_vehiculos",updatable=true)
	private Integer numeroVehiculos;
	
	@NotNull(groups = {ConductorPaquetesCabCreation.class}, message = "Hora de salida no puede ser nulo")
	@Column(name="hora_salida",updatable=true)
	private Date horaSalida;
	
	@NotNull(groups = {ConductorPaquetesCabCreation.class}, message = "Hora de entrega no puede ser nulo")
	@Column(name="hora_entrega",updatable=true)
	private Date horaEntrega;
	
	@NotNull(groups = {ConductorPaquetesCabCreation.class}, message = "Ruta no puede ser nulo")
	@NotBlank(groups = {ConductorPaquetesCabCreation.class}, message = "Ruta no puede estar vacía")
	@Column(name="ruta",updatable=true,length = 200)
	private String ruta;
	
	@PrePersist
	void preInsert() {
		this.fechaIngreso = new Date();
		this.fechaEstado = new Date();
	}
	
	@PreUpdate
	void preUpdate() {
		this.fechaModificacion = new Date();
		if(this.estado != null){
			this.fechaEstado = new Date();
		}
	}

	public CcoConductorPaquetesCab() {
		super();
	}


	public CcoConductorPaquetesCab(@Valid @NotNull(groups = { ConductorPaquetesCabCreation.class,
			ConductorPaquetesCabUpdate.class }, message = "El id de conductor paquetes cab no puede ser nulo") CcoConductorPaquetesCabPK id,
			@NotNull(groups = ConductorPaquetesCabCreation.class, message = "Peso de paquete desde no puede ser nulo") BigDecimal paqueteDesde,
			@NotNull(groups = ConductorPaquetesCabCreation.class, message = "Peso de paquete hasta no puede ser nulo") BigDecimal paqueteHasta,
			@NotNull(groups = ConductorPaquetesCabCreation.class, message = "valor total de seguro no puede ser nulo") BigDecimal valorTotalSeguro,
			@NotNull(groups = ConductorPaquetesCabCreation.class, message = "Fecha no puede ser nulo") Date fecha,
			@NotNull(groups = ConductorPaquetesCabCreation.class, message = "Código de conductor no puede ser nulo") Long ccoConducCodigo,
			@NotNull(groups = ConductorPaquetesCabCreation.class, message = "Código de licenciatario de conductor no puede ser nulo") Integer ccoConducAgeLicencCodigo,
			@NotNull(groups = ConductorPaquetesCabCreation.class, message = "Número de paquetes no puede ser nulo") @Min(value = 1, groups = ConductorPaquetesCabCreation.class, message = "Número de paquetes debe ser mayor a cero") Integer numeroPaquetes,
			@NotNull(groups = ConductorPaquetesCabCreation.class, message = "valor seguro no puede ser nulo") BigDecimal valorSeguro,
			@NotNull(groups = ConductorPaquetesCabCreation.class, message = "Número de vehículos no puede ser nulo") @Min(value = 1, groups = ConductorPaquetesCabCreation.class, message = "Número de vehículos debe ser mayor a cero") Integer numeroVehiculos,
			@NotNull(groups = ConductorPaquetesCabCreation.class, message = "Hora de salida no puede ser nulo") Date horaSalida,
			@NotNull(groups = ConductorPaquetesCabCreation.class, message = "Hora de entrega no puede ser nulo") Date horaEntrega,
			@NotNull(groups = ConductorPaquetesCabCreation.class, message = "Ruta no puede ser nulo") @NotBlank(groups = ConductorPaquetesCabCreation.class, message = "Ruta no puede estar vacía") String ruta) {
		super();
		this.id = id;
		this.paqueteDesde = paqueteDesde;
		this.paqueteHasta = paqueteHasta;
		this.valorTotalSeguro = valorTotalSeguro;
		this.fecha = fecha;
		this.ccoConducCodigo = ccoConducCodigo;
		this.ccoConducAgeLicencCodigo = ccoConducAgeLicencCodigo;
		this.numeroPaquetes = numeroPaquetes;
		this.valorSeguro = valorSeguro;
		this.numeroVehiculos = numeroVehiculos;
		this.horaSalida = horaSalida;
		this.horaEntrega = horaEntrega;
		this.ruta = ruta;
	}

	public CcoConductorPaquetesCabPK getId() {
		return id;
	}

	public void setId(CcoConductorPaquetesCabPK id) {
		this.id = id;
	}

	public BigDecimal getPaqueteDesde() {
		return paqueteDesde;
	}

	public void setPaqueteDesde(BigDecimal paqueteDesde) {
		this.paqueteDesde = paqueteDesde;
	}

	public BigDecimal getPaqueteHasta() {
		return paqueteHasta;
	}

	public void setPaqueteHasta(BigDecimal paqueteHasta) {
		this.paqueteHasta = paqueteHasta;
	}

	public BigDecimal getValorTotalSeguro() {
		return valorTotalSeguro;
	}

	public void setValorTotalSeguro(BigDecimal valorTotalSeguro) {
		this.valorTotalSeguro = valorTotalSeguro;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public Long getCcoConducCodigo() {
		return ccoConducCodigo;
	}

	public void setCcoConducCodigo(Long ccoConducCodigo) {
		this.ccoConducCodigo = ccoConducCodigo;
	}

	public Integer getCcoConducAgeLicencCodigo() {
		return ccoConducAgeLicencCodigo;
	}

	public void setCcoConducAgeLicencCodigo(Integer ccoConducAgeLicencCodigo) {
		this.ccoConducAgeLicencCodigo = ccoConducAgeLicencCodigo;
	}

	public Integer getNumeroPaquetes() {
		return numeroPaquetes;
	}

	public void setNumeroPaquetes(Integer numeroPaquetes) {
		this.numeroPaquetes = numeroPaquetes;
	}

	public BigDecimal getValorSeguro() {
		return valorSeguro;
	}

	public void setValorSeguro(BigDecimal valorSeguro) {
		this.valorSeguro = valorSeguro;
	}

	public Integer getNumeroVehiculos() {
		return numeroVehiculos;
	}

	public void setNumeroVehiculos(Integer numeroVehiculos) {
		this.numeroVehiculos = numeroVehiculos;
	}

	public Date getHoraSalida() {
		return horaSalida;
	}

	public void setHoraSalida(Date horaSalida) {
		this.horaSalida = horaSalida;
	}

	public Date getHoraEntrega() {
		return horaEntrega;
	}

	public void setHoraEntrega(Date horaEntrega) {
		this.horaEntrega = horaEntrega;
	}

	public String getRuta() {
		return ruta;
	}

	public void setRuta(String ruta) {
		this.ruta = ruta;
	}
	
	
}
