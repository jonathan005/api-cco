package sasf.net.cco.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import sasf.net.cco.utils.EntidadCamposGenerales;
import sasf.net.cco.utils.Globales;

@Cacheable(false)
@Entity
@Table(name="cco_conductores")
@NamedQuery(name="CcoConductores.findAll", query="SELECT c FROM CcoConductores c")

public class CcoConductores extends EntidadCamposGenerales implements Serializable {

	private static final long serialVersionUID = 1L;
    
	public interface ConductorCreation {};
	
	public interface ConductorUpdate {};
	
	@Valid
	@NotNull(groups = {ConductorCreation.class, ConductorUpdate.class}, message = "El id de conductor no puede ser nulo")
	@EmbeddedId
	private CcoConductoresPK id;
	
	@NotNull(groups = {ConductorCreation.class} ,message = "EL nombre no puede ser nulo")
	@NotBlank (groups = {ConductorCreation.class} ,message = "EL nombre no puede estar vacío")
	@Column(name="nombres",updatable=true,nullable = false,length=2000)
	private  String nombre ;
	
	@NotNull(groups = {ConductorCreation.class} ,message = "EL apellido no puede ser nulo")
	@NotBlank (groups = {ConductorCreation.class} ,message = "EL apellido no puede estar vacío")
	@Column(name="apellidos",updatable=true,nullable = false,length=2000)
	private String apellido;
	
	@NotNull(groups = {ConductorCreation.class} ,message = "EL número celular no puede ser nulo")
	@NotBlank (groups = {ConductorCreation.class} ,message = "EL número celular no puede estar vacío")
	@Column(name="numero_celular",updatable=true,nullable = false,length=20)
	private String numeroCelular ;
	
	@Column(name="correo_electronico",updatable=true,nullable = true,length=100)
	private String correoElectronico ;
	
	@NotNull(groups = {ConductorCreation.class} ,message = "EL número de matricula no puede ser nulo")
	@NotBlank (groups = {ConductorCreation.class} ,message = "EL número de matricula no puede estar vacío")
	@Column(name="numero_matricula",updatable=true,nullable = false,length=20)
	private String numeroMatricula;
	
	@Column(name="latitud",updatable=true,nullable = true,length=50)
	private String latitud;
	
	@Column(name="longitud",updatable=true,nullable = true,length=50)
	private String longitud;

	@NotNull(groups = ConductorCreation.class, message = "El estado de conductor no puede ser nulo")
	@Pattern(groups = { ConductorCreation.class,
			ConductorUpdate.class }, regexp = "["+Globales.ESTADO_CONDUC_REPART_DISPONIBLE+Globales.ESTADO_CONDUC_REPART_ASIGNADO+Globales.ESTADO_CONDUC_REPART_NO_DISPONIBLE+"]", message = "El estado de conductor debe ser "+Globales.ESTADO_CONDUC_REPART_DISPONIBLE+", "+Globales.ESTADO_CONDUC_REPART_ASIGNADO+" o "+Globales.ESTADO_CONDUC_REPART_NO_DISPONIBLE)
	@Column(name="estado_conductor",updatable=true,nullable = false,length=1)
	private String estadoConductor;
	
	@NotNull(groups = ConductorCreation.class, message = "El licenciatario de empresa recolectora no puede ser nulo")
	@Column(name="cco_emp_re_age_licenc_codigo",length = 5,nullable = false,updatable = true)
	private Integer ccoEmpReAgeLicencCodigo;
	
	@NotNull(groups = ConductorCreation.class, message = "El código de empresa recolectora no puede ser nulo")
	@Column(name="cco_emp_re_codigo",length = 10,nullable = false,updatable = true)
	private Long ccoEmpReCodigo;
	
	@NotNull(groups = {ConductorCreation.class}, message = "El código de localidad desde no puede ser nulo")	
	@Min(value=1,groups = {ConductorCreation.class}, message = "El código de localidad desde debe ser mayor a cero")
	@Column(name="age_locali_codigo",length = 5,nullable = false,updatable = true)
	private Integer ageLocaliCodigo;
	
	@NotNull(groups = {ConductorCreation.class}, message = "El código de tipo de localidad desde no puede ser nulo")	
	@Min(value=1,groups = {ConductorCreation.class}, message = "El código tipo de localidad desde debe ser mayor a cero")
	@Column(name="age_locali_tip_lo_codigo",length = 5,nullable = false,updatable = true)
	private Integer ageLocaliTipLoCodigo;
	
	@NotNull(groups = {ConductorCreation.class}, message = "El código de país desde no puede ser nulo")	
	@Min(value=1,groups = {ConductorCreation.class}, message = "El código país desde debe ser mayor a cero")
	@Column(name="agecali_tip_lo_age_pais_codigo",length = 5,nullable = false,updatable = true)
	private Integer agecaliTipLoAgePaisCodigo;
	
	@PrePersist
	void preInsert() {
		this.fechaIngreso = new Date();
		this.fechaEstado = new Date();
	}
	
	@PreUpdate
	void preUpdate() {
		this.fechaModificacion = new Date();
		if(this.estado != null){
			this.fechaEstado = new Date();
		}
	}

	public CcoConductores() {
		super();
	}
	

	public CcoConductores(
			@Valid @NotNull(groups = { ConductorCreation.class,
					ConductorUpdate.class }, message = "El id de conductor no puede ser nulo") CcoConductoresPK id,
			@NotNull(groups = ConductorCreation.class, message = "EL nombre no puede ser nulo") @NotBlank(groups = ConductorCreation.class, message = "EL nombre no puede estar vacío") String nombre,
			@NotNull(groups = ConductorCreation.class, message = "EL apellido no puede ser nulo") @NotBlank(groups = ConductorCreation.class, message = "EL apellido no puede estar vacío") String apellido,
			@NotNull(groups = ConductorCreation.class, message = "EL número celular no puede ser nulo") @NotBlank(groups = ConductorCreation.class, message = "EL número celular no puede estar vacío") String numeroCelular,
			String correoElectronico,
			@NotNull(groups = ConductorCreation.class, message = "EL número de matricula no puede ser nulo") @NotBlank(groups = ConductorCreation.class, message = "EL número de matricula no puede estar vacío") String numeroMatricula,
			String latitud, String longitud,
			@NotNull(groups = ConductorCreation.class, message = "El estado de conductor no puede ser nulo") @Pattern(groups = {
					ConductorCreation.class,
					ConductorUpdate.class }, regexp = "[DAN]", message = "El estado de conductor debe ser D, A o N") String estadoConductor,
			@NotNull(groups = ConductorCreation.class, message = "El licenciatario de empresa recolectora no puede ser nulo") Integer ccoEmpReAgeLicencCodigo,
			@NotNull(groups = ConductorCreation.class, message = "El código de empresa recolectora no puede ser nulo") Long ccoEmpReCodigo) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.apellido = apellido;
		this.numeroCelular = numeroCelular;
		this.correoElectronico = correoElectronico;
		this.numeroMatricula = numeroMatricula;
		this.latitud = latitud;
		this.longitud = longitud;
		this.estadoConductor = estadoConductor;
		this.ccoEmpReAgeLicencCodigo = ccoEmpReAgeLicencCodigo;
		this.ccoEmpReCodigo = ccoEmpReCodigo;
	}

	public CcoConductoresPK getId() {
		return id;
	}

	public void setId(CcoConductoresPK id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getNumeroCelular() {
		return numeroCelular;
	}

	public void setNumeroCelular(String numeroCelular) {
		this.numeroCelular = numeroCelular;
	}

	public String getCorreoElectronico() {
		return correoElectronico;
	}

	public void setCorreoElectronico(String correoElectronico) {
		this.correoElectronico = correoElectronico;
	}

	public String getNumeroMatricula() {
		return numeroMatricula;
	}

	public void setNumeroMatricula(String numeroMatricula) {
		this.numeroMatricula = numeroMatricula;
	}

	public String getLatitud() {
		return latitud;
	}

	public void setLatitud(String latitud) {
		this.latitud = latitud;
	}

	public String getLongitud() {
		return longitud;
	}

	public void setLongitud(String longitud) {
		this.longitud = longitud;
	}

	public String getEstadoConductor() {
		return estadoConductor;
	}

	public void setEstadoConductor(String estadoConductor) {
		this.estadoConductor = estadoConductor;
	}

	public Integer getCcoEmpReAgeLicencCodigo() {
		return ccoEmpReAgeLicencCodigo;
	}

	public void setCcoEmpReAgeLicencCodigo(Integer ccoEmpReAgeLicencCodigo) {
		this.ccoEmpReAgeLicencCodigo = ccoEmpReAgeLicencCodigo;
	}

	public Long getCcoEmpReCodigo() {
		return ccoEmpReCodigo;
	}

	public void setCcoEmpReCodigo(Long ccoEmpReCodigo) {
		this.ccoEmpReCodigo = ccoEmpReCodigo;
	}

	public Integer getAgeLocaliCodigo() {
		return ageLocaliCodigo;
	}

	public void setAgeLocaliCodigo(Integer ageLocaliCodigo) {
		this.ageLocaliCodigo = ageLocaliCodigo;
	}

	public Integer getAgeLocaliTipLoCodigo() {
		return ageLocaliTipLoCodigo;
	}

	public void setAgeLocaliTipLoCodigo(Integer ageLocaliTipLoCodigo) {
		this.ageLocaliTipLoCodigo = ageLocaliTipLoCodigo;
	}

	public Integer getAgecaliTipLoAgePaisCodigo() {
		return agecaliTipLoAgePaisCodigo;
	}

	public void setAgecaliTipLoAgePaisCodigo(Integer agecaliTipLoAgePaisCodigo) {
		this.agecaliTipLoAgePaisCodigo = agecaliTipLoAgePaisCodigo;
	}
	
	
	
	
	
	
	
	
}
