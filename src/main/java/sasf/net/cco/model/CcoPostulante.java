package sasf.net.cco.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import sasf.net.cco.utils.EntidadCamposGenerales;
import sasf.net.cco.utils.Globales;

@Cacheable(false)
@Entity
@Table(name="cco_postulante")
@NamedQuery(name="CcoPostulante.findAll", query="SELECT c FROM CcoPostulante c")
public class CcoPostulante extends EntidadCamposGenerales implements Serializable{
	

	private static final long serialVersionUID = 1L;

	public interface PostulanteCreation {}

	public interface PostulanteUpdate {}
	
	@Valid
	@NotNull(groups = {PostulanteCreation.class, PostulanteUpdate.class}, message = "El id de postulante no puede ser nulo")
	@EmbeddedId
	private CcoPostulantePK id;
	
	@NotNull(groups = {PostulanteCreation.class}, message = "Código de puesto de trabajo no puede ser nulo")
	@Min(value = 1,groups = {PostulanteCreation.class},message="Código de puesto de trabajo debe ser mayor a cero")
	@Column(name="cco_pu_tra_codigo",updatable=true,nullable=false)
	private Long ccoPuTraCodigo;
	
	@NotNull(groups = {PostulanteCreation.class}, message = "Código de licenciatario de puesto de trabajo no puede ser nulo")
	@Min(value = 1,groups = {PostulanteCreation.class},message="Código de licenciatario de puesto de trabajo debe ser mayor a cero")
	@Column(name="cco_pu_tra_age_licenc_codigo",updatable=true,nullable=false)
	private Integer ccoPuTraAgeLicencCodigo;
	
	@Column(name="nombre",updatable=true,nullable=true,length = 200)
	private String nombre;
	
	@Column(name="apellido",updatable=true,nullable=true,length = 200)
	private String apellido;
	
	@Column(name="telefono",updatable=true,nullable=true,length = 20)
	private String telefono;
	
	@Column(name="correo_electronico",updatable=true,nullable=true,length = 200)
	private String correoElectronico;
	
	@Column(name="direccion",updatable=true,nullable=true,length = 2000)
	private String direccion;
	
	@NotNull(groups = PostulanteCreation.class, message = "El estado de postulante no puede ser nulo")
	@Pattern(groups = { PostulanteCreation.class,
			PostulanteUpdate.class }, regexp = "["+Globales.ESTADO_POSTULANTE_INGRESADO+Globales.ESTADO_POSTULANTE_APROBADO+Globales.ESTADO_POSTULANTE_RECHAZADO+"]", message = "El estado de postulante debe ser "+Globales.ESTADO_POSTULANTE_INGRESADO+", "+Globales.ESTADO_POSTULANTE_APROBADO+" o "+Globales.ESTADO_POSTULANTE_RECHAZADO)
	@Column(name="estado_postulante",updatable=true,nullable = false,length=1)
	private String estadoPostulante;
	
	@Transient
	private List<CcoPostulanteDocumentos> postulanteDocumentos;
	
	@PrePersist
	void preInsert() {
		this.fechaIngreso = new Date();
		this.fechaEstado = new Date();
	}
	
	@PreUpdate
	void preUpdate() {
		this.fechaModificacion = new Date();
		if(this.estado != null){
			this.fechaEstado = new Date();
		}
	}
	
	public CcoPostulante() {
		super();
	}


	public CcoPostulante(
			@Valid @NotNull(groups = { PostulanteCreation.class,
					PostulanteUpdate.class }, message = "El id de postulante no puede ser nulo") CcoPostulantePK id,
			@NotNull(groups = PostulanteCreation.class, message = "Código de puesto de trabajo no puede ser nulo") @Min(value = 1, groups = PostulanteCreation.class, message = "Código de puesto de trabajo debe ser mayor a cero") Long ccoPuTraCodigo,
			@NotNull(groups = PostulanteCreation.class, message = "Código de licenciatario de puesto de trabajo no puede ser nulo") @Min(value = 1, groups = PostulanteCreation.class, message = "Código de licenciatario de puesto de trabajo debe ser mayor a cero") Integer ccoPuTraAgeLicencCodigo,
			String nombre, String apellido, String telefono, String correoElectronico, String direccion,
			@NotNull(groups = PostulanteCreation.class, message = "El estado de postulante no puede ser nulo") @Pattern(groups = {
					PostulanteCreation.class,
					PostulanteUpdate.class }, regexp = "[IAR]", message = "El estado de postulante debe ser I, A o R") String estadoPostulante) {
		super();
		this.id = id;
		this.ccoPuTraCodigo = ccoPuTraCodigo;
		this.ccoPuTraAgeLicencCodigo = ccoPuTraAgeLicencCodigo;
		this.nombre = nombre;
		this.apellido = apellido;
		this.telefono = telefono;
		this.correoElectronico = correoElectronico;
		this.direccion = direccion;
		this.estadoPostulante = estadoPostulante;
	}

	public CcoPostulantePK getId() {
		return id;
	}

	public void setId(CcoPostulantePK id) {
		this.id = id;
	}

	public Long getCcoPuTraCodigo() {
		return ccoPuTraCodigo;
	}

	public void setCcoPuTraCodigo(Long ccoPuTraCodigo) {
		this.ccoPuTraCodigo = ccoPuTraCodigo;
	}

	public Integer getCcoPuTraAgeLicencCodigo() {
		return ccoPuTraAgeLicencCodigo;
	}

	public void setCcoPuTraAgeLicencCodigo(Integer ccoPuTraAgeLicencCodigo) {
		this.ccoPuTraAgeLicencCodigo = ccoPuTraAgeLicencCodigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getCorreoElectronico() {
		return correoElectronico;
	}

	public void setCorreoElectronico(String correoElectronico) {
		this.correoElectronico = correoElectronico;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getEstadoPostulante() {
		return estadoPostulante;
	}

	public void setEstadoPostulante(String estadoPostulante) {
		this.estadoPostulante = estadoPostulante;
	}

	public List<CcoPostulanteDocumentos> getPostulanteDocumentos() {
		return postulanteDocumentos;
	}

	public void setPostulanteDocumentos(List<CcoPostulanteDocumentos> postulanteDocumentos) {
		this.postulanteDocumentos = postulanteDocumentos;
	}
	
	
	
}
