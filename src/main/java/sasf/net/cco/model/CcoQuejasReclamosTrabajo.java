package sasf.net.cco.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import sasf.net.cco.model.CcoPaquete.PaqueteCreation;
import sasf.net.cco.model.CcoPaquete.PaqueteUpdate;
import sasf.net.cco.utils.EntidadCamposGenerales;
import sasf.net.cco.utils.Globales;

@Cacheable(false)
@Entity
@Table(name="cco_quejas_reclamos_trabajo")
@NamedQuery(name="CcoQuejasReclamosTrabajo.findAll", query="SELECT c FROM CcoQuejasReclamosTrabajo c")
public class CcoQuejasReclamosTrabajo extends EntidadCamposGenerales implements Serializable {
	
	private static final long serialVersionUID = 1L;

	public interface QuejaReclamoTrabajoCreation {}

	public interface QuejaReclamoTrabajoUpdate {}
	
	@Valid
	@NotNull(groups = {QuejaReclamoTrabajoCreation.class, QuejaReclamoTrabajoUpdate.class}, message = "El id de embalaje no puede ser nulo")
	@EmbeddedId
	private CcoQuejasReclamosTrabajoPK id;
	
	@NotNull(groups = {QuejaReclamoTrabajoCreation.class}, message = "La descripción no puede ser nula")
	@NotBlank(groups = {QuejaReclamoTrabajoCreation.class}, message = "La descripción no puede estar vacío")
	@Column(name="descripcion",updatable=true,nullable = false,length=2000)
	private String descripcion;
	
	@Column(name="ruta_imagen",updatable=true,nullable = true,length=500)
	private String rutaImagen;
	
	@NotNull(groups = {QuejaReclamoTrabajoCreation.class}, message = "El campo tipo no puede ser nulo")
	@Pattern(groups = { QuejaReclamoTrabajoCreation.class,
			QuejaReclamoTrabajoUpdate.class }, regexp = "["+Globales.QUEJA_RECLAMO+Globales.TRABAJA_CON_NOSOTROS+"]", message = "El campo tipo debe ser "+Globales.QUEJA_RECLAMO+" o "+Globales.TRABAJA_CON_NOSOTROS+"")
	@Column(name="tipo",updatable=true,nullable = false,length=1)
	private String tipo;
	
	@Min(value = 1,groups = {QuejaReclamoTrabajoCreation.class},message = "Código de paquete debe ser mayor a cero")
	@Column(name="cco_paquete_codigo",updatable=true,nullable = true,length=5)
	private Long ccoPaqueteCodigo;
	
	@Min(value = 1,groups = {QuejaReclamoTrabajoCreation.class},message = "Código de paquete debe ser mayor a cero")
	@Column(name="cco_paquete_age_licenc_codigo",updatable=true,nullable = true,length=5)
	private Integer ccoPaqueteAgeLicencCodigo;
	
	@Transient
	private String nombresCliente;
	
	@Transient
	private String apellidosCliente;
	
	@Transient
	private String telefonoCelularCliente;
	
	@Transient
	private String correoElectronicoCliente;
	
	@NotNull(groups = {QuejaReclamoTrabajoCreation.class}, message = "El código de cliente no puede ser nulo")
	@Min(value=1,groups = {QuejaReclamoTrabajoCreation.class}, message = "El código de cliente debe ser mayor a cero")
	@Column(name="cli_client_codigo",length = 10)
	private Long cliClientCodigo;
	
	@NotNull(groups = {QuejaReclamoTrabajoCreation.class}, message = "El código de licenciatario de cliente no puede ser nulo")
	@Min(value=1,groups = {QuejaReclamoTrabajoCreation.class}, message = "El código licenciatario de cliente debe ser mayor a cero")
	@Column(name="cli_client_age_licenc_codigo",length = 5)
	private Integer cliClientAgeLicencCodigo;
	
	@PrePersist
	void preInsert() {
		this.fechaIngreso = new Date();
		this.fechaEstado = new Date();
	}
	
	@PreUpdate
	void preUpdate() {
		this.fechaModificacion = new Date();
		if(this.estado != null){
			this.fechaEstado = new Date();
		}
	}
	
	
	public CcoQuejasReclamosTrabajo() {
		super();
	}
	
	
	

	public CcoQuejasReclamosTrabajo(@Valid @NotNull(groups = { QuejaReclamoTrabajoCreation.class,
			QuejaReclamoTrabajoUpdate.class }, message = "El id de embalaje no puede ser nulo") CcoQuejasReclamosTrabajoPK id,
			@NotNull(groups = QuejaReclamoTrabajoCreation.class, message = "La descripción no puede ser nula") @NotBlank(groups = QuejaReclamoTrabajoCreation.class, message = "La descripción no puede estar vacío") String descripcion,
			String rutaImagen,
			@NotNull(groups = QuejaReclamoTrabajoCreation.class, message = "El campo tipo no puede ser nulo") @Pattern(groups = {
					QuejaReclamoTrabajoCreation.class,
					QuejaReclamoTrabajoUpdate.class }, regexp = "[QT]", message = "El campo tipo debe ser Q o T") String tipo,
			@Min(value = 1, groups = QuejaReclamoTrabajoCreation.class, message = "Código de paquete debe ser mayor a cero") Long ccoPaqueteCodigo,
			@Min(value = 1, groups = QuejaReclamoTrabajoCreation.class, message = "Código de paquete debe ser mayor a cero") Integer ccoPaqueteAgeLicencCodigo,
			String nombresCliente, String apellidosCliente, String telefonoCelularCliente,
			String correoElectronicoCliente,
			@NotNull(groups = QuejaReclamoTrabajoCreation.class, message = "El código de cliente no puede ser nulo") @Min(value = 1, groups = QuejaReclamoTrabajoCreation.class, message = "El código de cliente debe ser mayor a cero") Long cliClientCodigo,
			@NotNull(groups = QuejaReclamoTrabajoCreation.class, message = "El código de licenciatario de cliente no puede ser nulo") @Min(value = 1, groups = QuejaReclamoTrabajoCreation.class, message = "El código licenciatario de cliente debe ser mayor a cero") Integer cliClientAgeLicencCodigo) {
		super();
		this.id = id;
		this.descripcion = descripcion;
		this.rutaImagen = rutaImagen;
		this.tipo = tipo;
		this.ccoPaqueteCodigo = ccoPaqueteCodigo;
		this.ccoPaqueteAgeLicencCodigo = ccoPaqueteAgeLicencCodigo;
		this.nombresCliente = nombresCliente;
		this.apellidosCliente = apellidosCliente;
		this.telefonoCelularCliente = telefonoCelularCliente;
		this.correoElectronicoCliente = correoElectronicoCliente;
		this.cliClientCodigo = cliClientCodigo;
		this.cliClientAgeLicencCodigo = cliClientAgeLicencCodigo;
	}

	public CcoQuejasReclamosTrabajoPK getId() {
		return id;
	}

	public void setId(CcoQuejasReclamosTrabajoPK id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getRutaImagen() {
		return rutaImagen;
	}

	public void setRutaImagen(String rutaImagen) {
		this.rutaImagen = rutaImagen;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public Long getCcoPaqueteCodigo() {
		return ccoPaqueteCodigo;
	}

	public void setCcoPaqueteCodigo(Long ccoPaqueteCodigo) {
		this.ccoPaqueteCodigo = ccoPaqueteCodigo;
	}

	public Integer getCcoPaqueteAgeLicencCodigo() {
		return ccoPaqueteAgeLicencCodigo;
	}

	public void setCcoPaqueteAgeLicencCodigo(Integer ccoPaqueteAgeLicencCodigo) {
		this.ccoPaqueteAgeLicencCodigo = ccoPaqueteAgeLicencCodigo;
	}

	public String getNombresCliente() {
		return nombresCliente;
	}

	public void setNombresCliente(String nombresCliente) {
		this.nombresCliente = nombresCliente;
	}

	public String getApellidosCliente() {
		return apellidosCliente;
	}

	public void setApellidosCliente(String apellidosCliente) {
		this.apellidosCliente = apellidosCliente;
	}

	public String getTelefonoCelularCliente() {
		return telefonoCelularCliente;
	}

	public void setTelefonoCelularCliente(String telefonoCelularCliente) {
		this.telefonoCelularCliente = telefonoCelularCliente;
	}

	public String getCorreoElectronicoCliente() {
		return correoElectronicoCliente;
	}

	public void setCorreoElectronicoCliente(String correoElectronicoCliente) {
		this.correoElectronicoCliente = correoElectronicoCliente;
	}

	public Long getCliClientCodigo() {
		return cliClientCodigo;
	}

	public void setCliClientCodigo(Long cliClientCodigo) {
		this.cliClientCodigo = cliClientCodigo;
	}

	public Integer getCliClientAgeLicencCodigo() {
		return cliClientAgeLicencCodigo;
	}

	public void setCliClientAgeLicencCodigo(Integer cliClientAgeLicencCodigo) {
		this.cliClientAgeLicencCodigo = cliClientAgeLicencCodigo;
	}

	
	
	
}
