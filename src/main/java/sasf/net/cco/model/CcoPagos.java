package sasf.net.cco.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;


import sasf.net.cco.utils.EntidadCamposGenerales;

@Cacheable(false)
@Entity
@Table(name="cco_pagos")
@NamedQuery(name="CcoPagos.findAll", query="SELECT c FROM CcoPagos c")
public class CcoPagos extends EntidadCamposGenerales implements Serializable {
	
	private static final long serialVersionUID = 1L;

	public interface PagosCreation {}

	public interface PagosUpdate {}
	
	@Valid
	@NotNull(groups = {PagosCreation.class, PagosUpdate.class}, message = "El id de centro de acopio no puede ser nulo")
	@EmbeddedId
	private CcoPagosPK id;
	
	@NotNull(groups = {PagosCreation.class}, message = "El valor no puede ser nulo")
	@Column(name="valor",updatable=true,nullable = false,precision =18 ,scale = 6)
	private BigDecimal valor;
	
	@NotNull(groups = {PagosCreation.class}, message = "La descripción no puede ser nula")
	@NotBlank(groups = {PagosCreation.class}, message = "La descripción no puede estar vacío")
	@Column(name="descripcion",length=1000,nullable = false,updatable = true)
	private String descripcion;
	
	@Column(name="cco_conduc_codigo",length = 10,nullable = true,updatable = true)
	private Long ccoConducCodigo;
	
	@Column(name="cco_conduc_age_licenc_codigo",length = 5,nullable = true,updatable = true)
	private Integer ccoConducAgeLicencCodigo ;

	@Column(name="cco_repart_codigo",length = 10,nullable = true,updatable = true)
	private Long CcoRepartCodigo;
	
	@Column(name="cco_repart_age_licenc_codigo",length = 5,nullable = true,updatable = true)
	private Integer CcoRepartAgeLicencCodigo ;
	
	@Column(name="cco_cen_ac_codigo",length = 10,nullable = true,updatable = true)
	private Long ccoCenAcCodigo;
	
	@Column(name="cco_cen_ac_age_licenc_codigo",length = 5,nullable = true,updatable = true)
	private Integer ccoCenAcAgeLicencCodigo;

	public CcoPagos() {
		super();
	}

	public CcoPagos(
			@Valid @NotNull(groups = { PagosCreation.class,
					PagosUpdate.class }, message = "El id de centro de acopio no puede ser nulo") CcoPagosPK id,
			@NotNull(groups = PagosCreation.class, message = "El valor no puede ser nulo") BigDecimal valor,
			@NotNull(groups = PagosCreation.class, message = "La descripción no puede ser nula") @NotBlank(groups = PagosCreation.class, message = "La descripción no puede estar vacío") String descripcion,
			Long ccoConducCodigo, Integer ccoConducAgeLicencCodigo, Long ccoRepartCodigo,
			Integer ccoRepartAgeLicencCodigo, Long ccoCenAcCodigo, Integer ccoCenAcAgeLicencCodigo) {
		super();
		this.id = id;
		this.valor = valor;
		this.descripcion = descripcion;
		this.ccoConducCodigo = ccoConducCodigo;
		this.ccoConducAgeLicencCodigo = ccoConducAgeLicencCodigo;
		CcoRepartCodigo = ccoRepartCodigo;
		CcoRepartAgeLicencCodigo = ccoRepartAgeLicencCodigo;
		this.ccoCenAcCodigo = ccoCenAcCodigo;
		this.ccoCenAcAgeLicencCodigo = ccoCenAcAgeLicencCodigo;
	}

	public CcoPagosPK getId() {
		return id;
	}

	public void setId(CcoPagosPK id) {
		this.id = id;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Long getCcoConducCodigo() {
		return ccoConducCodigo;
	}

	public void setCcoConducCodigo(Long ccoConducCodigo) {
		this.ccoConducCodigo = ccoConducCodigo;
	}

	public Integer getCcoConducAgeLicencCodigo() {
		return ccoConducAgeLicencCodigo;
	}

	public void setCcoConducAgeLicencCodigo(Integer ccoConducAgeLicencCodigo) {
		this.ccoConducAgeLicencCodigo = ccoConducAgeLicencCodigo;
	}

	public Long getCcoRepartCodigo() {
		return CcoRepartCodigo;
	}

	public void setCcoRepartCodigo(Long ccoRepartCodigo) {
		CcoRepartCodigo = ccoRepartCodigo;
	}

	public Integer getCcoRepartAgeLicencCodigo() {
		return CcoRepartAgeLicencCodigo;
	}

	public void setCcoRepartAgeLicencCodigo(Integer ccoRepartAgeLicencCodigo) {
		CcoRepartAgeLicencCodigo = ccoRepartAgeLicencCodigo;
	}

	public Long getCcoCenAcCodigo() {
		return ccoCenAcCodigo;
	}

	public void setCcoCenAcCodigo(Long ccoCenAcCodigo) {
		this.ccoCenAcCodigo = ccoCenAcCodigo;
	}

	public Integer getCcoCenAcAgeLicencCodigo() {
		return ccoCenAcAgeLicencCodigo;
	}

	public void setCcoCenAcAgeLicencCodigo(Integer ccoCenAcAgeLicencCodigo) {
		this.ccoCenAcAgeLicencCodigo = ccoCenAcAgeLicencCodigo;
	}
	
	
	
	
}
