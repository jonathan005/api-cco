package sasf.net.cco.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

import sasf.net.cco.model.CcoPostulanteDocumentos.PostulanteDocumentoCreation;
import sasf.net.cco.model.CcoPostulanteDocumentos.PostulanteDocumentoUpdate;



@Embeddable
public class CcoPostulanteDocumentosPK implements Serializable {
	
private static final long serialVersionUID = 1L;
	
	@NotNull(groups = {PostulanteDocumentoCreation.class,PostulanteDocumentoUpdate.class}, message = "Código de postulante no puede ser nulo")
	@Column(name="cco_postul_codigo",updatable=false,nullable=false)
	private Long ccoPostulCodigo;
	
	@NotNull(groups = {PostulanteDocumentoCreation.class,PostulanteDocumentoUpdate.class}, message = "Código de licenciatario de postulante no puede ser nulo")
	@Column(name="cco_postul_age_licenc_codigo",updatable=false,nullable=false)
	private Integer ccoPostulAgeLicencCodigo;
	
	@NotNull(groups = {PostulanteDocumentoCreation.class,PostulanteDocumentoUpdate.class}, message = "Código de requisito trabajo no puede ser nulo")
	@Column(name="cco_re_tra_codigo",updatable=false,nullable=false)
	private Long ccoReTraCodigo;
	
	@NotNull(groups = {PostulanteDocumentoCreation.class,PostulanteDocumentoUpdate.class}, message = "Código de licenciatario de requisito trabajo no puede ser nulo")
	@Column(name="cco_re_tra_age_licenc_codigo",updatable=false,nullable=false)
	private Integer ccoReTraAgeLicencCodigo;

	public CcoPostulanteDocumentosPK() {
		super();
	}

	public CcoPostulanteDocumentosPK(@NotNull(groups = { PostulanteDocumentoCreation.class,
			PostulanteDocumentoUpdate.class }, message = "Código de postulante no puede ser nulo") Long ccoPostulCodigo,
			@NotNull(groups = { PostulanteDocumentoCreation.class,
					PostulanteDocumentoUpdate.class }, message = "Código de licenciatario de postulante no puede ser nulo") Integer ccoPostulAgeLicencCodigo,
			@NotNull(groups = { PostulanteDocumentoCreation.class,
					PostulanteDocumentoUpdate.class }, message = "Código de requisito trabajo no puede ser nulo") Long ccoReTraCodigo,
			@NotNull(groups = { PostulanteDocumentoCreation.class,
					PostulanteDocumentoUpdate.class }, message = "Código de licenciatario de requisito trabajo no puede ser nulo") Integer ccoReTraAgeLicencCodigo) {
		super();
		this.ccoPostulCodigo = ccoPostulCodigo;
		this.ccoPostulAgeLicencCodigo = ccoPostulAgeLicencCodigo;
		this.ccoReTraCodigo = ccoReTraCodigo;
		this.ccoReTraAgeLicencCodigo = ccoReTraAgeLicencCodigo;
	}

	public Long getCcoPostulCodigo() {
		return ccoPostulCodigo;
	}

	public void setCcoPostulCodigo(Long ccoPostulCodigo) {
		this.ccoPostulCodigo = ccoPostulCodigo;
	}

	public Integer getCcoPostulAgeLicencCodigo() {
		return ccoPostulAgeLicencCodigo;
	}

	public void setCcoPostulAgeLicencCodigo(Integer ccoPostulAgeLicencCodigo) {
		this.ccoPostulAgeLicencCodigo = ccoPostulAgeLicencCodigo;
	}

	public Long getCcoReTraCodigo() {
		return ccoReTraCodigo;
	}

	public void setCcoReTraCodigo(Long ccoReTraCodigo) {
		this.ccoReTraCodigo = ccoReTraCodigo;
	}

	public Integer getCcoReTraAgeLicencCodigo() {
		return ccoReTraAgeLicencCodigo;
	}

	public void setCcoReTraAgeLicencCodigo(Integer ccoReTraAgeLicencCodigo) {
		this.ccoReTraAgeLicencCodigo = ccoReTraAgeLicencCodigo;
	}
	
	
	
}
