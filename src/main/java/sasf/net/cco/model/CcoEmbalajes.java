package sasf.net.cco.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import sasf.net.cco.utils.EntidadCamposGenerales;




@Cacheable(false)
@Entity
@Table(name="cco_embalajes")
@NamedQuery(name="CcoEmbalajes.findAll", query="SELECT c FROM CcoEmbalajes c")
public class CcoEmbalajes extends EntidadCamposGenerales implements Serializable{

	private static final long serialVersionUID = 1L;

	public interface EmbalajeCreation {}

	public interface EmbalajeUpdate {}
	@Valid
	@NotNull(groups = {EmbalajeCreation.class, EmbalajeUpdate.class}, message = "El id de embalaje no puede ser nulo")
	@EmbeddedId
	private CcoEmbalajesPK id;
	
	@NotNull(groups = {EmbalajeCreation.class, EmbalajeUpdate.class}, message = "La descripción no puede ser nula")
	@NotBlank(groups = {EmbalajeCreation.class, EmbalajeUpdate.class}, message = "La descripción no puede estar vacío")
	@Column(name="descripcion",updatable=true,nullable = false,length=200)
	private String descripcion;
	
	@Column(name="costoAdicional",updatable=true,nullable = true,precision =18 ,scale = 4)
	private BigDecimal costoAdicional;
	
	@PrePersist
	void preInsert() {
		this.fechaIngreso = new Date();
		this.fechaEstado = new Date();
	}
	
	@PreUpdate
	void preUpdate() {
		this.fechaModificacion = new Date();
		if(this.estado != null){
			this.fechaEstado = new Date();
		}
	}

	public CcoEmbalajes() {
		super();
	}

	public CcoEmbalajes(
			@Valid @NotNull(groups = { EmbalajeCreation.class,
					EmbalajeUpdate.class }, message = "El id de embalaje no puede ser nulo") CcoEmbalajesPK id,
			@NotNull(groups = { EmbalajeCreation.class,
					EmbalajeUpdate.class }, message = "La descripción no puede ser nula") @NotBlank(groups = {
							EmbalajeCreation.class,
							EmbalajeUpdate.class }, message = "La descripción no puede estar vacío") String descripcion,
			BigDecimal costoAdicional) {
		super();
		this.id = id;
		this.descripcion = descripcion;
		this.costoAdicional = costoAdicional;
	}

	public CcoEmbalajesPK getId() {
		return id;
	}

	public void setId(CcoEmbalajesPK id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public BigDecimal getCostoAdicional() {
		return costoAdicional;
	}

	public void setCostoAdicional(BigDecimal costoAdicional) {
		this.costoAdicional = costoAdicional;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((costoAdicional == null) ? 0 : costoAdicional.hashCode());
		result = prime * result + ((descripcion == null) ? 0 : descripcion.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CcoEmbalajes other = (CcoEmbalajes) obj;
		if (costoAdicional == null) {
			if (other.costoAdicional != null)
				return false;
		} else if (!costoAdicional.equals(other.costoAdicional))
			return false;
		if (descripcion == null) {
			if (other.descripcion != null)
				return false;
		} else if (!descripcion.equals(other.descripcion))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	

	
}
