package sasf.net.cco.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import javax.validation.constraints.NotNull;

import sasf.net.cco.model.CcoPuestoTrabajoRequisito.PuestoTrabajoRequisitoCreation;
import sasf.net.cco.model.CcoPuestoTrabajoRequisito.PuestoTrabajoRequisitoUpdate;



@Embeddable
public class CcoPuestoTrabajoRequisitoPK implements Serializable {
	
private static final long serialVersionUID = 1L;
	
	@NotNull(groups = {PuestoTrabajoRequisitoCreation.class,PuestoTrabajoRequisitoUpdate.class}, message = "Código de puesto de trabajo no puede ser nulo")
	@Column(name="cco_pu_tra_codigo",updatable=false,nullable=false)
	private Long ccoPuTraCodigo;
	
	@NotNull(groups = {PuestoTrabajoRequisitoCreation.class,PuestoTrabajoRequisitoUpdate.class}, message = "Código de licenciatario de puesto de trabajo no puede ser nulo")
	@Column(name="cco_pu_tra_age_licenc_codigo",updatable=false,nullable=false)
	private Integer ccoPuTraAgeLicencCodigo;
	
	@NotNull(groups = {PuestoTrabajoRequisitoCreation.class,PuestoTrabajoRequisitoUpdate.class}, message = "Código de requisito de trabajo no puede ser nulo")
	@Column(name="cco_re_tra_codigo",updatable=false,nullable=false)
	private Long ccoReTraCodigo;
	
	@NotNull(groups = {PuestoTrabajoRequisitoCreation.class,PuestoTrabajoRequisitoUpdate.class}, message = "Código de licenciatario de requisito de trabajo no puede ser nulo")
	@Column(name="cco_re_tra_age_licenc_codigo",updatable=false,nullable=false)
	private Integer ccoReTraAgeLicencCodigo;

	
	public CcoPuestoTrabajoRequisitoPK() {
		super();
	}

	public CcoPuestoTrabajoRequisitoPK(@NotNull(groups = { PuestoTrabajoRequisitoCreation.class,
			PuestoTrabajoRequisitoUpdate.class }, message = "Código de puesto de trabajo no puede ser nulo") Long ccoPuTraCodigo,
			@NotNull(groups = { PuestoTrabajoRequisitoCreation.class,
					PuestoTrabajoRequisitoUpdate.class }, message = "Código de licenciatario de puesto de trabajo no puede ser nulo") Integer ccoPuTraAgeLicencCodigo,
			@NotNull(groups = { PuestoTrabajoRequisitoCreation.class,
					PuestoTrabajoRequisitoUpdate.class }, message = "Código de requisito de trabajo no puede ser nulo") Long ccoReTraCodigo,
			@NotNull(groups = { PuestoTrabajoRequisitoCreation.class,
					PuestoTrabajoRequisitoUpdate.class }, message = "Código de licenciatario de requisito de trabajo no puede ser nulo") Integer ccoReTraAgeLicencCodigo) {
		super();
		this.ccoPuTraCodigo = ccoPuTraCodigo;
		this.ccoPuTraAgeLicencCodigo = ccoPuTraAgeLicencCodigo;
		this.ccoReTraCodigo = ccoReTraCodigo;
		this.ccoReTraAgeLicencCodigo = ccoReTraAgeLicencCodigo;
	}

	public Long getCcoPuTraCodigo() {
		return ccoPuTraCodigo;
	}

	public void setCcoPuTraCodigo(Long ccoPuTraCodigo) {
		this.ccoPuTraCodigo = ccoPuTraCodigo;
	}

	public Integer getCcoPuTraAgeLicencCodigo() {
		return ccoPuTraAgeLicencCodigo;
	}

	public void setCcoPuTraAgeLicencCodigo(Integer ccoPuTraAgeLicencCodigo) {
		this.ccoPuTraAgeLicencCodigo = ccoPuTraAgeLicencCodigo;
	}

	public Long getCcoReTraCodigo() {
		return ccoReTraCodigo;
	}

	public void setCcoReTraCodigo(Long ccoReTraCodigo) {
		this.ccoReTraCodigo = ccoReTraCodigo;
	}

	public Integer getCcoReTraAgeLicencCodigo() {
		return ccoReTraAgeLicencCodigo;
	}

	public void setCcoReTraAgeLicencCodigo(Integer ccoReTraAgeLicencCodigo) {
		this.ccoReTraAgeLicencCodigo = ccoReTraAgeLicencCodigo;
	}
	
	
	
}
