package sasf.net.cco.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

import sasf.net.cco.model.CcoConductorPaquetesCab.ConductorPaquetesCabCreation;
import sasf.net.cco.model.CcoConductorPaquetesCab.ConductorPaquetesCabUpdate;



@Embeddable
public class CcoConductorPaquetesCabPK implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@NotNull(groups = ConductorPaquetesCabUpdate.class, message = "El código de conductor paquetes no puede ser nulo")
	@Column(name="codigo",length = 5,nullable = false,updatable = false)
	private Long codigo;

	@NotNull(groups = {ConductorPaquetesCabCreation.class, ConductorPaquetesCabUpdate.class}, message = "El código del licenciatario no puede ser nulo")	
	@Column(name="age_licenc_codigo",length = 5,nullable = false,updatable = false)
	private Integer ageLicencCodigo;

	public CcoConductorPaquetesCabPK() {
		super();
	}

	public CcoConductorPaquetesCabPK(
			@NotNull(groups = ConductorPaquetesCabUpdate.class, message = "El código de conductor paquetes no puede ser nulo") Long codigo,
			@NotNull(groups = { ConductorPaquetesCabCreation.class,
					ConductorPaquetesCabUpdate.class }, message = "El código del licenciatario no puede ser nulo") Integer ageLicencCodigo) {
		super();
		this.codigo = codigo;
		this.ageLicencCodigo = ageLicencCodigo;
	}

	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public Integer getAgeLicencCodigo() {
		return ageLicencCodigo;
	}

	public void setAgeLicencCodigo(Integer ageLicencCodigo) {
		this.ageLicencCodigo = ageLicencCodigo;
	}
	
	
	
}
