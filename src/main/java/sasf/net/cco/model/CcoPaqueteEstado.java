package sasf.net.cco.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import sasf.net.cco.utils.EntidadCamposGenerales;
import sasf.net.cco.utils.Globales;
@Cacheable(false)
@Entity
@Table(name="cco_paquete_estado")
@NamedQuery(name="CcoPaqueteEstado.findAll", query="SELECT c FROM CcoPaqueteEstado c")
public class CcoPaqueteEstado extends EntidadCamposGenerales implements Serializable {
	
	private static final long serialVersionUID = 1L;

	public interface PaqueteEstadoCreation {}

	public interface PaqueteEstadoUpdate {}
	
	@Valid
	@NotNull(groups = {PaqueteEstadoCreation.class, PaqueteEstadoUpdate.class}, message = "El id de paquete estado no puede ser nulo")
	@EmbeddedId
	private CcoPaqueteEstadoPK id;
	
	@NotNull(groups = {PaqueteEstadoCreation.class}, message = "El estado de paquete no puede ser nula")
	@Pattern(groups = { PaqueteEstadoCreation.class,
			PaqueteEstadoUpdate.class }, regexp = "("+Globales.PAQUETE_ESTADO_CENTRO_ACOPIO+")|("+Globales.PAQUETE_ESTADO_EN_CAMINO+")|("+Globales.PAQUETE_ESTADO_ENTREGADO+")|("+Globales.PAQUETE_ESTADO_INGRESADO+")|("+Globales.PAQUETE_ESTADO_OFICINA+")|("+Globales.PAQUETE_ESTADO_PENDIENTE_PAGO+")|("+Globales.PAQUETE_ESTADO_SOLICITUD_RECOLECCION+")", message = "El estado de paquete debe ser "+Globales.PAQUETE_ESTADO_CENTRO_ACOPIO+", "+Globales.PAQUETE_ESTADO_EN_CAMINO+", "+Globales.PAQUETE_ESTADO_ENTREGADO+", "+Globales.PAQUETE_ESTADO_INGRESADO+", "+Globales.PAQUETE_ESTADO_OFICINA+", "+Globales.PAQUETE_ESTADO_PENDIENTE_PAGO+" o "+Globales.PAQUETE_ESTADO_SOLICITUD_RECOLECCION)
	@Column(name="paquete_estado",updatable=true,nullable = false,length=2)
	private String paqueteEstado;
	
	@NotNull(groups = {PaqueteEstadoCreation.class}, message = "La fecha de paquete estado no puede ser nula")
	@Column(name = "fecha_paquete_estado",nullable=false,updatable = true)
	private Date fechaPaqueteEstado;
	
	@PrePersist
	void preInsert() {
		this.fechaIngreso = new Date();
		this.fechaEstado = new Date();
	}
	
	@PreUpdate
	void preUpdate() {
		this.fechaModificacion = new Date();
		if(this.estado != null){
			this.fechaEstado = new Date();
		}
	}

	public CcoPaqueteEstado() {
		super();
	}




	public CcoPaqueteEstado(@Valid @NotNull(groups = { PaqueteEstadoCreation.class,
			PaqueteEstadoUpdate.class }, message = "El id de paquete estado no puede ser nulo") CcoPaqueteEstadoPK id,
			@NotNull(groups = PaqueteEstadoCreation.class, message = "El estado de paquete no puede ser nula") @Pattern(groups = {
					PaqueteEstadoCreation.class,
					PaqueteEstadoUpdate.class }, regexp = "[(CA)|(EC)|(EN)|(IN)|(O)|(PP)|(SR)]", message = "El estado de paquete debe ser CA, EC, EN, IN, O, PP o SR") String paqueteEstado,
			@NotNull(groups = PaqueteEstadoCreation.class, message = "La fecha de paquete estado no puede ser nula") Date fechaPaqueteEstado) {
		super();
		this.id = id;
		this.paqueteEstado = paqueteEstado;
		this.fechaPaqueteEstado = fechaPaqueteEstado;
	}

	public CcoPaqueteEstadoPK getId() {
		return id;
	}

	public void setId(CcoPaqueteEstadoPK id) {
		this.id = id;
	}

	public String getPaqueteEstado() {
		return paqueteEstado;
	}

	public void setPaqueteEstado(String paqueteEstado) {
		this.paqueteEstado = paqueteEstado;
	}

	public Date getFechaPaqueteEstado() {
		return fechaPaqueteEstado;
	}

	public void setFechaPaqueteEstado(Date fechaPaqueteEstado) {
		this.fechaPaqueteEstado = fechaPaqueteEstado;
	}

		
}
