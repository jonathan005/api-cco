package sasf.net.cco.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import sasf.net.cco.utils.EntidadCamposGenerales;
import sasf.net.cco.utils.Globales;

@Cacheable(false)
@Entity
@Table(name="cco_requisitos_trabajo")
@NamedQuery(name="CcoRequisitosTrabajo.findAll", query="SELECT c FROM CcoRequisitosTrabajo c")
public class CcoRequisitosTrabajo extends EntidadCamposGenerales implements Serializable {
	
	private static final long serialVersionUID = 1L;

	public interface RequisitosTrabajoCreation {}

	public interface RequisitosTrabajoUpdate {}
	
	@Valid
	@NotNull(groups = {RequisitosTrabajoCreation.class, RequisitosTrabajoUpdate.class}, message = "El id de requisito trabajo no puede ser nulo")
	@EmbeddedId
	private CcoRequisitosTrabajoPK id;
	
	@NotNull(groups = {RequisitosTrabajoCreation.class}, message = "La descripción no puede ser nula")
	@NotBlank(groups = {RequisitosTrabajoCreation.class}, message = "La descripción no puede estar vacío")
	@Column(name="descripcion",updatable=true,nullable = false,length=200)
	private String descripcion;
	
	@NotNull(groups = {RequisitosTrabajoCreation.class}, message = "El título no puede ser nula")
	@NotBlank(groups = {RequisitosTrabajoCreation.class}, message = "El título no puede estar vacío")
	@Column(name="titulo",updatable=true,nullable = false,length=200)
	private String titulo;
	
	@NotNull(groups = RequisitosTrabajoCreation.class, message = "El tipo de archivo no puede ser nulo")
	@Pattern(groups = { RequisitosTrabajoCreation.class,
			RequisitosTrabajoUpdate.class}, regexp = "["+Globales.TIPO_ARCHIVO_IMAGEN+Globales.TIPO_ARCHIVO_PDF+Globales.TIPO_ARCHIVO_WORD+Globales.TIPO_ARCHIVO_NUMBER+Globales.TIPO_ARCHIVO_TEXTO+"]", message = "El tipo de archivo debe ser "+Globales.TIPO_ARCHIVO_IMAGEN+", "+Globales.TIPO_ARCHIVO_PDF+", "+Globales.TIPO_ARCHIVO_WORD+", "+Globales.TIPO_ARCHIVO_NUMBER+" o "+Globales.TIPO_ARCHIVO_TEXTO)
	@Column(name="tipo_archivo",updatable=true,nullable = false,length=1)
	private String tipoArchivo;
	
	@PrePersist
	void preInsert() {
		this.fechaIngreso = new Date();
		this.fechaEstado = new Date();
	}
	
	@PreUpdate
	void preUpdate() {
		this.fechaModificacion = new Date();
		if(this.estado != null){
			this.fechaEstado = new Date();
		}
	}
	
	public CcoRequisitosTrabajo() {
		super();
	}
	


	

	public CcoRequisitosTrabajo(@Valid @NotNull(groups = { RequisitosTrabajoCreation.class,
			RequisitosTrabajoUpdate.class }, message = "El id de requisito trabajo no puede ser nulo") CcoRequisitosTrabajoPK id,
			@NotNull(groups = RequisitosTrabajoCreation.class, message = "La descripción no puede ser nula") @NotBlank(groups = RequisitosTrabajoCreation.class, message = "La descripción no puede estar vacío") String descripcion,
			@NotNull(groups = RequisitosTrabajoCreation.class, message = "El título no puede ser nula") @NotBlank(groups = RequisitosTrabajoCreation.class, message = "El título no puede estar vacío") String titulo,
			@NotNull(groups = RequisitosTrabajoCreation.class, message = "El tipo de archivo no puede ser nulo") @Pattern(groups = {
					RequisitosTrabajoCreation.class,
					RequisitosTrabajoUpdate.class }, regexp = "[IPWNT]", message = "El tipo de archivo debe ser I, P, W, N o T") String tipoArchivo) {
		super();
		this.id = id;
		this.descripcion = descripcion;
		this.titulo = titulo;
		this.tipoArchivo = tipoArchivo;
	}

	public CcoRequisitosTrabajoPK getId() {
		return id;
	}

	public void setId(CcoRequisitosTrabajoPK id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getTipoArchivo() {
		return tipoArchivo;
	}

	public void setTipoArchivo(String tipoArchivo) {
		this.tipoArchivo = tipoArchivo;
	}
	
		
	
}
