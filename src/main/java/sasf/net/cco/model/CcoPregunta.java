package sasf.net.cco.model;

import java.io.Serializable;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;


import sasf.net.cco.utils.EntidadCamposGenerales;

@Cacheable(false)
@Entity
@Table(name="cco_pregunta")
@NamedQuery(name="CcoPregunta.findAll", query="SELECT c FROM CcoPregunta c")
public class CcoPregunta extends EntidadCamposGenerales implements Serializable{
	
	private static final long serialVersionUID = 1L;

	public interface PreguntaCreation {}

	public interface PreguntaUpdate {}
	
	@Valid
	@NotNull(groups = {PreguntaCreation.class, PreguntaUpdate.class}, message = "El id de pregunta no puede ser nulo")
	@EmbeddedId
	private CcoPreguntaPK id;
	
	@NotNull(groups = {PreguntaCreation.class}, message = "La pregunta no puede ser nula")
	@NotBlank(groups = {PreguntaCreation.class}, message = "La pregunta no puede estar vacío")
	@Column(name="pregunta",length=2000)
	private String pregunta;
	

	@Column(name="respuesta",length=2000)
	private String respuesta;


	public CcoPregunta() {
		super();
	}

	public CcoPregunta(
			@Valid @NotNull(groups = { PreguntaCreation.class,
					PreguntaUpdate.class }, message = "El id de pregunta no puede ser nulo") CcoPreguntaPK id,
			@NotNull(groups = PreguntaCreation.class, message = "La pregunta no puede ser nula") @NotBlank(groups = PreguntaCreation.class, message = "La pregunta no puede estar vacío") String pregunta,
			String respuesta) {
		super();
		this.id = id;
		this.pregunta = pregunta;
		this.respuesta = respuesta;
	}





	public CcoPreguntaPK getId() {
		return id;
	}


	public void setId(CcoPreguntaPK id) {
		this.id = id;
	}


	public String getPregunta() {
		return pregunta;
	}


	public void setPregunta(String pregunta) {
		this.pregunta = pregunta;
	}


	public String getRespuesta() {
		return respuesta;
	}


	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}
	
	
	
}
