package sasf.net.cco.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Cacheable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import sasf.net.cco.utils.EntidadCamposGenerales;

@Cacheable(false)
@Entity
@Table(name="cco_puesto_trabajo_requisitos")
@NamedQuery(name="CcoPuestoTrabajoRequisito.findAll", query="SELECT c FROM CcoPuestoTrabajoRequisito c")
public class CcoPuestoTrabajoRequisito extends EntidadCamposGenerales implements Serializable {
	
	private static final long serialVersionUID = 1L;

	public interface PuestoTrabajoRequisitoCreation {}

	public interface PuestoTrabajoRequisitoUpdate {}
	
	@Valid
	@NotNull(groups = {PuestoTrabajoRequisitoCreation.class, PuestoTrabajoRequisitoUpdate.class}, message = "El id de postulante requisito no puede ser nulo")
	@EmbeddedId
	private CcoPuestoTrabajoRequisitoPK id;
		
	
	@PrePersist
	void preInsert() {
		this.fechaIngreso = new Date();
		this.fechaEstado = new Date();
	}
	
	@PreUpdate
	void preUpdate() {
		this.fechaModificacion = new Date();
		if(this.estado != null){
			this.fechaEstado = new Date();
		}
	}

	public CcoPuestoTrabajoRequisito() {
		super();
	}

	public CcoPuestoTrabajoRequisito(@Valid @NotNull(groups = { PuestoTrabajoRequisitoCreation.class,
			PuestoTrabajoRequisitoUpdate.class }, message = "El id de postulante requisito no puede ser nulo") CcoPuestoTrabajoRequisitoPK id) {
		super();
		this.id = id;
	}

	public CcoPuestoTrabajoRequisitoPK getId() {
		return id;
	}

	public void setId(CcoPuestoTrabajoRequisitoPK id) {
		this.id = id;
	}
	
	
	
	
}
