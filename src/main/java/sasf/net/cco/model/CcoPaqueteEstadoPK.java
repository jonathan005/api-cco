package sasf.net.cco.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

import sasf.net.cco.model.CcoPaqueteEstado.PaqueteEstadoCreation;
import sasf.net.cco.model.CcoPaqueteEstado.PaqueteEstadoUpdate;


@Embeddable
public class CcoPaqueteEstadoPK implements Serializable{
	
private static final long serialVersionUID = 1L;
	
	@NotNull(groups = PaqueteEstadoUpdate.class, message = "El código de paquete no puede ser nulo")
	@Column(name="codigo",length = 5,nullable = false,updatable = false)
	private Long codigo;
	
	@NotNull(groups = {PaqueteEstadoCreation.class, PaqueteEstadoUpdate.class}, message = "El código de pedido no puede ser nulo")	
	@Column(name="coo_pedido_codigo",length = 10,nullable = false,updatable = false)
	private Long cooPedidoCodigo;
	
	@NotNull(groups = {PaqueteEstadoCreation.class, PaqueteEstadoUpdate.class}, message = "El código del licenciatario de pedido no puede ser nulo")	
	@Column(name="coo_pedido_age_licenc_codigo",length = 5,nullable = false,updatable = false)
	private Integer cooPedidoAgeLicencCodigo;

	
	public CcoPaqueteEstadoPK() {
		super();
	}


	public CcoPaqueteEstadoPK(
			@NotNull(groups = PaqueteEstadoUpdate.class, message = "El código de paquete no puede ser nulo") Long codigo,
			@NotNull(groups = { PaqueteEstadoCreation.class,
					PaqueteEstadoUpdate.class }, message = "El código de pedido no puede ser nulo") Long cooPedidoCodigo,
			@NotNull(groups = { PaqueteEstadoCreation.class,
					PaqueteEstadoUpdate.class }, message = "El código del licenciatario de pedido no puede ser nulo") Integer cooPedidoAgeLicencCodigo) {
		super();
		this.codigo = codigo;
		this.cooPedidoCodigo = cooPedidoCodigo;
		this.cooPedidoAgeLicencCodigo = cooPedidoAgeLicencCodigo;
	}


	public Long getCodigo() {
		return codigo;
	}


	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}


	public Long getCooPedidoCodigo() {
		return cooPedidoCodigo;
	}


	public void setCooPedidoCodigo(Long cooPedidoCodigo) {
		this.cooPedidoCodigo = cooPedidoCodigo;
	}


	public Integer getCooPedidoAgeLicencCodigo() {
		return cooPedidoAgeLicencCodigo;
	}


	public void setCooPedidoAgeLicencCodigo(Integer cooPedidoAgeLicencCodigo) {
		this.cooPedidoAgeLicencCodigo = cooPedidoAgeLicencCodigo;
	}


	


	
	
}
