package sasf.net.cco.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import sasf.net.cco.utils.EntidadCamposGenerales;

@Cacheable(false)
@Entity
@Table(name="cco_factores_distancias")
@NamedQuery(name="CcoFactoresDistancias.findAll", query="SELECT c FROM CcoFactoresDistancias c")
public class CcoFactoresDistancias  extends EntidadCamposGenerales implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	public interface FactoresDistanciasCreation {}

	public interface FactoresDistanciasUpdate {}
	
	@Valid
	@NotNull(groups = {FactoresDistanciasCreation.class, FactoresDistanciasUpdate.class}, message = "El id de factor distancia no puede ser nulo")
	@EmbeddedId
	private CcoFactoresDistanciasPK id;
	
	@NotNull(groups = {FactoresDistanciasCreation.class}, message = "El factor no puede ser nulo")
	@Column(name="factor",updatable=true,nullable = false,precision =18 ,scale = 4)
	private BigDecimal factor;
	
	@NotNull(groups = {FactoresDistanciasCreation.class}, message = "La distancia desde no puede ser nulo")
	@Column(name="distancia_desde",updatable=true,nullable = false,precision =18 ,scale = 4)
	private BigDecimal distanciaDesde;
	
	@NotNull(groups = {FactoresDistanciasCreation.class}, message = "La distancia hasta no puede ser nulo")
	@Column(name="distancia_hasta",updatable=true,nullable = false,precision =18 ,scale = 4)
	private BigDecimal distanciaHasta;
	
	@PrePersist
	void preInsert() {
		this.fechaIngreso = new Date();
		this.fechaEstado = new Date();
	}
	
	@PreUpdate
	void preUpdate() {
		this.fechaModificacion = new Date();
		if(this.estado != null){
			this.fechaEstado = new Date();
		}
	}
	
	

	public CcoFactoresDistancias() {
		super();
	}

	public CcoFactoresDistancias(@Valid @NotNull(groups = { FactoresDistanciasCreation.class,
			FactoresDistanciasUpdate.class }, message = "El id de factor distancia no puede ser nulo") CcoFactoresDistanciasPK id,
			@NotNull(groups = FactoresDistanciasCreation.class, message = "El factor no puede ser nulo") BigDecimal factor,
			@NotNull(groups = FactoresDistanciasCreation.class, message = "La distancia desde no puede ser nulo") BigDecimal distanciaDesde,
			@NotNull(groups = FactoresDistanciasCreation.class, message = "La distancia hasta no puede ser nulo") BigDecimal distanciaHasta) {
		super();
		this.id = id;
		this.factor = factor;
		this.distanciaDesde = distanciaDesde;
		this.distanciaHasta = distanciaHasta;
	}

	public CcoFactoresDistanciasPK getId() {
		return id;
	}

	public void setId(CcoFactoresDistanciasPK id) {
		this.id = id;
	}

	public BigDecimal getFactor() {
		return factor;
	}

	public void setFactor(BigDecimal factor) {
		this.factor = factor;
	}

	public BigDecimal getDistanciaDesde() {
		return distanciaDesde;
	}

	public void setDistanciaDesde(BigDecimal distanciaDesde) {
		this.distanciaDesde = distanciaDesde;
	}

	public BigDecimal getDistanciaHasta() {
		return distanciaHasta;
	}

	public void setDistanciaHasta(BigDecimal distanciaHasta) {
		this.distanciaHasta = distanciaHasta;
	}
	
	
	
	
}
