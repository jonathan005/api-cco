package sasf.net.cco.utils;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;


@MappedSuperclass
public class EntidadCamposGenerales {

	public interface CamposGeneralesCreation {
	}

	public interface CamposGeneralesUpdate {
	}

	@NotNull(groups = CamposGeneralesCreation.class, message = "El estado no puede ser nulo")
	@Pattern(groups = { CamposGeneralesCreation.class,
			CamposGeneralesUpdate.class }, regexp = "[AIN]", message = "El estado debe ser A o I")
	protected String estado;

	@NotNull(message = "Fecha estado no puede ser nulo")
	@Column(name = "fecha_estado")
	protected Date fechaEstado;

	@Column(name = "fecha_ingreso", updatable = false)
	protected Date fechaIngreso;

	@Column(name = "fecha_modificacion", insertable = false)
	protected Date fechaModificacion;

	@Column(name = "observacion_estado")
	protected String observacionEstado;

	@Column(name = "ubicacion_ingreso", updatable = false)
	protected String ubicacionIngreso;

	@Column(name = "ubicacion_modificacion", insertable = false)
	protected String ubicacionModificacion;

	@NotNull(groups = CamposGeneralesCreation.class, message = "El usuario no puede ser nulo")
	@Min(groups = CamposGeneralesCreation.class, value = 1, message = "El código del usuario debe ser mayor a 0")
	@Column(name = "usuario_ingreso", updatable = false)
	protected Long usuarioIngreso;

	@NotNull(groups = CamposGeneralesUpdate.class, message = "El usuario no puede ser nulo")
	@Min(groups = CamposGeneralesUpdate.class, value = 1, message = "El código del usuario debe ser mayor a 0")
	@Column(name = "usuario_modificacion", insertable = false)
	protected Long usuarioModificacion;

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Date getFechaEstado() {
		return fechaEstado;
	}

	public void setFechaEstado(Date fechaEstado) {
		this.fechaEstado = fechaEstado;
	}

	public Date getFechaIngreso() {
		return fechaIngreso;
	}

	public void setFechaIngreso(Date fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}

	public Date getFechaModificacion() {
		return fechaModificacion;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	public String getObservacionEstado() {
		return observacionEstado;
	}

	public void setObservacionEstado(String observacionEstado) {
		this.observacionEstado = observacionEstado;
	}

	public String getUbicacionIngreso() {
		return ubicacionIngreso;
	}

	public void setUbicacionIngreso(String ubicacionIngreso) {
		this.ubicacionIngreso = ubicacionIngreso;
	}

	public String getUbicacionModificacion() {
		return ubicacionModificacion;
	}

	public void setUbicacionModificacion(String ubicacionModificacion) {
		this.ubicacionModificacion = ubicacionModificacion;
	}

	public Long getUsuarioIngreso() {
		return usuarioIngreso;
	}

	public void setUsuarioIngreso(Long usuarioIngreso) {
		this.usuarioIngreso = usuarioIngreso;
	}

	public Long getUsuarioModificacion() {
		return usuarioModificacion;
	}

	public void setUsuarioModificacion(Long usuarioModificacion) {
		this.usuarioModificacion = usuarioModificacion;
	}

}
