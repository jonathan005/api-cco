package sasf.net.cco.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import java.util.List;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import org.hibernate.exception.DataException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import sasf.net.cco.model.CcoLicenciatariosAplicaSecu;

import sasf.net.cco.repository.CcoLicenciatariosAplicaSecuRepository;





@Service
public class FuncionesGenerales {
	
	
	@Autowired
	private CcoLicenciatariosAplicaSecuRepository ccoLicenciatariosAplicaSecuRepository;
	
	
	public void validar(Object objeto, Class<?> group) {
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		Validator validator = factory.getValidator();
		Set<ConstraintViolation<Object>> violations = validator.validate(objeto, group);
		if (!violations.isEmpty()) {
			System.out.println("HUBO UN PROBLEMA CON VALIDACION DE PERFIL");
			List<String> errores = new ArrayList<String>();
			for (ConstraintViolation<Object> violation : violations) {
				errores.add(violation.getMessage());
			}
			throw new DataException(errores.toString(), null);
		}
	}

	
	public Long obtenerSecuencia(Integer codigoLicenciatario, Integer codigoAplicacion,
			Integer codigoSecuencia) {
		if (codigoLicenciatario == null || codigoLicenciatario == 0) {
			return 0l;
		}
		if (codigoSecuencia == null || codigoSecuencia == 0) {
			return 0l;
		}
		CcoLicenciatariosAplicaSecu ccoLicenciatariosAplicaSecu = ccoLicenciatariosAplicaSecuRepository
				.consultarPorSecuencia(codigoLicenciatario, codigoAplicacion, codigoSecuencia);

		if (ccoLicenciatariosAplicaSecu == null || ccoLicenciatariosAplicaSecu.getEstado().equals(Globales.ESTADO_ANULADO)) {
			return 0l;
		}
		Long id = 0l;
		if(ccoLicenciatariosAplicaSecu.getValorInicial() > ccoLicenciatariosAplicaSecu.getValorActual()) {
			id = ccoLicenciatariosAplicaSecu.getValorInicial();
		} else {
			id = ccoLicenciatariosAplicaSecu.getValorActual() + ccoLicenciatariosAplicaSecu.getIncrementaEn();
		}
		ccoLicenciatariosAplicaSecu.setValorActual(id);
		ccoLicenciatariosAplicaSecuRepository.save(ccoLicenciatariosAplicaSecu);
		return id;
	}
	
	public File convertMultipartFiletoFile(MultipartFile file) throws IOException {
        File convFile = new File(file.getOriginalFilename());
        convFile.createNewFile();
        FileOutputStream fos = new FileOutputStream(convFile);
        fos.write(file.getBytes());
        fos.close();
        return convFile;
    }
}
