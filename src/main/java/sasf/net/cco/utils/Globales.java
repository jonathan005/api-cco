package sasf.net.cco.utils;

public class Globales {
	public final static String ESTADO_ACTIVO = "A";
	public final static String ESTADO_INACTIVO = "I";
	public final static String ESTADO_ANULADO = "N";
	
	public final static String SI = "S";
    public final static String NO = "N";
	
	public final static Integer CODIGO_APLICACION = 101;
	
	public final static Integer NIVEL_1 = 1;
	
	public final static String QUEJA_RECLAMO = "Q";
	public final static String TRABAJA_CON_NOSOTROS = "T";
	
	public final static String PAQUETE = "P";
	public final static String DOCUMENTO = "D";
	
	public final static String PAQUETE_ESTADO_OFICINA = "O";
	public final static String PAQUETE_ESTADO_ENTREGADO = "EN";
	public final static String PAQUETE_ESTADO_SOLICITUD_RECOLECCION = "SR";
	public final static String PAQUETE_ESTADO_INGRESADO = "IN";
	public final static String PAQUETE_ESTADO_PENDIENTE_PAGO = "PP";
	public final static String PAQUETE_ESTADO_EN_CAMINO = "EC";
	public final static String PAQUETE_ESTADO_CENTRO_ACOPIO = "CA";
	
	public final static String TIPO_ARCHIVO_IMAGEN = "I";
	public final static String TIPO_ARCHIVO_PDF = "P";
	public final static String TIPO_ARCHIVO_WORD = "W";
	public final static String TIPO_ARCHIVO_TEXTO = "T";
	public final static String TIPO_ARCHIVO_NUMBER = "N";
	
	public final static String ESTADO_POSTULANTE_INGRESADO = "I";
	public final static String ESTADO_POSTULANTE_APROBADO = "A";
	public final static String ESTADO_POSTULANTE_RECHAZADO = "R";
	
	public final static String ESTADO_CONDUC_REPART_DISPONIBLE = "D";
	public final static String ESTADO_CONDUC_REPART_NO_DISPONIBLE = "N";
	public final static String ESTADO_CONDUC_REPART_ASIGNADO = "A";
	
	
	//Secuencias  primarias
	
	//Secuencias por licenciatarios
	public final static Integer CODIGO_SECUENCIA_EMBALAJE = 1;
	public final static Integer CODIGO_SECUENCIA_QUEJA_RECLAMO_TRABAJO = 2;
	public final static Integer CODIGO_SECUENCIA_PAQUETE = 3;
	public final static Integer CODIGO_SECUENCIA_PAQUETE_ESTADO = 4;
	public final static Integer CODIGO_SECUENCIA_FACTOR_PESO = 5;
	public final static Integer CODIGO_SECUENCIA_FACTOR_DISTANCIA = 6;
	public final static Integer CODIGO_SECUENCIA_CONDUCTORES = 7 ;
	public final static Integer CODIGO_SECUENCIA_VALOR_SEGURO = 8 ;
	public final static Integer CODIGO_SECUENCIA_CONDUCTOR_PAQUETE_CABECERA = 9 ;
	public final static Integer CODIGO_SECUENCIA_CONDUCTOR_PAQUETE_DETALLE = 10 ;
	public final static Integer CODIGO_SECUENCIA_PREGUNTA = 11 ;
	public final static Integer CODIGO_SECUENCIA_PUESTO_TRABAJO = 12 ;
	public final static Integer CODIGO_SECUENCIA_REQUISITO_TRABAJO = 13 ;
	public final static Integer CODIGO_SECUENCIA_POSTULANTE = 15 ;
	public final static Integer CODIGO_SECUENCIA_CENTRO_ACOPIO = 14 ;
	public final static Integer CODIGO_SECUENCIA_PAGO = 16 ;
	public final static Integer CODIGO_SECUENCIA_REPARTIDOR = 17 ;
	public final static Integer CODIGO_SECUENCIA_INVENTARIO_DIARIO = 18 ;
	public final static Integer CODIGO_SECUENCIA_EMPRESA_RECOLECTORA = 19 ;
	
	
	
}
